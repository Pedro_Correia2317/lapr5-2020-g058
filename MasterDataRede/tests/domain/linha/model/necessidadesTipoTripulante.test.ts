
import { NecessidadesTipoTripulante } from '../../../../src/domain/linha/model/NecessidadesTipoTripulante';
import { TipoNecessidade } from '../../../../src/domain/linha/model/TipoNecessidade';
import { CodigoTipoTripulante } from '../../../../src/domain/tipoTripulante/model/CodigoTipoTripulante';

test('testarNecessidadesTipoTripulanteNulo', () => {
    expect(() => {new NecessidadesTipoTripulante(null, null)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoTripulante(undefined, undefined)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoTripulante([], null)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoTripulante([], undefined)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoTripulante(null, TipoNecessidade.PERMISSAO)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoTripulante(undefined, TipoNecessidade.PERMISSAO)}).toThrow(ReferenceError);
});

test('testarNecessidadesTipoTripulanteInvalidos', () => {
    const trip = new CodigoTipoTripulante('12345678901234567890');
    expect(() => {new NecessidadesTipoTripulante([], TipoNecessidade.PERMISSAO)}).toThrow(SyntaxError);
    expect(() => {new NecessidadesTipoTripulante([], TipoNecessidade.RESTRICAO)}).toThrow(SyntaxError);
    expect(() => {new NecessidadesTipoTripulante([trip], TipoNecessidade.NAO_APLICAVEL)}).toThrow(SyntaxError);
    expect(() => {new NecessidadesTipoTripulante([null], TipoNecessidade.RESTRICAO)}).toThrow(ReferenceError);
});

test('testarNecessidadesTipoTripulanteValidos', () => {
    const trip = new CodigoTipoTripulante('12345678901234567890');
    expect(new NecessidadesTipoTripulante([trip], TipoNecessidade.PERMISSAO)).toBeInstanceOf(NecessidadesTipoTripulante);
    expect(new NecessidadesTipoTripulante([trip], TipoNecessidade.RESTRICAO)).toBeInstanceOf(NecessidadesTipoTripulante);
    expect(new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL)).toBeInstanceOf(NecessidadesTipoTripulante);
});