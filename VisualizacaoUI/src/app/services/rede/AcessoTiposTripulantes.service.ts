
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarTipoTripulanteDTO } from 'src/app/models/tiposTripulantes/PedidoCriarTipoTripulanteDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { TipoTripulanteDTO } from 'src/app/models/tiposTripulantes/TipoTripulanteDTO';

@Injectable({
  providedIn: 'root'
})
export class AcessoTiposTripulantesService {

  private enderecoHttp: string = `${Configurations.API_SERVER_ADDRESS}/tiposTripulantes`;

  constructor(private http: HttpClient) { }

  procurarTiposTripulantes(pagina: number, tamanho: number, sort?: string, desc?: string): Observable<DetalhesPesquisaDTO<TipoTripulanteDTO>> {
    const aux: string = sort? sort : '';
    const descAux: string = desc? desc : '';
    return this.http.get<DetalhesPesquisaDTO<TipoTripulanteDTO>>(this.enderecoHttp, {
      observe: 'body',
      responseType: 'json',
      params: {
        page: '' + pagina,
        size: '' + tamanho,
        sort: aux,
        descricao: descAux
      }
    });
  }

  enviarTipoTripulante(pedido: PedidoCriarTipoTripulanteDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }
}