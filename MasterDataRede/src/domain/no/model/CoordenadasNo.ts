
export class CoordenadasNo {

    private latitude: number;
    private longitude: number;

    constructor(lat: number, lon: number){
        if (!Number.isFinite(lat) || !Number.isFinite(lon)){
            throw new TypeError(`Invalid coordinates: ${lat}, ${lon}`);
        }
        this.latitude = lat;
        this.longitude = lon;
    }

    getLatitude(): number {
        return this.latitude;
    }

    getLongitude(): number {
        return this.longitude;
    }

    toString(): string {
        return `CoordenadasNo: [${this.latitude}, ${this.longitude}]`;
    }
}