import Mongoose from 'mongoose';
import {ISegmentoRede} from './ISegmentoRede';

export interface IPercurso extends Mongoose.Document {

    codigo: string;

    descricao: string;

    distanciaTotal: number;

    duracaoTotal: number;

    segmentos: Mongoose.Types.DocumentArray<ISegmentoRede>;

}