
:-consult("bc_autocarros2.pl").
:- consult("utility.pl").

aStar(Src,Dest,StartingTime,Path,Costs):-
    get_time(Ti),
    aStar1(Dest,[(_,StartingTime,[(Src,0)])|_],Path,EndCosts,[]),
    Costs is EndCosts - StartingTime,
    get_time(Tf),TSol is Tf-Ti,
    write('Tempo de geracao da solucao:'),write(TSol),nl.

aStar1(Dest,[(_,Costs,[(Dest,Line)|T])|Rest],Path,Costs,_):-
    write('Numero de Solucoes:'),length(Rest,NS),write(NS),nl,
    reverse([(Dest,Line)|T],Path).

aStar1(Dest,[(_,CurrentCosts,AccumulatedPath)|Others],Path,Costs,ClosedList):-
    
    AccumulatedPath = [(CurrentNode,_)|_],
    append([CurrentNode],ClosedList,ClosedList1),
    findall((AccumulatedEstimateCosts,AccumulatedCosts,[(NextNode,Line)|AccumulatedPath]),
        (Dest\==CurrentNode, liga(CurrentNode,NextNode,Line),\+ member((NextNode,_),AccumulatedPath),
        \+ member(NextNode,ClosedList1),
        getTimeCostsForWaitingAndTravelling(CurrentCosts, Line, CurrentNode,NextNode,WaitingTravelCosts),
        estimativeTime(NextNode,Dest,EstCosts),
        AccumulatedCosts is CurrentCosts + WaitingTravelCosts, 
        AccumulatedEstimateCosts is AccumulatedCosts + EstCosts),News),
    append(Others,News,All),
    sort(All,Allsorted),
    aStar1(Dest,Allsorted,Path,Costs,ClosedList1).

estimativeTime(No1,No2, Estimate):-
    getAvgSpeedOfLines(Fastest),
    no(_,No1,_,_,X1,Y1), % first x = long, y = lat
    no(_,No2,_,_,X2,Y2),
    harversineFormulaForDistance(Y1,X1,Y2,X2,Distance),
    Estimate is Distance / Fastest,!.

getAvgSpeedOfLines(Fastest):-
    findall(
        (LineNr,AvgSpeed),
        (linha(_,LineNr,_,TimeInMinutes,DistanceInMeter),
        DistanceInKM is DistanceInMeter / 1000,
        TimeInHours is TimeInMinutes / 60,
        AvgSpeed is DistanceInKM / TimeInHours ),LinesSpeed),
    sort(2,@>=,LinesSpeed,Sorted),
    nth0(0,Sorted,(_,Fastest)).

