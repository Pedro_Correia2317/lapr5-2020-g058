
import { TipoNecessidadeDTO } from "../dto/TipoNecessidadeDTO";

export class TipoNecessidade {

    static PERMISSAO = new TipoNecessidade('PERMISSAO');

    static RESTRICAO = new TipoNecessidade('RESTRICAO');

    static NAO_APLICAVEL = new TipoNecessidade('NAO_APLICAVEL');

    private descricao: string;

    private constructor(descricao: string) {
      this.descricao = descricao;
    }

    toString(): string {
      return `TipoNecessidade.${this.descricao}`;
    }

    toBaseDTO(): TipoNecessidadeDTO {
        const tipoNoDTO = new TipoNecessidadeDTO();
        tipoNoDTO.descricao = this.descricao;
        return tipoNoDTO;
    }

    static valor(tipoComb: string): TipoNecessidade {
        let valor: TipoNecessidade;
        switch (tipoComb) {
            case 'PERMISSAO':
                valor = TipoNecessidade.PERMISSAO;
                break;
            case 'RESTRICAO':
                valor = TipoNecessidade.RESTRICAO;
                break;
            case 'NAO_APLICAVEL':
                valor = TipoNecessidade.NAO_APLICAVEL;
                break;
            default:
                throw new SyntaxError('Unknown needs type');
        }
        return valor;
    }

    static values(): TipoNecessidade[] {
      const array: TipoNecessidade[] = [];
      array.push(TipoNecessidade.PERMISSAO);
      array.push(TipoNecessidade.RESTRICAO);
      array.push(TipoNecessidade.NAO_APLICAVEL);

      return array;
    }

    static valuesInDTO(): TipoNecessidadeDTO[] {
      const array: TipoNecessidadeDTO[] = [];
      array.push(TipoNecessidade.PERMISSAO.toBaseDTO());
      array.push(TipoNecessidade.RESTRICAO.toBaseDTO());
      array.push(TipoNecessidade.NAO_APLICAVEL.toBaseDTO());

      return array;
    }

}