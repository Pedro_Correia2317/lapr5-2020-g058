
import { Container } from 'typedi';
import { LinhaBuilderImpl } from '../application/linha/services/LinhaBuilderImpl';
import { CriarLinhaUseCase } from '../application/linha/usecases/CriarLinhaUseCase';
import { CriarLinhaUseCaseImpl } from '../application/linha/usecases/CriarLinhaUseCaseImpl';
import { NoEstacaoRecolhaBuilderImpl } from '../application/no/services/NoEstacaoRecolhaBuilderImpl';
import { NoParagemBuilderImpl } from '../application/no/services/NoParagemBuilderImpl';
import { NoPontoRendicaoBuilderImpl } from '../application/no/services/NoPontoRendicaoBuilderImpl';
import { CriarNoEstacaoRecolhaUseCaseImpl } from '../application/no/usecases/criarNoImpl/CriarNoEstacaoRecolhaUseCaseImpl';
import { CriarNoParagemUseCaseImpl } from '../application/no/usecases/criarNoImpl/CriarNoParagemUseCaseImpl';
import { CriarNoPontoRendicaoUseCaseImpl } from '../application/no/usecases/criarNoImpl/CriarNoPontoRendicaoUseCaseImpl';
import { CriarNoUseCaseImpl } from '../application/no/usecases/criarNoImpl/CriarNoUseCaseImpl';
import { PercursoBuilderImpl } from '../application/percurso/services/PercursoBuilderImpl';
import { SegmentoRedeBuilderImpl } from '../application/percurso/services/SegmentoRedeBuilderImpl';
import { DefinirPercursoUseCaseImpl } from '../application/percurso/usecases/DefinirPercursoUseCaseImpl';
import { TipoTripulanteBuilderImpl } from '../application/tipoTripulante/services/TipoTripulanteBuilderImpl';
import { CriarTipoTripulanteUseCaseImpl } from '../application/tipoTripulante/usecases/CriarTipoTripulanteUseCaseImpl';
import { TipoViaturaBuilderImpl } from '../application/tipoViatura/services/TipoViaturaBuilderImpl';
import { CriarTipoViaturaUseCaseImpl } from '../application/tipoViatura/usecases/CriarTipoViaturaUseCaseImpl';
import { ServicoLogger } from '../application/utils/services/ServicoLogger';
import { ServicoLoggerImpl } from '../application/utils/services/ServicoLoggerImpl';
import { LinhaRepository } from '../repository/interfaces/LinhaRepository';
import { PercursoRepository } from '../repository/interfaces/PercursoRepository';
import { TipoTripulanteRepository } from '../repository/interfaces/TipoTripulanteRepository';
import { TipoViaturaRepository } from '../repository/interfaces/TipoViaturaRepository';
import { LinhaRepositoryImpl } from '../repository/mongooseImpls/LinhaRepositoryImpl';
import { NoRepositoryImpl } from '../repository/mongooseImpls/NoRepositoryImpl';
import { PercursoRepositoryImpl } from '../repository/mongooseImpls/PercursoRepositoryImpl';
import { TipoTripulanteRepositoryImpl } from '../repository/mongooseImpls/TipoTripulanteRepositoryImpl';
import { TipoViaturaRepositoryImpl } from '../repository/mongooseImpls/TipoViaturaRepositoryImpl';
import { CriarLinhaUseCaseDirector } from '../application/linha/director/CriarLinhaUseCaseDirector';
import { CriarNoUseCaseDirector } from '../application/no/director/CriarNoUseCaseDirector';
import { CriarNoUseCaseDirectorCommand } from '../application/no/director/CriarNoUseCaseDirectorCommand';
import { CriarNoEstacaoRecolhaUseCaseDirector } from '../application/no/director/CriarNoEstacaoRecolhaUseCaseDirector';
import { CriarNoParagemUseCaseDirector } from '../application/no/director/CriarNoParagemUseCaseDirector';
import { CriarNoPontoRendicaoUseCaseDirector } from '../application/no/director/CriarNoPontoRendicaoUseCaseDirector';
import { CriarPercursoUseCaseDirector } from '../application/percurso/director/CriarPercursoUseCaseDirector';
import { NoRepository } from '../repository/interfaces/NoRepository';
import { CriarTipoViaturaUseCaseDirector } from '../application/tipoViatura/director/CriarTipoViaturaUseCaseDirector';
import { CriarTipoTripulanteUseCaseDirector } from '../application/tipoTripulante/director/CriarTipoTripulanteUseCaseDirector';
import { ImportarDadosFicheiroServiceImpl } from '../application/importacao/ImportarDadosFicheiroServiceImpl';
import { ImportadorDadosFicheiroContextImpl } from '../application/importacao/services/ImportadorDadosFicheiroContextImpl';
import { ImportadorDadosFicheiro } from '../application/importacao/services/ImportadorDadosFicheiro';
import { ImportadorDadosFicheiroGLX } from '../application/importacao/services/ImportadorDadosFicheiroGLX';
import { ImportarDadosFicheiroUseCaseDirector } from '../application/importacao/director/ImportarDadosFicheiroUseCaseDirector';
import { NoBuilder } from '../application/no/services/NoBuilder';
import { ListarNosUseCaseImpl } from '../application/no/usecases/ListarNosUseCaseImpl';
import { ListarLinhasUseCaseImpl } from '../application/linha/usecases/ListarLinhasUseCaseImpl';
import { ListarPercursosLinhaUseCaseImpl } from '../application/linha/usecases/ListarPercursosLinhaUseCaseImpl';
import { ListarTiposViaturasUseCaseImpl } from '../application/tipoViatura/usecases/ListarTiposViaturasUseCaseImpl';
import { ListarTiposTripulantesUseCaseImpl } from '../application/tipoTripulante/usecases/ListarTiposTripulantesUseCaseImpl';
import { ListarPercursosUseCaseImpl } from '../application/percurso/usecases/ListarPercursosUseCaseImpl';

export class DependencyInjectorContainer {

    static setAllDependencies(logLevel: string): void {
        Container.set("PrimaryServicoLogger", new ServicoLoggerImpl(logLevel));
        DependencyInjectorContainer.setDependenciasNos();
        DependencyInjectorContainer.setDependenciasPercursos();
        DependencyInjectorContainer.setDependenciasTipoViatura();
        DependencyInjectorContainer.setDependenciasTipoTripulante();
        DependencyInjectorContainer.setDependenciasLinha();
        DependencyInjectorContainer.setDependenciasImportacao();
    }

    private static setDependenciasNos(){
        const noRepo = new NoRepositoryImpl();
        Container.set("PrimaryNoRepository", noRepo);
        Container.set({id: "PrimaryNoEstacaoRecolhaBuilder", transient: true, type: NoEstacaoRecolhaBuilderImpl});
        Container.set({id: "PrimaryNoParagemBuilder", transient: true, type: NoParagemBuilderImpl});
        Container.set({id: "PrimaryNoPontoRendicaoBuilder", transient: true, type: NoPontoRendicaoBuilderImpl});
        const criarNoUseCase = new CriarNoUseCaseImpl();
        Container.set("PrimaryCriarNoUseCase", criarNoUseCase);
        const criarNP = new CriarNoParagemUseCaseImpl(criarNoUseCase);
        Container.set("PrimaryCriarNoParagemUseCase", criarNP);
        const criarNPR = new CriarNoPontoRendicaoUseCaseImpl(criarNoUseCase);
        Container.set("PrimaryCriarNoPontoRendicaoUseCase", criarNPR);
        const criarNER = new CriarNoEstacaoRecolhaUseCaseImpl(criarNPR);
        Container.set("PrimaryCriarNoEstacaoRecolhaUseCase", criarNER);
        
        const logger: ServicoLogger = Container.get("PrimaryServicoLogger");
        const mapaUis: Map<string, CriarNoUseCaseDirectorCommand> = new Map();
        mapaUis.set('NO_PARAGEM', new CriarNoParagemUseCaseDirector(criarNP, logger, noRepo));
        mapaUis.set('NO_ESTACAO_RECOLHA', new CriarNoEstacaoRecolhaUseCaseDirector(criarNER, logger, noRepo));
        mapaUis.set('NO_PONTO_RENDICAO', new CriarNoPontoRendicaoUseCaseDirector(criarNPR, logger, noRepo));

        const builderUis: Map<string, string> = new Map();
        builderUis.set('NO_PARAGEM', "PrimaryNoParagemBuilder");
        builderUis.set('NO_ESTACAO_RECOLHA', "PrimaryNoEstacaoRecolhaBuilder");
        builderUis.set('NO_PONTO_RENDICAO', "PrimaryNoPontoRendicaoBuilder");
        Container.set("PrimaryEscolhaCriarNoDirector", new CriarNoUseCaseDirector(mapaUis, builderUis));

        const mapaBuilders: Map<string, NoBuilder> = new Map();
        mapaBuilders.set('NO_PARAGEM', new NoParagemBuilderImpl());
        mapaBuilders.set('NO_ESTACAO_RECOLHA', new NoEstacaoRecolhaBuilderImpl());
        mapaBuilders.set('NO_PONTO_RENDICAO', new NoPontoRendicaoBuilderImpl());
        Container.set("PrimaryBuilders", mapaBuilders);

        Container.set("PrimaryListarNosUseCase", new ListarNosUseCaseImpl());

    }

    private static setDependenciasPercursos(){
        const criarP = new DefinirPercursoUseCaseImpl();
        Container.set("PrimaryDefinirPercursoUseCase", criarP);
        const pRepo = new PercursoRepositoryImpl();
        Container.set("PrimaryPercursoRepository", pRepo);
        Container.set({id: "PrimarySegmentoRedeBuilder", transient: true, type: SegmentoRedeBuilderImpl});
        Container.set({id: "PrimaryPercursoBuilder", transient: true, type: PercursoBuilderImpl});
        const logger: ServicoLogger = Container.get("PrimaryServicoLogger");
        const noRepo: NoRepository = Container.get("PrimaryNoRepository");
        Container.set("PrimaryCriarPercursoUseCaseDirector", new CriarPercursoUseCaseDirector(criarP, pRepo, noRepo, logger));
        Container.set("PrimaryListarPercursosUseCase", new ListarPercursosUseCaseImpl());
    }

    private static setDependenciasTipoViatura(){
        const criarTV = new CriarTipoViaturaUseCaseImpl();
        Container.set("PrimaryCriarTipoViaturaUseCase", criarTV);
        const tvRepo = new TipoViaturaRepositoryImpl();
        Container.set("PrimaryTipoViaturaRepository", tvRepo);
        Container.set({id: "PrimaryTipoViaturaBuilder", transient: true, type: TipoViaturaBuilderImpl});
        const logger: ServicoLogger = Container.get("PrimaryServicoLogger");
        Container.set("PrimaryCriarTipoViaturaUseCaseDirector", new CriarTipoViaturaUseCaseDirector(criarTV, tvRepo, logger));

        Container.set("PrimaryListarTiposViaturasUseCase", new ListarTiposViaturasUseCaseImpl());
    }

    private static setDependenciasTipoTripulante(){
        const criarTT = new CriarTipoTripulanteUseCaseImpl();
        const ttRepo = new TipoTripulanteRepositoryImpl();
        const logger: ServicoLogger = Container.get("PrimaryServicoLogger");
        Container.set("PrimaryCriarTipoTripulanteUseCase", criarTT);
        Container.set("PrimaryTipoTripulanteRepository", ttRepo);
        Container.set({id: "PrimaryTipoTripulanteBuilder", transient: true, type: TipoTripulanteBuilderImpl});
        Container.set("PrimaryCriarTipoTripulanteUseCaseDirector", new CriarTipoTripulanteUseCaseDirector(criarTT, ttRepo, logger));
        Container.set("PrimaryListarTiposTripulantesUseCase", new ListarTiposTripulantesUseCaseImpl());
    }

    private static setDependenciasLinha(){
        Container.set("PrimaryCriarLinhaUseCase", new CriarLinhaUseCaseImpl());
        Container.set("PrimaryLinhaRepository", new LinhaRepositoryImpl());
        Container.set({id: "PrimaryLinhaBuilder", transient: true, type: LinhaBuilderImpl});
        const useCase: CriarLinhaUseCase = Container.get("PrimaryCriarLinhaUseCase");
        const lRepo: LinhaRepository = Container.get("PrimaryLinhaRepository");
        const tvRepo: TipoViaturaRepository = Container.get("PrimaryTipoViaturaRepository");
        const ttRepo: TipoTripulanteRepository = Container.get("PrimaryTipoTripulanteRepository");
        const pRepo: PercursoRepository = Container.get("PrimaryPercursoRepository");
        const logger: ServicoLogger = Container.get("PrimaryServicoLogger");
        Container.set("PrimaryCriarLinhaUseCaseDirector", new CriarLinhaUseCaseDirector(useCase, lRepo, tvRepo, ttRepo, pRepo, logger));

        Container.set("PrimaryListarLinhasUseCase", new ListarLinhasUseCaseImpl());
        Container.set("PrimaryListarPercursosLinhaUseCase", new ListarPercursosLinhaUseCaseImpl());
    }

    private static setDependenciasImportacao(){
        const servico = new ImportarDadosFicheiroServiceImpl();
        Container.set("PrimaryImportarDadosFicheiroService", servico);
        const mapaImportadores = new Map<string, ImportadorDadosFicheiro>();
        mapaImportadores.set('.glx', new ImportadorDadosFicheiroGLX());
        const context = new ImportadorDadosFicheiroContextImpl(mapaImportadores);
        Container.set("PrimaryImportadorDadosFicheiroContext", context);
        const director = new ImportarDadosFicheiroUseCaseDirector(servico, context, 
                                Container.get("PrimaryEscolhaCriarNoDirector"), 
                                Container.get("PrimaryCriarPercursoUseCaseDirector"), 
                                Container.get("PrimaryCriarLinhaUseCaseDirector"), 
                                Container.get("PrimaryCriarTipoViaturaUseCaseDirector"), 
                                Container.get("PrimaryCriarTipoTripulanteUseCaseDirector"),
                                Container.get("PrimaryServicoLogger"));
        director.aplicarBuilders(Container.get("PrimaryPercursoBuilder"), 
                                Container.get("PrimarySegmentoRedeBuilder"), 
                                Container.get("PrimaryLinhaBuilder"), 
                                Container.get("PrimaryTipoViaturaBuilder"), 
                                Container.get("PrimaryTipoTripulanteBuilder"));
        Container.set("PrimaryImportarDadosFicheiroUseCaseDirector", director);
    }

}