import {NoDTO} from '../dto/NoDTO';
import {TipoNo} from './TipoNo';

export interface No {

    toBaseDTO(): NoDTO;

    tipoNo(): TipoNo;
}