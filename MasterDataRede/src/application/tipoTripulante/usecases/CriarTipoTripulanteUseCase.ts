
import {TipoTripulanteRepository} from '../../../repository/interfaces/TipoTripulanteRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {TipoTripulanteBuilder} from '../services/TipoTripulanteBuilder';

export interface CriarTipoTripulanteUseCase {

    aplicarCodigo(tBuilder: TipoTripulanteBuilder, logger: ServicoLogger, todosTiposT: TipoTripulanteRepository, codigo: string): Promise<DetalhesOperacaoDTO>;

    aplicarDescricao(tBuilder: TipoTripulanteBuilder, logger: ServicoLogger, todosTiposT: TipoTripulanteRepository, descricao: string): Promise<DetalhesOperacaoDTO>;

    registarTipoTripulante(tBuilder: TipoTripulanteBuilder, logger: ServicoLogger, todosTiposT: TipoTripulanteRepository): Promise<DetalhesOperacaoDTO>;

}