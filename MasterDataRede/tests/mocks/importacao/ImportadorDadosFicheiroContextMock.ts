
import { ImportadorDadosFicheiro } from "../../../src/application/importacao/services/ImportadorDadosFicheiro";
import { ImportadorDadosFicheiroContext } from "../../../src/application/importacao/services/ImportadorDadosFicheiroContext";
import { ImportadorDadosFicheiroGLX } from '../../../src/application/importacao/services/ImportadorDadosFicheiroGLX';

export class ImportadorDadosFicheiroContextMockTrue implements ImportadorDadosFicheiroContext {

    obterImportadorAdequado(directorio: string): ImportadorDadosFicheiro {
        return new ImportadorDadosFicheiroGLX();
    }

}

export class ImportadorDadosFicheiroContextMockFalse implements ImportadorDadosFicheiroContext {

    obterImportadorAdequado(directorio: string): ImportadorDadosFicheiro {
        return null;
    }

}