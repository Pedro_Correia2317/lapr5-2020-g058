
export interface Repository<T, ID> {

    save(objecto: T): Promise<boolean>;

    findById(codigo: ID): Promise<T>;

    findPage(numPagina: number, tamanhoPagina: number): Promise<T[]>;

    count(): Promise<number>;
}