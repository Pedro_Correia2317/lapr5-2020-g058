
import {TipoTripulante} from '../../../domain/tipoTripulante/model/TipoTripulante';

export interface TipoTripulanteBuilder {

    aplicarCodigo(codigo: string): void;

    aplicarDescricao(descricao: string): void;

    obterResultado(): TipoTripulante;

    reset(): void;

}