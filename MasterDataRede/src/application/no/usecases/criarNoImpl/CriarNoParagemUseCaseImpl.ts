
import { NoRepository } from "../../../../repository/interfaces/NoRepository";
import { DetalhesOperacaoDTO } from "../../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../../utils/services/ServicoLogger";
import { NoParagemBuilder } from "../../services/NoParagemBuilder";
import { CriarNoParagemUseCase } from "../CriarNoParagemUseCase";
import { CriarNoUseCase } from "../CriarNoUseCase";

export class CriarNoParagemUseCaseImpl implements CriarNoParagemUseCase {

    private criarNo: CriarNoUseCase;

    constructor(criarNo: CriarNoUseCase){
        this.criarNo = criarNo;
    }

    aplicarNome(noBuilder: NoParagemBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO {
        return this.criarNo.aplicarNome(noBuilder, logger, nome);
    }

    async aplicarAbreviatura(noBuilder: NoParagemBuilder, logger: ServicoLogger, todosNos: NoRepository, abreviatura: string): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.aplicarAbreviatura(noBuilder, logger, todosNos, abreviatura);
    }

    aplicarCoordenadas(noBuilder: NoParagemBuilder, logger: ServicoLogger, lat: number, lon: number): DetalhesOperacaoDTO {
        return this.criarNo.aplicarCoordenadas(noBuilder, logger, lat, lon);
    }

    registarNo(noBuilder: NoParagemBuilder, logger: ServicoLogger, todosNos: NoRepository): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.registarNo(noBuilder, logger, todosNos);
    }

}