
import { NoPontoRendicaoBuilder } from '../../../src/application/no/services/NoPontoRendicaoBuilder';
import { AbreviaturaNo } from '../../../src/domain/no/model/AbreviaturaNo';
import { CoordenadasNo } from '../../../src/domain/no/model/CoordenadasNo';
import { NomeNo } from '../../../src/domain/no/model/NomeNo';
import { NoPontoRendicao } from '../../../src/domain/no/model/NoPontoRendicao';

export class NoPontoRendicaoBuilderMockSucesso implements NoPontoRendicaoBuilder {

    reset(): void {
    }

    isNoIgual(abreviatura: string): boolean {
        return true;
    }

    obterResultado(): NoPontoRendicao {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(23.4567, 12);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        return new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, tempos);
    }
    
    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void {
    }

    aplicarCodigo(codigo: string): void {
    }

    aplicarNome(nome: string): void {
    }

    aplicarAbreviatura(abreviatura: string): void {
    }

    aplicarCoordenadas(lat: number, lon: number): void {
    }

}

export class NoPontoRendicaoBuilderMockErro implements NoPontoRendicaoBuilder {

    reset(): void {
    }

    isNoIgual(abreviatura: string): boolean {
        return false;
    }

    obterResultado(): NoPontoRendicao {
        throw new SyntaxError('Teste');
    }
    
    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarNome(nome: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarAbreviatura(abreviatura: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarCoordenadas(lat: number, lon: number): void {
        throw new TypeError('Teste');    
    }

}