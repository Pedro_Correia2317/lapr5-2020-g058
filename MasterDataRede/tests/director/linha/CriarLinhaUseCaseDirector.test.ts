
import { CriarLinhaUseCaseDirector } from '../../../src/application/linha/director/CriarLinhaUseCaseDirector';
import { LinhaBuilderImpl } from '../../../src/application/linha/services/LinhaBuilderImpl';
import { CriarLinhaUseCaseImpl } from '../../../src/application/linha/usecases/CriarLinhaUseCaseImpl';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { LinhaRepositoryMockFalse, LinhaRepositoryMockTrue } from '../../mocks/linha/linhaRepositoryMock';
import { PedidoCriarLinhaDTO } from '../../../src/application/linha/dto/PedidoCriarLinhaDTO';
import { PercursoRepositoryMockFalse, PercursoRepositoryMockTrue } from '../../mocks/percurso/percursoRepositoryMock';
import { TipoViaturaRepositoryMockFalse, TipoViaturaRepositoryMockTrue } from '../../mocks/tipoViatura/tipoViaturaRepositoryMock';
import { TipoTripulanteRepositoryMockFalse, TipoTripulanteRepositoryMockTrue } from '../../mocks/tipoTripulante/tipoTripulanteRepositoryMock';

const loggerMock = new ServicoLoggerMock();
const lUseCase = new CriarLinhaUseCaseImpl();
const lRepoTrue = new LinhaRepositoryMockTrue();
const lRepoFalse = new LinhaRepositoryMockFalse();
const pRepoTrue = new PercursoRepositoryMockTrue();
const pRepoFalse = new PercursoRepositoryMockFalse();
const tvRepoTrue = new TipoViaturaRepositoryMockTrue();
const tvRepoFalse = new TipoViaturaRepositoryMockFalse();
const ttRepoTrue = new TipoTripulanteRepositoryMockTrue();
const ttRepoFalse = new TipoTripulanteRepositoryMockFalse();

test('testarPedidoValido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoComPercursoDesconhecido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoFalse, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTipoViaturaDesconhecido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoFalse, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTipoTripulanteDesconhecido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoFalse, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComCodigoRepetido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoFalse, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemTipoViatura1', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    //pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    //pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoFalse, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemTipoViatura2', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    //pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    //pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemTipoTripulante1', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    //pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    //pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoFalse, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemTipoTripulante2', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    // pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    // pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemRestricoes1', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    // pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    // pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    // pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    // pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoFalse, ttRepoFalse, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemRestricoes2', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    // pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    // pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    // pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    // pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemCodigo', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    // pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComCodigoInvalido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'Sou Invalido...';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemNome', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    // pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComNomeInvalido', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = '?';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemPercursosReforco', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    // pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemPercursosVazio', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    // pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemPercursosExtra', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    // pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    // pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemPercursosVolta', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    // pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemPercursosIda', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    // pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemPercursos', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemDados', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoDadosMinimos', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoComTiposViaturasMasSemTipoNecessidade', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    // pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTiposTripulantesMasSemTipoNecessidade', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    //pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemTiposTripulantesMasComTipoNecessidade', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    //pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoSemTiposViaturasMasComTipoNecessidade', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    // pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'PERMISSAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(true);
});

test('testarPedidoComTiposViaturasMasComTipoNecessidadeInvalido1', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'NAO_APLICAVEL';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTiposViaturasMasComTipoNecessidadeInvalido2', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'SOU_INVALIDO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'RESTRICAO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTiposTripulantesMasComTipoNecessidadeInvalido1', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'RESTRICAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'NAO_APLICAVEL';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTiposTripulantesMasComTipoNecessidadeInvalido2', async () => {
    const pedido: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
    pedido.codigo = 'XZEEMI62FWH79R3C2FMB';
    pedido.nome = 'Linha Verde';
    pedido.percursosIda = ['RNNXRZLBKHWVH48MAWD1', 'KX5CRUDT1GESJQ2IMA2K', 'RFY02HB3JOER1Q4QQO0K'];
    pedido.percursosVolta = ['MIBX0Z9X7VV7F7FB8X3G', '808ZE8ATEQQQQJFGECC0'];
    pedido.percursosReforco = ['U9TUJFO14P1XRN1Z3E99', 'K6X7XBEUL9U9UKLR5Y1T'];
    pedido.percursosVazio = ['6BRK38P9U3YXUIT2LWIH', 'RLEO34AWC220X542S8X9'];
    pedido.tiposViaturas = ['EJER2996B88BXXGUGPUP', 'QNMWE28NBI2AQUGPQAT3'];
    pedido.tipoNecessidadeTipoViatura = 'RESTRICAO';
    pedido.tiposTripulantes = ['P4HDLUJUGF2DBUH91QI5', 'E6Z060WMLYCBXAK90ML2', '482N92IRX4MVPEUX8E5V'];
    pedido.tipoNecessidadeTipoTripulante = 'SOU_INVALIDO';
    const lBuilder = new LinhaBuilderImpl();
    const director = new CriarLinhaUseCaseDirector(lUseCase, lRepoTrue, tvRepoTrue, ttRepoTrue, pRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, lBuilder)).sucesso).toBe(false);
});