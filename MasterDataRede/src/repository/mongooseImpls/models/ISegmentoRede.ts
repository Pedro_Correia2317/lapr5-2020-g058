
import Mongoose from 'mongoose';

export interface ISegmentoRede extends Mongoose.Document {

    abreviaturaNoInicio: string;

    abreviaturaNoFim: string;

    distancia: number;

    duracao: number;

    sequencia: number;

}