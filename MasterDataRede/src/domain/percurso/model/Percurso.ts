import { PercursoDTO } from "../dto/PercursoDTO";
import { SegmentoRedeDTO } from "../dto/SegmentoRedeDTO";
import { CodigoPercurso } from "./CodigoPercurso";
import { DescricaoPercurso } from "./DescricaoPercurso";
import { DistanciaTotalPercurso } from "./DistanciaTotalPercurso";
import { DuracaoTotalPercurso } from "./DuracaoTotalPercurso";
import { SegmentoRede } from "./SegmentoRede";

export class Percurso {

    private codigo: CodigoPercurso;

    private descricao: DescricaoPercurso;

    private distanciaTotal: DistanciaTotalPercurso;

    private duracaoTotal: DuracaoTotalPercurso;

    private segmentos: SegmentoRede[];

    constructor(cod: CodigoPercurso, segs: SegmentoRede[], desc: DescricaoPercurso,
                                dist: DistanciaTotalPercurso, dur: DuracaoTotalPercurso){
        if(cod == null || segs == null || desc == null || dist == null || dur == null || segs.length < 1){
            throw new SyntaxError('Null values were inserted');
        }
        segs.forEach((seg: SegmentoRede) => {
            if(seg == null){
                throw new SyntaxError('Detected invalid segment');
            }
        });
        this.segmentos = segs.slice();
        this.codigo = cod;
        this.descricao = desc;
        this.distanciaTotal = dist;
        this.duracaoTotal = dur;
    }

    toBaseDTO(): PercursoDTO {
        const dto = new PercursoDTO();
        dto.codigo = this.codigo.getValue();
        dto.descricao = this.descricao.getValue();
        dto.distanciaTotal = this.distanciaTotal.getValue();
        dto.duracaoTotal = this.duracaoTotal.getValue();

        return dto;
    }

    toSegmentosDTO(): SegmentoRedeDTO[] {
        const segmentosDTO: SegmentoRedeDTO[] = [];
        this.segmentos.forEach(segmento => {
            segmentosDTO.push(segmento.toBaseDTO());
        });

        return segmentosDTO;
    }
}