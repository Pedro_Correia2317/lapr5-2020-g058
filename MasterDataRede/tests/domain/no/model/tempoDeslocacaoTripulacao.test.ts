
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';
import { TempoDeslocacaoTripulacao } from '../../../../src/domain/no/model/TempoDeslocacaoTripulacao';

test("testarTempoDeslocacaoNulo", () => {
    const abreviaturaNo = new AbreviaturaNo('LORDELO');
    expect(() => {new TempoDeslocacaoTripulacao(null, abreviaturaNo)}).toThrow(ReferenceError);
    expect(() => {new TempoDeslocacaoTripulacao(undefined, abreviaturaNo)}).toThrow(ReferenceError);
    expect(() => {new TempoDeslocacaoTripulacao(5, null)}).toThrow(ReferenceError);
    expect(() => {new TempoDeslocacaoTripulacao(5, undefined)}).toThrow(ReferenceError);
});

test('testarTempoDeslocacaoValoresPositivos', () => {
    const abreviaturaNo = new AbreviaturaNo('LORDELO');
    expect(new TempoDeslocacaoTripulacao(1, abreviaturaNo)).toBeInstanceOf(TempoDeslocacaoTripulacao);
    expect(new TempoDeslocacaoTripulacao(13.0, abreviaturaNo)).toBeInstanceOf(TempoDeslocacaoTripulacao);
    expect(new TempoDeslocacaoTripulacao(200, abreviaturaNo)).toBeInstanceOf(TempoDeslocacaoTripulacao);
});

test('testarTempoDeslocacaoValoresNegativos', () => {
    const abreviaturaNo = new AbreviaturaNo('LORDELO');
    expect(() => {new TempoDeslocacaoTripulacao(-11, abreviaturaNo)}).toThrow(SyntaxError);
    expect(() => {new TempoDeslocacaoTripulacao(-21, abreviaturaNo)}).toThrow(SyntaxError);
    expect(() => {new TempoDeslocacaoTripulacao(-1, abreviaturaNo)}).toThrow(SyntaxError);
});

test('testarTempoDeslocacaoValoresCasasDecimais', () => {
    const abreviaturaNo = new AbreviaturaNo('LORDELO');
    expect(() => {new TempoDeslocacaoTripulacao(11.4422, abreviaturaNo)}).toThrow(ReferenceError);
    expect(() => {new TempoDeslocacaoTripulacao(21.0000001, abreviaturaNo)}).toThrow(ReferenceError);
    expect(() => {new TempoDeslocacaoTripulacao(-1.234522, abreviaturaNo)}).toThrow(ReferenceError);
});

test('testarTempoDeslocacaoValoresExtremos', () => {
    const abreviaturaNo = new AbreviaturaNo('LORDELO');
    expect(() => {new TempoDeslocacaoTripulacao(0, abreviaturaNo)}).toThrow(SyntaxError);
    expect(() => {new TempoDeslocacaoTripulacao(10801, abreviaturaNo)}).toThrow(SyntaxError);
});

test('testarTempoDeslocacaoValoresValidos', () => {
    const abreviaturaNo = new AbreviaturaNo('LORDELO');
    expect(new TempoDeslocacaoTripulacao(10800, abreviaturaNo)).toBeInstanceOf(TempoDeslocacaoTripulacao);
    expect(new TempoDeslocacaoTripulacao(123.0, abreviaturaNo)).toBeInstanceOf(TempoDeslocacaoTripulacao);
});