
import { DescricaoTipoViatura } from '../../../../src/domain/tipoViatura/model/DescricaoTipoViatura';

test("testarDescricaoNulo", () => {
    expect(() => {new DescricaoTipoViatura(null)}).toThrow(ReferenceError);
    expect(() => {new DescricaoTipoViatura(undefined)}).toThrow(ReferenceError);
});

test("testarDescricaoVazio", () => {
    expect(() => {new DescricaoTipoViatura('')}).toThrow(SyntaxError);
});

test("testarDescricaoDemasiadoComprido", () => {
    expect(() => {new DescricaoTipoViatura('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
                         + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
                         + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
                         + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetbv')}).toThrow(SyntaxError); //201 chars
});

test("testarDescricaoUsualComCarateresInvalidos", () => {
    expect(() => {new DescricaoTipoViatura('🌺Estação Principal N12 de Aguiar🌺')}).toThrow(SyntaxError);
});

test("testarDescricaoDemasiadoPequeno", () => {
    expect(() => {new DescricaoTipoViatura('1')}).toThrow(SyntaxError);
});

test("testarDescricaoPequenoValido", () => {
    expect(new DescricaoTipoViatura('12')).toBeInstanceOf(DescricaoTipoViatura);
});

test("testarDescricaoGrandeValido", () => {
    expect(new DescricaoTipoViatura('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
    + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
    + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
    + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetb')).toBeInstanceOf(DescricaoTipoViatura); //200 chars
});

test("testarDescricaoUsualValido", () => {
    expect(new DescricaoTipoViatura('Estacao Principal N12 de Aguiar')).toBeInstanceOf(DescricaoTipoViatura);
});

test("testarDescricaoSoNumeros", () => {
    expect(new DescricaoTipoViatura('15367363211')).toBeInstanceOf(DescricaoTipoViatura);
});

test("testarDescricaoSoLetras", () => {
    expect(new DescricaoTipoViatura('cbjjcjhfdHJJbbcjBJbjvUBbuubuUDwbbeS')).toBeInstanceOf(DescricaoTipoViatura);
});

test("testarDescricaoSoEspacos", () => {
    expect(() => {new DescricaoTipoViatura('         ')}).toThrow(SyntaxError);
});

test("testarDescricaoUsualComCarateresAcentos", () => {
    expect(new DescricaoTipoViatura('Estação Principal N12 de Aguiar')).toBeInstanceOf(DescricaoTipoViatura);
});