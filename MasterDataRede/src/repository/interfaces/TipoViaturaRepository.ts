

import { TipoViaturaBuilder } from '../../application/tipoViatura/services/TipoViaturaBuilder';
import {CodigoTipoViatura} from '../../domain/tipoViatura/model/CodigoTipoViatura';
import {TipoViatura} from '../../domain/tipoViatura/model/TipoViatura';
import {Repository} from './Repository';

export interface TipoViaturaRepository extends Repository<TipoViatura, CodigoTipoViatura> {

    isCodigoUnico(codigo: string): Promise<boolean>;

    isDescricaoUnico(descricao: string): Promise<boolean>;

    existe(codigo: string): Promise<boolean>;

    obterTiposViaturas(builder: TipoViaturaBuilder, sort: string, numPag: number, 
                    tamPag: number, descricao: string): Promise<TipoViatura[]>;

    countComFiltros(descricao: string): Promise<number>;

}