
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TipoViaturaDTO } from 'src/app/models/tipoViatura/TipoViaturaDTO';

@Component({
  selector: 'adicao-tipo-viatura',
  templateUrl: './adicao-tipo-viatura.component.html',
  styleUrls: ['./adicao-tipo-viatura.component.css']
})
export class AdicaoTipoViaturaComponent implements OnInit {

    codTipoViatura: string;

    constructor(public dialogRef: MatDialogRef<AdicaoTipoViaturaComponent>) {
    }
  
    ngOnInit() {
    }

    onCancelarClick(){
        this.dialogRef.close();
    }

    adicionarCodigoTipoViatura(codTipoViatura: string){
        this.codTipoViatura = codTipoViatura;
    }

    onAdicionarClick(){
        let codTipoViatura = undefined;
        if(this.codTipoViatura != null){
            codTipoViatura = this.codTipoViatura;
        }
        this.dialogRef.close(codTipoViatura);
    }

    onClickedVehicleType(tipoViatura: TipoViaturaDTO){
        this.adicionarCodigoTipoViatura(tipoViatura.codigo);
    }

}