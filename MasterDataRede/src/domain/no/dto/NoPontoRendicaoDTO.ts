import {NoDTO} from './NoDTO';

export class NoPontoRendicaoDTO extends NoDTO {

    public tempos: {tempo: number, abreviaturaNo: string}[];

}