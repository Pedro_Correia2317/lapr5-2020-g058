
import Mongoose from 'mongoose';

export interface ITempoDeslocacaoTripulacao extends Mongoose.Document {

    tempo: number;

    abreviaturaNo: string;

}