
import {TipoTripulante} from '../../../domain/tipoTripulante/model/TipoTripulante';
import {TipoTripulanteRepository} from '../../../repository/interfaces/TipoTripulanteRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {TipoTripulanteBuilder} from '../services/TipoTripulanteBuilder';
import {CriarTipoTripulanteUseCase} from './CriarTipoTripulanteUseCase';

export class CriarTipoTripulanteUseCaseImpl implements CriarTipoTripulanteUseCase {

    async aplicarCodigo(tBuilder: TipoTripulanteBuilder, logger: ServicoLogger, todosTiposT: TipoTripulanteRepository, codigo: string): Promise<DetalhesOperacaoDTO> {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            if (await todosTiposT.isCodigoUnico(codigo)){
                tBuilder.aplicarCodigo(codigo);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error(`Inserted repeated code ${codigo}`));
                resultados.mensagem = 'createDriverType.repeatedCode';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createDriverType.nullCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createDriverType.invalidCode';
            } else {
                throw err;
            }
        }

return resultados;
    }

    async aplicarDescricao(tBuilder: TipoTripulanteBuilder, logger: ServicoLogger, todosTiposT: TipoTripulanteRepository, descricao: string): Promise<DetalhesOperacaoDTO> {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            if (await todosTiposT.isDescricaoUnico(descricao)){
                tBuilder.aplicarDescricao(descricao);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error(`Inserted repeated description ${descricao}`));
                resultados.mensagem = 'createDriverType.repeatedDescription';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createDriverType.nullDescription';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createDriverType.invalidDescription';
            } else {
                throw err;
            }
        }

return resultados;
    }

    async registarTipoTripulante(tBuilder: TipoTripulanteBuilder, logger: ServicoLogger, todosTiposT: TipoTripulanteRepository): Promise<DetalhesOperacaoDTO> {
        const resultado = new DetalhesOperacaoDTO();
        try {
            const tipoViatura: TipoTripulante = tBuilder.obterResultado();
            if (await todosTiposT.save(tipoViatura)){
                resultado.sucesso = true;
                resultado.mensagem = 'newDriverType.creationSuccess';
            } else {
                resultado.mensagem = 'newDriverType.errorRegisteringDriverType';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultado.mensagem = 'newDriverType.driverTypeCreationFailure';
            } else {
                throw err;
            }
        }

return resultado;
    }

}