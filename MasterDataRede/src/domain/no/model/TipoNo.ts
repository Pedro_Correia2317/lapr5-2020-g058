import { TipoNoDTO } from "../dto/TipoNoDTO";

export class TipoNo {

    static NO_PARAGEM = new TipoNo('NO_PARAGEM', false, false);

    static NO_ESTACAO_RECOLHA = new TipoNo('NO_ESTACAO_RECOLHA', true, true);

    static NO_PONTO_RENDICAO = new TipoNo('NO_PONTO_RENDICAO', true, false);

    private nome: string;

    private temTempos: boolean;

    private temCapacidade: boolean;

    private constructor(nome: string, temTempos: boolean, temCapacidade: boolean) {
      this.nome = nome;
      this.temTempos = temTempos;
      this.temCapacidade = temCapacidade;
    }

    toString(): string {
      return `TipoNo.${this.nome}`;
    }

    toBaseDTO(): TipoNoDTO {
      const tipoNoDTO = new TipoNoDTO();
      tipoNoDTO.nome = this.nome;
      tipoNoDTO.temTempos = this.temTempos;
      tipoNoDTO.temCapacidade = this.temCapacidade;
      return tipoNoDTO;
    }

    static values(): TipoNo[] {
      const array: TipoNo[] = [];
      array.push(TipoNo.NO_PARAGEM);
      array.push(TipoNo.NO_ESTACAO_RECOLHA);
      array.push(TipoNo.NO_PONTO_RENDICAO);

      return array;
    }

    static valuesInDTO(): TipoNoDTO[] {
      const array: TipoNoDTO[] = [];
      array.push(TipoNo.NO_PARAGEM.toBaseDTO());
      array.push(TipoNo.NO_ESTACAO_RECOLHA.toBaseDTO());
      array.push(TipoNo.NO_PONTO_RENDICAO.toBaseDTO());

      return array;
    }

}