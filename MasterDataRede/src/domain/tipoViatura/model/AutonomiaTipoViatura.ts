
export class AutonomiaTipoViatura {

    private autonomia: number;

    constructor(autonomia: number){
        if (!Number.isInteger(autonomia)){
            throw new TypeError(`The autonomy is not a integer: ${autonomia}`);
        }
        if (autonomia <= 0 || autonomia > 9999999){
            throw new SyntaxError(`The autonomy ${autonomia} is <= 0 or > 9999999`);
        }
        this.autonomia = autonomia;
    }

    getValue(): number {
        return this.autonomia;
    }

    toString(): string {
        return `AutonomiaTipoViatura: [${this.autonomia}]`;
    }
}