
import { ConsumoTipoViatura } from '../../../../src/domain/tipoViatura/model/ConsumoTipoViatura';

test("testarConsumoTipoViaturaNulo", () => {
    expect(() => {new ConsumoTipoViatura(null)}).toThrow(TypeError);
    expect(() => {new ConsumoTipoViatura(undefined)}).toThrow(TypeError);
});

test("testarConsumoNegativo", () => {
    expect(() => {new ConsumoTipoViatura(-1)}).toThrow(SyntaxError);
});

test("testarConsumoNegativo2", () => {
    expect(() => {new ConsumoTipoViatura(-12.4563)}).toThrow(SyntaxError);
});

test("testarConsumoInvalido", () => {
    expect(() => {new ConsumoTipoViatura(999.001)}).toThrow(SyntaxError);
});

test("testarConsumoInvalido2", () => {
    expect(() => {new ConsumoTipoViatura(19373.24567)}).toThrow(SyntaxError);
});

test("testarConsumoInvalido3", () => {
    expect(() => {new ConsumoTipoViatura(0)}).toThrow(SyntaxError);
});

test("testarConsumoPequenoValido", () => {
    expect(new ConsumoTipoViatura(1)).toBeInstanceOf(ConsumoTipoViatura);
});

test("testarConsumoPequenoValido2", () => {
    expect(new ConsumoTipoViatura(999)).toBeInstanceOf(ConsumoTipoViatura);
});

test("testarConsumoPequenoValido3", () => {
    expect(new ConsumoTipoViatura(0.4828)).toBeInstanceOf(ConsumoTipoViatura);
});

test("testarConsumoValido4", () => {
    expect(new ConsumoTipoViatura(998.9999)).toBeInstanceOf(ConsumoTipoViatura);
});

test("testarConsumoValido5", () => {
    expect(new ConsumoTipoViatura(5)).toBeInstanceOf(ConsumoTipoViatura);
});

test("testarConsumoValido6", () => {
    expect(new ConsumoTipoViatura(999.000001)).toBeInstanceOf(ConsumoTipoViatura);
});