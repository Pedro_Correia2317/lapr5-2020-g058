
import { NecessidadesTipoViatura } from '../../../../src/domain/linha/model/NecessidadesTipoViatura';
import { TipoNecessidade } from '../../../../src/domain/linha/model/TipoNecessidade';
import { CodigoTipoViatura } from '../../../../src/domain/tipoViatura/model/CodigoTipoViatura';

test('testarNecessidadesTipoViaturaNulo', () => {
    expect(() => {new NecessidadesTipoViatura(null, null)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoViatura(undefined, undefined)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoViatura([], null)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoViatura([], undefined)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoViatura(null, TipoNecessidade.PERMISSAO)}).toThrow(ReferenceError);
    expect(() => {new NecessidadesTipoViatura(undefined, TipoNecessidade.PERMISSAO)}).toThrow(ReferenceError);
});

test('testarNecessidadesTipoViaturaInvalidos', () => {
    const trip = new CodigoTipoViatura('12345678901234567890');
    expect(() => {new NecessidadesTipoViatura([], TipoNecessidade.PERMISSAO)}).toThrow(SyntaxError);
    expect(() => {new NecessidadesTipoViatura([], TipoNecessidade.RESTRICAO)}).toThrow(SyntaxError);
    expect(() => {new NecessidadesTipoViatura([trip], TipoNecessidade.NAO_APLICAVEL)}).toThrow(SyntaxError);
    expect(() => {new NecessidadesTipoViatura([null], TipoNecessidade.RESTRICAO)}).toThrow(ReferenceError);
});

test('testarNecessidadesTipoViaturaValidos', () => {
    const trip = new CodigoTipoViatura('12345678901234567890');
    expect(new NecessidadesTipoViatura([trip], TipoNecessidade.PERMISSAO)).toBeInstanceOf(NecessidadesTipoViatura);
    expect(new NecessidadesTipoViatura([trip], TipoNecessidade.RESTRICAO)).toBeInstanceOf(NecessidadesTipoViatura);
    expect(new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL)).toBeInstanceOf(NecessidadesTipoViatura);
});