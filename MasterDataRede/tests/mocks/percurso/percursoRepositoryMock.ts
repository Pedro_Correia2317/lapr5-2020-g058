

import { PercursoBuilder } from "../../../src/application/percurso/services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../../../src/application/percurso/services/SegmentoRedeBuilder";
import { AcessoDadosError } from "../../../src/application/utils/errors/AcessoDadosError";
import { AbreviaturaNo } from "../../../src/domain/no/model/AbreviaturaNo";
import { CodigoPercurso } from "../../../src/domain/percurso/model/CodigoPercurso";
import { DescricaoPercurso } from "../../../src/domain/percurso/model/DescricaoPercurso";
import { DistanciaSegmento } from "../../../src/domain/percurso/model/DistanciaSegmento";
import { DistanciaTotalPercurso } from "../../../src/domain/percurso/model/DistanciaTotalPercurso";
import { DuracaoSegmento } from "../../../src/domain/percurso/model/DuracaoSegmento";
import { DuracaoTotalPercurso } from "../../../src/domain/percurso/model/DuracaoTotalPercurso";
import { Percurso } from "../../../src/domain/percurso/model/Percurso";
import { SegmentoRede } from "../../../src/domain/percurso/model/SegmentoRede";
import { SequenciaSegmento } from "../../../src/domain/percurso/model/SequenciaSegmento";
import { PercursoRepository } from "../../../src/repository/interfaces/PercursoRepository";


export class PercursoRepositoryMockFalse implements PercursoRepository {

    obterPercursos(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, sort: string, numPag: number, tamPag: number, descricao: string): Promise<Percurso[]> {
        throw new AcessoDadosError("Method not implemented.");
    }
    countComFiltros(descricao: string): Promise<number> {
        throw new AcessoDadosError("Method not implemented.");
    }

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return false;
    }

    async save(objecto: Percurso): Promise<boolean> {
        return false;
    }

    async findById(codigo: CodigoPercurso): Promise<Percurso> {
        return undefined;
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<Percurso[]> {
        return [];
    }

    async count(): Promise<number> {
        return 0;
    }

    async existe(codigo: string): Promise<boolean> {
        return false;
    }

    async findByCodigo(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, codigo: string): Promise<Percurso> {
        throw new AcessoDadosError('Teste');
    }

}

export class PercursoRepositoryMockTrue implements PercursoRepository {

    private percurso: Percurso;

    constructor(){
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        this.percurso = new Percurso(codigoPercurso, segmentos, descricao, distanciaT, duracaoT);
    }
    async obterPercursos(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, sort: string, numPag: number, tamPag: number, descricao: string): Promise<Percurso[]> {
        return [];
    }
    async countComFiltros(descricao: string): Promise<number> {
        return 0;
    }

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return true;
    }

    async save(objecto: Percurso): Promise<boolean> {
        return true;
    }

    async findById(codigo: CodigoPercurso): Promise<Percurso> {
        return this.percurso;
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<Percurso[]> {
        return [this.percurso];
    }

    async count(): Promise<number> {
        return 0;
    }

    async existe(codigo: string): Promise<boolean> {
        return true;
    }

    async findByCodigo(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, codigo: string): Promise<Percurso> {
        return this.percurso;
    }

}