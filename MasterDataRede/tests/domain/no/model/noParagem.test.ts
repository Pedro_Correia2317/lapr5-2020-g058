
import { NoParagem } from '../../../../src/domain/no/model/NoParagem';
import { NomeNo } from '../../../../src/domain/no/model/NomeNo';
import { CoordenadasNo } from '../../../../src/domain/no/model/CoordenadasNo';
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';

test("testarNomeNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        new NoParagem(abreviaturaNo, null, coordenadasNo)}).toThrow(SyntaxError);
});

test("testarCoordenadasNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        new NoParagem(abreviaturaNo, nomeNo, null)}).toThrow(SyntaxError);
});

test("testarAbreviaturaNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        new NoParagem(null, nomeNo, coordenadasNo)}).toThrow(SyntaxError);
});

test("testarNomeIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        new NoParagem(abreviaturaNo, undefined, coordenadasNo)}).toThrow(SyntaxError);
});

test("testarCoordenadasIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        new NoParagem(abreviaturaNo, nomeNo, undefined)}).toThrow(SyntaxError);
});

test("testarAbreviaturaIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        new NoParagem(undefined, nomeNo, coordenadasNo)}).toThrow(SyntaxError);
});

test("testarValoresValidos", () => {
    const nomeNo = new NomeNo('Estação de Aguiar');
    const coordenadasNo = new CoordenadasNo(23.4567, 12);
    const abreviaturaNo = new AbreviaturaNo('AGUIAR');
    expect(new NoParagem(abreviaturaNo, nomeNo, coordenadasNo)).toBeInstanceOf(NoParagem);
});