
import { PercursoBuilder } from "../services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../services/SegmentoRedeBuilder";
import { DefinirPercursoUseCase } from "../usecases/DefinirPercursoUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { NoRepository } from "../../../repository/interfaces/NoRepository";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { PedidoCriarPercursoDTO } from "../dto/PedidoCriarPercursoDTO";
import { PedidoCriarSegmentoDTO } from "../dto/PedidoCriarSegmentoDTO";

export class CriarPercursoUseCaseDirector {

    private criarUseCase: DefinirPercursoUseCase;

    private todosPercursos: PercursoRepository;
    
    private todosNos: NoRepository;

    private logger: ServicoLogger;

    constructor(c: DefinirPercursoUseCase, tv: PercursoRepository, tn: NoRepository, l: ServicoLogger){
        this.criarUseCase = c;
        this.todosPercursos = tv;
        this.todosNos = tn;
        this.logger = l;
    }

    async processarPedido(req: PedidoCriarPercursoDTO, pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarUseCase.aplicarCodigo(pBuilder, this.logger, this.todosPercursos, req.codigo);
        if(!resultado.sucesso) return resultado;
        if(req.segmentos){
            for(const segmento of req.segmentos){
                resultado = await this.processarSegmento(sBuilder, pBuilder, segmento);
                if(!resultado.sucesso) break;
            }
        }
        if(!resultado.sucesso) return resultado;
        return this.criarUseCase.registarPercurso(pBuilder, this.logger, this.todosPercursos);
    }
    
    private async processarSegmento(sBuilder: SegmentoRedeBuilder, pBuilder: PercursoBuilder, seg: PedidoCriarSegmentoDTO): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarUseCase.aplicarNoInicial(pBuilder, sBuilder, this.logger, this.todosNos, seg.abreviaturaNoInicial);
        if (!resultado.sucesso) return resultado;
        resultado = await this.criarUseCase.aplicarNoFinal(pBuilder, sBuilder, this.logger, this.todosNos, seg.abreviaturaNoFinal);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.adicionarDuracao(pBuilder, sBuilder, this.logger, seg.duracao);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.adicionarDistancia(pBuilder, sBuilder, this.logger, seg.distancia);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.adicionarSequenciaAutomatica(sBuilder, this.logger);
        if (!resultado.sucesso) return resultado;
        return this.criarUseCase.adicionarSegmentoCriadoAPercurso(pBuilder, sBuilder, this.logger);
    }


}