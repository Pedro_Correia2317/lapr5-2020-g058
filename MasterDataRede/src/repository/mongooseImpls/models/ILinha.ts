
import Mongoose from 'mongoose';

export interface ILinha extends Mongoose.Document {

    codigo: string;

    nome: string;

    percursosIda: Array<string>;

    percursosVolta: Array<string>;

    percursosReforco: Array<string>;

    percursosVazio: Array<string>;

    necessidadesTipoViatura: Array<string>;

    tipoNecessidadesTipoViatura: string;

    necessidadesTipoTripulante: Array<string>;

    tipoNecessidadesTipoTripulante: string;

}