
import { SequenciaSegmento } from '../../../../src/domain/percurso/model/SequenciaSegmento';

test("testarSequenciaSegmentoNulo", () => {
    expect(() => {new SequenciaSegmento(null)}).toThrow(TypeError);
    expect(() => {new SequenciaSegmento(undefined)}).toThrow(TypeError);
});

test("testarSequenciaNegativo", () => {
    expect(() => {new SequenciaSegmento(-1)}).toThrow(SyntaxError);
});

test("testarSequenciaNegativo2", () => {
    expect(() => {new SequenciaSegmento(-12.4563)}).toThrow(TypeError);
});

test("testarSequenciaInvalido", () => {
    expect(() => {new SequenciaSegmento(25.001)}).toThrow(TypeError);
});

test("testarSequenciaInvalido2", () => {
    expect(() => {new SequenciaSegmento(21)}).toThrow(SyntaxError);
});

test("testarSequenciaInvalido3", () => {
    expect(() => {new SequenciaSegmento(0)}).toThrow(SyntaxError);
});

test("testarSequenciaPequenoValido", () => {
    expect(new SequenciaSegmento(1)).toBeInstanceOf(SequenciaSegmento);
});

test("testarSequenciaPequenoValido2", () => {
    expect(new SequenciaSegmento(13)).toBeInstanceOf(SequenciaSegmento);
});

test("testarSequenciaPequenoValido3", () => {
    expect(new SequenciaSegmento(20)).toBeInstanceOf(SequenciaSegmento);
});