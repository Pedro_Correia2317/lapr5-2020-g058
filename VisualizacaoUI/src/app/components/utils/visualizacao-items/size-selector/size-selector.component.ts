import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-size-selector',
  templateUrl: './size-selector.component.html',
  styleUrls: ['./size-selector.component.css']
})
export class SizeSelectorComponent implements OnInit {

  values = [3, 6, 24, 48];

  tamanhoSelecionado: number;

  @Output() selectedValue = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    this.tamanhoSelecionado = this.values[0];
    this.selectedValue.emit(this.tamanhoSelecionado);
  }

  onChange(event: any): void {
    const number = Number.parseInt(event.target.value.substring(3), 10);
    this.tamanhoSelecionado = number;
    this.selectedValue.emit(number);
  }

}
