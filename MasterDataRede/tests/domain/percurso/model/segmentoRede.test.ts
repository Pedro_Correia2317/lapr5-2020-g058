
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';
import { DistanciaSegmento } from '../../../../src/domain/percurso/model/DistanciaSegmento';
import { DuracaoSegmento } from '../../../../src/domain/percurso/model/DuracaoSegmento';
import { SegmentoRede } from '../../../../src/domain/percurso/model/SegmentoRede';
import { SequenciaSegmento } from '../../../../src/domain/percurso/model/SequenciaSegmento';


test("testarAbreviaturaInicioNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(null, codigoNoFim, sequencia, distancia, duracao)}).toThrow(SyntaxError);
});

test("testarAbreviaturaFimNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, null, sequencia, distancia, duracao)}).toThrow(SyntaxError);
});

test("testarSequenciaNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, codigoNoFim, null, distancia, duracao)}).toThrow(SyntaxError);
});

test("testarDistanciaNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, null, duracao)}).toThrow(SyntaxError);
});

test("testarDuracaoNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, null)}).toThrow(SyntaxError);
});

test("testarAbreviaturaInicioIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(undefined, codigoNoFim, sequencia, distancia, duracao)}).toThrow(SyntaxError);
});

test("testarAbreviaturaFimIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, undefined, sequencia, distancia, duracao)}).toThrow(SyntaxError);
});

test("testarSequenciaIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, codigoNoFim, undefined, distancia, duracao)}).toThrow(SyntaxError);
});

test("testarDistanciaIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, undefined, duracao)}).toThrow(SyntaxError);
});

test("testarDuracaoIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, undefined)}).toThrow(SyntaxError);
});

test("testarValoresValidos", () => {
    const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
    const codigoNoFim = new AbreviaturaNo('12345678901234567890');
    const sequencia = new SequenciaSegmento(1);
    const distancia = new DistanciaSegmento(12.4567);
    const duracao = new DuracaoSegmento(3457);
    expect(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao)).toBeInstanceOf(SegmentoRede);
});