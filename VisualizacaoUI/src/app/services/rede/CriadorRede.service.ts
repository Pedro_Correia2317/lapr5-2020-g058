
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { AcessoPercursosService } from './AcessoPercursos.service';
import { AcessoNosService } from './AcessoNos.service';
import { AcessoLinhasService } from './AcessoLinhas.service';
import { NoDTO } from 'src/app/models/no/NoDTO';
import { LinhaDTO } from 'src/app/models/linhas/LinhaDTO';
import { SegmentoRedeDTO } from 'src/app/models/percursos/SegmentoRedeDTO';
import { PercursoDTO } from 'src/app/models/percursos/PercursoDTO';

@Injectable({
  providedIn: 'root'
})
export class CriadorRede {

    private nos: NoDTO[];

    constructor(private todosNos: AcessoNosService,
                private todosPerc: AcessoPercursosService,
                private todasLinhas: AcessoLinhasService) {
        this.nos = [];
    }

    procurarTodosNos(): Observable<number> {
        return new Observable<number>(observer => {
            this.todosNos.procurarNos(1, 48).subscribe((resultados: DetalhesPesquisaDTO<NoDTO>) => {
              if(!resultados.sucesso){
                observer.error(resultados.descricao);
              } else {
                this.nos = resultados.lista;
                observer.next(resultados.lista.length);
              }
            });
        });
    }

    procurarTodosSegmentos(): Observable<SegmentoAuxiliar[]> {
        return new Observable<SegmentoAuxiliar[]>(observer => {
            this.todasLinhas.procurarLinhas(1, 48).subscribe((resultados: DetalhesPesquisaDTO<LinhaDTO>) => {
                if(!resultados.sucesso){
                    observer.error(resultados.descricao);
                } else {
                    observer.next(this.extrairPercursosLinhas(resultados.lista));
                }
            });
        });
    }

    private extrairPercursosLinhas(linhas: LinhaDTO[]): SegmentoAuxiliar[] {
        const segmentos: SegmentoAuxiliar[] = [];
        let index = 1;
        for(const linha of linhas){
            this.todasLinhas.procurarPercursosLinha(linha.codigo).subscribe(function (this: CriadorRede, indexLinha: number, matrizPercursos: DetalhesPesquisaDTO<PercursoDTO[]>) {
                for(const ind in matrizPercursos.lista){
                    for(const perc of matrizPercursos.lista[ind]){
                        this.todosPerc.procurarSegmentosPercurso(perc.codigo).subscribe(function (this: CriadorRede, indexLinha: number, indexP: number,  resultados: DetalhesPesquisaDTO<SegmentoRedeDTO>) {
                          for(const segmento of resultados.lista){
                            const index1 = this.nos.findIndex(value => value.abreviatura === segmento.abreviaturaNoInicio);
                            const index2 = this.nos.findIndex(value => value.abreviatura === segmento.abreviaturaNoFim);
                            const sobreposicao = segmentos.filter(seg => (seg.index1 === index1 && seg.index2 === index2 || seg.index1 === index2 && seg.index2 === index1)).length;
                            segmentos.push(new SegmentoAuxiliar(index1, index2, indexLinha, sobreposicao, indexP));
                          }
                        }.bind(this, indexLinha, Number.parseInt(ind)));
                    }
                }
            }.bind(this, index));
            index++;
        }
        return segmentos;
    }

    latNo(index: number): number {
        return this.nos[index].latitude;
    }

    lonNo(index: number): number {
        return this.nos[index].longitude;
    }
}

export class SegmentoAuxiliar {

    public index1;
  
    public index2;
  
    public indexlinha;

    public indexTipoPercurso;

    public sobreposicao;
  
    constructor(index1: number, index2: number, indexLinha: number, sobreposicao: number, indexTipoPercurso: number){
      this.index1 = index1;
      this.index2 = index2;
      this.indexlinha = indexLinha;
      this.sobreposicao = sobreposicao;
      this.indexTipoPercurso = indexTipoPercurso;
    }
  }