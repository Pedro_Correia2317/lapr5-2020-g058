
import { PercursoBuilder } from "../../../src/application/percurso/services/PercursoBuilder";
import { Percurso } from "../../../src/domain/percurso/model/Percurso";
import { SegmentoRede } from "../../../src/domain/percurso/model/SegmentoRede";

export class PercursoBuilderMockSucesso implements PercursoBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {}

    aplicarSegmento(segmento: SegmentoRede): void {}

    aplicarNovaAbreviaturaNoInicial(abreviatura: string): void {}

    aplicarNovaAbreviaturaNoFinal(abreviatura: string): void {}
    
    aplicarNovaDistancia(distancia: number): void {}

    aplicarNovaDuracao(duracao: number): void {}

    obterResultado(): Percurso {
        return null;
    }

}

export class PercursoBuilderMockErro implements PercursoBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarSegmento(segmento: SegmentoRede): void {
        throw new SyntaxError('Teste');
    }

    aplicarNovaAbreviaturaNoInicial(abreviatura: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarNovaAbreviaturaNoFinal(abreviatura: string): void {
        throw new SyntaxError('Teste');
    }
    
    aplicarNovaDistancia(distancia: number): void {
        throw new SyntaxError('Teste');
    }

    aplicarNovaDuracao(duracao: number): void {
        throw new SyntaxError('Teste');
    }

    obterResultado(): Percurso {
        throw new SyntaxError('Teste');
    }

}