
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';

import { TempoDeslocacaoDTO } from 'src/app/models/no/NoDTO';
import { PedidoCriarNoDTO } from 'src/app/models/no/PedidoNoDTO';
import { TipoNoDTO } from 'src/app/models/no/TipoNoDTO';
import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { AcessoNosService } from 'src/app/services/rede/AcessoNos.service';
import { CriacaoTemposComponent } from './criacao-tempos/criacao-tempos.component';

@Component({
  selector: 'criacao-nos',
  templateUrl: './criacao-nos.component.html',
  styleUrls: ['./criacao-nos.component.css']
})
export class CriacaoNosComponent implements OnInit {

  formNoCriacao: any;

  tiposNos: TipoNoDTO[];

  tipoNoSelecionado: TipoNoDTO;

  temposSelecionados: TempoDeslocacaoDTO[];

  constructor(private todosNos: AcessoNosService,
              private builder: FormBuilder,
              private dialog: MatDialog,
              private translateService: TranslateService) {
    this.formNoCriacao = this.builder.group({
    nome: '',
    abreviatura: '',
    latitude: 0,
    longitude: 0,
    capacidade: 0
  });
  this.temposSelecionados = [];
  }

  ngOnInit() {
    this.todosNos.procurarTiposNos().subscribe((tiposNos: DetalhesPesquisaDTO<TipoNoDTO>) => {
      this.tiposNos = tiposNos.lista;
    });
  }

  onSubmit(dadosFormulario: any) {
    const dto = new PedidoCriarNoDTO();
    dto.abreviatura = dadosFormulario.abreviatura;
    dto.nome = dadosFormulario.nome;
    dto.lat = dadosFormulario.latitude;
    dto.lon = dadosFormulario.longitude;
    dto.capacidade = dadosFormulario.capacidade;
    dto.tipoNo = this.tipoNoSelecionado.nome;
    dto.tempos = this.temposSelecionados;
    this.todosNos.enviarNo(dto).subscribe((resultados: DetalhesOperacaoDTO) => {
      alert(resultados.sucesso + ' na operação: ' + this.translateService.instant(resultados.mensagem));
    });
  }

  onChange(event: any): void {
    this.tipoNoSelecionado = this.tiposNos[event];
    if(!this.tipoNoSelecionado.temTempos){
      this.temposSelecionados = [];
    }
  }

  eliminarTempo(event: TempoDeslocacaoDTO){
    const index = this.temposSelecionados.indexOf(event, 0);
    if(index > -1){
      this.temposSelecionados.splice(index, 1);
    }
  }

  openDialog(){
    const dialogRef = this.dialog.open(CriacaoTemposComponent, {
      maxWidth: '90vw',
      width: '90vw'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.temposSelecionados.push(result);
      }
    });
  }

}