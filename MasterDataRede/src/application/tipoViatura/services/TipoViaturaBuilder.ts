
import {TipoViatura} from '../../../domain/tipoViatura/model/TipoViatura';

export interface TipoViaturaBuilder {

    aplicarCodigo(codigo: string): void;

    aplicarDescricao(descricao: string): void;

    aplicarAutonomia(autonomia: number): void;

    aplicarCusto(custo: number): void;

    aplicarVelocidadeMedia(velocidade: number): void;

    aplicarConsumo(consumo: number): void;

    aplicarEmissoes(emissoes: number): void;

    aplicarTipoCombustivel(tipoCombustivel: string): void;

    obterResultado(): TipoViatura;

    reset(): void;

}