
export class DuracaoTotalPercurso {

    private duracao: number;

    constructor(duracaoSeg: number){
        if (!Number.isInteger(duracaoSeg)){
            throw new TypeError(`The duration is not a integer: ${duracaoSeg}`);
        }
        if (duracaoSeg <= 0 || duracaoSeg > 21600){ // 6 horas
            throw new SyntaxError(`The duration ${duracaoSeg} is <= 0 or > 21600`);
        }
        this.duracao = duracaoSeg;
    }

    juntarDuracao(duracao: number): DuracaoTotalPercurso {
        return new DuracaoTotalPercurso(this.duracao + duracao);
    }

    getValue(): number {
        return this.duracao;
    }

    toString(): string {
        return `DuracaoTotalPercurso: [${this.duracao}]`;
    }
}