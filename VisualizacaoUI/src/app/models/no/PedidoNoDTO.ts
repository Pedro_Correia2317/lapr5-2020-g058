

export class PedidoCriarNoDTO {

    public abreviatura: string;

    public nome: string;

    public lat: number;

    public lon: number;

    public capacidade: number;

    public tipoNo: string;

    public tempos: PedidoTempoDeslocacaoDTO[];

}

export class PedidoTempoDeslocacaoDTO {

    public tempo: number;
    
    public abreviaturaNo: string;
}