
import { CodigoTipoTripulante } from "../../../../src/domain/tipoTripulante/model/CodigoTipoTripulante";
import { DescricaoTipoTripulante } from "../../../../src/domain/tipoTripulante/model/DescricaoTipoTripulante";
import { TipoTripulante } from "../../../../src/domain/tipoTripulante/model/TipoTripulante";

test("testarCodigoNulo", () => {
    expect(() => {
        const codigoTipoT = new CodigoTipoTripulante('12345678901234567890');
        const descricaoTipoT = new DescricaoTipoTripulante('12345678901234567890');
        new TipoTripulante(null, descricaoTipoT)}).toThrow(SyntaxError);
});

test("testarCodigoIndefinido", () => {
    expect(() => {
        const codigoTipoT = new CodigoTipoTripulante('12345678901234567890');
        const descricaoTipoT = new DescricaoTipoTripulante('12345678901234567890');
        new TipoTripulante(undefined, descricaoTipoT)}).toThrow(SyntaxError);
});

test("testarDescricaoNulo", () => {
    expect(() => {
        const codigoTipoT = new CodigoTipoTripulante('12345678901234567890');
        const descricaoTipoT = new DescricaoTipoTripulante('12345678901234567890');
        new TipoTripulante(codigoTipoT, null)}).toThrow(SyntaxError);
});

test("testarDescricaoIndefinido", () => {
    expect(() => {
        const codigoTipoT = new CodigoTipoTripulante('12345678901234567890');
        const descricaoTipoT = new DescricaoTipoTripulante('12345678901234567890');
        new TipoTripulante(codigoTipoT, undefined)}).toThrow(SyntaxError);
});

test("testarPercursoApenasUmSegmento", () => {
    const codigoTipoT = new CodigoTipoTripulante('12345678901234567890');
    const descricaoTipoT = new DescricaoTipoTripulante('12345678901234567890');
    expect(new TipoTripulante(codigoTipoT, descricaoTipoT)).toBeInstanceOf(TipoTripulante);
});