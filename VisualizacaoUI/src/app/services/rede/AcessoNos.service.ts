
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PedidoCriarNoDTO } from 'src/app/models/no/PedidoNoDTO';
import { TipoNoDTO } from 'src/app/models/no/TipoNoDTO';
import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { NoDTO } from '../../models/no/NoDTO';
import { DetalhesPesquisaDTO } from '../../models/pedidos/DetalhesPesquisaDTO';
import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AcessoNosService {

  private enderecoHttp: string = `${Configurations.API_SERVER_ADDRESS}/nos`;

  private enderecoHttpTiposNos: string = `${Configurations.API_SERVER_ADDRESS}/tiposNos`;

  constructor(private http: HttpClient) { }

  procurarNos(pagina: number, tamanho: number, sort?: string, nome?: string): Observable<DetalhesPesquisaDTO<NoDTO>> {
    const aux: string = sort? sort : '';
    const nomeAux: string = nome? nome : '';
    return this.http.get<DetalhesPesquisaDTO<NoDTO>>(this.enderecoHttp, { 
        observe: 'body',
        responseType: 'json',
        params: {
          page: '' + pagina,
          size: '' + tamanho,
          sort: aux,
          nome: nomeAux
        }
      });
  }

  enviarNo(pedido: PedidoCriarNoDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }

  procurarTiposNos(): Observable<DetalhesPesquisaDTO<TipoNoDTO>> {
    return this.http.get<DetalhesPesquisaDTO<TipoNoDTO>>(this.enderecoHttpTiposNos, {
      observe: 'body',
      responseType: 'json'
    });
  }
}
