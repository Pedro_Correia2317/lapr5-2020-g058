
import { LinhaDTO } from "../../../domain/linha/dto/LinhaDTO";
import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaBuilder } from "../services/LinhaBuilder";
import { ListarLinhasUseCase } from "./ListarLinhasUseCase";

export class ListarLinhasUseCaseImpl implements ListarLinhasUseCase {

    async obterLinhas(builder: LinhaBuilder, logger: ServicoLogger, 
                        linhaRepo: LinhaRepository, sort: string, numPag: number, 
                        tamPag: number, codigo: string, nome: string): Promise<DetalhesPesquisaDTO<LinhaDTO>> {
        const resultados = new DetalhesPesquisaDTO<LinhaDTO>();
        await linhaRepo.obterLinhas(builder, sort, numPag, tamPag, codigo, nome).then((linhas) => {
            linhas.forEach((linha) => {
                resultados.lista.push(linha.toBaseDTO());
            });
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findLines.errorFindingLines';
        });
        await linhaRepo.countComFiltros(codigo, nome).then((numero) => {
            resultados.count = numero;
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findLines.errorCountingLines';
        });
        if (!resultados.descricao){
            resultados.sucesso = true;
            resultados.descricao = 'findLines.searchSuccess';
        }
        return resultados;
    }

}