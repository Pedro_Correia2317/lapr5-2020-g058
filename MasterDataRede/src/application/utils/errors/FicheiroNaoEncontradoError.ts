
export class FicheiroNaoEncontradoError extends Error {

    constructor(m: string){
        super(m);
        Object.setPrototypeOf(this, FicheiroNaoEncontradoError.prototype);
    }
}