

import { CodigoPercurso } from "../../../domain/percurso/model/CodigoPercurso";
import { DescricaoPercurso } from "../../../domain/percurso/model/DescricaoPercurso";
import { DistanciaTotalPercurso } from "../../../domain/percurso/model/DistanciaTotalPercurso";
import { DuracaoTotalPercurso } from "../../../domain/percurso/model/DuracaoTotalPercurso";
import { Percurso } from "../../../domain/percurso/model/Percurso";
import { SegmentoRede } from "../../../domain/percurso/model/SegmentoRede";
import { PercursoBuilder } from "./PercursoBuilder";

export class PercursoBuilderImpl implements PercursoBuilder {

    private codigo: CodigoPercurso;
    private descricao: DescricaoPercurso;
    private distancia: DistanciaTotalPercurso;
    private duracao: DuracaoTotalPercurso;
    private segmentos: SegmentoRede[];

    constructor(){
        this.reset();
    }

    reset(): void {
        this.segmentos = [];
        this.descricao = null;
        this.distancia = null;
        this.duracao = null;
        this.codigo = null;
    }

    aplicarCodigo(codigo: string): void {
        this.codigo = new CodigoPercurso(codigo);
    }

    aplicarSegmento(segmento: SegmentoRede): void {
        this.segmentos.push(segmento);
    }

    aplicarNovaAbreviaturaNoInicial(abreviatura: string): void {
        if (this.descricao === null){
            this.descricao = new DescricaoPercurso(abreviatura);
        }
    }

    aplicarNovaAbreviaturaNoFinal(abreviatura: string): void {
        if (this.descricao !== null){
            this.descricao = this.descricao.juntarAbreviatura(abreviatura);
        }
    }

    aplicarNovaDistancia(distancia: number): void {
        if (this.distancia === null){
            this.distancia = new DistanciaTotalPercurso(distancia);
        } else {
            this.distancia = this.distancia.juntarDistancia(distancia);
        }
    }

    aplicarNovaDuracao(duracao: number): void {
        if (this.duracao === null){
            this.duracao = new DuracaoTotalPercurso(duracao);
        } else {
            this.duracao = this.duracao.juntarDuracao(duracao);
        }
    }

    obterResultado(): Percurso {
        return new Percurso(
            this.codigo, this.segmentos,
            this.descricao, this.distancia, this.duracao
            );
    }

}