
export class CodigoTipoViatura {

    private codigo: string;

    constructor(codigo: string){
        if (codigo === null || typeof codigo === 'undefined'){
            throw new ReferenceError('The inserted id is null');
        }
        const trimmedId = codigo.trim();
        if (!(/^[\w\d:]{1,20}$/u).test(trimmedId)){
            throw new SyntaxError(`Id ${trimmedId} does not obey the rules`);
        }
        this.codigo = trimmedId;
    }

    getValue(): string {
        return this.codigo;
    }

    toString(): string {
        return `CodigoTipoViatura: [${this.codigo}]`;
    }

}