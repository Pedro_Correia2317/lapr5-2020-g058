

import { AbreviaturaNo } from "../../../domain/no/model/AbreviaturaNo";
import { CoordenadasNo } from "../../../domain/no/model/CoordenadasNo";
import { NomeNo } from "../../../domain/no/model/NomeNo";
import { NoPontoRendicao } from "../../../domain/no/model/NoPontoRendicao";
import { TempoDeslocacaoTripulacao } from "../../../domain/no/model/TempoDeslocacaoTripulacao";
import { NoPontoRendicaoBuilder } from "./NoPontoRendicaoBuilder";

export class NoPontoRendicaoBuilderImpl implements NoPontoRendicaoBuilder {

    private nome: NomeNo;
    private abreviatura: AbreviaturaNo;
    private coordenadas: CoordenadasNo;
    private tempos: TempoDeslocacaoTripulacao[];

    constructor(){
        this.reset();
    }

    reset(): void {
        this.nome = null;
        this.abreviatura = null;
        this.coordenadas = null;
        this.tempos = [];
    }

    isNoIgual(abreviatura: string): boolean {
        return this.abreviatura.equals(abreviatura);
    }

    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void {
        const abr = new AbreviaturaNo(abreviatura);
        this.tempos.push(new TempoDeslocacaoTripulacao(tempo, abr));
    }

    obterResultado(): NoPontoRendicao {
        return new NoPontoRendicao(this.abreviatura, this.nome, this.coordenadas, this.tempos);
    }

    aplicarNome(nome: string): void {
        this.nome = new NomeNo(nome);
    }

    aplicarAbreviatura(abreviatura: string): void {
        this.abreviatura = new AbreviaturaNo(abreviatura);
    }

    aplicarCoordenadas(lat: number, lon: number): void {
        this.coordenadas = new CoordenadasNo(lat, lon);
    }
    
}