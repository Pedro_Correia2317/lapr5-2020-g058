
import { CustoTipoViatura } from '../../../../src/domain/tipoViatura/model/CustoTipoViatura';

test("testarCustoTipoViaturaNulo", () => {
    expect(() => {new CustoTipoViatura(null)}).toThrow(TypeError);
    expect(() => {new CustoTipoViatura(undefined)}).toThrow(TypeError);
});

test("testarCustoNegativo", () => {
    expect(() => {new CustoTipoViatura(-1)}).toThrow(SyntaxError);
});

test("testarCustoNegativo2", () => {
    expect(() => {new CustoTipoViatura(-12.4563)}).toThrow(SyntaxError);
});

test("testarCustoInvalido", () => {
    expect(() => {new CustoTipoViatura(99.001)}).toThrow(SyntaxError);
});

test("testarCustoInvalido2", () => {
    expect(() => {new CustoTipoViatura(19373.24567)}).toThrow(SyntaxError);
});

test("testarCustoPequenoValido", () => {
    expect(new CustoTipoViatura(1)).toBeInstanceOf(CustoTipoViatura);
});

test("testarCustoPequenoValido2", () => {
    expect(new CustoTipoViatura(99)).toBeInstanceOf(CustoTipoViatura);
});

test("testarCustoPequenoValido3", () => {
    expect(new CustoTipoViatura(0.4828)).toBeInstanceOf(CustoTipoViatura);
});

test("testarCustoValido4", () => {
    expect(new CustoTipoViatura(98.9999)).toBeInstanceOf(CustoTipoViatura);
});

test("testarCustoValido5", () => {
    expect(new CustoTipoViatura(5)).toBeInstanceOf(CustoTipoViatura);
});

test("testarCustoValido6", () => {
    expect(new CustoTipoViatura(99.000001)).toBeInstanceOf(CustoTipoViatura);
});