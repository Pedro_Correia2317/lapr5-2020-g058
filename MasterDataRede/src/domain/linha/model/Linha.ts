

import { LinhaDTO } from "../dto/LinhaDTO";
import { NecessidadeTipoTripulanteDTO } from "../dto/NecessidadeTipoTripulanteDTO";
import { NecessidadeTipoViaturaDTO } from "../dto/NecessidadeTipoViaturaDTO";
import { PercursoLinhaDTO } from "../dto/PercursoLinhaDTO";
import { CodigoLinha } from "./CodigoLinha";
import { NecessidadesTipoTripulante } from "./NecessidadesTipoTripulante";
import { NecessidadesTipoViatura } from "./NecessidadesTipoViatura";
import { NomeLinha } from "./NomeLinha";
import { PercursosLinha } from "./PercursosLinha";

export class Linha {

    private codigo: CodigoLinha;

    private nome: NomeLinha;

    private percursosLinha: PercursosLinha;

    private necessidadesTipoViaturas: NecessidadesTipoViatura;

    private necessidadesTipoTripulantes: NecessidadesTipoTripulante;

    constructor(codigo: CodigoLinha, nome: NomeLinha, pLinha: PercursosLinha,
            nTiposV: NecessidadesTipoViatura, nTiposT: NecessidadesTipoTripulante){
        if(codigo == null || nome == null || pLinha == null || nTiposV == null || nTiposT == null){
            throw new ReferenceError('There are null values');
        }
        this.codigo = codigo;
        this.nome = nome;
        this.percursosLinha = pLinha;
        this.necessidadesTipoViaturas = nTiposV;
        this.necessidadesTipoTripulantes = nTiposT;
    }

    toBaseDTO(): LinhaDTO {
        const dto = new LinhaDTO();
        dto.codigo = this.codigo.getValue();
        dto.nome = this.nome.getValue();
        return dto;
    }

    toPercursosIdaDTO(): PercursoLinhaDTO[] {
        return this.percursosLinha.toPercursosIdaDTO();
    }

    toPercursosVoltaDTO(): PercursoLinhaDTO[] {
        return this.percursosLinha.toPercursosVoltaDTO();
    }
    
    toPercursosReforcoDTO(): PercursoLinhaDTO[] {
        return this.percursosLinha.toPercursosReforcoDTO();
    }
    
    toPercursosVazioDTO(): PercursoLinhaDTO[] {
        return this.percursosLinha.toPercursosVazioDTO();
    }
    
    toNecessidadesTipoViaturaDTO(): NecessidadeTipoViaturaDTO {
        return this.necessidadesTipoViaturas.toNecessidadesTipoViaturaDTO();
    }

    toNecessidadesTipoTripulanteDTO(): NecessidadeTipoTripulanteDTO {
        return this.necessidadesTipoTripulantes.toNecessidadesTipoTripulanteDTO();
    }

}