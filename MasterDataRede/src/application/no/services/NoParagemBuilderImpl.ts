

import { AbreviaturaNo } from "../../../domain/no/model/AbreviaturaNo";
import { CoordenadasNo } from "../../../domain/no/model/CoordenadasNo";
import { NomeNo } from "../../../domain/no/model/NomeNo";
import { NoParagem } from "../../../domain/no/model/NoParagem";
import { NoParagemBuilder } from "./NoParagemBuilder";

export class NoParagemBuilderImpl implements NoParagemBuilder {

    private nome: NomeNo;
    private abreviatura: AbreviaturaNo;
    private coordenadas: CoordenadasNo;

    constructor(){
        this.reset();
    }

    reset(): void {
        this.nome = null;
        this.abreviatura = null;
        this.coordenadas = null;
    }

    obterResultado(): NoParagem {
        return new NoParagem(this.abreviatura, this.nome, this.coordenadas);
    }

    aplicarNome(nome: string): void {
        this.nome = new NomeNo(nome);
    }

    aplicarAbreviatura(abreviatura: string): void {
        this.abreviatura = new AbreviaturaNo(abreviatura);
    }

    aplicarCoordenadas(lat: number, lon: number): void {
        this.coordenadas = new CoordenadasNo(lat, lon);
    }
    
}