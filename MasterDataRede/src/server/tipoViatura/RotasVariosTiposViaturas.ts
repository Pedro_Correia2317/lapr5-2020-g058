
import { Request, Response, Router } from "express";
import Container, { Inject, Service } from "typedi";
import { CriarTipoViaturaUseCaseDirector } from "../../application/tipoViatura/director/CriarTipoViaturaUseCaseDirector";
import { TipoViaturaBuilder } from "../../application/tipoViatura/services/TipoViaturaBuilder";
import { ListarTiposViaturasUseCase } from "../../application/tipoViatura/usecases/ListarTiposViaturasUseCase";
import { ServicoLogger } from "../../application/utils/services/ServicoLogger";
import { TipoViaturaRepository } from "../../repository/interfaces/TipoViaturaRepository";

@Service()
export class RotasVariosTiposViatura {

    @Inject("PrimaryCriarTipoViaturaUseCaseDirector")
    private criarDirector: CriarTipoViaturaUseCaseDirector;

    @Inject("PrimaryListarTiposViaturasUseCase")
    private listarTiposVUseCase: ListarTiposViaturasUseCase;

    @Inject("PrimaryTipoViaturaBuilder")
    private builder: TipoViaturaBuilder;

    @Inject("PrimaryTipoViaturaRepository")
    private ttRepository: TipoViaturaRepository;

    @Inject("PrimaryServicoLogger")
    private servicoLogger: ServicoLogger;
    
    insertRoutes = (router: Router): void => {
        router.route('/tiposCombustiveis')
            .get(this.rotaListagemTiposCombustiveis);
        router.route('/tiposViaturas')
            .post(this.inserirRotaCriacaoTipoViatura)
            .get(this.rotaListagemTiposViaturas);
    }
    
    rotaListagemTiposCombustiveis = (req: Request, res: Response): void => {
        res.send({'lista': this.listarTiposVUseCase.obterTiposCombustiveis(), 'sucesso': true});
    }

    inserirRotaCriacaoTipoViatura = async (req: Request, res: Response): Promise<void> => {
        const resultado = await this.criarDirector.processarPedido(req.body, Container.get("PrimaryTipoViaturaBuilder"));
        res.send({'sucesso': resultado.sucesso, 'mensagem': resultado.mensagem});
    }

    rotaListagemTiposViaturas = async (req: Request, res: Response): Promise<void> => {
        let num = req.query.page? Number.parseInt(req.query.page.toString()) : 1;
        num = num <= 0? 1 : num;
        let tam = req.query.size? Number.parseInt(req.query.size.toString()) : 24;
        tam = tam <= 0? 24 : tam;
        const desc = req.query.descricao? req.query.descricao.toString() : '';
        const sort = req.query.sort? req.query.sort.toString(): '';
        const resultado = await this.listarTiposVUseCase.listarTiposViaturas(this.builder, this.servicoLogger, this.ttRepository, sort, num, tam, desc);
        res.send({'sucesso': resultado.sucesso, 'lista': resultado.lista, 'numeroTotal': resultado.count, 'mensagem': resultado.descricao});
    }
    
}