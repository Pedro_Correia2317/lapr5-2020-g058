
import { CodigoLinha } from "../../../domain/linha/model/CodigoLinha";
import { Linha } from "../../../domain/linha/model/Linha";
import { NecessidadesTipoTripulante } from "../../../domain/linha/model/NecessidadesTipoTripulante";
import { NecessidadesTipoViatura } from "../../../domain/linha/model/NecessidadesTipoViatura";
import { NomeLinha } from "../../../domain/linha/model/NomeLinha";
import { PercursosLinha } from "../../../domain/linha/model/PercursosLinha";
import { TipoNecessidade } from "../../../domain/linha/model/TipoNecessidade";
import { CodigoPercurso } from "../../../domain/percurso/model/CodigoPercurso";
import { CodigoTipoTripulante } from "../../../domain/tipoTripulante/model/CodigoTipoTripulante";
import { CodigoTipoViatura } from "../../../domain/tipoViatura/model/CodigoTipoViatura";
import { LinhaBuilder } from "./LinhaBuilder";

export class LinhaBuilderImpl implements LinhaBuilder {

    private codigo: CodigoLinha;
    private nome: NomeLinha;
    private percursosIda: CodigoPercurso[];
    private percursosVolta: CodigoPercurso[];
    private percursosReforco: CodigoPercurso[];
    private percursosVazio: CodigoPercurso[];
    private tiposTripulantes: CodigoTipoTripulante[];
    private tiposViaturas: CodigoTipoViatura[];
    private tipoNecessidadeTipoViatura: TipoNecessidade;
    private tipoNecessidadeTipoTripulante: TipoNecessidade;

    constructor(){
        this.reset();
    }

    reset(): void{
        this.codigo = null;
        this.nome = null;
        this.percursosIda = [];
        this.percursosVolta = [];
        this.percursosReforco = [];
        this.percursosVazio = [];
        this.tiposTripulantes = [];
        this.tiposViaturas = [];
        this.tipoNecessidadeTipoViatura = null;
        this.tipoNecessidadeTipoTripulante = null;
    }

    aplicarCodigo(codigo: string): void {
        this.codigo = new CodigoLinha(codigo);
    }

    aplicarNome(nome: string): void {
        this.nome = new NomeLinha(nome);
    }

    adicionarPercursoIda(codigoPercurso: string): void {
        this.percursosIda.push(new CodigoPercurso(codigoPercurso));
    }

    adicionarPercursoVolta(codigoPercurso: string): void {
        this.percursosVolta.push(new CodigoPercurso(codigoPercurso));
    }

    adicionarPercursoReforco(codigoPercurso: string): void {
        this.percursosReforco.push(new CodigoPercurso(codigoPercurso));
    }

    adicionarPercursoVazio(codigoPercurso: string): void {
        this.percursosVazio.push(new CodigoPercurso(codigoPercurso));
    }

    adicionarTipoViatura(codigoTipoViatura: string): void {
        this.tiposViaturas.push(new CodigoTipoViatura(codigoTipoViatura));
    }

    adicionarTipoTripulante(codigoTipoTripulante: string): void {
        this.tiposTripulantes.push(new CodigoTipoTripulante(codigoTipoTripulante));
    }

    adicionarTipoNecessidadeTipoViatura(tipo: string): void {
        this.tipoNecessidadeTipoViatura = TipoNecessidade.valor(tipo);
    }

    adicionarTipoNecessidadeTipoTripulante(tipo: string): void {
        this.tipoNecessidadeTipoTripulante = TipoNecessidade.valor(tipo);
    }

    obterResultado(): Linha {
        const percLinha = new PercursosLinha(this.percursosIda, this.percursosVolta, this.percursosReforco, this.percursosVazio);
        let aux = this.tiposViaturas.length == 0? TipoNecessidade.NAO_APLICAVEL : this.tipoNecessidadeTipoViatura;
        const nTipoV = new NecessidadesTipoViatura(this.tiposViaturas, aux);
        aux = this.tiposTripulantes.length == 0? TipoNecessidade.NAO_APLICAVEL : this.tipoNecessidadeTipoTripulante;
        const nTipoT = new NecessidadesTipoTripulante(this.tiposTripulantes, aux);
        return new Linha(this.codigo, this.nome, percLinha, nTipoV, nTipoT);
    }

}