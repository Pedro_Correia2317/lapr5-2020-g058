

import { ImportadorDadosFicheiro } from "../../../src/application/importacao/services/ImportadorDadosFicheiro";
import { ImportadorDadosFicheiroContextImpl } from "../../../src/application/importacao/services/ImportadorDadosFicheiroContextImpl";
import { ImportadorDadosFicheiroMockSucesso } from "../../mocks/importacao/ImportadorDadosFicheiroMock";

const mapaImportadores: Map<string, ImportadorDadosFicheiro> = new Map();
mapaImportadores.set('.teste', new ImportadorDadosFicheiroMockSucesso());
const contexto = new ImportadorDadosFicheiroContextImpl(mapaImportadores);

test('testarFicheiroComExtensaoDesconhecida', () => {
    expect(contexto.obterImportadorAdequado('ficheiro.txt') == null).toBe(true);
});

test('testarFicheiroComExtensaoConhecida', () => {
    expect(contexto.obterImportadorAdequado('ficheiro.teste') == null).toBe(false);
});