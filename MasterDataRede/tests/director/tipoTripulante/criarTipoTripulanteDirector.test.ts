
import { CriarTipoTripulanteUseCaseDirector } from '../../../src/application/tipoTripulante/director/CriarTipoTripulanteUseCaseDirector';
import { TipoTripulanteBuilderImpl } from '../../../src/application/tipoTripulante/services/TipoTripulanteBuilderImpl';
import { CriarTipoTripulanteUseCaseImpl } from '../../../src/application/tipoTripulante/usecases/CriarTipoTripulanteUseCaseImpl';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { TipoTripulanteRepositoryMockFalse, TipoTripulanteRepositoryMockTrue } from '../../mocks/tipoTripulante/tipoTripulanteRepositoryMock';
import { PedidoCriarTipoTripulanteDTO } from '../../../src/application/tipoTripulante/dto/PedidoCriarTipoTripulanteDTO';

const loggerMock = new ServicoLoggerMock();
const ttUseCase = new CriarTipoTripulanteUseCaseImpl();
const ttRepoTrue = new TipoTripulanteRepositoryMockTrue();
const ttRepoFalse = new TipoTripulanteRepositoryMockFalse();

test('testarPedidoValidoComValoresNaoRepetidos', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    pedido.codigo = 'EGF6I49HRTI0G7N1UP3Q';
    pedido.descricao = 'Motoristas Suiços';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(true);
});

test('testarPedidoComCodigoInvalido', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    pedido.codigo = 'SouInvalido...';
    pedido.descricao = 'Motoristas Suiços';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(false);
});

test('testarPedidoComDescricaoInvalida', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    pedido.codigo = 'EGF6I49HRTI0G7N1UP3Q';
    pedido.descricao = '?';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComCodigoRepetido', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    pedido.codigo = 'EGF6I49HRTI0G7N1UP3Q';
    pedido.descricao = 'Motoristas Suiços';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoFalse, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(false);
});

test('testarPedidoComCodigoFalta', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    // pedido.codigo = 'EGF6I49HRTI0G7N1UP3Q';
    pedido.descricao = 'Motoristas Suiços';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(false);
});

test('testarPedidoComDescricaoFalta', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    pedido.codigo = 'EGF6I49HRTI0G7N1UP3Q';
    // pedido.descricao = 'Motoristas Suiços';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemDados', async () => {
    const pedido: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
    // pedido.codigo = 'EGF6I49HRTI0G7N1UP3Q';
    // pedido.descricao = 'Motoristas Suiços';
    const ttBuilder = new TipoTripulanteBuilderImpl();
    const director = new CriarTipoTripulanteUseCaseDirector(ttUseCase, ttRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, ttBuilder)).sucesso).toBe(false);
});