
import {NoRepository} from '../../../repository/interfaces/NoRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {NoBuilder} from '../services/NoBuilder';

export interface CriarNoUseCase {

    aplicarAbreviatura(noBuilder: NoBuilder, logger: ServicoLogger, todosNos: NoRepository, abreviatura: string): Promise<DetalhesOperacaoDTO>;

    aplicarNome(noBuilder: NoBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO;

    aplicarCoordenadas(noBuilder: NoBuilder, logger: ServicoLogger, lat: number, lon: number): DetalhesOperacaoDTO;

    registarNo(noBuilder: NoBuilder, logger: ServicoLogger, todosNos: NoRepository): Promise<DetalhesOperacaoDTO>;
}