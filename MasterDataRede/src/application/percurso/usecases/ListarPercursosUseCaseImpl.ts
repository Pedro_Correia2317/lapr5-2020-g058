
import { PercursoDTO } from "../../../domain/percurso/dto/PercursoDTO";
import { SegmentoRedeDTO } from "../../../domain/percurso/dto/SegmentoRedeDTO";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { PercursoBuilder } from "../services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../services/SegmentoRedeBuilder";
import { ListarPercursosUseCase } from "./ListarPercursosUseCase";

export class ListarPercursosUseCaseImpl implements ListarPercursosUseCase {

    async obterSegmentosPercurso(builder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, logger: ServicoLogger, 
                    pRepo: PercursoRepository, codigo: string): Promise<DetalhesPesquisaDTO<SegmentoRedeDTO>> {
        const resultados = new DetalhesPesquisaDTO<SegmentoRedeDTO>();
        await pRepo.findByCodigo(builder, sBuilder, codigo).then((percurso) => {
            resultados.lista = percurso.toSegmentosDTO();
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findPath.errorFindingPath';
        });
        if (!resultados.descricao){
            resultados.sucesso = true;
            resultados.descricao = 'findPath.searchSuccess';
        }
        return resultados;
    }

    async listarPercursos(builder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, logger: ServicoLogger, 
                        pRepo: PercursoRepository, sort: string, numPag: number, 
                        tamPag: number, descricao: string): Promise<DetalhesPesquisaDTO<PercursoDTO>> {
        const resultados = new DetalhesPesquisaDTO<PercursoDTO>();
        await pRepo.obterPercursos(builder, sBuilder, sort, numPag, tamPag, descricao).then((percursos) => {
            percursos.forEach((perc) => {
                resultados.lista.push(perc.toBaseDTO());
            });
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findPaths.errorFindingPaths';
        });
        await pRepo.countComFiltros(descricao).then((numero) => {
            resultados.count = numero;
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findPaths.errorCountingPaths';
        });
        if (!resultados.descricao){
            resultados.sucesso = true;
            resultados.descricao = 'findPaths.searchSuccess';
        }
        return resultados;
    }
    
}