
import { TipoTripulanteDTO } from "../../../domain/tipoTripulante/dto/TipoTripulanteDTO";
import { TipoTripulanteRepository } from "../../../repository/interfaces/TipoTripulanteRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoTripulanteBuilder } from "../services/TipoTripulanteBuilder";
import { ListarTiposTripulantesUseCase } from "./ListarTiposTripulantesUseCase";

export class ListarTiposTripulantesUseCaseImpl implements ListarTiposTripulantesUseCase {

    async listarTiposTripulantes(builder: TipoTripulanteBuilder, logger: ServicoLogger, 
                            ttRepo: TipoTripulanteRepository, sort: string, numPag: number, 
                            tamPag: number, descricao: string): Promise<DetalhesPesquisaDTO<TipoTripulanteDTO>> {
        const resultados = new DetalhesPesquisaDTO<TipoTripulanteDTO>();
        await ttRepo.obterTiposTripulantes(builder, sort, numPag, tamPag, descricao).then((tiposT) => {
            tiposT.forEach((tipoT) => {
                resultados.lista.push(tipoT.toBaseDTO());
            });
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findDriverTypes.errorFindingDriverTypes';
        });
        await ttRepo.countComFiltros(descricao).then((numero) => {
            resultados.count = numero;
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findDriverTypes.errorCountingDriverTypes';
        });
        if (!resultados.descricao){
            resultados.sucesso = true;
            resultados.descricao = 'findDriverTypes.searchSuccess';
        }
        return resultados;
    }

}