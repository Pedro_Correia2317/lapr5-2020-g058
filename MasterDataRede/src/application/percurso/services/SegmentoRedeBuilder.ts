
import {SegmentoRede} from '../../../domain/percurso/model/SegmentoRede';

export interface SegmentoRedeBuilder {

    aplicarNoInicio(codigoNo: string): void;

    aplicarNoFim(codigoNo: string): void;

    aplicarDistancia(distancia: number): void;

    aplicarDuracao(duracao: number): void;

    aplicarSequencia(sequencia: number): void;

    aplicarSequenciaAutomatico(): void;

    obterResultado(): SegmentoRede;

    reset(): void;
}