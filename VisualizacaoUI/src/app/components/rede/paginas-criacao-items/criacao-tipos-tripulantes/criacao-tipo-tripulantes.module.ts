
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { CriacaoTiposTripulantesComponent } from './criacao-tipos-tripulantes.component';

const routes: Routes = [
    {path: '', component: CriacaoTiposTripulantesComponent}
];

@NgModule({
  declarations: [CriacaoTiposTripulantesComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriacaoTiposTripulantesModule { }
