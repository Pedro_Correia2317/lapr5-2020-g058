
import { LinhaBuilder } from '../../../src/application/linha/services/LinhaBuilder';
import { Linha } from '../../../src/domain/linha/model/Linha';

export class LinhaBuilderMockErro implements LinhaBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    aplicarNome(nome: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarPercursoIda(codigoPercurso: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarPercursoVolta(codigoPercurso: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarPercursoReforco(codigoPercurso: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarPercursoVazio(codigoPercurso: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarTipoViatura(codigoTipoViatura: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarTipoTripulante(codigoTipoTripulante: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarTipoNecessidadeTipoViatura(tipo: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    adicionarTipoNecessidadeTipoTripulante(tipo: string): void {
        throw new SyntaxError('Method not implemented.');
    }
    obterResultado(): Linha {
        throw new SyntaxError('Method not implemented.');
    }

}

export class LinhaBuilderMockSucesso implements LinhaBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {}

    aplicarNome(nome: string): void {}

    adicionarPercursoIda(codigoPercurso: string): void {}

    adicionarPercursoVolta(codigoPercurso: string): void {}

    adicionarPercursoReforco(codigoPercurso: string): void {}

    adicionarPercursoVazio(codigoPercurso: string): void {}

    adicionarTipoViatura(codigoTipoViatura: string): void {}

    adicionarTipoTripulante(codigoTipoTripulante: string): void {}

    adicionarTipoNecessidadeTipoViatura(tipo: string): void {}

    adicionarTipoNecessidadeTipoTripulante(tipo: string): void {}

    obterResultado(): Linha {
        return null;
    }

}