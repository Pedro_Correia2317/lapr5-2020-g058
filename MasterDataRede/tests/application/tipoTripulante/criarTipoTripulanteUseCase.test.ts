
import { CriarTipoTripulanteUseCaseImpl } from '../../../src/application/tipoTripulante/usecases/CriarTipoTripulanteUseCaseImpl';
import { TipoTripulanteRepositoryMockFalse, TipoTripulanteRepositoryMockTrue } from '../../mocks/tipoTripulante/tipoTripulanteRepositoryMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { TipoTripulanteBuilderMockErro, TipoTripulanteBuilderMockSucesso } from '../../mocks/tipoTripulante/tipoTripulanteBuilderMock';

const ttBuilderErro = new TipoTripulanteBuilderMockErro();
const ttBuilderSucesso = new TipoTripulanteBuilderMockSucesso();
const logger = new ServicoLoggerMock();
const ttRepoTrue = new TipoTripulanteRepositoryMockTrue();
const ttRepoFalse = new TipoTripulanteRepositoryMockFalse();
const useCase = new CriarTipoTripulanteUseCaseImpl();

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderErro, logger, ttRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoRepetido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderSucesso, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderErro, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoValido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderSucesso, logger, ttRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarDescricaoInvalido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderErro, logger, ttRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarDescricaoRepetido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderSucesso, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarDescricaoInvalido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderErro, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarDescricaoValido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderSucesso, logger, ttRepoTrue, "")).sucesso).toBe(true);
});

test("testarRegistoTipoTripulanteInvalido", async () => {
    expect((await useCase.registarTipoTripulante(ttBuilderErro, logger, ttRepoTrue)).sucesso).toBe(false);
});

test("testarRegistoTipoTripulanteErroAcessoBD", async () => {
    expect((await useCase.registarTipoTripulante(ttBuilderSucesso, logger, ttRepoFalse)).sucesso).toBe(false);
});

test("testarRegistoTipoTripulanteInvalido2", async () => {
    expect((await useCase.registarTipoTripulante(ttBuilderErro, logger, ttRepoFalse)).sucesso).toBe(false);
});

test("testarRegistoTipoTripulanteValido", async () => {
    expect((await useCase.registarTipoTripulante(ttBuilderSucesso, logger, ttRepoTrue)).sucesso).toBe(true);
});