

import { CodigoLinha } from '../../../../src/domain/linha/model/CodigoLinha';

test('testarCodigoNulo', () => {
    expect(() => {new CodigoLinha(null)}).toThrow(ReferenceError);
    expect(() => {new CodigoLinha(undefined)}).toThrow(ReferenceError);
});

test('testarCodigoVazio', () => {
    expect(() => {new CodigoLinha('')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoComprido', () => {
    expect(() => {new CodigoLinha('123456789012345678901')}).toThrow(SyntaxError); //21 chars
});

test('testarCodigoUsualComCarateresInvalidos', () => {
    expect(() => {new CodigoLinha('🌺ENF9229N2D9D2N3H6S🌺')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoPequeno', () => {
    expect(new CodigoLinha('A')).toBeInstanceOf(CodigoLinha);
});

test('testarCodigoPequenoValido', () => {
    expect(new CodigoLinha('1D020DN22D0ND0SKWI44')).toBeInstanceOf(CodigoLinha);
});

test('testarCodigoSoNumeros', () => {
    expect(new CodigoLinha('15367363211256232115')).toBeInstanceOf(CodigoLinha);
});

test('testarCodigoSoLetras', () => {
    expect(new CodigoLinha('IBIEBIFEIENIENIEFEFN')).toBeInstanceOf(CodigoLinha);
});

test('testarCodigoSoEspacos', () => {
    expect(() => {new CodigoLinha('                    ')}).toThrow(SyntaxError);
});

test('testarCodigoUsualComCarateresAcentos', () => {
    expect(() => {new CodigoLinha('ÁÕÍININIFWIFWÉÊÂÔSEÙ')}).toThrow(SyntaxError);
});