
import { Router, Request, Response } from 'express';
import { Container, Inject, Service } from 'typedi';
import { PercursoBuilder } from '../../application/percurso/services/PercursoBuilder';
import { SegmentoRedeBuilder } from '../../application/percurso/services/SegmentoRedeBuilder';
import { ListarPercursosUseCase } from '../../application/percurso/usecases/ListarPercursosUseCase';
import { ServicoLogger } from '../../application/utils/services/ServicoLogger';
import { PercursoRepository } from '../../repository/interfaces/PercursoRepository';

@Service()
export class RotaPercursoUnico {

    @Inject("PrimaryListarPercursosUseCase")
    private listarUseCase: ListarPercursosUseCase;

    @Inject("PrimaryServicoLogger")
    private logger: ServicoLogger;

    @Inject("PrimaryPercursoRepository")
    private pRepo: PercursoRepository;
    
    insertRoutes = (router: Router): void => {
        router.route('/percurso/:cod_percurso')
            .get(this.rotaObterSegmentosPercurso);
    }

    rotaObterSegmentosPercurso = async (req: Request, res: Response): Promise<void> => {
        const pBuilder: PercursoBuilder = Container.get("PrimaryPercursoBuilder");
        const sBuilder: SegmentoRedeBuilder = Container.get("PrimarySegmentoRedeBuilder");
        const resultado = await this.listarUseCase.obterSegmentosPercurso(pBuilder, sBuilder, this.logger, this.pRepo, req.params.cod_percurso);
        res.send({'sucesso': resultado.sucesso, 'mensagem': resultado.descricao, 'lista': resultado.lista});
    }

}