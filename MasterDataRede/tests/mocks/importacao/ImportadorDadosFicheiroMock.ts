
import { ObjetoImportado } from "../../../src/application/importacao/objetosImportacao/ObjetoImportado";
import { ImportadorDadosFicheiro } from "../../../src/application/importacao/services/ImportadorDadosFicheiro";
import { FicheiroNaoEncontradoError } from "../../../src/application/utils/errors/FicheiroNaoEncontradoError";

export class ImportadorDadosFicheiroMockFicheiroInvalido implements ImportadorDadosFicheiro {

    async importarDadosFicheiro(directorio: string, apagarFicheiro: boolean): Promise<ObjetoImportado[]> {
        throw new FicheiroNaoEncontradoError('Teste');
    }
    
}

export class ImportadorDadosFicheiroMockSemValores implements ImportadorDadosFicheiro {

    async importarDadosFicheiro(directorio: string, apagarFicheiro: boolean): Promise<ObjetoImportado[]> {
        return [];
    }
    
}

export class ImportadorDadosFicheiroMockSucesso implements ImportadorDadosFicheiro {

    async importarDadosFicheiro(directorio: string, apagarFicheiro: boolean): Promise<ObjetoImportado[]> {
        return [null, null];
    }
    
}