import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizadorNosComponent } from './visualizador-nos.component';

describe('VisualizadorNosComponent', () => {
  let component: VisualizadorNosComponent;
  let fixture: ComponentFixture<VisualizadorNosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizadorNosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizadorNosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
