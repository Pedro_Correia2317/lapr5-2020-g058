
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualizacaoRedeComponent } from './visualizacao-rede.component';

const routes: Routes = [
    {path: '', component: VisualizacaoRedeComponent}
];

@NgModule({
  declarations: [VisualizacaoRedeComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoRedeModule { }