
import { NoEstacaoRecolhaBuilder } from '../../../src/application/no/services/NoEstacaoRecolhaBuilder';
import { AbreviaturaNo } from '../../../src/domain/no/model/AbreviaturaNo';
import { CoordenadasNo } from '../../../src/domain/no/model/CoordenadasNo';
import { NomeNo } from '../../../src/domain/no/model/NomeNo';
import { NoEstacaoRecolha } from '../../../src/domain/no/model/NoEstacaoRecolha';
import { CapacidadeVeiculos } from '../../../src/domain/no/model/CapacidadeVeiculos';

export class NoEstacaoRecolhaBuilderMockSucesso implements NoEstacaoRecolhaBuilder {

    reset(): void {
    }

    isNoIgual(abreviatura: string): boolean {
        return true;
    }

    obterResultado(): NoEstacaoRecolha {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(23.4567, 12);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(2);
        const tempos = [];
        return new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, tempos, capacidade);
    }

    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void {
    }

    aplicarCodigo(codigo: string): void {
    }

    aplicarNome(nome: string): void {
    }

    aplicarAbreviatura(abreviatura: string): void {
    }

    aplicarCoordenadas(lat: number, lon: number): void {
    }

    aplicarCapacidadeVeiculos(capacidade: number): void {
    }

}

export class NoEstacaoRecolhaBuilderMockErro implements NoEstacaoRecolhaBuilder {

    reset(): void {
    }

    isNoIgual(abreviatura: string): boolean {
        return false;
    }

    obterResultado(): NoEstacaoRecolha {
        throw new SyntaxError('Teste');
    }
    
    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarNome(nome: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarAbreviatura(abreviatura: string): void {
        throw new SyntaxError('Teste');
    }
    
    aplicarCoordenadas(lat: number, lon: number): void {
        throw new TypeError('Teste');    
    }

    aplicarCapacidadeVeiculos(capacidade: number): void {
        throw new SyntaxError('Teste');    
    }

}