
import { NoDTO } from "../../../domain/no/dto/NoDTO";
import { TipoNoDTO } from "../../../domain/no/dto/TipoNoDTO";
import { NoRepository } from "../../../repository/interfaces/NoRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { NoBuilder } from "../services/NoBuilder";

export interface ListarNosUseCase {

    obterTiposNos(): TipoNoDTO[];

    obterNos(builders: Map<string, NoBuilder>, logger: ServicoLogger, noRepo: NoRepository, 
        sort: string, numPag: number, tamPag: number, codigo: string, nome: string): Promise<DetalhesPesquisaDTO<NoDTO>>;
}