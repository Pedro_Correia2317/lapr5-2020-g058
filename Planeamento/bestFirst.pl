:-consult("bc_autocarros2.pl").
:- consult("utility.pl").

bestfs(Src,Dest,StartingTime,Path,Costs):-
    get_time(Ti),
    bestfs2(Dest,(StartingTime,[(Src,0)]),Path,TimeCosts),
    Costs is TimeCosts - StartingTime,
    get_time(Tf),TSol is Tf-Ti,
    write('Tempo de geracao da solucao:'),write(TSol),nl,!.

bestfs2(Dest,(Costs,[(Dest,Line)|T]),Path,Costs):-
    reverse([(Dest,Line)|T],Path),!.

bestfs2(Dest,(CurrentCosts,CurrentPath),Path,Costs):-
    CurrentPath=[(CurrentNode,_)|_],
    findall((EstX,NextCosts,[(NextNode,Line)|CurrentPath]),
            (liga(CurrentNode,NextNode,Line), \+member((NextNode,_),CurrentPath),
            getTimeCostsForWaitingAndTravelling(CurrentCosts, Line, CurrentNode,NextNode,WaitingTravelCosts),
            estimate(NextNode,Dest,EstX),
            NextCosts is CurrentCosts + WaitingTravelCosts),NewOnes),
    sort(NewOnes,NewSorted),
    next(NewSorted,C,P),
    bestfs2(Dest,(C,P),Path,Costs).

next([(_,C,P)|_],C,P).
next([_|L],C,P):- next(L,C,P).

estimate(No1,No2,Estimate):-
    no(_,No1,_,_,X1,Y1), % first x = long, y = lat
    no(_,No2,_,_,X2,Y2),
    harversineFormulaForDistance(Y1,X1,Y2,X2,Estimate).

