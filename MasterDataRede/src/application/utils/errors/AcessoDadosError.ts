

export class AcessoDadosError extends Error {

    constructor(m: string){
        super(m);
        Object.setPrototypeOf(this, AcessoDadosError.prototype);
    }
}