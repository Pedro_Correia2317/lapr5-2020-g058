
import { PercursosLinha } from '../../../../src/domain/linha/model/PercursosLinha';
import { CodigoPercurso } from '../../../../src/domain/percurso/model/CodigoPercurso';

test('testarPercursosLinhaNulo', () => {
    expect(() => {new PercursosLinha([], [], [], null)}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [], null, [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], null, [], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha(null, [], [], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [], [], undefined)}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [], undefined, [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], undefined, [], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha(undefined, [], [], [])}).toThrow(ReferenceError);
});

test('testarPercursosLinhaValoresNulos', () => {
    expect(() => {new PercursosLinha([], [], [], [null])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [], [null], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [null], [], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([null], [], [], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [], [], [undefined])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [], [undefined], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([], [undefined], [], [])}).toThrow(ReferenceError);
    expect(() => {new PercursosLinha([undefined], [], [], [])}).toThrow(ReferenceError);
});

test('testarPercursosLinhaValoresInvalidos', () => {
    const perc = new CodigoPercurso('12345678901234567890');
    expect(() => {new PercursosLinha([], [], [], [])}).toThrow(SyntaxError);
    expect(() => {new PercursosLinha([], [], [], [perc])}).toThrow(SyntaxError);
    expect(() => {new PercursosLinha([], [], [perc], [])}).toThrow(SyntaxError);
    expect(() => {new PercursosLinha([], [perc], [], [])}).toThrow(SyntaxError);
    expect(() => {new PercursosLinha([perc], [], [], [])}).toThrow(SyntaxError);
    expect(() => {new PercursosLinha([], [], [perc], [perc])}).toThrow(SyntaxError);
});

test('testarPercursosLinhaValidos', () => {
    const perc = new CodigoPercurso('12345678901234567890');
    expect(new PercursosLinha([perc], [perc], [], [])).toBeInstanceOf(PercursosLinha);
    expect(new PercursosLinha([perc], [perc], [perc], [])).toBeInstanceOf(PercursosLinha);
    expect(new PercursosLinha([perc], [perc], [], [perc])).toBeInstanceOf(PercursosLinha);
    expect(new PercursosLinha([perc], [perc], [perc], [perc])).toBeInstanceOf(PercursosLinha);
    expect(new PercursosLinha([perc, perc], [perc, perc], [], [perc])).toBeInstanceOf(PercursosLinha);
});