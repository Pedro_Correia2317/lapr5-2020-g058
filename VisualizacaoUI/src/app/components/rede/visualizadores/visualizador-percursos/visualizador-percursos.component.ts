import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { PercursoDTO } from 'src/app/models/percursos/PercursoDTO';
import { AcessoPercursosService } from 'src/app/services/rede/AcessoPercursos.service';

@Component({
  selector: 'visualizador-percursos',
  templateUrl: './visualizador-percursos.component.html',
  styleUrls: ['./visualizador-percursos.component.css']
})
export class VisualizadorPercursosComponent implements OnInit {

  @Output() percursoClicado: EventEmitter<PercursoDTO> = new EventEmitter();

  percursos: PercursoDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  pesquisa: string;

  constructor(private todosPercs: AcessoPercursosService) {
    this.percursos = [];
  }

  ngOnInit() {
  }

  onClickedPath(percursoSelecionado: PercursoDTO){
    this.percursoClicado.emit(percursoSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarPercursos(pagina, this.tamanho, this.sort);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarPercursos(1, this.tamanho, this.sort, pesquisa);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarPercursos(1, tamanho, this.sort, this.pesquisa);
  }

  private atualizarPercursos(pagina: number, tamanho: number, sort?: string, pesquisa?: string){
    this.todosPercs.procurarPercursos(pagina, tamanho, sort, pesquisa).subscribe((resultados: DetalhesPesquisaDTO<PercursoDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.percursos = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
      }
    });
  }

}
