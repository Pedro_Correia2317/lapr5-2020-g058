
import { AutonomiaTipoViatura } from "../../../domain/tipoViatura/model/AutonomiaTipoViatura";
import { CodigoTipoViatura } from "../../../domain/tipoViatura/model/CodigoTipoViatura";
import { ConsumoTipoViatura } from "../../../domain/tipoViatura/model/ConsumoTipoViatura";
import { CustoTipoViatura } from "../../../domain/tipoViatura/model/CustoTipoViatura";
import { DescricaoTipoViatura } from "../../../domain/tipoViatura/model/DescricaoTipoViatura";
import { EmissoesTipoViatura } from "../../../domain/tipoViatura/model/EmissoesTipoViatura";
import { TipoCombustivel } from "../../../domain/tipoViatura/model/TipoCombustivel";
import { TipoViatura } from "../../../domain/tipoViatura/model/TipoViatura";
import { VelocidadeMediaTipoViatura } from "../../../domain/tipoViatura/model/VelocidadeMediaTipoViatura";
import { TipoViaturaBuilder } from "./TipoViaturaBuilder";

export class TipoViaturaBuilderImpl implements TipoViaturaBuilder {
    
    private codigo: CodigoTipoViatura;

    private descricao: DescricaoTipoViatura;

    private autonomia: AutonomiaTipoViatura;

    private consumo: ConsumoTipoViatura;

    private custo: CustoTipoViatura;

    private emissoes: EmissoesTipoViatura;

    private velocidadeMedia: VelocidadeMediaTipoViatura;

    private tipoCombustivel: TipoCombustivel;

    reset(): void {
        this.codigo = null;
        this.descricao = null;
        this.autonomia = null;
        this.consumo = null;
        this.emissoes = null;
        this.custo = null;
        this.velocidadeMedia = null;
        this.tipoCombustivel = null;
    }

    aplicarCodigo(codigo: string): void {
        this.codigo = new CodigoTipoViatura(codigo);
    }

    aplicarDescricao(descricao: string): void {
        this.descricao = new DescricaoTipoViatura(descricao);
    }

    aplicarAutonomia(autonomia: number): void {
        this.autonomia = new AutonomiaTipoViatura(autonomia);
    }

    aplicarCusto(custo: number): void {
        this.custo = new CustoTipoViatura(custo);
    }

    aplicarVelocidadeMedia(velocidade: number): void {
        this.velocidadeMedia = new VelocidadeMediaTipoViatura(velocidade);
    }

    aplicarConsumo(consumo: number): void {
        this.consumo = new ConsumoTipoViatura(consumo);
    }

    aplicarEmissoes(emissoes: number): void {
        this.emissoes = new EmissoesTipoViatura(emissoes);
    }

    aplicarTipoCombustivel(tipoCombustivel: string): void {
        this.tipoCombustivel = TipoCombustivel.valor(tipoCombustivel);
    }

    obterResultado(): TipoViatura {
        return new TipoViatura(this.codigo, this.descricao, this.autonomia,
                this.consumo, this.custo, this.emissoes,
                this.velocidadeMedia, this.tipoCombustivel
        );
    }

}