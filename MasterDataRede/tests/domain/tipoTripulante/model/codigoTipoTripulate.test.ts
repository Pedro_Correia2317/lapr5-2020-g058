
import { CodigoTipoTripulante } from '../../../../src/domain/tipoTripulante/model/CodigoTipoTripulante';

test('testarCodigoNulo', () => {
    expect(() => {new CodigoTipoTripulante(null)}).toThrow(ReferenceError);
    expect(() => {new CodigoTipoTripulante(undefined)}).toThrow(ReferenceError);
});

test('testarCodigoVazio', () => {
    expect(() => {new CodigoTipoTripulante('')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoComprido', () => {
    expect(() => {new CodigoTipoTripulante('123456789012345678901')}).toThrow(SyntaxError); //21 chars
});

test('testarCodigoUsualComCarateresInvalidos', () => {
    expect(() => {new CodigoTipoTripulante('🌺ENF9229N2D9D2N3H6S🌺')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoPequeno', () => {
    expect(new CodigoTipoTripulante('A')).toBeInstanceOf(CodigoTipoTripulante);
});

test('testarCodigoPequenoValido', () => {
    expect(new CodigoTipoTripulante('1D020DN22D0ND0SKWI44')).toBeInstanceOf(CodigoTipoTripulante);
});

test('testarCodigoSoNumeros', () => {
    expect(new CodigoTipoTripulante('15367363211256232115')).toBeInstanceOf(CodigoTipoTripulante);
});

test('testarCodigoSoLetras', () => {
    expect(new CodigoTipoTripulante('IBIEBIFEIENIENIEFEFN')).toBeInstanceOf(CodigoTipoTripulante);
});

test('testarCodigoSoEspacos', () => {
    expect(() => {new CodigoTipoTripulante('                    ')}).toThrow(SyntaxError);
});

test('testarCodigoUsualComCarateresAcentos', () => {
    expect(() => {new CodigoTipoTripulante('ÁÕÍININIFWIFWÉÊÂÔSEÙ')}).toThrow(SyntaxError);
});