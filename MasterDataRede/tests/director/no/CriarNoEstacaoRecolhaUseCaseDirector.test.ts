
import { CriarNoEstacaoRecolhaUseCaseDirector } from '../../../src/application/no/director/CriarNoEstacaoRecolhaUseCaseDirector';
import { NoEstacaoRecolhaBuilderImpl } from '../../../src/application/no/services/NoEstacaoRecolhaBuilderImpl';
import { CriarNoPontoRendicaoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoPontoRendicaoUseCaseImpl';
import { CriarNoEstacaoRecolhaUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoEstacaoRecolhaUseCaseImpl';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { NoRepositoryMockFalse, NoRepositoryMockTrue } from '../../mocks/no/NoRepositoryMock';
import { PedidoCriarNoEstacaoRecolhaDTO } from '../../../src/application/no/dto/PedidoCriarNoEstacaoRecolhaDTO';
import { CriarNoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoUseCaseImpl';
import { PedidoTempoDeslocacaoDTO } from '../../../src/application/no/dto/PedidoCriarNoPontoRendicaoDTO';

const loggerMock = new ServicoLoggerMock();
const nUseCase = new CriarNoEstacaoRecolhaUseCaseImpl(new CriarNoPontoRendicaoUseCaseImpl(new CriarNoUseCaseImpl()));
const nRepoTrue = new NoRepositoryMockTrue();
const nRepoFalse = new NoRepositoryMockFalse();

test('testarPedidoValidoComValoresUnicos', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComAbreviaturaRepetida', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoFalse);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComAbreviaturaInvalida', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'Sou Invalido...';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemAbreviatura', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    // pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComNomeInvalido', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = '?';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemNome', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    // pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemLatitude', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    // pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemLongitude', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    // pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemCapacidade', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    // pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComCapacidadeInvalida', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = -10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemTipoNo', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    // pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTipoNoInvalido1', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PONTO_RENDICAO';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTipoNoInvalido2', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'TIPO_NO_INVALIDO';
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTemposDeslocacao', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1234;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTemposDeslocacaoNosInvalidos', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1234;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoFalse);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = -1245;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos2', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1245;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    //tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos3', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1245;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    //tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos4', async () => {
    const pedido: PedidoCriarNoEstacaoRecolhaDTO = new PedidoCriarNoEstacaoRecolhaDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1245;
    tempo1.abreviaturaNo = 'SouInvalido...';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    pedido.capacidade = 10;
    const tvBuilder = new NoEstacaoRecolhaBuilderImpl();
    const director = new CriarNoEstacaoRecolhaUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});