import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { PedidoCriarTipoViaturaDTO } from 'src/app/models/tipoViatura/PedidoCriarTipoViaturaDTO';
import { TipoCombustivelDTO } from 'src/app/models/tipoViatura/TipoCombustivelDTO';
import { AcessoTiposViaturasService } from 'src/app/services/rede/AcessoTiposViaturasService';

@Component({
  selector: 'app-criacao-tipos-viaturas',
  templateUrl: './criacao-tipos-viaturas.component.html',
  styleUrls: ['./criacao-tipos-viaturas.component.css']
})
export class CriacaoTiposViaturasComponent implements OnInit {

  formCriacaoTipoViatura: any;

  tiposComb: TipoCombustivelDTO[];

  unidade: string;

  constructor(private todosTiposViaturas: AcessoTiposViaturasService,
              private builder: FormBuilder) {
        this.tiposComb = [];
        this.formCriacaoTipoViatura = this.builder.group({
          codigo: '',
          descricao: '',
          autonomia: 0,
          custo: 0,
          emissoes: 0,
          velocidade: 0,
          consumo: 0,
          tipoComb: ''
        });
    }

  ngOnInit(): void {
    this.todosTiposViaturas.procurarTiposCombustiveis().subscribe((tiposComb: DetalhesPesquisaDTO<TipoCombustivelDTO>) => {
      this.tiposComb = tiposComb.lista;
    });
  }

  onChangeTipoCombustivel(indexTipoComb: any){
    this.unidade = this.tiposComb[indexTipoComb].unidade;
  }

  onSubmit(dadosFormulario: any) {
    const dto = new PedidoCriarTipoViaturaDTO();
    dto.codigo = dadosFormulario.codigo;
    dto.descricao = dadosFormulario.descricao;
    dto.autonomia = dadosFormulario.autonomia;
    dto.consumo = dadosFormulario.consumo;
    dto.emissoes = dadosFormulario.emissoes;
    dto.velocidadeMedia = dadosFormulario.velocidade;
    dto.custo = dadosFormulario.custo;
    dto.tipoCombustivel = this.tiposComb[dadosFormulario.tipoComb].descricao;
    this.todosTiposViaturas.enviarTipoViatura(dto).subscribe((resultados: DetalhesOperacaoDTO) => {
      alert(resultados.sucesso + ' na operação: ' + resultados.mensagem);
    });
  }

}
