
export class DescricaoPercurso {

    private descricao: string;

    constructor(descricao: string){
        if (descricao === null || typeof descricao === 'undefined'){
            throw new ReferenceError('The inserted description is null');
        }
        const desc = descricao.trim();
        if (!(/^[\wÀ-ú\d\s]{2,40}(?:> [\wÀ-ú\d\s]{2,40}){0,19}$/u).test(desc)){
            throw new SyntaxError(`The description ${desc} is invalid`);
        }
        this.descricao = desc;
    }

    juntarAbreviatura(abreviatura: string): DescricaoPercurso {
        return new DescricaoPercurso(`${this.descricao} > ${abreviatura}`);
    }

    getValue(): string {
        return this.descricao;
    }

    toString(): string {
        return `DescricaoPercurso: [${this.descricao}]`;
    }
}