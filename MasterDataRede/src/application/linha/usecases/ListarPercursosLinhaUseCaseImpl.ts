

import { PercursoLinhaDTO } from "../../../domain/linha/dto/PercursoLinhaDTO";
import { Linha } from "../../../domain/linha/model/Linha";
import { PercursoDTO } from "../../../domain/percurso/dto/PercursoDTO";
import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { PercursoBuilder } from "../../percurso/services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../../percurso/services/SegmentoRedeBuilder";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaBuilder } from "../services/LinhaBuilder";
import { ListarPercursosLinhaUseCase } from "./ListarPercursosLinhaUseCase";

export class ListarPercursosLinhaUseCaseImpl implements ListarPercursosLinhaUseCase {

    async obterPercursosLinha(lBuilder: LinhaBuilder, pBuilder: PercursoBuilder,
                                sBuilder: SegmentoRedeBuilder, lRepo: LinhaRepository,
                                pRepo: PercursoRepository, logger: ServicoLogger, 
                                codigoLinha: string): Promise<DetalhesPesquisaDTO<PercursoDTO[]>> {
        const resultados = new DetalhesPesquisaDTO<PercursoDTO[]>();
        let linha: Linha = null;
        await lRepo.findByCodigo(lBuilder, codigoLinha).then((resultado) => {
            linha = resultado;
            if (linha == null) {
                resultados.descricao = 'findLinePaths.unknownLine';
            }
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findLinePaths.errorFindingLine';
        });
        if (!resultados.descricao){
            await this.procurarPercursos(pBuilder, sBuilder, pRepo, linha.toPercursosIdaDTO()).then(async (lista) => {
                resultados.lista.push(lista);
                await this.procurarPercursos(pBuilder, sBuilder, pRepo, linha.toPercursosVoltaDTO()).then(async (lista) => {
                    resultados.lista.push(lista);
                    await this.procurarPercursos(pBuilder, sBuilder, pRepo, linha.toPercursosReforcoDTO()).then(async (lista) => {
                        resultados.lista.push(lista);
                        await this.procurarPercursos(pBuilder, sBuilder, pRepo, linha.toPercursosVazioDTO()).then((lista) => {
                            resultados.lista.push(lista);
                        });
                    });
                });
            }).catch((err) => {
                logger.registarFalha(err);
                resultados.descricao = 'findLinePaths.errorFindingPaths';
            });
        }
        if (!resultados.descricao) {
            resultados.sucesso = true;
            resultados.descricao = 'findLinePaths.searchSuccess';
        }
        return resultados;
    }

    private async procurarPercursos(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                            pRepo: PercursoRepository, percursos: PercursoLinhaDTO[]): Promise<PercursoDTO[]> {
        const resultados: PercursoDTO[] = [];
        for(const codPerc of percursos){
            const percurso = await pRepo.findByCodigo(pBuilder, sBuilder, codPerc.codigoPercurso);
            resultados.push(percurso.toBaseDTO());
        }
        return resultados;
    }

}