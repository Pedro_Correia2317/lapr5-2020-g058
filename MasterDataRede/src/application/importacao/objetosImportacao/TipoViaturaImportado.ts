import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../director/ImportarDadosFicheiroUseCaseDirector";
import { ObjetoImportado } from "./ObjetoImportado";

export class TipoViaturaImportado implements ObjetoImportado {

    public codigo: string;

    public descricao: string;

    public autonomia: number;

    public custo: number;

    public consumo: number;

    public emissoes: number;

    public velocidadeMedia: number;

    public tipoCombustivel: string;

    visitar(director: ImportarDadosFicheiroUseCaseDirector): Promise<DetalhesOperacaoDTO> {
        return director.processarTipoViatura(this);
    }
}