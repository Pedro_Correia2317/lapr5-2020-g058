
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarTipoViaturaDTO } from 'src/app/models/tipoViatura/PedidoCriarTipoViaturaDTO';
import { TipoCombustivelDTO } from 'src/app/models/tipoViatura/TipoCombustivelDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { TipoViaturaDTO } from 'src/app/models/tipoViatura/TipoViaturaDTO';

@Injectable({
  providedIn: 'root'
})
export class AcessoTiposViaturasService {

  private enderecoHttp: string = `${Configurations.API_SERVER_ADDRESS}/tiposViaturas`;

  private enderecoHttpTiposCombustiveis: string = `${Configurations.API_SERVER_ADDRESS}/tiposCombustiveis`;

  constructor(private http: HttpClient) { }

  procurarTiposViaturas(pagina: number, tamanho: number, sort?: string, desc?: string): Observable<DetalhesPesquisaDTO<TipoViaturaDTO>> {
    const aux: string = sort? sort : '';
    const descAux: string = desc? desc : '';
    return this.http.get<DetalhesPesquisaDTO<TipoViaturaDTO>>(this.enderecoHttp, {
      observe: 'body',
      responseType: 'json',
      params: {
        page: '' + pagina,
        size: '' + tamanho,
        sort: aux,
        descricao: descAux
      }
    });
  }

  enviarTipoViatura(pedido: PedidoCriarTipoViaturaDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }

  procurarTiposCombustiveis(): Observable<DetalhesPesquisaDTO<TipoCombustivelDTO>> {
    return this.http.get<DetalhesPesquisaDTO<TipoCombustivelDTO>>(this.enderecoHttpTiposCombustiveis, {
      observe: 'body',
      responseType: 'json'
    });
  }
}