import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoNosComponent } from './visualizacao-nos.component';

describe('VisualizacaoNosComponent', () => {
  let component: VisualizacaoNosComponent;
  let fixture: ComponentFixture<VisualizacaoNosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoNosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoNosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
