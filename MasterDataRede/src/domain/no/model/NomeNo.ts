
export class NomeNo {

    private nome: string;

    constructor(nome: string){
        if (nome === null || typeof nome === 'undefined'){
            throw new ReferenceError('The inserted name is null');
        }
        const trimmedName = nome.trim();
        if (!(/^[\wÀ-ú\d\s ()]{2,200}$/u).test(trimmedName)){
            throw new SyntaxError(`The name ${trimmedName} is invalid`);
        }
        this.nome = trimmedName;
    }

    getValue(): string {
        return this.nome;
    }

    toString(): string {
        return `NomeNo: [${this.nome}]`;
    }
}