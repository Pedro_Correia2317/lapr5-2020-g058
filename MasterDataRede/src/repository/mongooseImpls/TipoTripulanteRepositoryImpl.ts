
import Mongoose, { Schema } from 'mongoose';
import { TipoTripulanteBuilder } from '../../application/tipoTripulante/services/TipoTripulanteBuilder';
import { AcessoDadosError } from '../../application/utils/errors/AcessoDadosError';
import { CodigoTipoTripulante } from "../../domain/tipoTripulante/model/CodigoTipoTripulante";
import { TipoTripulante } from "../../domain/tipoTripulante/model/TipoTripulante";
import { TipoTripulanteRepository } from "../interfaces/TipoTripulanteRepository";
import { ITipoTripulante } from './models/ITipoTripulante';
import { MongooseRepositoryImpl } from './RepositoryImpl';

export class TipoTripulanteRepositoryImpl implements TipoTripulanteRepository {

    private static readonly SCHEMA_TIPO_TRIPULANTE: Mongoose.Schema = new Schema({
        'codigo': {'type': String, 'required': true, 'unique': true},
        'descricao': {'type': String, 'required': true, 'unique': true}
    });

    private static readonly MODELO_TIPO_TRIPULANTE 
                    = Mongoose.model<ITipoTripulante>('TipoTripulante', TipoTripulanteRepositoryImpl.SCHEMA_TIPO_TRIPULANTE);
                    
    private static BASE_REPO: MongooseRepositoryImpl = new MongooseRepositoryImpl();

    private modelarTipoTripulante(tipoV: TipoTripulante): ITipoTripulante {
        const modelo = new TipoTripulanteRepositoryImpl.MODELO_TIPO_TRIPULANTE(),
        tipoVDTO = tipoV.toBaseDTO();
        modelo.codigo = tipoVDTO.codigo;
        modelo.descricao = tipoVDTO.descricao;
        return modelo;
    }

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return await TipoTripulanteRepositoryImpl.BASE_REPO.isUnico(TipoTripulanteRepositoryImpl.MODELO_TIPO_TRIPULANTE, {codigo});
    }

    async isDescricaoUnico(descricao: string): Promise<boolean> {
        return await TipoTripulanteRepositoryImpl.BASE_REPO.isUnico(TipoTripulanteRepositoryImpl.MODELO_TIPO_TRIPULANTE, {descricao});
    }

    async existe(codigo: string): Promise<boolean> {
        return await TipoTripulanteRepositoryImpl.MODELO_TIPO_TRIPULANTE.exists({codigo});
    }

    save(objecto: TipoTripulante): Promise<boolean> {
        const modeloTipoV: ITipoTripulante = this.modelarTipoTripulante(objecto);
        return TipoTripulanteRepositoryImpl.BASE_REPO.save(modeloTipoV);
    }

    private mapearDataModel(builder: TipoTripulanteBuilder, dataModel: ITipoTripulante): TipoTripulante {
        builder.aplicarCodigo(dataModel.codigo);
        builder.aplicarDescricao(dataModel.descricao);
        return builder.obterResultado();
    }

    async obterTiposTripulantes(builder: TipoTripulanteBuilder, sort: string, numPag: number, 
                        tamPag: number, descricao: string): Promise<TipoTripulante[]> {
        const tiposT: TipoTripulante[] = [];
        const regexDesc = new RegExp(descricao, 'i');
        await TipoTripulanteRepositoryImpl.MODELO_TIPO_TRIPULANTE.find({descricao: regexDesc}).sort(sort)
                                        .skip((numPag-1) * tamPag).limit(tamPag).then((results) => {
            results.forEach((tipoT) => {
                tiposT.push(this.mapearDataModel(builder, tipoT));
            });
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return tiposT;
    }

    async countComFiltros(descricao: string): Promise<number> {
        let numero: number;
        const regexDesc = new RegExp(descricao, 'i');
        await TipoTripulanteRepositoryImpl.MODELO_TIPO_TRIPULANTE.countDocuments({descricao: regexDesc}).exec().then((num) => {
            numero = num;
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return numero;
    }

    findById(codigo: CodigoTipoTripulante): Promise<TipoTripulante> {
        throw new Error("Method not implemented.");
    }
    findPage(numPagina: number, tamanhoPagina: number): Promise<TipoTripulante[]> {
        throw new Error("Method not implemented.");
    }
    count(): Promise<number> {
        throw new Error("Method not implemented.");
    }

}