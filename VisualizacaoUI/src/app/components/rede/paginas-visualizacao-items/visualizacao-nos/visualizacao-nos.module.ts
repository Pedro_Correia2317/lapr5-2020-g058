
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisualizadorNosModule } from '../../visualizadores/visualizador-nos/visualizador-nos.module';
import { VisualizacaoNosComponent } from './visualizacao-nos.component';

const routes: Routes = [
    {path: '', component: VisualizacaoNosComponent}
];

@NgModule({
  declarations: [VisualizacaoNosComponent],
  imports: [VisualizadorNosModule, CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoNosModule { }
