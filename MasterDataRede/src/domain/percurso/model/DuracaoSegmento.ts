
export class DuracaoSegmento {

    private duracao: number;

    constructor(duracaoSeg: number){
        if (!Number.isInteger(duracaoSeg)){
            throw new TypeError(`The duration is not a integer: ${duracaoSeg}`);
        }
        if (duracaoSeg <= 0 || duracaoSeg > 10800){ // 3 horas
            throw new SyntaxError(`The duration ${duracaoSeg} is <= 0 or > 10800`);
        }
        this.duracao = duracaoSeg;
    }

    getValue(): number{
        return this.duracao;
    }

    toString(): string{
        return `DuracaoSegmento: [${this.duracao}]`;
    }
}