
import { ImportarDadosFicheiroService } from "./ImportarDadosFicheiroService";
import { ImportadorDadosFicheiroContext } from "./services/ImportadorDadosFicheiroContext";
import { DetalhesImportacaoDTO } from "../utils/dto/DetalhesImportacaoDTO";
import { FicheiroNaoEncontradoError } from "../utils/errors/FicheiroNaoEncontradoError";
import { ServicoLogger } from "../utils/services/ServicoLogger";
import { ImportadorDadosFicheiro } from "./services/ImportadorDadosFicheiro";
import { ParsingFicheiroError } from "../utils/errors/ParsingFicheiroError";

export class ImportarDadosFicheiroServiceImpl implements ImportarDadosFicheiroService {

    async importarDadosFicheiro(impContext: ImportadorDadosFicheiroContext, logger: ServicoLogger, dir: string, apagarFicheiro: boolean): Promise<DetalhesImportacaoDTO> {
        const resultados = new DetalhesImportacaoDTO();
        const importador = impContext.obterImportadorAdequado(dir);
        if (importador == null){
            resultados.descricaoErro = 'importData.UnsupportedExtension';
        } else {
            return this.importar(importador, logger, dir, apagarFicheiro);
        }
        return resultados;
    }

    private async importar(importador: ImportadorDadosFicheiro, logger: ServicoLogger, dir: string, apagarFicheiro: boolean): Promise<DetalhesImportacaoDTO> {
        const resultados = new DetalhesImportacaoDTO();
        try {
            const dadosImportados = await importador.importarDadosFicheiro(dir, apagarFicheiro);
            if(dadosImportados.length == 0){
                resultados.descricaoErro = 'importData.NoValidDataFound';
            } else {
                resultados.dadosImportados = dadosImportados;
                resultados.sucesso = true;
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof FicheiroNaoEncontradoError){
                resultados.descricaoErro = 'importData.FileNotFound';
            } else if (err instanceof ParsingFicheiroError) {
                resultados.descricaoErro = 'importData.InvalidFileStructure';
            } else {
                throw err;
            }
        }
        return resultados;
    }
    
}