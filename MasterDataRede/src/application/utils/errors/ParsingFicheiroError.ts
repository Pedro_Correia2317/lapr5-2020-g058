
export class ParsingFicheiroError extends Error {

    constructor(m: string){
        super(m);
        Object.setPrototypeOf(this, ParsingFicheiroError.prototype);
    }
}