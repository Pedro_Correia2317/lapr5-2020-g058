
import { NoEstacaoRecolha } from '../../../../src/domain/no/model/NoEstacaoRecolha';
import { NomeNo } from '../../../../src/domain/no/model/NomeNo';
import { CoordenadasNo } from '../../../../src/domain/no/model/CoordenadasNo';
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';
import { CapacidadeVeiculos } from '../../../../src/domain/no/model/CapacidadeVeiculos';
import { TempoDeslocacaoTripulacao } from '../../../../src/domain/no/model/TempoDeslocacaoTripulacao';

test("testarNomeNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, null, coordenadasNo, tempos, capacidade)}).toThrow(SyntaxError);
});

test("testarCoordenadasNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, null, tempos, capacidade)}).toThrow(SyntaxError);
});

test("testarAbreviaturaNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(null, nomeNo, coordenadasNo, tempos, capacidade)}).toThrow(SyntaxError);
});

test("testarCapacidadeNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, tempos, null)}).toThrow(SyntaxError);
});

test("testarTemposNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, null, capacidade)}).toThrow(SyntaxError);
});

test("testarNomeIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, undefined, coordenadasNo, tempos, capacidade)}).toThrow(SyntaxError);
});

test("testarCoordenadasIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, undefined, tempos, capacidade)}).toThrow(SyntaxError);
});

test("testarAbreviaturaIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(undefined, nomeNo, coordenadasNo, tempos, capacidade)}).toThrow(SyntaxError);
});

test("testarCapacidadeIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, tempos, undefined)}).toThrow(SyntaxError);
});

test("testarTemposIndefinidos", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const capacidade = new CapacidadeVeiculos(5);
        const tempos = [];
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, undefined, capacidade)}).toThrow(SyntaxError);
});

test("testarValoresValidos", () => {
    const nomeNo = new NomeNo('Estação de Aguiar');
    const coordenadasNo = new CoordenadasNo(23.4567, 12);
    const abreviaturaNo = new AbreviaturaNo('AGUIAR');
    const capacidade = new CapacidadeVeiculos(5);
    const tempos = [];
    expect(new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, tempos, capacidade)).toBeInstanceOf(NoEstacaoRecolha);
});

test("testarValoresInvalidosTemposNulos", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        tempos.push(new TempoDeslocacaoTripulacao(500, abreviaturaNo));
        tempos.push(null);
        const capacidade = new CapacidadeVeiculos(5);
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, tempos, capacidade)}).toThrow(ReferenceError);
});

test("testarValoresInvalidosTemposIndefinidos", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        tempos.push(new TempoDeslocacaoTripulacao(500, abreviaturaNo));
        tempos.push(undefined);
        const capacidade = new CapacidadeVeiculos(5);
        new NoEstacaoRecolha(abreviaturaNo, nomeNo, coordenadasNo, tempos, capacidade)}).toThrow(ReferenceError);
});