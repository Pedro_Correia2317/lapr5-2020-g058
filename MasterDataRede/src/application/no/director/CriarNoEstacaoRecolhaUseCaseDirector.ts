
import { NoEstacaoRecolhaBuilder } from "../services/NoEstacaoRecolhaBuilder";
import { CriarNoEstacaoRecolhaUseCase } from "../usecases/CriarNoEstacaoRecolhaUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { NoRepository } from "../../../repository/interfaces/NoRepository";
import { PedidoCriarNoEstacaoRecolhaDTO } from "../dto/PedidoCriarNoEstacaoRecolhaDTO";
import { CriarNoUseCaseDirectorCommand } from "./CriarNoUseCaseDirectorCommand";

export class CriarNoEstacaoRecolhaUseCaseDirector implements CriarNoUseCaseDirectorCommand {

    private criarNoService: CriarNoEstacaoRecolhaUseCase;

    private logger: ServicoLogger;

    private todosNos: NoRepository;

    constructor(c: CriarNoEstacaoRecolhaUseCase, l: ServicoLogger, tn: NoRepository){
        this.criarNoService = c;
        this.logger = l;
        this.todosNos = tn;
    }
    
    async executar(req: PedidoCriarNoEstacaoRecolhaDTO, noBuilder: NoEstacaoRecolhaBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarNoService.aplicarAbreviatura(noBuilder, this.logger, this.todosNos, req.abreviatura);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarNome(noBuilder, this.logger, req.nome);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarCoordenadas(noBuilder, this.logger, req.lat, req.lon);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarCapacidade(noBuilder, this.logger, req.capacidade);
        if(req.tempos != null && typeof req.tempos[Symbol.iterator] === 'function'){ //Se for iteravel
            for(const tempo of req.tempos){
                if(!resultado.sucesso) return resultado;
                resultado = await this.criarNoService.adicionarTempoDeslocacaoTripulacao(noBuilder, this.logger,
                                                        this.todosNos, tempo.tempo, tempo.abreviaturaNo);
            }
        }
        if(!resultado.sucesso) return resultado;
        return this.criarNoService.registarNo(noBuilder, this.logger, this.todosNos);
    }
}