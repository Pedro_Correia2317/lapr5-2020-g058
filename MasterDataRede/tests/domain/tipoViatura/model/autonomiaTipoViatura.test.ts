
import { AutonomiaTipoViatura } from '../../../../src/domain/tipoViatura/model/AutonomiaTipoViatura';

test('testarCapacidadeNulo', () => {
    expect(() => {new AutonomiaTipoViatura(null)}).toThrow(TypeError);
    expect(() => {new AutonomiaTipoViatura(undefined)}).toThrow(TypeError);
});

test('testarCapacidadeValoresPositivos', () => {
    expect(new AutonomiaTipoViatura(1)).toBeInstanceOf(AutonomiaTipoViatura);
    expect(new AutonomiaTipoViatura(13.0)).toBeInstanceOf(AutonomiaTipoViatura);
    expect(new AutonomiaTipoViatura(9999999)).toBeInstanceOf(AutonomiaTipoViatura);
});

test('testarCapacidadeValoresNegativos', () => {
    expect(() => {new AutonomiaTipoViatura(-11)}).toThrow(SyntaxError);
    expect(() => {new AutonomiaTipoViatura(-21)}).toThrow(SyntaxError);
    expect(() => {new AutonomiaTipoViatura(-1)}).toThrow(SyntaxError);
});

test('testarCapacidadeValoresCasasDecimais', () => {
    expect(() => {new AutonomiaTipoViatura(1.3346)}).toThrow(TypeError);
    expect(() => {new AutonomiaTipoViatura(2.0000000001)}).toThrow(TypeError);
    expect(() => {new AutonomiaTipoViatura(-1.236)}).toThrow(TypeError);
});

test('testarCapacidadeValoresExtremos', () => {
    expect(() => {new AutonomiaTipoViatura(10000000)}).toThrow(SyntaxError);
    expect(() => {new AutonomiaTipoViatura(28481694619494)}).toThrow(SyntaxError);
});