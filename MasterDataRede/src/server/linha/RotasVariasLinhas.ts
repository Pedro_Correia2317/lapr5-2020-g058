
import { Router, Request, Response } from "express";
import Container, { Inject, Service } from "typedi";
import { CriarLinhaUseCaseDirector } from "../../application/linha/director/CriarLinhaUseCaseDirector";
import { ListarLinhasUseCase } from "../../application/linha/usecases/ListarLinhasUseCase";
import { LinhaBuilder } from "../../application/linha/services/LinhaBuilder";
import { ServicoLogger } from "../../application/utils/services/ServicoLogger";
import { LinhaRepository } from "../../repository/interfaces/LinhaRepository";

@Service()
export class RotasVariasLinhas {

    @Inject("PrimaryCriarLinhaUseCaseDirector")
    private diretor: CriarLinhaUseCaseDirector;

    @Inject("PrimaryListarLinhasUseCase")
    private listarUseCase: ListarLinhasUseCase;

    @Inject("PrimaryServicoLogger")
    private logger: ServicoLogger;

    @Inject("PrimaryLinhaRepository")
    private linhaRepo: LinhaRepository;

    @Inject("PrimaryLinhaBuilder")
    private builder: LinhaBuilder;
    
    insertRoutes = (router: Router): void => {
        router.route('/linhas')
            .post(this.inserirRotaCriacaoLinha)
            .get(this.rotaListagemLinhas);
    }

    inserirRotaCriacaoLinha = async (req: Request, res: Response): Promise<void> => {
        const resultado = await this.diretor.processarPedido(req.body, Container.get("PrimaryLinhaBuilder"));
        res.send({'sucesso': resultado.sucesso, 'mensagem': resultado.mensagem});
    }

    rotaListagemLinhas = async (req: Request, res: Response): Promise<void> => {
        let num = req.query.page? Number.parseInt(req.query.page.toString()) : 1;
        num = num <= 0? 1 : num;
        let tam = req.query.size? Number.parseInt(req.query.size.toString()) : 24;
        tam = tam <= 0? 24 : tam;
        const cod = req.query.codigo? req.query.codigo.toString() : '';
        const nome = req.query.nome? req.query.nome.toString(): '';
        const sort = req.query.sort? req.query.sort.toString(): '';
        const resultado = await this.listarUseCase.obterLinhas(this.builder, this.logger, this.linhaRepo, sort, num, tam, cod, nome);
        res.send({'sucesso': resultado.sucesso, 'lista': resultado.lista, 'numeroTotal': resultado.count, 'mensagem': resultado.descricao});
    }
}