

import { PercursoDTO } from "../../../domain/percurso/dto/PercursoDTO";
import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { PercursoBuilder } from "../../percurso/services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../../percurso/services/SegmentoRedeBuilder";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaBuilder } from "../services/LinhaBuilder";

export interface ListarPercursosLinhaUseCase {

    obterPercursosLinha(lBuilder: LinhaBuilder, pBuilder: PercursoBuilder, 
                        sBuilder: SegmentoRedeBuilder, lRepo: LinhaRepository, 
                        pRepo: PercursoRepository, logger: ServicoLogger,
                        codigoLinha: string): Promise<DetalhesPesquisaDTO<PercursoDTO[]>>;

}