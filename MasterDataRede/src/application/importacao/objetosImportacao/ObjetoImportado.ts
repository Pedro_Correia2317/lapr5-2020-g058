import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../director/ImportarDadosFicheiroUseCaseDirector";

export interface ObjetoImportado {

    visitar(director: ImportarDadosFicheiroUseCaseDirector): Promise<DetalhesOperacaoDTO>;

}