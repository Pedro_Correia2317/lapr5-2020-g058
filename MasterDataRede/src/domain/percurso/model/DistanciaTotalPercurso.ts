
export class DistanciaTotalPercurso {

    private distancia: number;

    constructor(distancia: number){
        if (!Number.isFinite(distancia)){
            throw new TypeError(`The distance is not a number: ${distancia}`);
        }
        const aux = parseFloat(distancia.toFixed(3));
        if (aux <= 0 || aux > 100000){
            throw new SyntaxError(`The distance ${aux} is <= 0 or > 100000`);
        }
        this.distancia = aux;
    }

    juntarDistancia(distancia: number): DistanciaTotalPercurso {
        return new DistanciaTotalPercurso(this.distancia + distancia);
    }

    getValue(): number {
        return this.distancia;
    }

    toString(): string {
        return `DistanciaTotalPercurso: [${this.distancia}]`;
    }
}