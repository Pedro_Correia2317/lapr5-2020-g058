

import Mongoose from 'mongoose';


export interface ITipoTripulante extends Mongoose.Document {

    codigo: string;

    descricao: string;

}