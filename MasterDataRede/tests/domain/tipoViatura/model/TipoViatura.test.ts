

import { AutonomiaTipoViatura } from "../../../../src/domain/tipoViatura/model/AutonomiaTipoViatura";
import { CodigoTipoViatura } from "../../../../src/domain/tipoViatura/model/CodigoTipoViatura";
import { ConsumoTipoViatura } from "../../../../src/domain/tipoViatura/model/ConsumoTipoViatura";
import { CustoTipoViatura } from "../../../../src/domain/tipoViatura/model/CustoTipoViatura";
import { DescricaoTipoViatura } from "../../../../src/domain/tipoViatura/model/DescricaoTipoViatura";
import { EmissoesTipoViatura } from "../../../../src/domain/tipoViatura/model/EmissoesTipoViatura";
import { TipoCombustivel } from "../../../../src/domain/tipoViatura/model/TipoCombustivel";
import { TipoViatura } from "../../../../src/domain/tipoViatura/model/TipoViatura";
import { VelocidadeMediaTipoViatura } from "../../../../src/domain/tipoViatura/model/VelocidadeMediaTipoViatura";

test("testarCodigoNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(null, desc, auto, con, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarCodigoIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(undefined, desc, auto, con, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarDescricaoNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, null, auto, con, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarDescricaoIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, undefined, auto, con, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarAutonomiaNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, null, con, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarAutonomiaIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, undefined, con, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarConsumoNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, null, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarConsumoIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, undefined, custo, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarCustoNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, null, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarCustoIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, undefined, emi, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarEmissoesNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, custo, null, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarEmissoesIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, custo, undefined, vlc, tipo)}).toThrow(SyntaxError);
});

test("testarVelocidadeNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, custo, emi, null, tipo)}).toThrow(SyntaxError);
});

test("testarVelocidadeIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, custo, emi, undefined, tipo)}).toThrow(SyntaxError);
});

test("testarTipoCombustivelNulo", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, custo, emi, vlc, null)}).toThrow(SyntaxError);
});

test("testarTipoCombustivelIndefinido", () => {
    expect(() => {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        new TipoViatura(cod, desc, auto, con, custo, emi, vlc, undefined)}).toThrow(SyntaxError);
});

test("testarTipoViaturaValido", () => {
    const cod = new CodigoTipoViatura('12345678901234567890');
    const desc = new DescricaoTipoViatura('12345678901234567890');
    const auto = new AutonomiaTipoViatura(23);
    const con = new ConsumoTipoViatura(23);
    const custo = new CustoTipoViatura(23);
    const emi = new EmissoesTipoViatura(23);
    const vlc = new VelocidadeMediaTipoViatura(23);
    const tipo = TipoCombustivel.ELECTRICO;
    expect(new TipoViatura(cod, desc, auto, con, custo, emi, vlc, tipo)).toBeInstanceOf(TipoViatura);
});