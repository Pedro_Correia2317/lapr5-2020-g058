
import { LinhaBuilder } from "../services/LinhaBuilder";
import { CriarLinhaUseCase } from "../usecases/CriarLinhaUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { Repository } from "../../../repository/interfaces/Repository";
import { TipoTripulanteRepository } from "../../../repository/interfaces/TipoTripulanteRepository";
import { TipoViaturaRepository } from "../../../repository/interfaces/TipoViaturaRepository";
import { PedidoCriarLinhaDTO } from "../dto/PedidoCriarLinhaDTO";

export class CriarLinhaUseCaseDirector {

    private criarUseCase: CriarLinhaUseCase;
    
    private todasLinhas: LinhaRepository;
    
    private todosTiposV: TipoViaturaRepository;

    private todosTiposT: TipoTripulanteRepository;

    private todosPercursos: PercursoRepository;

    private logger: ServicoLogger;

    constructor(c: CriarLinhaUseCase, tL: LinhaRepository, tV: TipoViaturaRepository, tT: TipoTripulanteRepository, tP: PercursoRepository, l: ServicoLogger){
        this.criarUseCase = c;
        this.todasLinhas = tL;
        this.todosTiposV = tV;
        this.todosTiposT = tT;
        this.todosPercursos = tP;
        this.logger = l;
    }

    async processarPedido(req: PedidoCriarLinhaDTO, lBuilder: LinhaBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarUseCase.aplicarCodigo(lBuilder, this.logger, this.todasLinhas, req.codigo);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarNome(lBuilder, this.logger, req.nome);
        if(!resultado.sucesso) return resultado;
        if (req.percursosIda != null && typeof req.percursosIda[Symbol.iterator] === 'function'){
            resultado = await this.processarIteravelLinha(lBuilder, req.percursosIda, 
                                        this.todosPercursos, this.criarUseCase.adicionarPercursoIda);
            if(!resultado.sucesso) return resultado;
        }
        if (req.percursosVolta != null && typeof req.percursosVolta[Symbol.iterator] === 'function'){
            resultado = await this.processarIteravelLinha(lBuilder, req.percursosVolta, 
                                        this.todosPercursos, this.criarUseCase.adicionarPercursoVolta);
            if(!resultado.sucesso) return resultado;
        }
        const aux = await this.processarDadosOpcionais(req, lBuilder);
        resultado = aux == null? resultado : aux;
        if(!resultado.sucesso) return resultado;
        return this.criarUseCase.registarLinha(lBuilder, this.logger, this.todasLinhas);
    }

    private async processarDadosOpcionais(req: PedidoCriarLinhaDTO, lBuilder: LinhaBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado: DetalhesOperacaoDTO = null;
        if (req.percursosReforco != null && typeof req.percursosReforco[Symbol.iterator] === 'function' && req.percursosReforco.length > 0){
            resultado = await this.processarIteravelLinha(lBuilder, req.percursosReforco, 
                                        this.todosPercursos, this.criarUseCase.adicionarPercursoReforco);
            if(!resultado.sucesso) return resultado;
        }
        if (req.percursosVazio != null && typeof req.percursosVazio[Symbol.iterator] === 'function' && req.percursosVazio.length > 0){
            resultado = await this.processarIteravelLinha(lBuilder, req.percursosVazio, 
                                        this.todosPercursos, this.criarUseCase.adicionarPercursoVazio);
            if(!resultado.sucesso) return resultado;
        }
        if (req.tiposViaturas != null && typeof req.tiposViaturas[Symbol.iterator] === 'function' && req.tiposViaturas.length > 0){
            resultado = this.criarUseCase.aplicarTipoNecessidadeTipoViatura(lBuilder, this.logger, req.tipoNecessidadeTipoViatura);
            if(!resultado.sucesso) return resultado;
            resultado = await this.processarIteravelLinha(lBuilder, req.tiposViaturas, 
                                        this.todosTiposV, this.criarUseCase.adicionarTipoViatura);
            if(!resultado.sucesso) return resultado;
        }
        if (req.tiposTripulantes != null && typeof req.tiposTripulantes[Symbol.iterator] === 'function' && req.tiposTripulantes.length > 0){
            resultado = this.criarUseCase.aplicarTipoNecessidadeTipoTripulante(lBuilder, this.logger, req.tipoNecessidadeTipoTripulante);
            if(!resultado.sucesso) return resultado;
            resultado = await this.processarIteravelLinha(lBuilder, req.tiposTripulantes, 
                                        this.todosTiposT, this.criarUseCase.adicionarTipoTripulante);
            if(!resultado.sucesso) return resultado;
        }
        return resultado;
    }

    private async processarIteravelLinha<T, ID>(lBuilder: LinhaBuilder, iteravel: string[], repo: Repository<T, ID>,
                funcaoIter: (l: LinhaBuilder, l2: ServicoLogger, r: Repository<T, ID> , c: string) => Promise<DetalhesOperacaoDTO>){
        let resultado: DetalhesOperacaoDTO;
        for(const cod of iteravel){
            resultado = await funcaoIter(lBuilder, this.logger, repo, cod);
            if(!resultado.sucesso){
                break;
            }
        }
        return resultado;

    }
}