import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../director/ImportarDadosFicheiroUseCaseDirector";
import { ObjetoImportado } from "./ObjetoImportado";

export class PercursoImportado implements ObjetoImportado {

    public codigo: string;

    public segmentos: SegmentoImportado[];

    visitar(director: ImportarDadosFicheiroUseCaseDirector): Promise<DetalhesOperacaoDTO> {
        return director.processarPercurso(this);
    }
}

class SegmentoImportado {

    abreviaturaNoInicial: string;

    abreviaturaNoFinal: string;

    duracao: number;
    
    distancia: number;
}