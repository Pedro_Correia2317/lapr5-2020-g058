
import { DescricaoTipoTripulante } from '../../../../src/domain/tipoTripulante/model/DescricaoTipoTripulante';

test("testarDescricaoNulo", () => {
    expect(() => {new DescricaoTipoTripulante(null)}).toThrow(ReferenceError);
    expect(() => {new DescricaoTipoTripulante(undefined)}).toThrow(ReferenceError);
});

test("testarDescricaoVazio", () => {
    expect(() => {new DescricaoTipoTripulante('')}).toThrow(SyntaxError);
});

test("testarDescricaoDemasiadoComprido", () => {
    expect(() => {new DescricaoTipoTripulante('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
                         + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
                         + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
                         + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetbv')}).toThrow(SyntaxError); //201 chars
});

test("testarDescricaoUsualComCarateresInvalidos", () => {
    expect(() => {new DescricaoTipoTripulante('🌺Estação Principal N12 de Aguiar🌺')}).toThrow(SyntaxError);
});

test("testarDescricaoDemasiadoPequeno", () => {
    expect(() => {new DescricaoTipoTripulante('1')}).toThrow(SyntaxError);
});

test("testarDescricaoPequenoValido", () => {
    expect(new DescricaoTipoTripulante('12')).toBeInstanceOf(DescricaoTipoTripulante);
});

test("testarDescricaoGrandeValido", () => {
    expect(new DescricaoTipoTripulante('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
    + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
    + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
    + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetb')).toBeInstanceOf(DescricaoTipoTripulante); //200 chars
});

test("testarDescricaoUsualValido", () => {
    expect(new DescricaoTipoTripulante('Estacao Principal N12 de Aguiar')).toBeInstanceOf(DescricaoTipoTripulante);
});

test("testarDescricaoSoNumeros", () => {
    expect(new DescricaoTipoTripulante('15367363211')).toBeInstanceOf(DescricaoTipoTripulante);
});

test("testarDescricaoSoLetras", () => {
    expect(new DescricaoTipoTripulante('cbjjcjhfdHJJbbcjBJbjvUBbuubuUDwbbeS')).toBeInstanceOf(DescricaoTipoTripulante);
});

test("testarDescricaoSoEspacos", () => {
    expect(() => {new DescricaoTipoTripulante('         ')}).toThrow(SyntaxError);
});

test("testarDescricaoUsualComCarateresAcentos", () => {
    expect(new DescricaoTipoTripulante('Estação Principal N12 de Aguiar')).toBeInstanceOf(DescricaoTipoTripulante);
});