
import {TipoViatura} from '../../../domain/tipoViatura/model/TipoViatura';
import {TipoViaturaRepository} from '../../../repository/interfaces/TipoViaturaRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {TipoViaturaBuilder} from '../services/TipoViaturaBuilder';
import {CriarTipoViaturaUseCase} from './CriarTipoViaturaUseCase';


export class CriarTipoViaturaUseCaseImpl implements CriarTipoViaturaUseCase {

    async aplicarCodigo(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, todosTiposV: TipoViaturaRepository, codigo: string): Promise<DetalhesOperacaoDTO> {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            if (await todosTiposV.isCodigoUnico(codigo)){
                tBuilder.aplicarCodigo(codigo);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error(`Inserted repeated code ${codigo}`));
                resultados.mensagem = 'createVehicleType.repeatedCode';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createVehicleType.nullCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidCode';
            } else {
                throw err;
            }
        }

return resultados;
    }

    async aplicarDescricao(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, todosTiposV: TipoViaturaRepository, descricao: string): Promise<DetalhesOperacaoDTO> {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            if (await todosTiposV.isDescricaoUnico(descricao)){
                tBuilder.aplicarDescricao(descricao);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error(`Inserted repeated description ${descricao}`));
                resultados.mensagem = 'createVehicleType.repeatedDescription';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createVehicleType.nullDescription';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidDescription';
            } else {
                throw err;
            }
        }

return resultados;
    }

    aplicarAutonomia(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, autonomia: number): DetalhesOperacaoDTO {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            tBuilder.aplicarAutonomia(autonomia);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultados.mensagem = 'createVehicleType.NaNAutonomy';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidAutonomy';
            } else {
                throw err;
            }
        }

return resultados;
    }

    aplicarCusto(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, custo: number): DetalhesOperacaoDTO {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            tBuilder.aplicarCusto(custo);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultados.mensagem = 'createVehicleType.NaNCost';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidCost';
            } else {
                throw err;
            }
        }

return resultados;
    }

    aplicarVelocidadeMedia(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, velocidade: number): DetalhesOperacaoDTO {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            tBuilder.aplicarVelocidadeMedia(velocidade);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultados.mensagem = 'createVehicleType.NaNSpeed';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidSpeed';
            } else {
                throw err;
            }
        }

return resultados;
    }

    aplicarConsumo(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, consumo: number): DetalhesOperacaoDTO {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            tBuilder.aplicarConsumo(consumo);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultados.mensagem = 'createVehicleType.NaNConsumption';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidConsumption';
            } else {
                throw err;
            }
        }

return resultados;
    }

    aplicarEmissoes(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, emissoes: number): DetalhesOperacaoDTO {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            tBuilder.aplicarEmissoes(emissoes);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultados.mensagem = 'createVehicleType.NaNEmissions';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidEmissions';
            } else {
                throw err;
            }
        }

return resultados;
    }

    aplicarTipoCombustivel(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, tipoCombustivel: string): DetalhesOperacaoDTO {
        const resultados: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            tBuilder.aplicarTipoCombustivel(tipoCombustivel);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultados.mensagem = 'createVehicleType.invalidFuelType';
            } else {
                throw err;
            }
        }

return resultados;
    }

    async registarTipoViatura(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, todosTiposV: TipoViaturaRepository): Promise<DetalhesOperacaoDTO> {
        const resultado = new DetalhesOperacaoDTO();
        try {
            const tipoViatura: TipoViatura = tBuilder.obterResultado();
            if (await todosTiposV.save(tipoViatura)){
                resultado.sucesso = true;
                resultado.mensagem = 'createVehicleType.creationSuccess';
            } else {
                resultado.mensagem = 'createVehicleType.errorRegisteringVehicleType';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultado.mensagem = 'createVehicleType.vehicleTypeCreationFailure';
            } else {
                throw err;
            }
        }

return resultado;
    }

}