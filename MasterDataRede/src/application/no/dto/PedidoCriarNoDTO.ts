
export class PedidoCriarNoDTO {

    public abreviatura: string;

    public nome: string;

    public lat: number;

    public lon: number;

    public tipoNo: string;

}