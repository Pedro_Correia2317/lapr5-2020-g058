

import { TipoTripulanteDTO } from "../dto/TipoTripulanteDTO";
import { CodigoTipoTripulante } from "./CodigoTipoTripulante";
import { DescricaoTipoTripulante } from "./DescricaoTipoTripulante";

export class TipoTripulante {

    private codigo: CodigoTipoTripulante;

    private descricao: DescricaoTipoTripulante;

    constructor(codigo: CodigoTipoTripulante, descricao: DescricaoTipoTripulante){
        if(codigo == null || descricao == null){
            throw new SyntaxError('Null values inserted');
        }
        this.codigo = codigo;
        this.descricao = descricao;
    }

    toBaseDTO(): TipoTripulanteDTO {
        const dto: TipoTripulanteDTO = new TipoTripulanteDTO();
        dto.codigo = this.codigo.getValue();
        dto.descricao = this.descricao.getValue();
        return dto;
    }
}