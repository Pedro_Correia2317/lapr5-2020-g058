
export class CapacidadeVeiculos {

    private capacidade: number;

    constructor(capacidade: number){
        if (!Number.isInteger(capacidade)){
            throw new TypeError('The inserted capacity is not a integer');
        }
        if (capacidade < 0 || capacidade > 200){
            throw new SyntaxError(`${capacidade} is not a valid number`);
        }
        this.capacidade = capacidade;
    }

    getValue(): number {
        return this.capacidade;
    }

    toString(): string {
        return `CapacidadeVeiculos: [${this.capacidade}]`;
    }
}