
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualizadorLinhasModule } from '../../visualizadores/visualizador-linhas/visualizador-linhas.module';
import { VisualizacaoLinhasComponent } from './visualizacao-linhas.component';

const routes: Routes = [
    {path: '', component: VisualizacaoLinhasComponent}
];

@NgModule({
  declarations: [VisualizacaoLinhasComponent],
  imports: [CommonModule, VisualizadorLinhasModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoLinhasModule { }