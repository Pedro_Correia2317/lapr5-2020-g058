
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisualizadorTiposViaturasModule } from '../../visualizadores/visualizador-tipos-viaturas/visualizador-tipos-viaturas.module';

import { VisualizacaoTiposViaturasComponent } from './visualizacao-tipos-viaturas.component';

const routes: Routes = [
    {path: '', component: VisualizacaoTiposViaturasComponent}
];

@NgModule({
  declarations: [VisualizacaoTiposViaturasComponent],
  imports: [CommonModule, VisualizadorTiposViaturasModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoTiposViaturasModule { }