import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizadorLinhasComponent } from './visualizador-linhas.component';

describe('VisualizadorLinhasComponent', () => {
  let component: VisualizadorLinhasComponent;
  let fixture: ComponentFixture<VisualizadorLinhasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizadorLinhasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizadorLinhasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
