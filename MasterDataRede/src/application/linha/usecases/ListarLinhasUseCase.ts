
import { LinhaDTO } from "../../../domain/linha/dto/LinhaDTO";
import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaBuilder } from "../services/LinhaBuilder";

export interface ListarLinhasUseCase {

    obterLinhas(builder: LinhaBuilder, logger: ServicoLogger, linhaRepo: LinhaRepository, 
        sort: string, numPag: number, tamPag: number, codigo: string, nome: string): Promise<DetalhesPesquisaDTO<LinhaDTO>>;
}