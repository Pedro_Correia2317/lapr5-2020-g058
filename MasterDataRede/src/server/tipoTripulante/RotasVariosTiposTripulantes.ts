
import { Request, Response, Router } from "express";
import Container, { Inject, Service } from "typedi";
import { CriarTipoTripulanteUseCaseDirector } from "../../application/tipoTripulante/director/CriarTipoTripulanteUseCaseDirector";
import { TipoTripulanteBuilder } from "../../application/tipoTripulante/services/TipoTripulanteBuilder";
import { ListarTiposTripulantesUseCase } from "../../application/tipoTripulante/usecases/ListarTiposTripulantesUseCase";
import { ServicoLogger } from "../../application/utils/services/ServicoLogger";
import { TipoTripulanteRepository } from "../../repository/interfaces/TipoTripulanteRepository";

@Service()
export class RotasVariosTiposTripulante {

    @Inject("PrimaryCriarTipoTripulanteUseCaseDirector")
    private criarDirector: CriarTipoTripulanteUseCaseDirector;

    @Inject("PrimaryListarTiposTripulantesUseCase")
    private listarTiposTUseCase: ListarTiposTripulantesUseCase;

    @Inject("PrimaryTipoTripulanteBuilder")
    private builder: TipoTripulanteBuilder;

    @Inject("PrimaryTipoTripulanteRepository")
    private ttRepository: TipoTripulanteRepository;

    @Inject("PrimaryServicoLogger")
    private servicoLogger: ServicoLogger;
    
    insertRoutes = (router: Router): void => {
        router.route('/tiposTripulantes')
            .post(this.inserirRotaCriacaoTipoTripulante)
            .get(this.rotaListagemTiposTripulantes);
    }

    inserirRotaCriacaoTipoTripulante = async (req: Request, res: Response): Promise<void> => {
        const resultado = await this.criarDirector.processarPedido(req.body, Container.get("PrimaryTipoTripulanteBuilder"));
        res.send({'sucesso': resultado.sucesso, 'mensagem': resultado.mensagem});
    }

    rotaListagemTiposTripulantes = async (req: Request, res: Response): Promise<void> => {
        let num = req.query.page? Number.parseInt(req.query.page.toString()) : 1;
        num = num <= 0? 1 : num;
        let tam = req.query.size? Number.parseInt(req.query.size.toString()) : 24;
        tam = tam <= 0? 24 : tam;
        const desc = req.query.descricao? req.query.descricao.toString() : '';
        const sort = req.query.sort? req.query.sort.toString(): '';
        const resultado = await this.listarTiposTUseCase.listarTiposTripulantes(this.builder, this.servicoLogger, this.ttRepository, sort, num, tam, desc);
        res.send({'sucesso': resultado.sucesso, 'lista': resultado.lista, 'numeroTotal': resultado.count, 'mensagem': resultado.descricao});
    }
    
}