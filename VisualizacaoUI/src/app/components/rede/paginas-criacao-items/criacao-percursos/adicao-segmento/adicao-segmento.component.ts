
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NoDTO } from 'src/app/models/no/NoDTO';
import { PedidoCriarSegmentoDTO } from 'src/app/models/percursos/PedidoCriarSegmentoDTO';

@Component({
  selector: 'adicao-segmento',
  templateUrl: './adicao-segmento.component.html',
  styleUrls: ['./adicao-segmento.component.css']
})
export class AdicaoSegmentoComponent implements OnInit {

    duracaoSegmento: number;

    distanciaSegmento: number;

    abreviaturaNo: string;

    abreviaturaNoInicial: string;

    constructor(public dialogRef: MatDialogRef<AdicaoSegmentoComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) {
        this.abreviaturaNoInicial = data.abreviaturaNoInicial;
    }
  
    ngOnInit() {
    }

    onCancelarClick(){
        this.dialogRef.close();
    }

    adicionarDuracaoSegmento(duracao: any){
        var aux = duracao.target.value.split(':');
        var segundos = (+aux[0]) * 60 * 60 + (+aux[1]) * 60 + (+aux[2]);
        this.duracaoSegmento = segundos;
    }

    adicionarDistanciaSegmento(distancia: any){
        this.distanciaSegmento = distancia;
    }

    adicionarAbreviaturaNo(abreviaturaNo: string){
        this.abreviaturaNo = abreviaturaNo;
    }

    onAdicionarClick(){
        let dto = undefined;
        if(this.duracaoSegmento != null && this.abreviaturaNo != null 
                    && this.distanciaSegmento != null && this.abreviaturaNoInicial !== ''){
            dto = new PedidoCriarSegmentoDTO();
            dto.abreviaturaNoInicial = this.abreviaturaNoInicial;
            dto.abreviaturaNoFinal = this.abreviaturaNo;
            dto.distancia = this.distanciaSegmento;
            dto.duracao = this.duracaoSegmento;
        }
        this.dialogRef.close(dto);
    }

    onClickedNode(no: NoDTO){
        if(this.abreviaturaNoInicial === ''){
            this.abreviaturaNoInicial = no.abreviatura;
        } else {
            this.adicionarAbreviaturaNo(no.abreviatura);
        }
    }

}