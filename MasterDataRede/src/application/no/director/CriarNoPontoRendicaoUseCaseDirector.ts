
import { NoPontoRendicaoBuilder } from "../services/NoPontoRendicaoBuilder";
import { CriarNoPontoRendicaoUseCase } from "../usecases/CriarNoPontoRendicaoUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { NoRepository } from "../../../repository/interfaces/NoRepository";
import { PedidoCriarNoPontoRendicaoDTO } from '../dto/PedidoCriarNoPontoRendicaoDTO';
import { CriarNoUseCaseDirectorCommand } from "./CriarNoUseCaseDirectorCommand";

export class CriarNoPontoRendicaoUseCaseDirector implements CriarNoUseCaseDirectorCommand {

    private criarNoService: CriarNoPontoRendicaoUseCase;

    private logger: ServicoLogger;

    private todosNos: NoRepository;

    constructor(c: CriarNoPontoRendicaoUseCase, l: ServicoLogger, tn: NoRepository){
        this.criarNoService = c;
        this.logger = l;
        this.todosNos = tn;
    }
    
    async executar(req: PedidoCriarNoPontoRendicaoDTO, noBuilder: NoPontoRendicaoBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarNoService.aplicarAbreviatura(noBuilder, this.logger, this.todosNos, req.abreviatura);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarNome(noBuilder, this.logger, req.nome);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarCoordenadas(noBuilder, this.logger, req.lat, req.lon);
        if(req.tempos != null && typeof req.tempos[Symbol.iterator] === 'function'){
            for(const tempo of req.tempos){
                if(!resultado.sucesso) return resultado;
                resultado = await this.criarNoService.adicionarTempoDeslocacaoTripulacao(noBuilder, this.logger,
                                                        this.todosNos, tempo.tempo, tempo.abreviaturaNo);
            }
        }
        if(!resultado.sucesso) return resultado;
        return this.criarNoService.registarNo(noBuilder, this.logger, this.todosNos);
    }
}
