
import { NoPontoRendicaoBuilderMockSucesso, NoPontoRendicaoBuilderMockErro } from '../../mocks/no/NoPontoRendicaoBuilderMock';
import { NoRepositoryMockTrue, NoRepositoryMockFalse } from '../../mocks/no/NoRepositoryMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { CriarNoPontoRendicaoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoPontoRendicaoUseCaseImpl';
import { CriarNoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoUseCaseImpl';

const logger = new ServicoLoggerMock();
const noBuilderSucesso = new NoPontoRendicaoBuilderMockSucesso();
const noBuilderErro = new NoPontoRendicaoBuilderMockErro();
const noRepoTrue = new NoRepositoryMockTrue();
const noRepoFalse = new NoRepositoryMockFalse();
const useCase = new CriarNoPontoRendicaoUseCaseImpl(new CriarNoUseCaseImpl());

test('testarAplicarTempoDeslocacaoInvalido', async () => {
    expect((await useCase.adicionarTempoDeslocacaoTripulacao(noBuilderErro, logger, noRepoTrue, 0, '')).sucesso).toBe(false);
});

test('testarAplicarTempoDeslocacaoInvalido2', async () => {
    expect((await useCase.adicionarTempoDeslocacaoTripulacao(noBuilderErro, logger, noRepoFalse, 0, '')).sucesso).toBe(false);
});

test('testarAplicarTempoDeslocacaoDesconhecida', async () => {
    const funcaoAntiga = noBuilderSucesso.isNoIgual;
    noBuilderSucesso.isNoIgual = () => {return false;};
    expect((await useCase.adicionarTempoDeslocacaoTripulacao(noBuilderSucesso, logger, noRepoFalse, 0, '')).sucesso).toBe(false);
    noBuilderSucesso.isNoIgual = funcaoAntiga;
});

test('testarAplicarTempoDeslocacaoIgualANoCriado', async () => {
    expect((await useCase.adicionarTempoDeslocacaoTripulacao(noBuilderSucesso, logger, noRepoFalse, 0, '')).sucesso).toBe(true);
});

test('testarAplicarTempoDeslocacaoValido', async () => {
    expect((await useCase.adicionarTempoDeslocacaoTripulacao(noBuilderSucesso, logger, noRepoTrue, 0, '')).sucesso).toBe(true);
});

test('testarAplicarNomeInvalido', () => {
    expect(( useCase.aplicarNome(noBuilderErro, logger, "")).sucesso).toBe(false);
});

test('testarAplicarNomeValido', () => {
    expect(( useCase.aplicarNome(noBuilderSucesso, logger, "")).sucesso).toBe(true);
});

test('testarAplicarAbreviaturaInvalido', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderErro, logger, noRepoTrue, "")).sucesso).toBe(false);
});

test('testarAplicarAbreviaturaInvalido2', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderSucesso, logger, noRepoFalse, "")).sucesso).toBe(false);
});

test('testarAplicarAbreviaturaInvalido3', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderErro, logger, noRepoFalse, "")).sucesso).toBe(false);
});

test('testarAplicarAbreviaturaValido', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderSucesso, logger, noRepoTrue, "")).sucesso).toBe(true);
});

test('testarAplicarCoordenadasInvalido', () => {
    expect(( useCase.aplicarCoordenadas(noBuilderErro, logger, 0, 0)).sucesso).toBe(false);
});

test('testarAplicarCoordenadasValido', () => {
    expect(( useCase.aplicarCoordenadas(noBuilderSucesso, logger, 0, 0)).sucesso).toBe(true);
});

test('testarRegistarNoInvalido', async () => {
    expect(( await useCase.registarNo(noBuilderErro, logger, noRepoTrue)).sucesso).toBe(false);
});

test('testarRegistarNoErroRegisto', async () => {
    expect(( await useCase.registarNo(noBuilderSucesso, logger, noRepoFalse)).sucesso).toBe(false);
});

test('testarRegistarNoInvalido2', async () => {
    expect(( await useCase.registarNo(noBuilderErro, logger, noRepoFalse)).sucesso).toBe(false);
});

test('testarRegistarNoSucesso', async () => {
    expect(( await useCase.registarNo(noBuilderSucesso, logger, noRepoTrue)).sucesso).toBe(true);
});