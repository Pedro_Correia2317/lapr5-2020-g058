import { ObjetoImportado } from "../objetosImportacao/ObjetoImportado";
import { ImportadorDadosFicheiro } from "./ImportadorDadosFicheiro";
import { Parser } from 'xml2js';
import * as fs from 'fs';
import * as xpath from 'xml2js-xpath';
import { PercursoImportado } from "../objetosImportacao/PercursoImportado";
import { TipoViaturaImportado } from "../objetosImportacao/TipoViaturaImportado";
import { TipoTripulanteImportado } from "../objetosImportacao/TipoTripulanteImportado";
import { LinhaImportado } from "../objetosImportacao/LinhaImportado";
import { NoImportado } from "../objetosImportacao/NoImportado";
import { FicheiroNaoEncontradoError } from "../../utils/errors/FicheiroNaoEncontradoError";
import { ParsingFicheiroError } from "../../utils/errors/ParsingFicheiroError";

export class ImportadorDadosFicheiroGLX implements ImportadorDadosFicheiro {

    async importarDadosFicheiro(directorio: string, apagarFicheiro: boolean): Promise<ObjetoImportado[]> {
        const dados: ObjetoImportado[] = [];
        const ficheiro = this.lerFicheiro(directorio);
        await new Parser().parseStringPromise(ficheiro).then((resultado) => {
            const documentoRedes = this.obterDocumentoRedes(resultado);
            if(documentoRedes !== undefined){
                this.importarNos(documentoRedes, dados);
                this.importarPercursos(documentoRedes, dados);
                this.importarTiposVeiculos(documentoRedes, dados);
                this.importarTiposTripulantes(documentoRedes, dados);
                this.importarLinhas(documentoRedes, dados);
                if (apagarFicheiro){
                    fs.unlinkSync(directorio);
                }
            }
        }).catch((err) => {
            throw new ParsingFicheiroError(err.message);
        });
        return dados;
    }

    private lerFicheiro(directorio: string){
        try {
            const ficheiro = fs.readFileSync(directorio);
            return ficheiro;
        } catch (err) {
            throw new FicheiroNaoEncontradoError(err.message);
        }
    }

    private obterDocumentoRedes(resultado: any){
        if (resultado == null) return undefined;
        if (resultado.GlDocumentInfo == null) return undefined;
        if (resultado.GlDocumentInfo.world == null) return undefined;
        if (resultado.GlDocumentInfo.world[0].GlDocument == null) return undefined;
        if (resultado.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork == null) return undefined;
        if (resultado.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network == null) return undefined;
        return resultado.GlDocumentInfo.world[0].GlDocument[0].GlDocumentNetwork[0].Network[0];
    }

    private importarNos(documentoRedes: any, dados: ObjetoImportado[]): void {
        try{
            for(const no of documentoRedes.Nodes[0].Node){
                const noDTO = new NoImportado();
                noDTO.abreviatura = no.$.ShortName;
                noDTO.nome = no.$.Name;
                noDTO.lat = Number.parseFloat(no.$.Latitude);
                noDTO.lon = Number.parseFloat(no.$.Longitude);
                noDTO.temposDeslocacao = [];
                noDTO.capacidade = 0;
                noDTO.isEstacaoRecolha = 'True' === no.$.IsDepot;
                noDTO.isPontoRendicao = 'True' === no.$.IsReliefPoint;
                if (no.CrewTravelTimes[0].CrewTravelTime){
                    for(const tempo of no.CrewTravelTimes[0].CrewTravelTime){
                        const node = xpath.find(documentoRedes, `/Nodes/Node[@key='${tempo.$.Node}']`)[0];
                        noDTO.temposDeslocacao.push({
                            tempo: Number.parseFloat(tempo.$.Duration), 
                            abreviaturaNo: node.$.ShortName
                        });
                    }
                }
                dados.push(noDTO);
            }
        } catch (err){
            if (err instanceof TypeError){
                return;
            } else {
                throw err;
            }
        }
    }

    private importarPercursos(documentoRedes: any, dados: ObjetoImportado[]): void {
        try{
            for(const percurso of documentoRedes.Paths[0].Path){
                const percursoDTO = new PercursoImportado();
                percursoDTO.codigo = percurso.$.key;
                percursoDTO.segmentos = [];
                for(let i = 0; i < percurso.PathNodes[0].PathNode.length-1; i++){
                    const segmento1 = percurso.PathNodes[0].PathNode[i];
                    const segmento2 = percurso.PathNodes[0].PathNode[i+1];
                    const node1 = xpath.find(documentoRedes, `/Nodes/Node[@key='${segmento1.$.Node}']`)[0];
                    const node2 = xpath.find(documentoRedes, `/Nodes/Node[@key='${segmento2.$.Node}']`)[0];
                    percursoDTO.segmentos.push({
                        abreviaturaNoInicial: node1.$.ShortName,
                        abreviaturaNoFinal: node2.$.ShortName,
                        distancia: Number.parseFloat(segmento2.$.Distance),
                        duracao: Number.parseFloat(segmento2.$.Duration)
                    });
                }
                dados.push(percursoDTO);
            }
        } catch (err){
            if (err instanceof TypeError){
                return;
            } else {
                throw err;
            }
        }
    }

    private importarTiposVeiculos(documentoRedes: any, dados: ObjetoImportado[]){
        try {
            for(const tipoViatura of documentoRedes.VehicleTypes[0].VehicleType){
                const tipoViaturaDTO = new TipoViaturaImportado();
                tipoViaturaDTO.codigo = tipoViatura.$.key;
                tipoViaturaDTO.descricao = tipoViatura.$.Name;
                tipoViaturaDTO.autonomia = Number.parseFloat(tipoViatura.$.Autonomy);
                tipoViaturaDTO.custo = Number.parseFloat(tipoViatura.$.Cost);
                tipoViaturaDTO.velocidadeMedia = Number.parseFloat(tipoViatura.$.AverageSpeed);
                tipoViaturaDTO.consumo = Number.parseFloat(tipoViatura.$.Consumption);
                tipoViaturaDTO.emissoes = Number.parseFloat(tipoViatura.$.Emissions);
                tipoViaturaDTO.tipoCombustivel = this.obterTipoCombustivel(tipoViatura.$.EnergySource);
                dados.push(tipoViaturaDTO);
            }
        } catch (err){
            if (err instanceof TypeError){
                return;
            } else {
                throw err;
            }
        }
    }

    private importarTiposTripulantes(documentoRedes: any, dados: ObjetoImportado[]){
        try {
            for(const tipoTripulante of documentoRedes.DriverTypes[0].DriverType){
                const tipoTripulanteDTO = new TipoTripulanteImportado();
                tipoTripulanteDTO.codigo = tipoTripulante.$.key;
                tipoTripulanteDTO.descricao = tipoTripulante.$.name;
                dados.push(tipoTripulanteDTO);
            }
        } catch (err){
            if (err instanceof TypeError){
                return;
            } else {
                throw err;
            }
        }
    }

    private importarLinhas(documentoRedes: any, dados: ObjetoImportado[]){
        try {
            for(const linha of documentoRedes.Lines[0].Line){
                const linhaDTO = new LinhaImportado();
                linhaDTO.codigo = linha.$.key;
                linhaDTO.nome = linha.$.Name;
                linhaDTO.percursosIda = [];
                linhaDTO.percursosVolta = [];
                for(const percursoIda of xpath.find(linha, `/LinePaths/LinePath[@Orientation='Go']`)){
                    linhaDTO.percursosIda.push(percursoIda.$.Path);
                }
                for(const percursosVolta of xpath.find(linha, `/LinePaths/LinePath[@Orientation='Return']`)){
                    linhaDTO.percursosVolta.push(percursosVolta.$.Path);
                }
                dados.push(linhaDTO);
            }
        } catch (err){
            if (err instanceof TypeError){
                return;
            } else {
                throw err;
            }
        }
    }

    private obterTipoCombustivel(cod: string){
        switch(cod){
            case '23': return 'DIESEL';
            case '20': return 'GPL';
            case '50': return 'HIDROGENIO';
            case '75': return 'ELECTRICO';
            case '1': return 'GASOLINA';
        }
    }
    
}