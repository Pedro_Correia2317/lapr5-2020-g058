
import { DistanciaTotalPercurso } from '../../../../src/domain/percurso/model/DistanciaTotalPercurso';

test("testarDistanciaTotalPercursoNulo", () => {
    expect(() => {new DistanciaTotalPercurso(null)}).toThrow(TypeError);
    expect(() => {new DistanciaTotalPercurso(undefined)}).toThrow(TypeError);
});

test("testarDistanciaNegativo", () => {
    expect(() => {new DistanciaTotalPercurso(-1)}).toThrow(SyntaxError);
});

test("testarDistanciaNegativo2", () => {
    expect(() => {new DistanciaTotalPercurso(-12.4563)}).toThrow(SyntaxError);
});

test("testarDistanciaInvalido", () => {
    expect(() => {new DistanciaTotalPercurso(100000.001)}).toThrow(SyntaxError);
});

test("testarDistanciaInvalido2", () => {
    expect(() => {new DistanciaTotalPercurso(164000.99567)}).toThrow(SyntaxError);
});

test("testarDistanciaInvalido3", () => {
    expect(() => {new DistanciaTotalPercurso(0)}).toThrow(SyntaxError);
});

test("testarDistanciaPequenoValido", () => {
    expect(new DistanciaTotalPercurso(1)).toBeInstanceOf(DistanciaTotalPercurso);
});

test("testarDistanciaPequenoValido2", () => {
    expect(new DistanciaTotalPercurso(100000)).toBeInstanceOf(DistanciaTotalPercurso);
});

test("testarDistanciaPequenoValido3", () => {
    expect(new DistanciaTotalPercurso(0.4828)).toBeInstanceOf(DistanciaTotalPercurso);
});

test("testarDistanciaValido4", () => {
    expect(new DistanciaTotalPercurso(99999.9999)).toBeInstanceOf(DistanciaTotalPercurso);
});

test("testarDistanciaValido5", () => {
    expect(new DistanciaTotalPercurso(5000)).toBeInstanceOf(DistanciaTotalPercurso);
});

test("testarDistanciaValido6", () => {
    expect(new DistanciaTotalPercurso(100000.000001)).toBeInstanceOf(DistanciaTotalPercurso);
});