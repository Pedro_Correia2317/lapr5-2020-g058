import { NoParagemDTO } from "../dto/NoParagemDTO";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { CoordenadasNo } from "./CoordenadasNo";
import { No } from "./No";
import { NomeNo } from "./NomeNo";
import { TipoNo } from "./TipoNo";

export class NoParagem implements No {

    private nome: NomeNo;

    private coordenadas: CoordenadasNo;

    private abreviatura: AbreviaturaNo;

    constructor(abr: AbreviaturaNo, nome: NomeNo, coor: CoordenadasNo){
        if(nome == null || coor == null || abr == null){
            throw new SyntaxError('There are null values');
        }
        this.abreviatura = abr;
        this.nome = nome;
        this.coordenadas = coor;
    }

    toBaseDTO(): NoParagemDTO {
        const dto = new NoParagemDTO();
        dto.abreviatura = this.abreviatura.getValue();
        dto.nome = this.nome.getValue();
        dto.latitude = this.coordenadas.getLatitude();
        dto.longitude = this.coordenadas.getLongitude();
        dto.tipoNo = TipoNo.NO_PARAGEM.toBaseDTO().nome;
        return dto;
    }

    tipoNo(): TipoNo {
        return TipoNo.NO_PARAGEM;
    }

}