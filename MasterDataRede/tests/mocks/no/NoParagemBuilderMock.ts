
import { NoParagemBuilder } from '../../../src/application/no/services/NoParagemBuilder';
import { AbreviaturaNo } from '../../../src/domain/no/model/AbreviaturaNo';
import { CoordenadasNo } from '../../../src/domain/no/model/CoordenadasNo';
import { NomeNo } from '../../../src/domain/no/model/NomeNo';
import { NoParagem } from '../../../src/domain/no/model/NoParagem';

export class NoParagemBuilderMockSucesso implements NoParagemBuilder {

    reset(): void {
    }

    obterResultado(): NoParagem {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(23.4567, 12);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        return new NoParagem(abreviaturaNo, nomeNo, coordenadasNo);
    }
    aplicarCodigo(codigo: string): void {
    }

    aplicarNome(nome: string): void {
    }

    aplicarAbreviatura(abreviatura: string): void {
    }

    aplicarCoordenadas(lat: number, lon: number): void {
    }

}

export class NoParagemBuilderMockErro implements NoParagemBuilder {

    reset(): void {
    }

    obterResultado(): NoParagem {
        throw new SyntaxError('Teste');
    }
    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Teste');
    }
    aplicarNome(nome: string): void {
        throw new SyntaxError('Teste');
    }
    aplicarAbreviatura(abreviatura: string): void {
        throw new SyntaxError('Teste');
    }
    aplicarCoordenadas(lat: number, lon: number): void {
        throw new TypeError('Teste');    
    }

}