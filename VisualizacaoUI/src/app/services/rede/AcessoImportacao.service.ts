
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';

@Injectable({
  providedIn: 'root'
})
export class AcessoImportacaoService {

  private enderecoHttp: string = `${Configurations.API_SERVER_ADDRESS}/importacao`;

  constructor(private http: HttpClient) { }

  enviarFicheiro(ficheiro: File): Observable<DetalhesOperacaoDTO[]> {
    const formData: FormData = new FormData();
    formData.append('ficheiro', ficheiro, ficheiro.name);
    return this.http.post<DetalhesOperacaoDTO[]>(this.enderecoHttp, formData, {
      observe: 'body'
    });
  }
}