

import Mongoose, { Schema } from 'mongoose';
import { TipoViaturaBuilder } from '../../application/tipoViatura/services/TipoViaturaBuilder';
import { AcessoDadosError } from '../../application/utils/errors/AcessoDadosError';
import { CodigoTipoViatura } from "../../domain/tipoViatura/model/CodigoTipoViatura";
import { TipoViatura } from "../../domain/tipoViatura/model/TipoViatura";
import { TipoViaturaRepository } from "../interfaces/TipoViaturaRepository";
import { ITipoViatura } from './models/ITipoViatura';
import { MongooseRepositoryImpl } from './RepositoryImpl';


export class TipoViaturaRepositoryImpl implements TipoViaturaRepository {

    private static readonly SCHEMA_TIPO_VIATURA: Mongoose.Schema = new Schema({
        'codigo': {'type': String, 'required': true, 'unique': true},
        'descricao': {'type': String, 'required': true, 'unique': true},
        'autonomia': {'type': Number, 'required': true},
        'velocidadeMedia': {'type': Number, 'required': true},
        'custo': {'type': Number, 'required': true},
        'consumo': {'type': Number, 'required': true},
        'emissoes': {'type': Number, 'required': true},
        'tipoCombustivel': {'type': String, 'required': true},
    });

    private static readonly MODELO_TIPO_VIATURA = Mongoose.model<ITipoViatura>('TipoViatura', TipoViaturaRepositoryImpl.SCHEMA_TIPO_VIATURA);
                    
    private static BASE_REPO: MongooseRepositoryImpl = new MongooseRepositoryImpl();

    private modelarTipoViatura(tipoV: TipoViatura): ITipoViatura {
        const modelo = new TipoViaturaRepositoryImpl.MODELO_TIPO_VIATURA(),
        tipoVDTO = tipoV.toBaseDTO();
        modelo.codigo = tipoVDTO.codigo;
        modelo.descricao = tipoVDTO.descricao;
        modelo.autonomia = tipoVDTO.autonomia;
        modelo.velocidadeMedia = tipoVDTO.velocidadeMedia;
        modelo.custo = tipoVDTO.custo;
        modelo.consumo = tipoVDTO.consumo;
        modelo.emissoes = tipoVDTO.emissoes;
        modelo.tipoCombustivel = tipoVDTO.tipoCombustivel.descricao;
        return modelo;
    }

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return await TipoViaturaRepositoryImpl.BASE_REPO.isUnico(TipoViaturaRepositoryImpl.MODELO_TIPO_VIATURA, {codigo});
    }

    async isDescricaoUnico(descricao: string): Promise<boolean> {
        return await TipoViaturaRepositoryImpl.BASE_REPO.isUnico(TipoViaturaRepositoryImpl.MODELO_TIPO_VIATURA, {descricao});
    }

    async existe(codigo: string): Promise<boolean> {
        return await TipoViaturaRepositoryImpl.MODELO_TIPO_VIATURA.exists({codigo});
    }

    save(objecto: TipoViatura): Promise<boolean> {
        const modeloTipoV: ITipoViatura = this.modelarTipoViatura(objecto);
        return TipoViaturaRepositoryImpl.BASE_REPO.save(modeloTipoV);
    }

    private mapearDataModel(builder: TipoViaturaBuilder, dataModel: ITipoViatura): TipoViatura {
        builder.aplicarCodigo(dataModel.codigo);
        builder.aplicarDescricao(dataModel.descricao);
        builder.aplicarAutonomia(dataModel.autonomia);
        builder.aplicarCusto(dataModel.custo);
        builder.aplicarConsumo(dataModel.consumo);
        builder.aplicarVelocidadeMedia(dataModel.velocidadeMedia);
        builder.aplicarTipoCombustivel(dataModel.tipoCombustivel);
        builder.aplicarEmissoes(dataModel.emissoes);
        return builder.obterResultado();
    }

    async obterTiposViaturas(builder: TipoViaturaBuilder, sort: string, numPag: number, 
                        tamPag: number, descricao: string): Promise<TipoViatura[]> {
        const nos: TipoViatura[] = [];
        const regexDesc = new RegExp(descricao, 'i');
        await TipoViaturaRepositoryImpl.MODELO_TIPO_VIATURA.find({descricao: regexDesc}).sort(sort)
                                        .skip((numPag-1) * tamPag).limit(tamPag).then((results) => {
            results.forEach((no) => {
                nos.push(this.mapearDataModel(builder, no));
            });
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return nos;
    }

    async countComFiltros(descricao: string): Promise<number> {
        let numero: number;
        const regexDesc = new RegExp(descricao, 'i');
        await TipoViaturaRepositoryImpl.MODELO_TIPO_VIATURA.countDocuments({descricao: regexDesc}).exec().then((num) => {
            numero = num;
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return numero;
    }

    findById(codigo: CodigoTipoViatura): Promise<TipoViatura> {
        throw new Error("Method not implemented.");
    }
    findPage(numPagina: number, tamanhoPagina: number): Promise<TipoViatura[]> {
        throw new Error("Method not implemented.");
    }
    count(): Promise<number> {
        throw new Error("Method not implemented.");
    }

}