
import { Linha } from "../../../domain/linha/model/Linha";
import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { TipoTripulanteRepository } from "../../../repository/interfaces/TipoTripulanteRepository";
import { TipoViaturaRepository } from "../../../repository/interfaces/TipoViaturaRepository";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaBuilder } from "../services/LinhaBuilder";
import { CriarLinhaUseCase } from "./CriarLinhaUseCase";

export class CriarLinhaUseCaseImpl implements CriarLinhaUseCase {
    
    async aplicarCodigo(lBuilder: LinhaBuilder, logger: ServicoLogger, todasLinhas: LinhaRepository, codigo: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todasLinhas.isCodigoUnico(codigo)){
                lBuilder.aplicarCodigo(codigo);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Repeated Line Code ' + codigo));
                resultados.mensagem = 'createLine.repeatedCode';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }
    
    aplicarNome(lBuilder: LinhaBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO {
        const resultados = new DetalhesOperacaoDTO();
        try {
            lBuilder.aplicarNome(nome);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullName';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidName';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async adicionarPercursoIda(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todosPercursos.existe(codigoP)){
                lBuilder.adicionarPercursoIda(codigoP);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unknown Path Code ' + codigoP));
                resultados.mensagem = 'createLine.unknownPath';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullPathCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidPathCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async adicionarPercursoVolta(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todosPercursos.existe(codigoP)){
                lBuilder.adicionarPercursoVolta(codigoP);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unknown Path Code ' + codigoP));
                resultados.mensagem = 'createLine.unknownPath';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullPathCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidPathCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async adicionarPercursoReforco(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todosPercursos.existe(codigoP)){
                lBuilder.adicionarPercursoReforco(codigoP);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unknown Path Code ' + codigoP));
                resultados.mensagem = 'createLine.unknownPath';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullPathCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidPathCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async adicionarPercursoVazio(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todosPercursos.existe(codigoP)){
                lBuilder.adicionarPercursoVazio(codigoP);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unknown Path Code ' + codigoP));
                resultados.mensagem = 'createLine.unknownPath';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullPathCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidPathCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async adicionarTipoTripulante(lBuilder: LinhaBuilder, logger: ServicoLogger, todasTiposT: TipoTripulanteRepository, codigoT: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todasTiposT.existe(codigoT)){
                lBuilder.adicionarTipoTripulante(codigoT);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unknown Driver Type Code ' + codigoT));
                resultados.mensagem = 'createLine.unknownDriverType';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullDriverTypeCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidDriverTypeCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async adicionarTipoViatura(lBuilder: LinhaBuilder, logger: ServicoLogger, todasTiposV: TipoViaturaRepository, codigoV: string): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            if (await todasTiposV.existe(codigoV)){
                lBuilder.adicionarTipoViatura(codigoV);
                resultados.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unknown Vehicle Type Code ' + codigoV));
                resultados.mensagem = 'createLine.unknownVehicleType';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultados.mensagem = 'createLine.nullVehicleTypeCode';
            } else if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidVehicleTypeCode';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    aplicarTipoNecessidadeTipoTripulante(lBuilder: LinhaBuilder, logger: ServicoLogger, tipoT: string): DetalhesOperacaoDTO {
        const resultados = new DetalhesOperacaoDTO();
        try {
            lBuilder.adicionarTipoNecessidadeTipoTripulante(tipoT);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidDriverNeedType';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    aplicarTipoNecessidadeTipoViatura(lBuilder: LinhaBuilder, logger: ServicoLogger, tipoV: string): DetalhesOperacaoDTO {
        const resultados = new DetalhesOperacaoDTO();
        try {
            lBuilder.adicionarTipoNecessidadeTipoViatura(tipoV);
            resultados.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidVehicleNeedType';
            } else {
                throw err;
            }
        }
        return resultados;
    }

    async registarLinha(lBuilder: LinhaBuilder, logger: ServicoLogger, todasLinhas: LinhaRepository): Promise<DetalhesOperacaoDTO> {
        const resultados = new DetalhesOperacaoDTO();
        try {
            const linha: Linha = lBuilder.obterResultado();
            if (await todasLinhas.save(linha)){
                resultados.sucesso = true;
                resultados.mensagem = 'createLine.creationSuccess';
            } else {
                resultados.mensagem = 'createLine.errorRegisteringLine';
            }
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultados.mensagem = 'createLine.invalidLineCreation';
            } else if (err instanceof ReferenceError) {
                resultados.mensagem = 'createLine.nullValues';
            } else {
                throw err;
            }
        }
        return resultados;
    }
    
}