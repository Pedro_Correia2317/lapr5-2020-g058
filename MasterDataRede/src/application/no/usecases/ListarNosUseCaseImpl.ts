
import { NoDTO } from "../../../domain/no/dto/NoDTO";
import { TipoNoDTO } from "../../../domain/no/dto/TipoNoDTO";
import { TipoNo } from "../../../domain/no/model/TipoNo";
import { NoRepository } from "../../../repository/interfaces/NoRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { NoBuilder } from "../services/NoBuilder";
import { ListarNosUseCase } from "./ListarNosUseCase";

export class ListarNosUseCaseImpl implements ListarNosUseCase {

    obterTiposNos(): TipoNoDTO[] {
        return TipoNo.valuesInDTO();
    }

    async obterNos(builders: Map<string, NoBuilder>, logger: ServicoLogger, noRepo: NoRepository,
                    sort: string, numPag: number, tamPag: number, codigo: string, nome: string): Promise<DetalhesPesquisaDTO<NoDTO>> {
        const resultados = new DetalhesPesquisaDTO<NoDTO>();
        await noRepo.obterNos(builders, sort, numPag, tamPag, codigo, nome).then((nos) => {
            nos.forEach((no) => {
                resultados.lista.push(no.toBaseDTO());
            });
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findNodes.errorFindingNodes';
        });
        await noRepo.countComFiltros(codigo, nome).then((numero) => {
            resultados.count = numero;
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findNodes.errorCountingNodes';
        });
        if (!resultados.descricao){
            resultados.sucesso = true;
            resultados.descricao = 'findNodes.searchSuccess';
        }
        return resultados;
    }
    
}