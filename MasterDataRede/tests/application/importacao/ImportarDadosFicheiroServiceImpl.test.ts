
import { ImportarDadosFicheiroServiceImpl } from '../../../src/application/importacao/ImportarDadosFicheiroServiceImpl';
import { ImportadorDadosFicheiroContextMockFalse, ImportadorDadosFicheiroContextMockTrue } from '../../mocks/importacao/ImportadorDadosFicheiroContextMock';
import { ImportadorDadosFicheiroMockFicheiroInvalido, ImportadorDadosFicheiroMockSemValores, ImportadorDadosFicheiroMockSucesso } from '../../mocks/importacao/ImportadorDadosFicheiroMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';

const contextFalse = new ImportadorDadosFicheiroContextMockFalse();
const servico = new ImportarDadosFicheiroServiceImpl();
const logger = new ServicoLoggerMock();

test('testarFicheiroComExtensaoDesconhecida', async () => {
    expect((await servico.importarDadosFicheiro(contextFalse, logger, '', false)).sucesso).toBe(false);
});

test('testarFicheiroComExtensaoReconhecidaMasNaoLegivel', async () => {
    const context = new ImportadorDadosFicheiroContextMockTrue();
    context.obterImportadorAdequado = (dir: string) => {return new ImportadorDadosFicheiroMockFicheiroInvalido()};
    expect((await servico.importarDadosFicheiro(context, logger, '', false)).sucesso).toBe(false);
});

test('testarFicheiroComExtensaoReconhecidaMasFormatoInvalido', async () => {
    const context = new ImportadorDadosFicheiroContextMockTrue();
    context.obterImportadorAdequado = (dir: string) => {return new ImportadorDadosFicheiroMockSemValores()};
    expect((await servico.importarDadosFicheiro(context, logger, '', false)).sucesso).toBe(false);
});

test('testarFicheiroSucesso', async () => {
    const context = new ImportadorDadosFicheiroContextMockTrue();
    context.obterImportadorAdequado = (dir: string) => {return new ImportadorDadosFicheiroMockSucesso()};
    expect((await servico.importarDadosFicheiro(context, logger, '', false)).sucesso).toBe(true);
});