

import { CriarLinhaUseCaseImpl } from '../../../src/application/linha/usecases/CriarLinhaUseCaseImpl';
import { LinhaRepositoryMockFalse, LinhaRepositoryMockTrue } from '../../mocks/linha/linhaRepositoryMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { LinhaBuilderMockErro, LinhaBuilderMockSucesso } from '../../mocks/linha/linhaBuilderMock';
import { PercursoRepositoryMockFalse, PercursoRepositoryMockTrue } from '../../mocks/percurso/percursoRepositoryMock';
import { TipoViaturaRepositoryMockFalse, TipoViaturaRepositoryMockTrue } from '../../mocks/tipoViatura/tipoViaturaRepositoryMock';
import { TipoTripulanteRepositoryMockFalse, TipoTripulanteRepositoryMockTrue } from '../../mocks/tipoTripulante/tipoTripulanteRepositoryMock';

const lBuilderErro = new LinhaBuilderMockErro();
const lBuilderSucesso = new LinhaBuilderMockSucesso();
const logger = new ServicoLoggerMock();
const lRepoTrue = new LinhaRepositoryMockTrue();
const lRepoFalse = new LinhaRepositoryMockFalse();
const pRepoTrue = new PercursoRepositoryMockTrue();
const pRepoFalse = new PercursoRepositoryMockFalse();
const tvRepoTrue = new TipoViaturaRepositoryMockTrue();
const tvRepoFalse = new TipoViaturaRepositoryMockFalse();
const ttRepoTrue = new TipoTripulanteRepositoryMockTrue();
const ttRepoFalse = new TipoTripulanteRepositoryMockFalse();
const useCase = new CriarLinhaUseCaseImpl();

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(lBuilderErro, logger, lRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoRepetido", async () => {
    expect((await useCase.aplicarCodigo(lBuilderSucesso, logger, lRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(lBuilderErro, logger, lRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoValido", async () => {
    expect((await useCase.aplicarCodigo(lBuilderSucesso, logger, lRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarNomeInvalido", async () => {
    expect((useCase.aplicarNome(lBuilderErro, logger, "")).sucesso).toBe(false);
});

test("testarAplicarNomeValido", async () => {
    expect((useCase.aplicarNome(lBuilderSucesso, logger, "")).sucesso).toBe(true);
});

test("testarAplicarPercursoIdaInvalido", async () => {
    expect((await useCase.adicionarPercursoIda(lBuilderErro, logger, pRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoIdaRepetido", async () => {
    expect((await useCase.adicionarPercursoIda(lBuilderSucesso, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoIdaInvalido", async () => {
    expect((await useCase.adicionarPercursoIda(lBuilderErro, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoIdaValido", async () => {
    expect((await useCase.adicionarPercursoIda(lBuilderSucesso, logger, pRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarPercursoVoltaInvalido", async () => {
    expect((await useCase.adicionarPercursoVolta(lBuilderErro, logger, pRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoVoltaRepetido", async () => {
    expect((await useCase.adicionarPercursoVolta(lBuilderSucesso, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoVoltaInvalido", async () => {
    expect((await useCase.adicionarPercursoVolta(lBuilderErro, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoVoltaValido", async () => {
    expect((await useCase.adicionarPercursoVolta(lBuilderSucesso, logger, pRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarPercursoReforcoInvalido", async () => {
    expect((await useCase.adicionarPercursoReforco(lBuilderErro, logger, pRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoReforcoRepetido", async () => {
    expect((await useCase.adicionarPercursoReforco(lBuilderSucesso, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoReforcoInvalido", async () => {
    expect((await useCase.adicionarPercursoReforco(lBuilderErro, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoReforcoValido", async () => {
    expect((await useCase.adicionarPercursoReforco(lBuilderSucesso, logger, pRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarPercursoVazioInvalido", async () => {
    expect((await useCase.adicionarPercursoVazio(lBuilderErro, logger, pRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoVazioRepetido", async () => {
    expect((await useCase.adicionarPercursoVazio(lBuilderSucesso, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoVazioInvalido", async () => {
    expect((await useCase.adicionarPercursoVazio(lBuilderErro, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarPercursoVazioValido", async () => {
    expect((await useCase.adicionarPercursoVazio(lBuilderSucesso, logger, pRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarTipoViaturaInvalido", async () => {
    expect((await useCase.adicionarTipoViatura(lBuilderErro, logger, tvRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarTipoViaturaRepetido", async () => {
    expect((await useCase.adicionarTipoViatura(lBuilderSucesso, logger, tvRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarTipoViaturaInvalido", async () => {
    expect((await useCase.adicionarTipoViatura(lBuilderErro, logger, tvRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarTipoViaturaValido", async () => {
    expect((await useCase.adicionarTipoViatura(lBuilderSucesso, logger, tvRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarTipoTripulanteInvalido", async () => {
    expect((await useCase.adicionarTipoTripulante(lBuilderErro, logger, ttRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarTipoTripulanteRepetido", async () => {
    expect((await useCase.adicionarTipoTripulante(lBuilderSucesso, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarTipoTripulanteInvalido", async () => {
    expect((await useCase.adicionarTipoTripulante(lBuilderErro, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarTipoTripulanteValido", async () => {
    expect((await useCase.adicionarTipoTripulante(lBuilderSucesso, logger, ttRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarTipoNecessidadeTipoViaturaInvalido", async () => {
    expect((useCase.aplicarTipoNecessidadeTipoViatura(lBuilderErro, logger, "")).sucesso).toBe(false);
});

test("testarAplicarTipoNecessidadeTipoViaturaValido", async () => {
    expect((useCase.aplicarTipoNecessidadeTipoViatura(lBuilderSucesso, logger, "")).sucesso).toBe(true);
});

test("testarAplicarTipoNecessidadeTipoTripulanteInvalido", async () => {
    expect((useCase.aplicarTipoNecessidadeTipoTripulante(lBuilderErro, logger, "")).sucesso).toBe(false);
});

test("testarAplicarTipoNecessidadeTipoTripulanteValido", async () => {
    expect((useCase.aplicarTipoNecessidadeTipoTripulante(lBuilderSucesso, logger, "")).sucesso).toBe(true);
});

test("testarRegistarLinhaInvalida", async () => {
    expect((await useCase.registarLinha(lBuilderErro, logger, lRepoTrue)).sucesso).toBe(false);
});

test("testarRegistarLinhaErroAcessoBD", async () => {
    expect((await useCase.registarLinha(lBuilderSucesso, logger, lRepoFalse)).sucesso).toBe(false);
});

test("testarRegistarLinhaInvalido2", async () => {
    expect((await useCase.registarLinha(lBuilderErro, logger, lRepoFalse)).sucesso).toBe(false);
});

test("testarRegistarLinhaValido", async () => {
    expect((await useCase.registarLinha(lBuilderSucesso, logger, lRepoTrue)).sucesso).toBe(true);
});