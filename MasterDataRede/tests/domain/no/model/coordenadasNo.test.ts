
import { CoordenadasNo } from '../../../../src/domain/no/model/CoordenadasNo';

test("testarCoordenadasNulo", () => {
    expect(() => {new CoordenadasNo(null, 12)}).toThrow(TypeError);
    expect(() => {new CoordenadasNo(32.12, null)}).toThrow(TypeError);
    expect(() => {new CoordenadasNo(23.2356, undefined)}).toThrow(TypeError);
    expect(() => {new CoordenadasNo(undefined, 43.221)}).toThrow(TypeError);
});

test("testarCoordenadasValoresPositivos", () => {
    expect(new CoordenadasNo(11.2334, 24.35642)).toBeInstanceOf(CoordenadasNo);
    expect(new CoordenadasNo(13, 28)).toBeInstanceOf(CoordenadasNo);
    expect(new CoordenadasNo(8.322, 0)).toBeInstanceOf(CoordenadasNo);
});

test("testarCoordenadasValoresNegativos", () => {
    expect(new CoordenadasNo(-11.2334, 24.35642)).toBeInstanceOf(CoordenadasNo);
    expect(new CoordenadasNo(13, -28)).toBeInstanceOf(CoordenadasNo);
    expect(new CoordenadasNo(-8.322, 0)).toBeInstanceOf(CoordenadasNo);
    expect(new CoordenadasNo(-8.322, -4.6783)).toBeInstanceOf(CoordenadasNo);
});