

export class ConsumoTipoViatura {

    private consumo: number;

    constructor(consumo: number){
        if (!Number.isFinite(consumo)){
            throw new TypeError(`The consumption is not a number: ${consumo}`);
        }
        const aux = parseFloat(consumo.toFixed(3));
        if (aux <= 0 || aux > 999){
            throw new SyntaxError(`The consumption ${aux} is <= 0 or > 999`);
        }
        this.consumo = aux;
    }

    getValue(): number {
        return this.consumo;
    }

    toString(): string {
        return `ConsumoTipoViatura: [${this.consumo}]`;
    }

}