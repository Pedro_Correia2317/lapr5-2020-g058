import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../director/ImportarDadosFicheiroUseCaseDirector";
import { ObjetoImportado } from "./ObjetoImportado";

export class LinhaImportado implements ObjetoImportado {

    public codigo: string;

    public nome: string;

    public percursosIda: string[];

    public percursosVolta: string[];

    visitar(director: ImportarDadosFicheiroUseCaseDirector): Promise<DetalhesOperacaoDTO> {
        return director.processarLinha(this);
    }

}