
import { LinhaBuilder } from "../../application/linha/services/LinhaBuilder";
import { CodigoLinha } from "../../domain/linha/model/CodigoLinha";
import { Linha } from "../../domain/linha/model/Linha";
import { Repository } from "./Repository";


export interface LinhaRepository extends Repository<Linha, CodigoLinha> {

    isCodigoUnico(codigo: string): Promise<boolean>;

    obterLinhas(builder: LinhaBuilder, sort: string, numPag: number, 
                    tamPag: number, codigo: string, nome: string): Promise<Linha[]>;

    countComFiltros(codigo: string, nome: string): Promise<number>;

    findByCodigo(builder: LinhaBuilder, codigo: string): Promise<Linha>;

}