
import { NoBuilder } from "../../../src/application/no/services/NoBuilder";
import { AbreviaturaNo } from "../../../src/domain/no/model/AbreviaturaNo";
import { No } from "../../../src/domain/no/model/No";
import { NoRepository } from "../../../src/repository/interfaces/NoRepository";
import { AcessoDadosError } from '../../../src/application/utils/errors/AcessoDadosError';

export class NoRepositoryMockTrue implements NoRepository {

    async isAbreviaturaUnico(codigo: string): Promise<boolean> {
        return true;
    }

    async existeNo(abreviatura: string): Promise<boolean> {
        return true;
    }

    async save(objecto: No): Promise<boolean> {
        return true;
    }

    async findById(codigo: AbreviaturaNo): Promise<No> {
        throw new Error("Method not implemented.");
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<No[]> {
        throw new Error("Method not implemented.");
    }

    async count(): Promise<number> {
        return 0;
    }

    async obterNos(builders: Map<string, NoBuilder>, sort: string, numPag: number, tamPag: number, abreviatura: string, nome: string): Promise<No[]> {
        return [];
    }

    async countComFiltros(abreviatura: string, nome: string): Promise<number> {
        return 0;
    }

}

export class NoRepositoryMockFalse implements NoRepository {

    async isAbreviaturaUnico(codigo: string): Promise<boolean> {
        return false;
    }
    async existeNo(abreviatura: string): Promise<boolean> {
        return false;
    }
    async save(objecto: No): Promise<boolean> {
        return false;
    }
    async findById(codigo: AbreviaturaNo): Promise<No> {
        throw new Error("Method not implemented.");
    }
    async findPage(numPagina: number, tamanhoPagina: number): Promise<No[]> {
        throw new Error("Method not implemented.");
    }
    async count(): Promise<number> {
        throw new Error("Method not implemented.");
    }

    async obterNos(builders: Map<string, NoBuilder>, sort: string, numPag: number, tamPag: number, abreviatura: string, nome: string): Promise<No[]> {
        throw new AcessoDadosError('Teste');
    }

    async countComFiltros(abreviatura: string, nome: string): Promise<number> {
        throw new AcessoDadosError('Teste');
    }

}