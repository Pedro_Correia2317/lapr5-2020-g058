
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';
import { VisualizadorNosComponent } from './visualizador-nos.component';

@NgModule({
  declarations: [VisualizadorNosComponent],
  imports: [VisualizacaoItemsModule, CommonModule],
  exports: [VisualizadorNosComponent, RouterModule]
})
export class VisualizadorNosModule { }
