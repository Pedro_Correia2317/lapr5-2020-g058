
export class NoDTO {

    public nome: string;

    public abreviatura: string;

    public latitude: number;

    public longitude: number;

    public tipoNo: string;
    
}