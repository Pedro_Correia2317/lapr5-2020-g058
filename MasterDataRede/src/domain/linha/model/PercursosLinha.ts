import { CodigoPercurso } from "../../percurso/model/CodigoPercurso";
import { PercursoLinhaDTO } from "../dto/PercursoLinhaDTO";


export class PercursosLinha {
    
    private percursosIda: CodigoPercurso[];

    private percursosVolta: CodigoPercurso[];

    private percursosReforco: CodigoPercurso[];

    private percursosVazio: CodigoPercurso[];

    constructor(pIda: CodigoPercurso[], pVolta: CodigoPercurso[], 
                    pReforco: CodigoPercurso[], pVazio: CodigoPercurso[]){
        if (pIda == null || pVolta == null || pReforco == null || pVazio == null){
            throw new ReferenceError('The inserted description is null');
        }
        if(pIda.includes(null) || pVolta.includes(null) || pReforco.includes(null) || pVazio.includes(null)){
            throw new ReferenceError('Null paths detected.');
        }
        if(pIda.includes(undefined) || pVolta.includes(undefined) || pReforco.includes(undefined) || pVazio.includes(undefined)){
            throw new ReferenceError('Null paths detected.');
        }
        if(pIda.length < 1 || pVolta.length < 1){
            throw new SyntaxError('There needs to be a return path or go path');
        }
        this.percursosIda = pIda.slice();
        this.percursosVolta = pVolta.slice();
        this.percursosReforco = pReforco.slice();
        this.percursosVazio = pVazio.slice();
    }

    getPercursosIda(): CodigoPercurso[] {
        return this.percursosIda.slice();
    }

    getPercursosVolta(): CodigoPercurso[] {
        return this.percursosVolta.slice();
    }

    getPercursosReforco(): CodigoPercurso[] {
        return this.percursosReforco.slice();
    }

    getPercursosVazio(): CodigoPercurso[] {
        return this.percursosVazio.slice();
    }

    toPercursosIdaDTO(): PercursoLinhaDTO[] {
        return this.converterParaListaDTO(this.percursosIda);
    }

    toPercursosVoltaDTO(): PercursoLinhaDTO[] {
        return this.converterParaListaDTO(this.percursosVolta);
    }

    toPercursosReforcoDTO(): PercursoLinhaDTO[] {
        return this.converterParaListaDTO(this.percursosReforco);
    }
    
    toPercursosVazioDTO(): PercursoLinhaDTO[] {
        return this.converterParaListaDTO(this.percursosVazio);
    }

    private converterParaListaDTO(lista: CodigoPercurso[]): PercursoLinhaDTO[] {
        const listaDTO = [];
        lista.forEach((cod: CodigoPercurso) => {
            const percDTO = new PercursoLinhaDTO();
            percDTO.codigoPercurso = cod.getValue();
            listaDTO.push(percDTO);
        });
        return listaDTO;
    }

    toString(): string {
        return `PercursosLinha: [${this.percursosIda}, ${this.percursosVolta}, 
                            ${this.percursosReforco}, ${this.percursosVazio}]`;
    }

}