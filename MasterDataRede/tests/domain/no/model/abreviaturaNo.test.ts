
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';

test("testarAbreviaturaNulo", () => {
    expect(() => {new AbreviaturaNo(null)}).toThrow(ReferenceError);
    expect(() => {new AbreviaturaNo(undefined)}).toThrow(ReferenceError);
});

test("testarAbreviaturaVazio", () => {
    expect(() => {new AbreviaturaNo('')}).toThrow(SyntaxError);
});

test("testarAbreviaturaDemasiadoComprido", () => {
    expect(() => {new AbreviaturaNo('12345678901234567890a')}).toThrow(SyntaxError); //21 chars
});

test("testarAbreviaturaUsualComCarateresInvalidos", () => {
    expect(() => {new AbreviaturaNo('🌺Estação AGUIAR🌺')}).toThrow(SyntaxError);
});

test("testarAbreviaturaDemasiadoPequeno", () => {
    expect(() => {new AbreviaturaNo('1')}).toThrow(SyntaxError);
});

test("testarAbreviaturaPequenoValido", () => {
    expect(new AbreviaturaNo('12')).toBeInstanceOf(AbreviaturaNo);
});

test("testarAbreviaturaGrandeValido", () => {
    expect(new AbreviaturaNo('12345678901234567890')).toBeInstanceOf(AbreviaturaNo); //2 chars
});

test("testarAbreviaturaUsualValido", () => {
    expect(new AbreviaturaNo('AGUIAR')).toBeInstanceOf(AbreviaturaNo);
});

test("testarAbreviaturaSoNumeros", () => {
    expect(new AbreviaturaNo('15367363211')).toBeInstanceOf(AbreviaturaNo);
});

test("testarAbreviaturaSoLetras", () => {
    expect(new AbreviaturaNo('cbjjcjhfdHJJbb')).toBeInstanceOf(AbreviaturaNo);
});

test("testarAbreviaturaSoEspacos", () => {
    expect(() => {new AbreviaturaNo('         ')}).toThrow(SyntaxError);
});

test("testarAbreviaturaUsualComCarateresAcentos", () => {
    expect(new AbreviaturaNo('Estação de Aguiar')).toBeInstanceOf(AbreviaturaNo);
});