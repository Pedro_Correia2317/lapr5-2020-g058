
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PageSelectorComponent } from './page-selector/page-selector.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SizeSelectorComponent } from './size-selector/size-selector.component';

@NgModule({
    declarations: [PageSelectorComponent, SearchBarComponent, SizeSelectorComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    exports: [PageSelectorComponent, SearchBarComponent, SizeSelectorComponent, RouterModule]
  })
export class VisualizacaoItemsModule {}