import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacaoTiposViaturasComponent } from './visualizacao-tipos-viaturas.component';

describe('VisualizacaoTiposViaturasComponent', () => {
  let component: VisualizacaoTiposViaturasComponent;
  let fixture: ComponentFixture<VisualizacaoTiposViaturasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizacaoTiposViaturasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacaoTiposViaturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
