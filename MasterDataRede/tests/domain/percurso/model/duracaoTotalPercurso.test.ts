
import { DuracaoTotalPercurso } from '../../../../src/domain/percurso/model/DuracaoTotalPercurso';

test("testarDuracaoTotalPercursoNulo", () => {
    expect(() => {new DuracaoTotalPercurso(null)}).toThrow(TypeError);
    expect(() => {new DuracaoTotalPercurso(undefined)}).toThrow(TypeError);
});

test("testarDuracaoNegativo", () => {
    expect(() => {new DuracaoTotalPercurso(-1)}).toThrow(SyntaxError);
});

test("testarDuracaoNegativo2", () => {
    expect(() => {new DuracaoTotalPercurso(-12.4563)}).toThrow(TypeError);
});

test("testarDuracaoInvalido", () => {
    expect(() => {new DuracaoTotalPercurso(25.001)}).toThrow(TypeError);
});

test("testarDuracaoInvalido2", () => {
    expect(() => {new DuracaoTotalPercurso(21601)}).toThrow(SyntaxError);
});

test("testarDuracaoInvalido3", () => {
    expect(() => {new DuracaoTotalPercurso(0)}).toThrow(SyntaxError);
});

test("testarDuracaoPequenoValido", () => {
    expect(new DuracaoTotalPercurso(1)).toBeInstanceOf(DuracaoTotalPercurso);
});

test("testarDuracaoPequenoValido2", () => {
    expect(new DuracaoTotalPercurso(25)).toBeInstanceOf(DuracaoTotalPercurso);
});

test("testarDuracaoPequenoValido3", () => {
    expect(new DuracaoTotalPercurso(21600)).toBeInstanceOf(DuracaoTotalPercurso);
});