

import {NoParagem} from '../../../domain/no/model/NoParagem';
import {NoBuilder} from './NoBuilder';

export interface NoParagemBuilder extends NoBuilder {

    obterResultado(): NoParagem;

}