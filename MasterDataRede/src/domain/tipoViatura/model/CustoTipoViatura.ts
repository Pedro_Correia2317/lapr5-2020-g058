

export class CustoTipoViatura {

    private custo: number;

    constructor(custo: number){
        if (!Number.isFinite(custo)){
            throw new TypeError(`The cost is not a number: ${custo}`);
        }
        const aux = parseFloat(custo.toFixed(3));
        if (aux < 0 || aux > 99){
            throw new SyntaxError(`The cost ${aux} is < 0 or > 99`);
        }
        this.custo = aux;
    }

    getValue(): number {
        return this.custo;
    }

    toString(): string {
        return `CustoTipoViatura: [${this.custo}]`;
    }

}