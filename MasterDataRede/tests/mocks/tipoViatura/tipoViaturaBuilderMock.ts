
import { TipoViaturaBuilder } from '../../../src/application/tipoViatura/services/TipoViaturaBuilder';
import { AutonomiaTipoViatura } from '../../../src/domain/tipoViatura/model/AutonomiaTipoViatura';
import { CodigoTipoViatura } from '../../../src/domain/tipoViatura/model/CodigoTipoViatura';
import { ConsumoTipoViatura } from '../../../src/domain/tipoViatura/model/ConsumoTipoViatura';
import { CustoTipoViatura } from '../../../src/domain/tipoViatura/model/CustoTipoViatura';
import { DescricaoTipoViatura } from '../../../src/domain/tipoViatura/model/DescricaoTipoViatura';
import { EmissoesTipoViatura } from '../../../src/domain/tipoViatura/model/EmissoesTipoViatura';
import { TipoCombustivel } from '../../../src/domain/tipoViatura/model/TipoCombustivel';
import { TipoViatura } from '../../../src/domain/tipoViatura/model/TipoViatura';
import { VelocidadeMediaTipoViatura } from '../../../src/domain/tipoViatura/model/VelocidadeMediaTipoViatura';

export class TipoViaturaBuilderMockSucesso implements TipoViaturaBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {}

    aplicarDescricao(descricao: string): void {}

    aplicarAutonomia(autonomia: number): void {}

    aplicarCusto(custo: number): void {}

    aplicarVelocidadeMedia(velocidade: number): void {}

    aplicarConsumo(consumo: number): void {}

    aplicarEmissoes(emissoes: number): void {}

    aplicarTipoCombustivel(tipoCombustivel: string): void {}
    
    obterResultado(): TipoViatura {
        const cod = new CodigoTipoViatura('12345678901234567890');
        const desc = new DescricaoTipoViatura('12345678901234567890');
        const auto = new AutonomiaTipoViatura(23);
        const con = new ConsumoTipoViatura(23);
        const custo = new CustoTipoViatura(23);
        const emi = new EmissoesTipoViatura(23);
        const vlc = new VelocidadeMediaTipoViatura(23);
        const tipo = TipoCombustivel.ELECTRICO;
        return new TipoViatura(cod, desc, auto, con, custo, emi, vlc, tipo);
    }

}

export class TipoViaturaBuilderMockErro implements TipoViaturaBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarDescricao(descricao: string): void {
        throw new SyntaxError('Teste');}

    aplicarAutonomia(autonomia: number): void {
        throw new SyntaxError('Teste');}

    aplicarCusto(custo: number): void {
        throw new SyntaxError('Teste');}

    aplicarVelocidadeMedia(velocidade: number): void {
        throw new SyntaxError('Teste');}

    aplicarConsumo(consumo: number): void {
        throw new SyntaxError('Teste');}

    aplicarEmissoes(emissoes: number): void {
        throw new SyntaxError('Teste');}

    aplicarTipoCombustivel(tipoCombustivel: string): void {
        throw new SyntaxError('Teste');}
    
    obterResultado(): TipoViatura {
        throw new SyntaxError('Teste');
    }

}