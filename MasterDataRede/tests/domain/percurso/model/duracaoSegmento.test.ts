
import { DuracaoSegmento } from '../../../../src/domain/percurso/model/DuracaoSegmento';

test("testarDuracaoSegmentoNulo", () => {
    expect(() => {new DuracaoSegmento(null)}).toThrow(TypeError);
    expect(() => {new DuracaoSegmento(undefined)}).toThrow(TypeError);
});

test("testarDuracaoNegativo", () => {
    expect(() => {new DuracaoSegmento(-1)}).toThrow(SyntaxError);
});

test("testarDuracaoNegativo2", () => {
    expect(() => {new DuracaoSegmento(-12.4563)}).toThrow(TypeError);
});

test("testarDuracaoInvalido", () => {
    expect(() => {new DuracaoSegmento(25.001)}).toThrow(TypeError);
});

test("testarDuracaoInvalido2", () => {
    expect(() => {new DuracaoSegmento(10801)}).toThrow(SyntaxError);
});

test("testarDuracaoInvalido3", () => {
    expect(() => {new DuracaoSegmento(0)}).toThrow(SyntaxError);
});

test("testarDuracaoPequenoValido", () => {
    expect(new DuracaoSegmento(1)).toBeInstanceOf(DuracaoSegmento);
});

test("testarDuracaoPequenoValido2", () => {
    expect(new DuracaoSegmento(25)).toBeInstanceOf(DuracaoSegmento);
});

test("testarDuracaoPequenoValido3", () => {
    expect(new DuracaoSegmento(10800)).toBeInstanceOf(DuracaoSegmento);
});