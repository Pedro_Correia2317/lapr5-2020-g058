

import {PontoRendicao} from '../../../domain/no/model/PontoRendicao';
import {NoBuilder} from './NoBuilder';

export interface NoPontoRendicaoBuilder extends NoBuilder {

    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void;

    isNoIgual(abreviatura: string): boolean;

    obterResultado(): PontoRendicao;

}