import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { PedidoCriarTipoTripulanteDTO } from 'src/app/models/tiposTripulantes/PedidoCriarTipoTripulanteDTO';
import { AcessoTiposTripulantesService } from 'src/app/services/rede/AcessoTiposTripulantes.service';

@Component({
  selector: 'app-criacao-tipos-tripulantes',
  templateUrl: './criacao-tipos-tripulantes.component.html',
  styleUrls: ['./criacao-tipos-tripulantes.component.css']
})
export class CriacaoTiposTripulantesComponent implements OnInit {

  formCriacaoTipoTripulante: any;

  constructor(private todosTiposTripulantes: AcessoTiposTripulantesService,
              private builder: FormBuilder) {
        this.formCriacaoTipoTripulante = this.builder.group({
          codigo: '',
          descricao: ''
        });
    }

  ngOnInit(): void {
  }

  onSubmit(dadosFormulario: any) {
    const dto = new PedidoCriarTipoTripulanteDTO();
    dto.codigo = dadosFormulario.codigo;
    dto.descricao = dadosFormulario.descricao;
    this.todosTiposTripulantes.enviarTipoTripulante(dto).subscribe((resultados: DetalhesOperacaoDTO) => {
      alert(resultados.sucesso + ' na operação: ' + resultados.mensagem);
    });
  }

}
