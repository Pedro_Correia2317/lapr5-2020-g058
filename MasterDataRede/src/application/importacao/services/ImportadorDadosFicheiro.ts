
import { ObjetoImportado } from "../objetosImportacao/ObjetoImportado";

export interface ImportadorDadosFicheiro {

    importarDadosFicheiro(directorio: string, apagarFicheiro: boolean): Promise<ObjetoImportado[]>;
    
}