
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RedeComponent } from './rede.component';

const routes: Routes = [
  {
    path: '', component: RedeComponent, children: 
      [
        {
          path: 'nos', 
          loadChildren: () => import('./paginas-visualizacao-items/visualizacao-nos/visualizacao-nos.module').then(m => m.VisualizacaoNosModule)
        },{
          path: 'novoNo', 
          loadChildren: () => import('./paginas-criacao-items/criacao-nos/criacao-nos.module').then(m => m.CriacaoNosModule)
        },{
          path: 'percursos', 
          loadChildren: () => import('./paginas-visualizacao-items/visualizacao-percursos/visualizacao-percursos.module').then(m => m.VisualizacaoPercursosModule)
        },{
          path: 'novoPercurso', 
          loadChildren: () => import('./paginas-criacao-items/criacao-percursos/criacao-percursos.module').then(m => m.CriacaoPercursosModule)
        },{
          path: 'linhas', 
          loadChildren: () => import('./paginas-visualizacao-items/visualizacao-linhas/visualizacao-linhas.module').then(m => m.VisualizacaoLinhasModule)
        },{
          path: 'novaLinha', 
          loadChildren: () => import('./paginas-criacao-items/criacao-linhas/criacao-linhas.module').then(m => m.CriacaoLinhasModule)
        },{
          path: 'tiposTripulantes', 
          loadChildren: () => import('./paginas-visualizacao-items/visualizacao-tipos-tripulantes/visualizacao-tipos-tripulantes.module').then(m => m.VisualizacaoTiposTripulantesModule)
        },{
          path: 'novoTipoTripulante', 
          loadChildren: () => import('./paginas-criacao-items/criacao-tipos-tripulantes/criacao-tipo-tripulantes.module').then(m => m.CriacaoTiposTripulantesModule)
        },{
          path: 'tiposViaturas', 
          loadChildren: () => import('./paginas-visualizacao-items/visualizacao-tipos-viaturas/visualizacao-tipos-viaturas.module').then(m => m.VisualizacaoTiposViaturasModule)
        },{
          path: 'novoTipoViatura', 
          loadChildren: () => import('./paginas-criacao-items/criacao-tipos-viaturas/criacao-tipos-viaturas.module').then(m => m.CriacaoTiposViaturasModule)
        },{
          path: 'toda', 
          loadChildren: () => import('./paginas-visualizacao-items/visualizacao-rede/visualizacao-rede.module').then(m => m.VisualizacaoRedeModule)
        },{
          path: 'importacao', 
          loadChildren: () => import('./paginas-criacao-items/importacao/importacao.module').then(m => m.ImportacaoModule)
        }]
  }
];

@NgModule({
  declarations: [RedeComponent],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedeModule { }
