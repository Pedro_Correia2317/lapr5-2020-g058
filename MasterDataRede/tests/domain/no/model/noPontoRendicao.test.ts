
import { NoPontoRendicao } from '../../../../src/domain/no/model/NoPontoRendicao';
import { NomeNo } from '../../../../src/domain/no/model/NomeNo';
import { CoordenadasNo } from '../../../../src/domain/no/model/CoordenadasNo';
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';
import { TempoDeslocacaoTripulacao } from '../../../../src/domain/no/model/TempoDeslocacaoTripulacao';

test("testarNomeNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(abreviaturaNo, null, coordenadasNo, tempos)}).toThrow(SyntaxError);
});

test("testarCoordenadasNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(abreviaturaNo, nomeNo, null, tempos)}).toThrow(SyntaxError);
});

test("testarAbreviaturaNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(null, nomeNo, coordenadasNo, tempos)}).toThrow(SyntaxError);
});

test("testarTemposNulo", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, null)}).toThrow(SyntaxError);
});

test("testarNomeIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(abreviaturaNo, undefined, coordenadasNo, tempos)}).toThrow(SyntaxError);
});

test("testarCoordenadasIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(abreviaturaNo, nomeNo, undefined, tempos)}).toThrow(SyntaxError);
});

test("testarAbreviaturaIndefenido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(undefined, nomeNo, coordenadasNo, tempos)}).toThrow(SyntaxError);
});

test("testarTemposIndefinido", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, undefined)}).toThrow(SyntaxError);
});

test("testarValoresValidosTemposVazios", () => {
    const nomeNo = new NomeNo('Estação de Aguiar');
    const coordenadasNo = new CoordenadasNo(23.4567, 12);
    const abreviaturaNo = new AbreviaturaNo('AGUIAR');
    const tempos = [];
    expect(new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, tempos)).toBeInstanceOf(NoPontoRendicao);
});

test("testarValoresValidosTemposPreenchidos", () => {
    const nomeNo = new NomeNo('Estação de Aguiar');
    const coordenadasNo = new CoordenadasNo(23.4567, 12);
    const abreviaturaNo = new AbreviaturaNo('AGUIAR');
    const tempos = [];
    tempos.push(new TempoDeslocacaoTripulacao(500, abreviaturaNo));
    tempos.push(new TempoDeslocacaoTripulacao(5100, abreviaturaNo));
    expect(new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, tempos)).toBeInstanceOf(NoPontoRendicao);
});

test("testarValoresInvalidosTemposNulos", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        tempos.push(new TempoDeslocacaoTripulacao(500, abreviaturaNo));
        tempos.push(null);
        new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, tempos)}).toThrow(ReferenceError);
});

test("testarValoresInvalidosTemposIndefinidos", () => {
    expect(() => {
        const nomeNo = new NomeNo('Estação de Aguiar');
        const coordenadasNo = new CoordenadasNo(12.4567, -12.23456);
        const abreviaturaNo = new AbreviaturaNo('AGUIAR');
        const tempos = [];
        tempos.push(new TempoDeslocacaoTripulacao(500, abreviaturaNo));
        tempos.push(undefined);
        new NoPontoRendicao(abreviaturaNo, nomeNo, coordenadasNo, tempos)}).toThrow(ReferenceError);
});