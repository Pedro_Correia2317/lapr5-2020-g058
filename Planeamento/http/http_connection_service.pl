
:- module(http_connection_service, [search_nodes/1, search_lines/1, search_timetables/1]).

% Bibliotecas
:- use_module(library(http/http_open)).
:- use_module(library(http/json)).

nodes_url("http://localhost:8080/nos").
lines_url("http://localhost:8080/linhas").
paths_url("http://localhost:8080/linha/").
timetables_url("http://localhost:8080/horarios").

search_nodes(Data) :-
    nodes_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json')]),
        json_read_dict(In, Data),
        close(In)
    ).

search_lines(Data) :-
    lines_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json')]),
        json_read_dict(In, Lines),
        close(In)
    ),
    search_paths(Lines.get(lista), Data).

search_paths([], []).
search_paths([Line|OtherLines], [Paths|Data]) :-
	paths_url(URL),
	string_concat(URL, Line.get(codigo), FinalURL),
	setup_call_cleanup(
        	http_open(FinalURL, In, [request_header('Accept'='application/json')]),
        	json_read_dict(In, Paths),
        	close(In)
    	),
	search_paths(OtherLines, Data).

search_timetables(Data) :-
    nos_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json')]),
        json_read_dict(In, Data),
        close(In)
    ).