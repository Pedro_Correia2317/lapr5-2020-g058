
import Mongoose, {Document, Model} from 'mongoose';

export class MongooseRepositoryImpl {

    async save(objectToSave: Mongoose.Document): Promise<boolean> {
        let success = false;
        await objectToSave.save().then((document: Mongoose.Document) => {
            if (document){
                success = true;
            }
        });

        return success;
    }

    async isUnico(modelo: Model<Document>, parametros): Promise<boolean> {
        const exists = await modelo.exists(parametros);

return !exists;
    }

}