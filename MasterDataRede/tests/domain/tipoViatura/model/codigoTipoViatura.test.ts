

import { CodigoTipoViatura } from '../../../../src/domain/tipoViatura/model/CodigoTipoViatura';

test('testarCodigoNulo', () => {
    expect(() => {new CodigoTipoViatura(null)}).toThrow(ReferenceError);
    expect(() => {new CodigoTipoViatura(undefined)}).toThrow(ReferenceError);
});

test('testarCodigoVazio', () => {
    expect(() => {new CodigoTipoViatura('')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoComprido', () => {
    expect(() => {new CodigoTipoViatura('123456789012345678901')}).toThrow(SyntaxError); //21 chars
});

test('testarCodigoUsualComCarateresInvalidos', () => {
    expect(() => {new CodigoTipoViatura('🌺ENF9229N2D9D2N3H6S🌺')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoPequeno', () => {
    expect(new CodigoTipoViatura('A')).toBeInstanceOf(CodigoTipoViatura);
});

test('testarCodigoPequenoValido', () => {
    expect(new CodigoTipoViatura('1D020DN22D0ND0SKWI44')).toBeInstanceOf(CodigoTipoViatura);
});

test('testarCodigoSoNumeros', () => {
    expect(new CodigoTipoViatura('15367363211256232115')).toBeInstanceOf(CodigoTipoViatura);
});

test('testarCodigoSoLetras', () => {
    expect(new CodigoTipoViatura('IBIEBIFEIENIENIEFEFN')).toBeInstanceOf(CodigoTipoViatura);
});

test('testarCodigoSoEspacos', () => {
    expect(() => {new CodigoTipoViatura('                    ')}).toThrow(SyntaxError);
});

test('testarCodigoUsualComCarateresAcentos', () => {
    expect(() => {new CodigoTipoViatura('ÁÕÍININIFWIFWÉÊÂÔSEÙ')}).toThrow(SyntaxError);
});