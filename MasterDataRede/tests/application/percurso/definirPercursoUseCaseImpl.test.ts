
import { DefinirPercursoUseCaseImpl } from '../../../src/application/percurso/usecases/DefinirPercursoUseCaseImpl';
import { NoRepositoryMockFalse, NoRepositoryMockTrue } from '../../mocks/no/NoRepositoryMock';
import { PercursoBuilderMockErro, PercursoBuilderMockSucesso } from '../../mocks/percurso/percursoBuilderMock';
import { PercursoRepositoryMockFalse, PercursoRepositoryMockTrue } from '../../mocks/percurso/percursoRepositoryMock';
import { SegmentoRedeBuilderMockErro, SegmentoRedeBuilderMockSucesso } from '../../mocks/percurso/segmentoRedeBuilderMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';

const pBuilderErro = new PercursoBuilderMockErro();
const pBuilderSucesso = new PercursoBuilderMockSucesso();
const sBuilderErro = new SegmentoRedeBuilderMockErro();
const sBuilderSucesso = new SegmentoRedeBuilderMockSucesso();
const logger = new ServicoLoggerMock();
const nRepoTrue = new NoRepositoryMockTrue();
const nRepoFalse = new NoRepositoryMockFalse();
const pRepoTrue = new PercursoRepositoryMockTrue();
const pRepoFalse = new PercursoRepositoryMockFalse();
const useCase = new DefinirPercursoUseCaseImpl();

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(pBuilderErro, logger, pRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoRepetido", async () => {
    expect((await useCase.aplicarCodigo(pBuilderSucesso, logger, pRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoValidoNaoRepetido", async () => {
    expect((await useCase.aplicarCodigo(pBuilderSucesso, logger, pRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarCodigoNulo", async () => {
    expect((await useCase.aplicarCodigo(pBuilderErro, logger, pRepoFalse, null)).sucesso).toBe(false);
});


test("testarAplicarNoInicialInvalido", async () => {
    expect((await useCase.aplicarNoInicial(pBuilderErro, sBuilderErro, logger, nRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarNoInicialNaoExistente", async () => {
    expect((await useCase.aplicarNoInicial(pBuilderSucesso, sBuilderSucesso, logger, nRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarNoInicialNulo", async () => {
    expect((await useCase.aplicarNoInicial(pBuilderErro, sBuilderErro, logger, nRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarNoInicialSucesso", async () => {
    expect((await useCase.aplicarNoInicial(pBuilderSucesso, sBuilderSucesso, logger, nRepoTrue, "")).sucesso).toBe(true);
});


test("testarAplicarNoFinalInvalido", async () => {
    expect((await useCase.aplicarNoFinal(pBuilderErro, sBuilderErro, logger, nRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarNoFinalNaoExistente", async () => {
    expect((await useCase.aplicarNoFinal(pBuilderSucesso, sBuilderSucesso, logger, nRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarNoFinalNulo", async () => {
    expect((await useCase.aplicarNoFinal(pBuilderErro, sBuilderErro, logger, nRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarNoFinalSucesso", async () => {
    expect((await useCase.aplicarNoFinal(pBuilderSucesso, sBuilderSucesso, logger, nRepoTrue, "")).sucesso).toBe(true);
});


test("testarAplicarDuracaoInvalido", async () => {
    expect((useCase.adicionarDuracao(pBuilderErro, sBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarDuracaoInvalido2", async () => {
    expect((useCase.adicionarDuracao(pBuilderErro, sBuilderSucesso, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarDuracaoInvalido3", async () => {
    expect((useCase.adicionarDuracao(pBuilderSucesso, sBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarDuracaoValido", async () => {
    expect((useCase.adicionarDuracao(pBuilderSucesso, sBuilderSucesso, logger, 0)).sucesso).toBe(true);
});


test("testarAplicarDistanciaInvalido", async () => {
    expect((useCase.adicionarDistancia(pBuilderErro, sBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarDistanciaInvalido2", async () => {
    expect((useCase.adicionarDistancia(pBuilderErro, sBuilderSucesso, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarDistanciaInvalido3", async () => {
    expect((useCase.adicionarDistancia(pBuilderSucesso, sBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarDistanciaValido", async () => {
    expect((useCase.adicionarDistancia(pBuilderSucesso, sBuilderSucesso, logger, 0)).sucesso).toBe(true);
});


test("testarAplicarSequenciaInvalido", async () => {
    expect((useCase.adicionarSequencia(sBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarSequenciaValido", async () => {
    expect((useCase.adicionarSequencia(sBuilderSucesso, logger, 0)).sucesso).toBe(true);
});


test("testarAplicarSequenciaAutoInvalido", async () => {
    expect((useCase.adicionarSequenciaAutomatica(sBuilderErro, logger)).sucesso).toBe(false);
});

test("testarAplicarSequenciaAutoValido", async () => {
    expect((useCase.adicionarSequenciaAutomatica(sBuilderSucesso, logger)).sucesso).toBe(true);
});


test("testarAplicarSegmentoPercursoInvalido", async () => {
    expect((useCase.adicionarSegmentoCriadoAPercurso(pBuilderErro, sBuilderErro, logger)).sucesso).toBe(false);
});

test("testarAplicarSegmentoPercursoInvalido2", async () => {
    expect((useCase.adicionarSegmentoCriadoAPercurso(pBuilderSucesso, sBuilderErro, logger)).sucesso).toBe(false);
});

test("testarAplicarSegmentoPercursoInvalido3", async () => {
    expect((useCase.adicionarSegmentoCriadoAPercurso(pBuilderErro, sBuilderSucesso, logger)).sucesso).toBe(false);
});

test("testarAplicarSegmentoPercursoInvalido4", async () => {
    expect((useCase.adicionarSegmentoCriadoAPercurso(pBuilderSucesso, sBuilderSucesso, logger)).sucesso).toBe(true);
});

test("testarRegistarPercursoErroAcessoBD", async () => {
    expect((await useCase.registarPercurso(pBuilderSucesso, logger, pRepoFalse)).sucesso).toBe(false);
});

test("testarRegistarPercursoInvalido", async () => {
    expect((await useCase.registarPercurso(pBuilderErro, logger, pRepoTrue)).sucesso).toBe(false);
});

test("testarRegistarPercursoTudoErro", async () => {
    expect((await useCase.registarPercurso(pBuilderErro, logger, pRepoFalse)).sucesso).toBe(false);
});

test("testarRegistarPercursoSucesso", async () => {
    expect((await useCase.registarPercurso(pBuilderSucesso, logger, pRepoTrue)).sucesso).toBe(true);
});