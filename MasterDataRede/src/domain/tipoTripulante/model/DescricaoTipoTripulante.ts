

export class DescricaoTipoTripulante {

    private descricao: string;

    constructor(descricao: string){
        if (descricao == null){
            throw new ReferenceError('The inserted description is null');
        }
        const desc = descricao.trim();
        if (!(/^[\wÀ-ú\d\s ()!\\.,?'+"#-]{2,200}$/u).test(desc)){
            throw new SyntaxError(`The description ${desc} is invalid`);
        }
        this.descricao = desc;
    }

    getValue(): string {
        return this.descricao;
    }

    toString(): string {
        return `DescricaoTipoTripulante: [${this.descricao}]`;
    }
}