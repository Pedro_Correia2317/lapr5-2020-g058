
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { Routes, RouterModule } from '@angular/router';
import { VisualizadorNosModule } from '../../visualizadores/visualizador-nos/visualizador-nos.module';
import { AdicaoSegmentoComponent } from './adicao-segmento/adicao-segmento.component';

import { CriacaoPercursosComponent } from './criacao-percursos.component';

const routes: Routes = [
    {path: '', component: CriacaoPercursosComponent}
];

@NgModule({
  declarations: [AdicaoSegmentoComponent, CriacaoPercursosComponent],
  imports: [VisualizadorNosModule, MatDialogModule, CommonModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [AdicaoSegmentoComponent]
})
export class CriacaoPercursosModule { }