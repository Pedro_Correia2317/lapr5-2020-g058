

import { LinhaRepositoryMockTrue, LinhaRepositoryMockFalse } from '../../mocks/linha/linhaRepositoryMock';
import { LinhaBuilderMockSucesso } from '../../mocks/linha/linhaBuilderMock';
import { PercursoBuilderMockSucesso } from '../../mocks/percurso/percursoBuilderMock';
import { SegmentoRedeBuilderMockSucesso } from '../../mocks/percurso/segmentoRedeBuilderMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { ListarPercursosLinhaUseCaseImpl } from '../../../src/application/linha/usecases/ListarPercursosLinhaUseCaseImpl';
import { LinhaBuilder } from '../../../src/application/linha/services/LinhaBuilder';
import { Linha } from '../../../src/domain/linha/model/Linha';
import { PercursoRepositoryMockTrue, PercursoRepositoryMockFalse } from '../../mocks/percurso/percursoRepositoryMock';
import { CodigoLinha } from '../../../src/domain/linha/model/CodigoLinha';
import { NecessidadesTipoTripulante } from '../../../src/domain/linha/model/NecessidadesTipoTripulante';
import { NecessidadesTipoViatura } from '../../../src/domain/linha/model/NecessidadesTipoViatura';
import { NomeLinha } from '../../../src/domain/linha/model/NomeLinha';
import { PercursosLinha } from '../../../src/domain/linha/model/PercursosLinha';
import { TipoNecessidade } from '../../../src/domain/linha/model/TipoNecessidade';
import { CodigoPercurso } from '../../../src/domain/percurso/model/CodigoPercurso';

const logger = new ServicoLoggerMock();
const linhaRepoTrue = new LinhaRepositoryMockTrue();
const linhaRepoFalse = new LinhaRepositoryMockFalse();
const pRepoTrue = new PercursoRepositoryMockTrue();
const pRepoFalse = new PercursoRepositoryMockFalse();
const useCase = new ListarPercursosLinhaUseCaseImpl();
const builder = new LinhaBuilderMockSucesso();
const pBuilder = new PercursoBuilderMockSucesso();
const sBuilder = new SegmentoRedeBuilderMockSucesso();

test('testarPesquisaComLinhaDesconhecida', async () => {
    const aux = linhaRepoTrue.findByCodigo;
    linhaRepoTrue.findByCodigo = async (builder: LinhaBuilder, codigo: string): Promise<Linha> => { return null;};
    expect((await useCase.obterPercursosLinha(builder, pBuilder, sBuilder, linhaRepoTrue, pRepoTrue, logger, '')).sucesso).toBe(false);
});

test('testarPesquisaComSucesso', async () => {
    const aux = linhaRepoTrue.findByCodigo;
    linhaRepoTrue.findByCodigo = async (builder: LinhaBuilder, codigo: string): Promise<Linha> => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        return new Linha(cod, nome, percs, necTV, necTT);
    };
    const resultado = await useCase.obterPercursosLinha(builder, pBuilder, sBuilder, linhaRepoTrue, pRepoTrue, logger, '');

    expect(resultado.sucesso).toBe(true);
    expect(resultado.lista[0].length).toBe(1);
    expect(resultado.lista[1].length).toBe(1);
    expect(resultado.lista[2].length).toBe(0);
    expect(resultado.lista[3].length).toBe(0);
});

test('testarPesquisaComErroProcurarPercursos', async () => {
    const aux = linhaRepoTrue.findByCodigo;
    linhaRepoTrue.findByCodigo = async (builder: LinhaBuilder, codigo: string): Promise<Linha> => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        return new Linha(cod, nome, percs, necTV, necTT);
    };
    const resultado = await useCase.obterPercursosLinha(builder, pBuilder, sBuilder, linhaRepoTrue, pRepoFalse, logger, '');
    expect(resultado.sucesso).toBe(false);
});

test('testarPesquisaComErroProcurarLinha', async () => {
    expect((await useCase.obterPercursosLinha(builder, pBuilder, sBuilder, linhaRepoFalse, pRepoTrue, logger, '')).sucesso).toBe(false);
});