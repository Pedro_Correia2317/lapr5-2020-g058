
export class PedidoCriarSegmentoDTO {

    public abreviaturaNoInicial: string;

    public abreviaturaNoFinal: string;

    public duracao: number;

    public distancia: number;
    
}