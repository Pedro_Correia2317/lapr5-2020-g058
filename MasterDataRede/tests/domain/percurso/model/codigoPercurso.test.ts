
import { CodigoPercurso } from '../../../../src/domain/percurso/model/CodigoPercurso';

test('testarCodigoNulo', () => {
    expect(() => {new CodigoPercurso(null)}).toThrow(ReferenceError);
    expect(() => {new CodigoPercurso(undefined)}).toThrow(ReferenceError);
});

test('testarCodigoVazio', () => {
    expect(() => {new CodigoPercurso('')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoComprido', () => {
    expect(() => {new CodigoPercurso('123456789012345678901')}).toThrow(SyntaxError); //21 chars
});

test('testarCodigoUsualComCarateresInvalidos', () => {
    expect(() => {new CodigoPercurso('🌺ENF9229N2D9D2N3H6S🌺')}).toThrow(SyntaxError);
});

test('testarCodigoDemasiadoPequeno', () => {
    expect(new CodigoPercurso('A')).toBeInstanceOf(CodigoPercurso);
});

test('testarCodigoPequenoValido', () => {
    expect(new CodigoPercurso('1D020DN22D0ND0SKWI44')).toBeInstanceOf(CodigoPercurso);
});

test('testarCodigoSoNumeros', () => {
    expect(new CodigoPercurso('15367363211256232115')).toBeInstanceOf(CodigoPercurso);
});

test('testarCodigoSoLetras', () => {
    expect(new CodigoPercurso('IBIEBIFEIENIENIEFEFN')).toBeInstanceOf(CodigoPercurso);
});

test('testarCodigoSoEspacos', () => {
    expect(() => {new CodigoPercurso('                    ')}).toThrow(SyntaxError);
});

test('testarCodigoUsualComCarateresAcentos', () => {
    expect(() => {new CodigoPercurso('ÁÕÍININIFWIFWÉÊÂÔSEÙ')}).toThrow(SyntaxError);
});