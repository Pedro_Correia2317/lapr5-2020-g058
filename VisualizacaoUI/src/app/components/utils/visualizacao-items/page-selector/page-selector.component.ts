
import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-page-selector',
  templateUrl: './page-selector.component.html',
  styleUrls: ['./page-selector.component.css']
})
export class PageSelectorComponent implements OnInit, OnChanges {

  @Input() dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};

  paginaSelecionada: number;

  values: number[];

  @Output() selectedValue = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit(): void {
  }

  private criarPaginas(dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number}){
    let i = 1;
    this.values = [];
    let count = dadosCriacaoPaginas.numeroTotal;
    const tamanho = dadosCriacaoPaginas.tamanhoPagina;
    while(count > tamanho){
      this.values.push(i);
      i++;
      count -= tamanho;
    }
    if (count > 0){
      this.values.push(i);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(!changes.dadosCriacaoPaginas.isFirstChange()){
      this.criarPaginas(changes.dadosCriacaoPaginas.currentValue);
      this.paginaSelecionada = changes.dadosCriacaoPaginas.currentValue.pagina;
    }
  }

  onChange(event: any): void {
    this.selectedValue.emit(event.target.value);
  }

}
