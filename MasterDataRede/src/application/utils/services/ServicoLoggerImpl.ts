
import { Service } from 'typedi';
import * as Winston from 'winston';
import { ServicoLogger } from "./ServicoLogger";

@Service("PrimaryServicoLogger")
export class ServicoLoggerImpl implements ServicoLogger {

    private static logger: Winston.Logger;

    constructor(log_level: string){
        const transports = [];
        transports.push(new Winston.transports.Console({
        'format': Winston.format.combine(
            Winston.format.cli(),
            Winston.format.splat()
        )
        }));

        ServicoLoggerImpl.logger = Winston.createLogger({
            'format': Winston.format.combine(
                Winston.format.timestamp({
                'format': 'YYYY-MM-DD HH:mm:ss'
              }),
              Winston.format.errors({'stack': true}),
              Winston.format.splat(),
              Winston.format.json()
            ),
            'level': log_level,
            'levels': Winston.config.npm.levels,
            transports
          });
    }

    registarFalha(err: Error): void {
        ServicoLoggerImpl.logger.info(err);
    }

}