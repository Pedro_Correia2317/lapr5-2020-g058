
import { TipoTripulanteBuilder } from '../../../src/application/tipoTripulante/services/TipoTripulanteBuilder';
import { CodigoTipoTripulante } from '../../../src/domain/tipoTripulante/model/CodigoTipoTripulante';
import { DescricaoTipoTripulante } from '../../../src/domain/tipoTripulante/model/DescricaoTipoTripulante';
import { TipoTripulante } from '../../../src/domain/tipoTripulante/model/TipoTripulante';

export class TipoTripulanteBuilderMockErro implements TipoTripulanteBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {
        throw new SyntaxError('Teste.');
    }
    aplicarDescricao(descricao: string): void {
        throw new SyntaxError('Teste.');
    }
    obterResultado(): TipoTripulante {
        throw new SyntaxError('Teste.');
    }

}

export class TipoTripulanteBuilderMockSucesso implements TipoTripulanteBuilder {

    reset(): void {}

    aplicarCodigo(codigo: string): void {
    }
    aplicarDescricao(descricao: string): void {
    }
    obterResultado(): TipoTripulante {
        const codigoTipoT = new CodigoTipoTripulante('12345678901234567890');
        const descricaoTipoT = new DescricaoTipoTripulante('12345678901234567890');
        return new TipoTripulante(codigoTipoT, descricaoTipoT);
    }

}