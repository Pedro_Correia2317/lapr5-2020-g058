import Mongoose from 'mongoose';
import {ITempoDeslocacaoTripulacao} from './ITempoDeslocacaoTripulacao';

export interface INo extends Mongoose.Document {

    abreviatura: string;

    nome: string;

    latitude: number;

    longitude: number;

    tipoNo: string;

    capacidade: number;

    temposDeslocacaoTripulacao: Mongoose.Types.DocumentArray<ITempoDeslocacaoTripulacao>;

}