
import { NoBuilder } from '../../application/no/services/NoBuilder';
import {AbreviaturaNo} from '../../domain/no/model/AbreviaturaNo';
import {No} from '../../domain/no/model/No';
import {Repository} from './Repository';

export interface NoRepository extends Repository<No, AbreviaturaNo> {

    isAbreviaturaUnico(codigo: string): Promise<boolean>;

    existeNo(abreviatura: string): Promise<boolean>;

    obterNos(builders: Map<string, NoBuilder>, sort: string, numPag: number, 
                    tamPag: number, abreviatura: string, nome: string): Promise<No[]>;

    countComFiltros(abreviatura: string, nome: string): Promise<number>;

}