import { NoPontoRendicaoDTO } from "../dto/NoPontoRendicaoDTO";
import { TempoDeslocacaoTripulacaoDTO } from "../dto/TempoDeslocacaoTripulacaoDTO";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { CoordenadasNo } from "./CoordenadasNo";
import { No } from "./No";
import { NomeNo } from "./NomeNo";
import { PontoRendicao } from "./PontoRendicao";
import { TempoDeslocacaoTripulacao } from "./TempoDeslocacaoTripulacao";
import { TipoNo } from "./TipoNo";

export class NoPontoRendicao implements PontoRendicao {

    private abreviatura: AbreviaturaNo;

    private nome: NomeNo;

    private coordenadas: CoordenadasNo;

    private tempos: TempoDeslocacaoTripulacao[];

    constructor(abr: AbreviaturaNo, nome: NomeNo, coor: CoordenadasNo, tempos: TempoDeslocacaoTripulacao[]){
        if(nome == null || coor == null || abr == null || tempos == null){
            throw new SyntaxError('There are null values');
        }
        for(const tempo of tempos){
            if(tempo == null){
                throw new ReferenceError('There is a null crew travel time');
            }
        }
        this.abreviatura = abr;
        this.nome = nome;
        this.coordenadas = coor;
        this.tempos = tempos.slice();
    }

    toTempoDeslocacaoTripulacaoDTO(): TempoDeslocacaoTripulacaoDTO[] {
        const lista: TempoDeslocacaoTripulacaoDTO[] = [];
        for(const tempo of this.tempos){
            const dto = new TempoDeslocacaoTripulacaoDTO();
            dto.no = tempo.getNo().getValue();
            dto.tempo = tempo.getTempo();
            lista.push(dto);
        }
        return lista;
    }

    toBaseDTO(): NoPontoRendicaoDTO {
        const dto = new NoPontoRendicaoDTO();
        dto.nome = this.nome.getValue();
        dto.abreviatura = this.abreviatura.getValue();
        dto.latitude = this.coordenadas.getLatitude();
        dto.longitude = this.coordenadas.getLongitude();
        dto.tempos = [];
        this.tempos.forEach((tempo) => {
            dto.tempos.push({tempo: tempo.getTempo(), abreviaturaNo: tempo.getNo().getValue()});
        });
        dto.tipoNo = TipoNo.NO_PONTO_RENDICAO.toBaseDTO().nome;
        return dto;
    }

    tipoNo(): TipoNo {
        return TipoNo.NO_PONTO_RENDICAO;
    }

}