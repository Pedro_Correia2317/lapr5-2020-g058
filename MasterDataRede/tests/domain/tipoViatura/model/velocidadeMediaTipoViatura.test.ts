
import { VelocidadeMediaTipoViatura } from '../../../../src/domain/tipoViatura/model/VelocidadeMediaTipoViatura';

test('testarCapacidadeNulo', () => {
    expect(() => {new VelocidadeMediaTipoViatura(null)}).toThrow(TypeError);
    expect(() => {new VelocidadeMediaTipoViatura(undefined)}).toThrow(TypeError);
});

test('testarCapacidadeValoresPositivos', () => {
    expect(new VelocidadeMediaTipoViatura(1)).toBeInstanceOf(VelocidadeMediaTipoViatura);
    expect(new VelocidadeMediaTipoViatura(13.0)).toBeInstanceOf(VelocidadeMediaTipoViatura);
    expect(new VelocidadeMediaTipoViatura(300)).toBeInstanceOf(VelocidadeMediaTipoViatura);
});

test('testarCapacidadeValoresNegativos', () => {
    expect(() => {new VelocidadeMediaTipoViatura(-11)}).toThrow(SyntaxError);
    expect(() => {new VelocidadeMediaTipoViatura(-21)}).toThrow(SyntaxError);
    expect(() => {new VelocidadeMediaTipoViatura(-1)}).toThrow(SyntaxError);
});

test('testarCapacidadeValoresCasasDecimais', () => {
    expect(() => {new VelocidadeMediaTipoViatura(1.3346)}).toThrow(TypeError);
    expect(() => {new VelocidadeMediaTipoViatura(2.0000000001)}).toThrow(TypeError);
    expect(() => {new VelocidadeMediaTipoViatura(-1.236)}).toThrow(TypeError);
});

test('testarCapacidadeValoresExtremos', () => {
    expect(() => {new VelocidadeMediaTipoViatura(301)}).toThrow(SyntaxError);
    expect(() => {new VelocidadeMediaTipoViatura(28481694619494)}).toThrow(SyntaxError);
});