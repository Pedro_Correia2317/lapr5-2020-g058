

export class EmissoesTipoViatura {

    private emissoes: number;

    constructor(emissoes: number){
        if (!Number.isInteger(emissoes)){
            throw new TypeError(`The emissions is not a integer: ${emissoes}`);
        }
        if (emissoes <= 0 || emissoes > 9999999){
            throw new SyntaxError(`The emissions ${emissoes} is <= 0 or > 9999999`);
        }
        this.emissoes = emissoes;
    }

    getValue(): number {
        return this.emissoes;
    }

    toString(): string {
        return `EmissoesTipoViatura: [${this.emissoes}]`;
    }

}