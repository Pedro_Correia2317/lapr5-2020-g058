
:- dynamic no/6.
:- dynamic linha/5.

:- dynamic liga/3.
/*
    timeT is for the generating of the timetable facts, 
    it's used to temporarely save the times of a trip and afterwards it is collected to generate the timetable fact
*/
:- dynamic timeT/1.
/* 
    not used because generating the stations and lines out of the xml is not necessary
*/
:- dynamic noStation/2.
:- dynamic pathDistance/2.
:- dynamic pathDuration/2.
:- dynamic pathName/2.
:- dynamic pathStation/2.

gera_ligacoes:- 
    retractall(liga(_,_,_)),
    findall(_,
        ((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),
         (no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
         No1\==No2,
         linha(_,N,LNos,_,_),
         ordem_membros(No1,No2,LNos),
         assertz(liga(No1,No2,N))),
        _).

ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).

%% IMPORT OF XML
importXMLData(FileName):-
    retractall(timetable(_,_)),
    load_xml(FileName,XML,_),
    XML=[element(_,_,DocumentInfo)],
    member(element('world',_,World),DocumentInfo),
    member(element('GlDocument',_,GlDocument),World),
    loadHorarios(GlDocument).
    

/*
    generates all the No-facts of the XML File while looping through all the Nodes
*/
loadStations(GlDocument):-
    member(element('GlDocumentNetwork',_,Kids3),GlDocument),
    member(element('Network',_,Kids4),Kids3),
    member(element('Nodes',_,Nodes),Kids4),!,
    (getNodeDetails(Nodes);true).

/*
    generates all the temporarely pathName facts which are used to map the path key in the Path Informations to the Name later of the line by looping through the line details
*/
loadLineNames(GlDocument):-
    member(element('GlDocumentNetwork',_,Kids3),GlDocument),
    member(element('Network',_,Kids4),Kids3),
    member(element('Lines',_,Lines),Kids4),!,
    (getLineDetails(Lines);true).

/*
    generates all the linha facts from the Path nodes of the XML File 
*/
loadPaths(GlDocument):-
    member(element('GlDocumentNetwork',_,Kids3),GlDocument),
    member(element('Network',_,Kids4),Kids3),
    member(element('Paths',_,Paths),Kids4),!,
    (getPathDetails(Paths);true),
    retractall(noStation(_,_)).

/*
    generates all the timetable facts from the Trips of the XML File
*/
loadHorarios(GlDocument):- 
    member(element('GlDocumentSchedule',_,Kids3),GlDocument),
    member(element('Schedule',_,Kids4),Kids3),
    member(element('Trips',_,Trips),Kids4),!,
    (getTripDetails(Trips);true).

/*
    generates all the timeT facts from a Trip which are later collected to create the timetable fact of a line
*/
getPassingTimes(PassingTimes):-
    !,
    member(element('PassingTime',TimeDetails,_),PassingTimes),
    extractValue(1,TimeDetails,"=",TimeStamp),
    convertToNumber(TimeStamp,TimeNum),
    assertz(timeT(TimeNum)),
    fail.

/*
    converts a String to Number -> just seperated in a utility function
*/
convertToNumber(X,Y):-
    number_string(Y,X).

/*
    loops through every Trip Node in the XML and creates the timetable nodes by collecting the temporare timeT facts for every Trip
*/
getTripDetails(Trips):-
    !,
    member(element('Trip',TripDetails,TripKids),Trips),
    extractPathNumber(TripDetails,PathNr),
    convertToNumber(PathNr,PathNumber),
    member(element('PassingTimes',_,PassingTimes),TripKids),
    (getPassingTimes(PassingTimes);true),
    findall(A,timeT(A),Times),
    retractall(timeT(_)),
    assertz(timetable(PathNumber,Times)),
    fail.

/*
    extract the Path Number from the Detail String
*/
extractPathNumber(TripDetails,PathNr):-
    term_string(TripDetails,String),
    (sub_string(String,_,_,_,'Line'),extractValue(4,TripDetails,":",PathNr);
    extractValue(3,TripDetails,":",PathNr)),!.

 /*
    generates the temporare pathName facts to have the associated Line Name when creating the linha facts
 */   
getLinePaths(LinePaths,LineName):-
    !,
    member(element('LinePath',Details,_),LinePaths),
    extractValue(1,Details,":",PathNr),
    convertToNumber(PathNr,PathNum),
    assertz(pathName(PathNum,LineName)),
    fail.

 /*
    generates the linha facts from the Path Nodes by looping through all the PathNodes and summarizing duration and distance
 */ 
getPathDetails(Paths):-
    !,
    member(element('Path',PathDetail,Kids),Paths),
    extractValue(0,PathDetail,":",PathKey),
    convertToNumber(PathKey,PathId),
    member(element('PathNodes',_,PathNodes),Kids),
    (getPathNodes(PathNodes,PathId);true),
    findall(A,pathStation(PathId,A),Stops),
    retractall(pathStation(PathId,_)),

    findall(S,pathDuration(PathId,S),Durations),
    retractall(pathDuration(PathId,_)),
    sum_list(Durations,TotalDurationInSecs),
    TotalDuration is TotalDurationInSecs / 60,

    findall(X,pathDistance(PathId,X),Distances),
    retractall(pathDistance(PathId,_)),
    sum_list(Distances,TotalDistance),

    pathName(PathId,Linename),
    retract(pathName(PathId,Linename)),

    assertz(linha(Linename,PathId,Stops,TotalDuration,TotalDistance)),
    fail.

/*
    generates the temporare pathStation, pathDuration & pathDistance facts to collect all the linha information later
*/
getPathNodes(PathNodes,PathId):-
    !,
    member(element('PathNode',PathNodeDetails,_),PathNodes),
    extractValue(1,PathNodeDetails,":",NodeId),
    convertToNumber(NodeId,NodeNum),
    noStation(NodeNum,Name),
    assertz(pathStation(PathId,Name)),
    getDuration(PathId,PathNodeDetails),
    getDistance(PathId,PathNodeDetails),
    fail.    

/*
    extracts the duration information for every PathNode
*/
getDuration(PathId,PathNodeDetails):-
    (extractValue(2,PathNodeDetails,"=",Duration),convertToNumber(Duration,DurationNum),assertz(pathDuration(PathId,DurationNum))
    ;true),!.

/*
    extracts the distance information for every PathNode
*/
getDistance(PathId,PathNodeDetails):-
    (extractValue(3,PathNodeDetails,"=",Distance),convertToNumber(Distance,DistanceNum),assertz(pathDistance(PathId,DistanceNum))
    ;true),!.

/*
    utility function to get the nth Element out of the details string
*/
extractValue(Key,Details,Char,Value):-    
    nth0(Key,Details,ValueTerm),
    term_string(ValueTerm,ValueString),
    sub_string(ValueString,Before,Length,After,Char),
    (Char == "=", Index is Before + Length + 1,NewL is After - 2;Index is Before + Length,NewL is After - 1),    
    sub_string(ValueString,Index,NewL,_,Value),!.

/*
    converts the boolean value to the t/f value for the no-facts
*/
convertBool(Bool,Value):-
    (Bool == "False",Value = f;Value = t),!.

/*
    generates the no facts from the Nodes
*/
getNodeDetails(Nodes):-
    !,
    member(element('Node',NodeDetails,_),Nodes),
    extractValue(0,NodeDetails,":",NodeId),
    extractValue(1,NodeDetails,"=",Name),
    extractValue(2,NodeDetails,"=",Latitude),
    extractValue(3,NodeDetails,"=",Longitude),
    extractValue(4,NodeDetails,"=",ShortName),
    extractValue(5,NodeDetails,"=",IsDepot),
    extractValue(6,NodeDetails,"=",IsReliefpoint),
    convertBool(IsDepot,DepotBool),
    convertBool(IsReliefpoint,ReliefBool),
    convertToNumber(Longitude,LongitudeNum),
    convertToNumber(Latitude,LatitudeNum),
    convertToNumber(NodeId,NodeNum),
    assertz(no(Name,ShortName,DepotBool,ReliefBool,LongitudeNum,LatitudeNum)),
    assertz(noStation(NodeNum,ShortName)),
    fail.

/*
    generates all the temporarely pathName facts
*/
getLineDetails(Lines):-
    !,
    member(element('Line',LineDetails,LineKids),Lines),
    extractValue(1,LineDetails,"=",LineName),
    member(element('LinePaths',_,LinePaths),LineKids),
    (getLinePaths(LinePaths,LineName);true),
    fail.

/*
    utility function to get the sorted timetables of a line
*/
getSortedTimetables(LineId,Timetables):-
    findall(X,timetable(LineId,X),All),
    sort(All,Timetables).

/*
    calculates the Waiting and Travel Time from a specific Node with a line to destination
*/
getWaitingAndTravelTime(CurrentTime,CurrentNode,NextNode,Line,WaitingTime,Traveltime):-
    linha(_,Line, Stops,_,_),
    nth1(PosCurrent,Stops,CurrentNode),
    nth1(PosNext,Stops,NextNode),
    getSortedTimetables(Line,Timetables),
    getFittingTimetable(PosCurrent,CurrentTime,Timetables,DepartureTime,Timetables),
    getFittingTimetable(PosNext,CurrentTime,Timetables,ArrivalTime,Timetables),
    WaitingTime1 is DepartureTime - CurrentTime,
    (WaitingTime1 < 0, WaitingTime is DepartureTime + 86400 - CurrentTime;WaitingTime is WaitingTime1),
    Traveltime is ArrivalTime - DepartureTime,!.

/*
    returns the timestamp in the specific timetable for a specific node
*/
getFittingTimetable(Pos,_,[],Timestamp,[H|_]):-
    nth1(Pos,H,Timestamp).
getFittingTimetable(Pos,CurrentTime,[H|T],Timestamp,TimetablesBackup):-
    nth1(Pos,H,Time),
    (Time > CurrentTime, Timestamp is Time;getFittingTimetable(Pos,CurrentTime,T,Timestamp,TimetablesBackup)).

/*
    calculates the Waiting and Travel Time from a specific Node with a line to destination
*/
getTimeCostsForWaitingAndTravelling(CurrentCost, Line, CurrentNode,NextNode,Costs):-
    getWaitingAndTravelTime(CurrentCost,CurrentNode,NextNode,Line,WaitingTime,Traveltime),
    Costs is WaitingTime + Traveltime,!.
   
/*
    calculates the distance between 2 points by the harversine formula
*/
harversineFormulaForDistance(Lat1,Long1,Lat2,Long2,Result):-
    R is 6371e3, %Earths Radius in Meters
    Radian1 is Lat1 * pi/180, %// φ, λ in radians
    Radian2 is Lat2 * pi/180,
    DeltaLat = (Lat2-Lat1) * pi/180,
    DeltaLong = (Long2-Long1) * pi/180,
    A is (sin(DeltaLat/2)*sin(DeltaLat/2) + 
        cos(Radian1)*cos(Radian2)*sin(DeltaLong/2)*sin(DeltaLong/2)),
    C is 2 * atan(sqrt(A), sqrt(1-A)),
    Result is R * C. % in metres

/*
    just a util function to analyze the algorithm better
*/
printItNice([]):- nl,!.
printItNice([H|T]):-
    write(H),nl,printItNice(T).



:- gera_ligacoes().
:- importXMLData("demo.xml").