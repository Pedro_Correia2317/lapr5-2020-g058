import { ImportadorDadosFicheiro } from "./ImportadorDadosFicheiro";

export interface ImportadorDadosFicheiroContext {

    obterImportadorAdequado(directorio: string): ImportadorDadosFicheiro;

}