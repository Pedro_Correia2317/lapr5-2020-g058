

import { PercursoBuilder } from '../../application/percurso/services/PercursoBuilder';
import { SegmentoRedeBuilder } from '../../application/percurso/services/SegmentoRedeBuilder';
import {CodigoPercurso} from '../../domain/percurso/model/CodigoPercurso';
import {Percurso} from '../../domain/percurso/model/Percurso';
import {Repository} from './Repository';

export interface PercursoRepository extends Repository<Percurso, CodigoPercurso> {

    isCodigoUnico(codigo: string): Promise<boolean>;

    existe(codigo: string): Promise<boolean>;

    findByCodigo(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, codigo: string): Promise<Percurso>;

    obterPercursos(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, sort: string, numPag: number, 
                    tamPag: number, descricao: string): Promise<Percurso[]>;

    countComFiltros(descricao: string): Promise<number>;

}