
import { CriarTipoViaturaUseCaseDirector } from '../../../src/application/tipoViatura/director/CriarTipoViaturaUseCaseDirector';
import { TipoViaturaBuilderImpl } from '../../../src/application/tipoViatura/services/TipoViaturaBuilderImpl';
import { CriarTipoViaturaUseCaseImpl } from '../../../src/application/tipoViatura/usecases/CriarTipoViaturaUseCaseImpl';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { TipoViaturaRepositoryMockFalse, TipoViaturaRepositoryMockTrue } from '../../mocks/tipoViatura/tipoViaturaRepositoryMock';
import { PedidoCriarTipoViaturaDTO } from '../../../src/application/tipoViatura/dto/PedidoCriarTipoViaturaDTO';

const loggerMock = new ServicoLoggerMock();
const tvUseCase = new CriarTipoViaturaUseCaseImpl();
const tvRepoTrue = new TipoViaturaRepositoryMockTrue();
const tvRepoFalse = new TipoViaturaRepositoryMockFalse();

test('testarPedidoValidoComValoresUnicos', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoComCodigoInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'SouInvalido...';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComDescricaoInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = '?';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComAutonomiaInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 50.87786;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComCustoInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = -20;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComVelocidadeMediaInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 20;
    pedido.velocidadeMedia = -30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComConsumoInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 20;
    pedido.velocidadeMedia = 30;
    pedido.consumo = -30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTipoCombustivelInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'TIPO_COMBUSTIVEL_INVALIDO';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComEmissoesInvalido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 20;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050.8675;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComCodigoFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    // pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComDescricaoFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    // pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComAutonomiaFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    // pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComCustoFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    // pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComVelocidadeMediaFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    // pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComConsumoFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    // pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComTipoCombustivelFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    // pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoComEmissoesFalta', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    // pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComCodigoRepetido', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    pedido.codigo = 'HC3GL99ZOZOAK25NKIRL';
    pedido.descricao = 'Autocarros de 2 Andares';
    pedido.autonomia = 500000;
    pedido.custo = 10;
    pedido.velocidadeMedia = 30;
    pedido.consumo = 30;
    pedido.tipoCombustivel = 'DIESEL';
    pedido.emissoes = 1050;
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoFalse, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoSemDados', async () => {
    const pedido: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
    const tvBuilder = new TipoViaturaBuilderImpl();
    const director = new CriarTipoViaturaUseCaseDirector(tvUseCase, tvRepoTrue, loggerMock);
    expect((await director.processarPedido(pedido, tvBuilder)).sucesso).toBe(false);
});