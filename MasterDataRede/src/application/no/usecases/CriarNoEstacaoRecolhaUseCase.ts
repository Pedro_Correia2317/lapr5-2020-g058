

import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {NoEstacaoRecolhaBuilder} from '../services/NoEstacaoRecolhaBuilder';
import {CriarNoPontoRendicaoUseCase} from './CriarNoPontoRendicaoUseCase';

export interface CriarNoEstacaoRecolhaUseCase extends CriarNoPontoRendicaoUseCase {

    aplicarCapacidade(noBuilder: NoEstacaoRecolhaBuilder,
                        logger: ServicoLogger, capacidade: number): DetalhesOperacaoDTO;

}