
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { PercursoDTO } from 'src/app/models/percursos/PercursoDTO';

@Component({
  selector: 'adicao-percurso',
  templateUrl: './adicao-percurso.component.html',
  styleUrls: ['./adicao-percurso.component.css']
})
export class AdicaoPercursoComponent implements OnInit {

    codPercurso: string;

    constructor(public dialogRef: MatDialogRef<AdicaoPercursoComponent>) {
    }
  
    ngOnInit() {
    }

    onCancelarClick(){
        this.dialogRef.close();
    }

    adicionarCodigoPercurso(codPercurso: string){
        this.codPercurso = codPercurso;
    }

    onAdicionarClick(){
        let codPercurso = undefined;
        if(this.codPercurso != null){
            codPercurso = this.codPercurso;
        }
        this.dialogRef.close(codPercurso);
    }

    onClickedPath(percurso: PercursoDTO){
        this.adicionarCodigoPercurso(percurso.codigo);
    }

}