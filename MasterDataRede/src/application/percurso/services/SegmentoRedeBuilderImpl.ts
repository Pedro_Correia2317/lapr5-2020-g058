

import { AbreviaturaNo } from "../../../domain/no/model/AbreviaturaNo";
import { DistanciaSegmento } from "../../../domain/percurso/model/DistanciaSegmento";
import { DuracaoSegmento } from "../../../domain/percurso/model/DuracaoSegmento";
import { SegmentoRede } from "../../../domain/percurso/model/SegmentoRede";
import { SequenciaSegmento } from "../../../domain/percurso/model/SequenciaSegmento";
import { SegmentoRedeBuilder } from "./SegmentoRedeBuilder";

export class SegmentoRedeBuilderImpl implements SegmentoRedeBuilder {

    private abreviaturaNoInicio: AbreviaturaNo;
    private abreviaturaNoFim: AbreviaturaNo;
    private distancia: DistanciaSegmento;
    private duracao: DuracaoSegmento;
    private sequencia: SequenciaSegmento;
    private numSegmentos: number;

    constructor(){
        this.reset();
    }

    reset(): void {
        this.abreviaturaNoInicio = null;
        this.abreviaturaNoFim = null;
        this.distancia = null;
        this.duracao = null;
        this.sequencia = null;
        this.numSegmentos = 1;
    }

    aplicarNoInicio(codigoNo: string): void {
        this.abreviaturaNoInicio = new AbreviaturaNo(codigoNo);
    }

    aplicarNoFim(codigoNo: string): void {
        this.abreviaturaNoFim = new AbreviaturaNo(codigoNo);
    }

    aplicarDistancia(distancia: number): void {
        this.distancia = new DistanciaSegmento(distancia);
    }

    aplicarDuracao(duracao: number): void {
        this.duracao = new DuracaoSegmento(duracao);
    }

    aplicarSequencia(sequencia: number): void {
        this.sequencia = new SequenciaSegmento(sequencia);
        this.numSegmentos++;
    }

    aplicarSequenciaAutomatico(): void {
        this.sequencia = new SequenciaSegmento(this.numSegmentos);
        this.numSegmentos++;
    }

    obterResultado(): SegmentoRede {
        return new SegmentoRede(
            this.abreviaturaNoInicio, this.abreviaturaNoFim,
            this.sequencia, this.distancia, this.duracao
            );
    }

}