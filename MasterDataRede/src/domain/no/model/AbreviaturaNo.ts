
export class AbreviaturaNo {

    private abreviatura: string;

    constructor(abreviatura: string){
        if (abreviatura === null || typeof abreviatura === 'undefined'){
            throw new ReferenceError('The inserted shortname is null');
        }
        const trimmedName = abreviatura.trim();
        if (!(/^[\wÀ-ú\d\s]{2,20}$/u).test(trimmedName)){
            const text = `The shortname ${trimmedName} does not obey the rules`;
            throw new SyntaxError(text);
        }
        this.abreviatura = trimmedName;
    }

    getValue(): string {
        return this.abreviatura;
    }

    toString(): string {
        return `AbreviaturaNo: [${this.abreviatura}]`;
    }

    equals(abreviatura: string): boolean {
        return this.abreviatura === abreviatura;
    }
}