
import { EmissoesTipoViatura } from '../../../../src/domain/tipoViatura/model/EmissoesTipoViatura';

test('testarCapacidadeNulo', () => {
    expect(() => {new EmissoesTipoViatura(null)}).toThrow(TypeError);
    expect(() => {new EmissoesTipoViatura(undefined)}).toThrow(TypeError);
});

test('testarCapacidadeValoresPositivos', () => {
    expect(new EmissoesTipoViatura(1)).toBeInstanceOf(EmissoesTipoViatura);
    expect(new EmissoesTipoViatura(13.0)).toBeInstanceOf(EmissoesTipoViatura);
    expect(new EmissoesTipoViatura(9999999)).toBeInstanceOf(EmissoesTipoViatura);
});

test('testarCapacidadeValoresNegativos', () => {
    expect(() => {new EmissoesTipoViatura(-11)}).toThrow(SyntaxError);
    expect(() => {new EmissoesTipoViatura(-21)}).toThrow(SyntaxError);
    expect(() => {new EmissoesTipoViatura(-1)}).toThrow(SyntaxError);
});

test('testarCapacidadeValoresCasasDecimais', () => {
    expect(() => {new EmissoesTipoViatura(1.3346)}).toThrow(TypeError);
    expect(() => {new EmissoesTipoViatura(2.0000000001)}).toThrow(TypeError);
    expect(() => {new EmissoesTipoViatura(-1.236)}).toThrow(TypeError);
});

test('testarCapacidadeValoresExtremos', () => {
    expect(() => {new EmissoesTipoViatura(10000000)}).toThrow(SyntaxError);
    expect(() => {new EmissoesTipoViatura(28481694619494)}).toThrow(SyntaxError);
});