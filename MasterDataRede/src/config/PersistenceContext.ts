
export class PersistenceContext {

    private static readonly mongoose = require('mongoose');

    public static async connectToDatabase(dbUri: string): Promise<boolean> {
        await this.mongoose.connect(dbUri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
        return this.mongoose.connection.readyState === 1;
    }


}