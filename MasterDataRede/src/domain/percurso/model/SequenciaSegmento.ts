
export class SequenciaSegmento {

    private sequencia: number;

    constructor(sequencia: number){
        if (!Number.isInteger(sequencia)){
            throw new TypeError(`The duration is not a integer: ${sequencia}`);
        }
        if (sequencia <= 0 || sequencia > 20){
            throw new SyntaxError(`The sequence ${sequencia} is <= or > 20`);
        }
        this.sequencia = sequencia;
    }

    getValue(): number {
        return this.sequencia;
    }

    toString(): string {
        return `SequenciaSegmento: [${this.sequencia}]`;
    }
}