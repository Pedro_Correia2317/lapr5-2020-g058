
import { CodigoTipoTripulante } from '../../../src/domain/tipoTripulante/model/CodigoTipoTripulante';
import { TipoTripulante } from '../../../src/domain/tipoTripulante/model/TipoTripulante';
import { TipoTripulanteRepository } from '../../../src/repository/interfaces/TipoTripulanteRepository';
import { AcessoDadosError } from '../../../src/application/utils/errors/AcessoDadosError';
import { TipoTripulanteBuilder } from '../../../src/application/tipoTripulante/services/TipoTripulanteBuilder';

export class TipoTripulanteRepositoryMockFalse implements TipoTripulanteRepository {

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return false;
    }

    async isDescricaoUnico(descricao: string): Promise<boolean> {
        return false;
    }

    async existe(codigo: string): Promise<boolean> {
        return false;
    }

    async save(objecto: TipoTripulante): Promise<boolean> {
        return false;
    }

    async findById(codigo: CodigoTipoTripulante): Promise<TipoTripulante> {
        return null;
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<TipoTripulante[]> {
        return null;
    }

    async count(): Promise<number> {
        return 0;
    }

    async obterTiposTripulantes(builder: TipoTripulanteBuilder, sort: string, numPag: number, tamPag: number, descricao: string): Promise<TipoTripulante[]> {
        return [];
    }
    
    async countComFiltros(descricao: string): Promise<number> {
        return 0;
    }

}

export class TipoTripulanteRepositoryMockTrue implements TipoTripulanteRepository {

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return true;
    }

    async isDescricaoUnico(descricao: string): Promise<boolean> {
        return true;
    }

    async existe(codigo: string): Promise<boolean> {
        return true;
    }

    async save(objecto: TipoTripulante): Promise<boolean> {
        return true;
    }

    async findById(codigo: CodigoTipoTripulante): Promise<TipoTripulante> {
        return null;
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<TipoTripulante[]> {
        return null;
    }

    async count(): Promise<number> {
        return 0;
    }

    async obterTiposTripulantes(builder: TipoTripulanteBuilder, sort: string, numPag: number, tamPag: number, descricao: string): Promise<TipoTripulante[]> {
        throw new AcessoDadosError('Teste');
    }
    
    async countComFiltros(descricao: string): Promise<number> {
        throw new AcessoDadosError('Teste');
    }

}