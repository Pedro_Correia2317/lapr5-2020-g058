import { NoEstacaoRecolhaDTO } from "../dto/NoEstacaoRecolhaDTO";
import { TempoDeslocacaoTripulacaoDTO } from "../dto/TempoDeslocacaoTripulacaoDTO";
import { AbreviaturaNo } from "./AbreviaturaNo";
import { CapacidadeVeiculos } from "./CapacidadeVeiculos";
import { CoordenadasNo } from "./CoordenadasNo";
import { NomeNo } from "./NomeNo";
import { PontoRendicao } from "./PontoRendicao";
import { TempoDeslocacaoTripulacao } from "./TempoDeslocacaoTripulacao";
import { TipoNo } from "./TipoNo";

export class NoEstacaoRecolha implements PontoRendicao {

    private abreviatura: AbreviaturaNo;

    private nome: NomeNo;

    private coordenadas: CoordenadasNo;

    private tempos: TempoDeslocacaoTripulacao[];

    private capacidade: CapacidadeVeiculos;

    constructor(abr: AbreviaturaNo, nome: NomeNo, coor: CoordenadasNo, tempos: TempoDeslocacaoTripulacao[], cap: CapacidadeVeiculos){
        if(nome == null || coor == null || abr == null || tempos == null || cap == null){
                    throw new SyntaxError('There are null values');
        }
        for(const tempo of tempos){
            if(tempo == null){
                throw new ReferenceError('There is a null crew travel time');
            }
        }
        this.nome = nome;
        this.coordenadas = coor;
        this.abreviatura = abr;
        this.capacidade = cap;
        this.tempos = tempos.slice();
    }

    toTempoDeslocacaoTripulacaoDTO(): TempoDeslocacaoTripulacaoDTO[] {
        const lista: TempoDeslocacaoTripulacaoDTO[] = [];
        for(const tempo of this.tempos){
            const dto = new TempoDeslocacaoTripulacaoDTO();
            dto.no = tempo.getNo().getValue();
            dto.tempo = tempo.getTempo();
            lista.push(dto);
        }
        return lista;
    }

    toBaseDTO(): NoEstacaoRecolhaDTO {
        const dto = new NoEstacaoRecolhaDTO();
        dto.abreviatura = this.abreviatura.getValue();
        dto.nome = this.nome.getValue();
        dto.latitude = this.coordenadas.getLatitude();
        dto.longitude = this.coordenadas.getLongitude();
        dto.capacidade = this.capacidade.getValue();
        dto.tempos = [];
        this.tempos.forEach((tempo) => {
            dto.tempos.push({tempo: tempo.getTempo(), abreviaturaNo: tempo.getNo().getValue()});
        });
        dto.tipoNo = TipoNo.NO_ESTACAO_RECOLHA.toBaseDTO().nome;
        return dto;
    }

    tipoNo(): TipoNo {
        return TipoNo.NO_ESTACAO_RECOLHA;
    }

}