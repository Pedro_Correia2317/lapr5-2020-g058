
import { TipoViaturaBuilder } from '../../../src/application/tipoViatura/services/TipoViaturaBuilder';
import { AcessoDadosError } from '../../../src/application/utils/errors/AcessoDadosError';
import { CodigoTipoViatura } from '../../../src/domain/tipoViatura/model/CodigoTipoViatura';
import { TipoViatura } from '../../../src/domain/tipoViatura/model/TipoViatura';
import { TipoViaturaRepository } from '../../../src/repository/interfaces/TipoViaturaRepository';

export class TipoViaturaRepositoryMockTrue implements TipoViaturaRepository {

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return true;
    }

    async isDescricaoUnico(descricao: string): Promise<boolean> {
        return true;
    }

    async existe(codigo: string): Promise<boolean> {
        return true;
    }

    async save(objecto: TipoViatura): Promise<boolean> {
        return true;
    }

    async findById(codigo: CodigoTipoViatura): Promise<TipoViatura> {
        throw new Error('Method not implemented.');
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<TipoViatura[]> {
        throw new Error('Method not implemented.');
    }

    async count(): Promise<number> {
        throw new Error('Method not implemented.');
    }

    async obterTiposViaturas(builder: TipoViaturaBuilder, sort: string, numPag: number, tamPag: number, descricao: string): Promise<TipoViatura[]> {
        return [];
    }

    async countComFiltros(descricao: string): Promise<number> {
        return 0;
    }

}

export class TipoViaturaRepositoryMockFalse implements TipoViaturaRepository {

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return false;
    }
    
    async isDescricaoUnico(descricao: string): Promise<boolean> {
        return false;
    }

    async existe(codigo: string): Promise<boolean> {
        return false;
    }

    async save(objecto: TipoViatura): Promise<boolean> {
        return false;
    }

    async findById(codigo: CodigoTipoViatura): Promise<TipoViatura> {
        throw new Error('Method not implemented.');
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<TipoViatura[]> {
        throw new Error('Method not implemented.');
    }

    async count(): Promise<number> {
        return 0;
    }

    async obterTiposViaturas(builder: TipoViaturaBuilder, sort: string, numPag: number, tamPag: number, descricao: string): Promise<TipoViatura[]> {
        throw new AcessoDadosError('Teste');
    }
    
    async countComFiltros(descricao: string): Promise<number> {
        throw new AcessoDadosError('Teste');
    }

}