import { LinhaRepository } from "../../../repository/interfaces/LinhaRepository";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { TipoTripulanteRepository } from "../../../repository/interfaces/TipoTripulanteRepository";
import { TipoViaturaRepository } from "../../../repository/interfaces/TipoViaturaRepository";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { LinhaBuilder } from "../services/LinhaBuilder";

export interface CriarLinhaUseCase {

    aplicarCodigo(lBuilder: LinhaBuilder, logger: ServicoLogger, todasLinhas: LinhaRepository, codigoP: string): Promise<DetalhesOperacaoDTO>;

    aplicarNome(lBuilder: LinhaBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO;

    adicionarPercursoIda(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO>;

    adicionarPercursoVolta(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO>;

    adicionarPercursoReforco(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO>;

    adicionarPercursoVazio(lBuilder: LinhaBuilder, logger: ServicoLogger, todosPercursos: PercursoRepository, codigoP: string): Promise<DetalhesOperacaoDTO>;

    adicionarTipoTripulante(lBuilder: LinhaBuilder, logger: ServicoLogger, todasTiposT: TipoTripulanteRepository, codigoT: string): Promise<DetalhesOperacaoDTO>;

    adicionarTipoViatura(lBuilder: LinhaBuilder, logger: ServicoLogger, todasTiposV: TipoViaturaRepository, codigoV: string): Promise<DetalhesOperacaoDTO>;

    aplicarTipoNecessidadeTipoTripulante(lBuilder: LinhaBuilder, logger: ServicoLogger, codigoT: string): DetalhesOperacaoDTO;

    aplicarTipoNecessidadeTipoViatura(lBuilder: LinhaBuilder, logger: ServicoLogger, codigoV: string): DetalhesOperacaoDTO;

    registarLinha(lBuilder: LinhaBuilder, logger: ServicoLogger, todasLinhas: LinhaRepository): Promise<DetalhesOperacaoDTO>;

}