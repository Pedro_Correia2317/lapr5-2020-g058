


import { AbreviaturaNo } from "../../../domain/no/model/AbreviaturaNo";
import { CapacidadeVeiculos } from "../../../domain/no/model/CapacidadeVeiculos";
import { CoordenadasNo } from "../../../domain/no/model/CoordenadasNo";
import { NoEstacaoRecolha } from "../../../domain/no/model/NoEstacaoRecolha";
import { NomeNo } from "../../../domain/no/model/NomeNo";
import { TempoDeslocacaoTripulacao } from "../../../domain/no/model/TempoDeslocacaoTripulacao";
import { NoEstacaoRecolhaBuilder } from "./NoEstacaoRecolhaBuilder";

export class NoEstacaoRecolhaBuilderImpl implements NoEstacaoRecolhaBuilder {

    private capacidade: CapacidadeVeiculos;
    private nome: NomeNo;
    private abreviatura: AbreviaturaNo;
    private coordenadas: CoordenadasNo;
    private tempos: TempoDeslocacaoTripulacao[];

    constructor(){
        this.reset();
    }

    reset(): void {
        this.capacidade = null;
        this.nome = null;
        this.abreviatura = null;
        this.coordenadas = null;
        this.tempos = [];
    }

    isNoIgual(abreviatura: string): boolean {
        return this.abreviatura.equals(abreviatura);
    }

    adicionarTempoDeslocacaoTripulacao(tempo: number, abreviatura: string): void {
        const abr = new AbreviaturaNo(abreviatura);
        this.tempos.push(new TempoDeslocacaoTripulacao(tempo, abr));
    }

    aplicarCapacidadeVeiculos(capacidade: number): void {
        this.capacidade = new CapacidadeVeiculos(capacidade);
    }

    aplicarNome(nome: string): void {
        this.nome = new NomeNo(nome);
    }

    aplicarAbreviatura(abreviatura: string): void {
        this.abreviatura = new AbreviaturaNo(abreviatura);
    }

    aplicarCoordenadas(lat: number, lon: number): void {
        this.coordenadas = new CoordenadasNo(lat, lon);
    }

    obterResultado(): NoEstacaoRecolha {
        return new NoEstacaoRecolha(this.abreviatura, this.nome,
                    this.coordenadas, this.tempos, this.capacidade
        );
    }
    
}