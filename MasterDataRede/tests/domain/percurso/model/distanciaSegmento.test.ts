
import { DistanciaSegmento } from '../../../../src/domain/percurso/model/DistanciaSegmento';

test("testarDistanciaSegmentoNulo", () => {
    expect(() => {new DistanciaSegmento(null)}).toThrow(TypeError);
    expect(() => {new DistanciaSegmento(undefined)}).toThrow(TypeError);
});

test("testarDistanciaNegativo", () => {
    expect(() => {new DistanciaSegmento(-1)}).toThrow(SyntaxError);
});

test("testarDistanciaNegativo2", () => {
    expect(() => {new DistanciaSegmento(-12.4563)}).toThrow(SyntaxError);
});

test("testarDistanciaInvalido", () => {
    expect(() => {new DistanciaSegmento(25000.001)}).toThrow(SyntaxError);
});

test("testarDistanciaInvalido2", () => {
    expect(() => {new DistanciaSegmento(64000.24567)}).toThrow(SyntaxError);
});

test("testarDistanciaInvalido3", () => {
    expect(() => {new DistanciaSegmento(0)}).toThrow(SyntaxError);
});

test("testarDistanciaPequenoValido", () => {
    expect(new DistanciaSegmento(1)).toBeInstanceOf(DistanciaSegmento);
});

test("testarDistanciaPequenoValido2", () => {
    expect(new DistanciaSegmento(25000)).toBeInstanceOf(DistanciaSegmento);
});

test("testarDistanciaPequenoValido3", () => {
    expect(new DistanciaSegmento(0.4828)).toBeInstanceOf(DistanciaSegmento);
});

test("testarDistanciaValido4", () => {
    expect(new DistanciaSegmento(24999.9999)).toBeInstanceOf(DistanciaSegmento);
});

test("testarDistanciaValido5", () => {
    expect(new DistanciaSegmento(5000)).toBeInstanceOf(DistanciaSegmento);
});

test("testarDistanciaValido6", () => {
    expect(new DistanciaSegmento(25000.000001)).toBeInstanceOf(DistanciaSegmento);
});