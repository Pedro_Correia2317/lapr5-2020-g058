
import { NoRepository } from "../../../../repository/interfaces/NoRepository";
import { DetalhesOperacaoDTO } from "../../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../../utils/services/ServicoLogger";
import { NoPontoRendicaoBuilder } from "../../services/NoPontoRendicaoBuilder";
import { CriarNoPontoRendicaoUseCase } from "../CriarNoPontoRendicaoUseCase";
import { CriarNoUseCase } from "../CriarNoUseCase";

export class CriarNoPontoRendicaoUseCaseImpl implements CriarNoPontoRendicaoUseCase {

    private criarNo: CriarNoUseCase;

    constructor(criarNo: CriarNoUseCase){
        this.criarNo = criarNo;
    }

    async adicionarTempoDeslocacaoTripulacao(noBuilder: NoPontoRendicaoBuilder, 
                        logger: ServicoLogger, todosNos: NoRepository, 
                        tempo: number, abreviatura: string): Promise<DetalhesOperacaoDTO> {
        const resultDTO = new DetalhesOperacaoDTO();
        try {
            if(noBuilder.isNoIgual(abreviatura) || await todosNos.existeNo(abreviatura)){
                noBuilder.adicionarTempoDeslocacaoTripulacao(tempo, abreviatura);
                resultDTO.sucesso = true;
            } else {
                logger.registarFalha(new Error('Unkown node inserted'));
                resultDTO.mensagem = 'createNode.unknownNodeInserted';
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultDTO.mensagem = 'createNode.nullCrewTravelTime';
            } else if (err instanceof SyntaxError){
                resultDTO.mensagem = 'createNode.invalidCrewTravelTime';
            } else {
                throw err;
            }
        }
        return resultDTO;
    }

    aplicarNome(noBuilder: NoPontoRendicaoBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO {
        return this.criarNo.aplicarNome(noBuilder, logger, nome);
    }

    async aplicarAbreviatura(noBuilder: NoPontoRendicaoBuilder, logger: ServicoLogger, todosNos: NoRepository, abreviatura: string): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.aplicarAbreviatura(noBuilder, logger, todosNos, abreviatura);
    }

    aplicarCoordenadas(noBuilder: NoPontoRendicaoBuilder, logger: ServicoLogger, lat: number, lon: number): DetalhesOperacaoDTO {
        return this.criarNo.aplicarCoordenadas(noBuilder, logger, lat, lon);
    }

    registarNo(noBuilder: NoPontoRendicaoBuilder, logger: ServicoLogger, todosNos: NoRepository): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.registarNo(noBuilder, logger, todosNos);
    }

}