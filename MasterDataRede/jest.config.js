module.exports = {
  'preset': 'ts-jest',
  'testEnvironment': 'node',
  'moduleNameMapper': {
    'mockData': '<rootDir>/tests/data',
    'mockService': '<rootDir>/tests/services'
  }
};