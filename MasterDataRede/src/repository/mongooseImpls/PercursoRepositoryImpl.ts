
import Mongoose, { Schema } from 'mongoose';
import { PercursoBuilder } from '../../application/percurso/services/PercursoBuilder';
import { SegmentoRedeBuilder } from '../../application/percurso/services/SegmentoRedeBuilder';
import { AcessoDadosError } from '../../application/utils/errors/AcessoDadosError';
import { CodigoPercurso } from '../../domain/percurso/model/CodigoPercurso';
import { Percurso } from '../../domain/percurso/model/Percurso';
import { PercursoRepository } from '../interfaces/PercursoRepository';
import { IPercurso } from './models/IPercurso';
import { MongooseRepositoryImpl } from './RepositoryImpl';

export class PercursoRepositoryImpl implements PercursoRepository {

    private static readonly SCHEMA_PERCURSO: Mongoose.Schema = new Schema({
        'codigo': {'type': String, 'required': true, 'unique': true},
        'descricao': {'type': String, 'required': true, 'unique': false},
        'distanciaTotal': {'type': Number, 'required': true, 'unique': false},
        'duracaoTotal': {'type': Number, 'required': true, 'unique': false},
        'segmentos': [
            {
            'abreviaturaNoInicio': {'type': String, 'required': true},
            'abreviaturaNoFim': {'type': String, 'required': true},
            'duracao': {'type': Number, 'required': true},
            'distancia': {'type': Number, 'required': true},
            'sequencia': {'type': Number, 'required': true}
            }
        ]
    });

    private static readonly MODELO_PERCURSO 
                    = Mongoose.model<IPercurso>('Percurso', PercursoRepositoryImpl.SCHEMA_PERCURSO);

    static BASE_REPO: MongooseRepositoryImpl = new MongooseRepositoryImpl();

    private modelarPercurso(percurso: Percurso): IPercurso {
        const modelo = new PercursoRepositoryImpl.MODELO_PERCURSO(),
        percursoDTO = percurso.toBaseDTO();
       modelo.codigo = percursoDTO.codigo;
       modelo.descricao = percursoDTO.descricao;
       modelo.distanciaTotal = percursoDTO.distanciaTotal;
       modelo.duracaoTotal = percursoDTO.duracaoTotal;
       percurso.toSegmentosDTO().forEach(segmento => {
           modelo.segmentos.push({
               'abreviaturaNoInicio': segmento.abreviaturaNoInicio,
               'abreviaturaNoFim': segmento.abreviaturaNoFim,
               'duracao': segmento.duracao,
               'distancia': segmento.distancia,
               'sequencia': segmento.sequencia
           });
       });

       return modelo;
    }
    
    isCodigoUnico(codigo: string): Promise<boolean> {
        return PercursoRepositoryImpl.BASE_REPO.isUnico(PercursoRepositoryImpl.MODELO_PERCURSO, {codigo});
    }

    async existe(codigo: string): Promise<boolean> {
        return await PercursoRepositoryImpl.MODELO_PERCURSO.exists({codigo});
    }

    save(objecto: Percurso): Promise<boolean> {
        const modeloNo: IPercurso = this.modelarPercurso(objecto);
        return PercursoRepositoryImpl.BASE_REPO.save(modeloNo);
    }
    
    private mapearDataModel(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, dataModel: IPercurso): Percurso {
        pBuilder.reset();
        sBuilder.reset();
        pBuilder.aplicarCodigo(dataModel.codigo);
        dataModel.segmentos.forEach((segmento) => {
            sBuilder.aplicarDistancia(segmento.distancia);
            pBuilder.aplicarNovaDistancia(segmento.distancia);
            sBuilder.aplicarDuracao(segmento.duracao);
            pBuilder.aplicarNovaDuracao(segmento.duracao);
            sBuilder.aplicarSequenciaAutomatico();
            sBuilder.aplicarNoInicio(segmento.abreviaturaNoInicio);
            pBuilder.aplicarNovaAbreviaturaNoInicial(segmento.abreviaturaNoInicio);
            sBuilder.aplicarNoFim(segmento.abreviaturaNoFim);
            pBuilder.aplicarNovaAbreviaturaNoFinal(segmento.abreviaturaNoFim);
            pBuilder.aplicarSegmento(sBuilder.obterResultado());
        });
        return pBuilder.obterResultado();
    }

    async findByCodigo(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, codigo: string): Promise<Percurso> {
        let percurso = null;
        await PercursoRepositoryImpl.MODELO_PERCURSO.findOne({codigo}).then((resultado) => {
            percurso = this.mapearDataModel(pBuilder, sBuilder, resultado);
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return percurso;
    }

    async obterPercursos(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,sort: string, numPag: number, 
                        tamPag: number, descricao: string): Promise<Percurso[]> {
        const percursos: Percurso[] = [];
        const regexDesc = new RegExp(descricao, 'i');
        await PercursoRepositoryImpl.MODELO_PERCURSO.find({descricao: regexDesc}).sort(sort)
                                        .skip((numPag-1) * tamPag).limit(tamPag).then((results) => {
            results.forEach((perc) => {
                percursos.push(this.mapearDataModel(pBuilder, sBuilder, perc));
            });
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return percursos;
    }

    async countComFiltros(descricao: string): Promise<number> {
        let numero: number;
        const regexDesc = new RegExp(descricao, 'i');
        await PercursoRepositoryImpl.MODELO_PERCURSO.countDocuments({descricao: regexDesc}).exec().then((num) => {
            numero = num;
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return numero;
    }

    findById(codigo: CodigoPercurso): Promise<Percurso> {
        throw new Error("Method not implemented.");
    }

    findPage(numPagina: number, tamanhoPagina: number): Promise<Percurso[]> {
        throw new Error("Method not implemented.");
    }

    count(): Promise<number> {
        throw new Error("Method not implemented.");
    }

}