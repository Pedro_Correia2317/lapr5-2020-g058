import { ImportadorDadosFicheiro } from "./ImportadorDadosFicheiro";
import { ImportadorDadosFicheiroContext } from "./ImportadorDadosFicheiroContext";


export class ImportadorDadosFicheiroContextImpl implements ImportadorDadosFicheiroContext {

    private mapaImportadores: Map<string, ImportadorDadosFicheiro>;

    constructor(mapaImportadores: Map<string, ImportadorDadosFicheiro>){
        this.mapaImportadores = mapaImportadores;
    }

    obterImportadorAdequado(directorio: string): ImportadorDadosFicheiro {
        const aux: string = directorio.trim();
        return this.mapaImportadores.get(aux.substring(aux.lastIndexOf('.')));
    }
    
}