import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { TipoTripulanteDTO } from 'src/app/models/tiposTripulantes/TipoTripulanteDTO';
import { AcessoTiposTripulantesService } from 'src/app/services/rede/AcessoTiposTripulantes.service';

@Component({
  selector: 'visualizador-tipos-tripulantes',
  templateUrl: './visualizador-tipos-tripulantes.component.html',
  styleUrls: ['./visualizador-tipos-tripulantes.component.css']
})
export class VisualizadorTiposTripulantesComponent implements OnInit {

  @Output() tipoTripulanteClicada: EventEmitter<TipoTripulanteDTO> = new EventEmitter();

  tiposTripulantes: TipoTripulanteDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  pesquisa: string;

  constructor(private todosTiposT: AcessoTiposTripulantesService) {
    this.tiposTripulantes = [];
  }

  ngOnInit() {
  }

  onClickedDriverType(tipoTripulanteSelecionado: TipoTripulanteDTO){
    this.tipoTripulanteClicada.emit(tipoTripulanteSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarTiposTripulantes(pagina, this.tamanho, this.sort);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarTiposTripulantes(1, this.tamanho, this.sort, pesquisa);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarTiposTripulantes(1, tamanho, this.sort, this.pesquisa);
  }

  private atualizarTiposTripulantes(pagina: number, tamanho: number, sort?: string, pesquisa?: string){
    this.todosTiposT.procurarTiposTripulantes(pagina, tamanho, sort, pesquisa).subscribe((resultados: DetalhesPesquisaDTO<TipoTripulanteDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.tiposTripulantes = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
      }
    });
  }

}
