import Mongoose, { Schema } from "mongoose";
import { LinhaBuilder } from "../../application/linha/services/LinhaBuilder";
import { AcessoDadosError } from "../../application/utils/errors/AcessoDadosError";
import { PercursoLinhaDTO } from "../../domain/linha/dto/PercursoLinhaDTO";
import { CodigoLinha } from "../../domain/linha/model/CodigoLinha";
import { Linha } from "../../domain/linha/model/Linha";
import { LinhaRepository } from "../interfaces/LinhaRepository";
import { ILinha } from "./models/ILinha";
import { MongooseRepositoryImpl } from "./RepositoryImpl";


export class LinhaRepositoryImpl implements LinhaRepository {

    private static readonly SCHEMA_LINHA: Mongoose.Schema = new Schema({
        'codigo': {'type': String, 'required': true, 'unique': true},
        'nome': {'type': String, 'required': true, 'unique': false},
        'percursosIda': [{'type': String, 'required': true}],
        'percursosVolta': [{'type': String, 'required': true}],
        'percursosReforco': [{'type': String, 'required': false}],
        'percursosVazio': [{'type': String, 'required': false}],
        'necessidadesTipoViatura': [{'type': String, 'required': false}],
        'tipoNecessidadesTipoViatura': {'type': String, 'required': true, 'unique': false},
        'necessidadesTipoTripulante': [{'type': String, 'required': false}],
        'tipoNecessidadesTipoTripulante': {'type': String, 'required': true, 'unique': false}
    });

    private static readonly MODELO_LINHA 
                    = Mongoose.model<ILinha>('Linha', LinhaRepositoryImpl.SCHEMA_LINHA);

    static BASE_REPO: MongooseRepositoryImpl = new MongooseRepositoryImpl();

    private modelarLinha(linha: Linha): ILinha {
        const modelo = new LinhaRepositoryImpl.MODELO_LINHA();
        const linhaDTO = linha.toBaseDTO();
        modelo.codigo = linhaDTO.codigo;
        modelo.nome = linhaDTO.nome;
        linha.toPercursosIdaDTO().forEach((percDTO: PercursoLinhaDTO) => {
            modelo.percursosIda.push(percDTO.codigoPercurso);
        });
        linha.toPercursosVoltaDTO().forEach((percDTO: PercursoLinhaDTO) => {
            modelo.percursosVolta.push(percDTO.codigoPercurso);
        });
        linha.toPercursosReforcoDTO().forEach((percDTO: PercursoLinhaDTO) => {
            modelo.percursosReforco.push(percDTO.codigoPercurso);
        });
        linha.toPercursosVazioDTO().forEach((percDTO: PercursoLinhaDTO) => {
            modelo.percursosVazio.push(percDTO.codigoPercurso);
        });
        const nTiposV = linha.toNecessidadesTipoViaturaDTO();
        nTiposV.codigoTipoViatura.forEach((cod: string) => {
            modelo.necessidadesTipoViatura.push(cod);
        });
        modelo.tipoNecessidadesTipoViatura = nTiposV.tipoNecessidade;
        const nTiposT = linha.toNecessidadesTipoTripulanteDTO();
        nTiposT.codigoTipoTripulante.forEach((cod: string) => {
            modelo.necessidadesTipoTripulante.push(cod);
        });
        modelo.tipoNecessidadesTipoTripulante = nTiposT.tipoNecessidade;
        return modelo;
    }

    isCodigoUnico(codigo: string): Promise<boolean> {
        return LinhaRepositoryImpl.BASE_REPO.isUnico(LinhaRepositoryImpl.MODELO_LINHA, {codigo});
    }

    save(objecto: Linha): Promise<boolean> {
        const modeloLinha: ILinha = this.modelarLinha(objecto);
        return LinhaRepositoryImpl.BASE_REPO.save(modeloLinha);
    }

    private mapearDataModel(builder: LinhaBuilder, dataModel: ILinha): Linha {
        builder.reset();
        builder.aplicarCodigo(dataModel.codigo);
        builder.aplicarNome(dataModel.nome);
        dataModel.percursosIda.forEach((codPerc) => {
            builder.adicionarPercursoIda(codPerc);
        });
        dataModel.percursosVolta.forEach((codPerc) => {
            builder.adicionarPercursoVolta(codPerc);
        });
        dataModel.percursosReforco.forEach((codPerc) => {
            builder.adicionarPercursoReforco(codPerc);
        });
        dataModel.percursosVazio.forEach((codPerc) => {
            builder.adicionarPercursoVazio(codPerc);
        });
        dataModel.necessidadesTipoViatura.forEach((codTipoV) => {
            builder.adicionarTipoViatura(codTipoV);
        });
        dataModel.necessidadesTipoTripulante.forEach((codTipoT) => {
            builder.adicionarTipoTripulante(codTipoT);
        });
        builder.adicionarTipoNecessidadeTipoViatura(dataModel.tipoNecessidadesTipoViatura);
        builder.adicionarTipoNecessidadeTipoTripulante(dataModel.tipoNecessidadesTipoTripulante);
        return builder.obterResultado();
    }
    
    async findByCodigo(builder: LinhaBuilder, codigo: string): Promise<Linha> {
        let linha = null;
        await LinhaRepositoryImpl.MODELO_LINHA.findOne({codigo}).then((resultado) => {
            linha = this.mapearDataModel(builder, resultado);
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return linha;
    }

    async obterLinhas(builder: LinhaBuilder, sort: string, numPag: number, 
                        tamPag: number, codigo: string, nome: string): Promise<Linha[]> {
        const linhas: Linha[] = [];
        const regexCod = new RegExp(codigo, 'i');
        const regexNome = new RegExp(nome, 'i');
        await LinhaRepositoryImpl.MODELO_LINHA.find({codigo: regexCod, nome: regexNome}).sort(sort)
                                        .skip((numPag-1) * tamPag).limit(tamPag).then((results) => {
            results.forEach((no) => {
                linhas.push(this.mapearDataModel(builder, no));
            });
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return linhas;
    }

    async countComFiltros(codigo: string, nome: string): Promise<number> {
        let numero: number;
        const regexCod = new RegExp(codigo, 'i');
        const regexNome = new RegExp(nome, 'i');
        await LinhaRepositoryImpl.MODELO_LINHA.countDocuments({codigo: regexCod, nome: regexNome}).exec().then((num) => {
            numero = num;
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return numero;
    }

    findById(codigo: CodigoLinha): Promise<Linha> {
        throw new Error("Method not implemented.");
    }

    findPage(numPagina: number, tamanhoPagina: number): Promise<Linha[]> {
        throw new Error("Method not implemented.");
    }
    
    count(): Promise<number> {
        throw new Error("Method not implemented.");
    }

}