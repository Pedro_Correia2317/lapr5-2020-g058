
import { ImportadorDadosFicheiroGLX } from '../../../src/application/importacao/services/ImportadorDadosFicheiroGLX';
import { FicheiroNaoEncontradoError } from '../../../src/application/utils/errors/FicheiroNaoEncontradoError';
import { ParsingFicheiroError } from '../../../src/application/utils/errors/ParsingFicheiroError';

const importador = new ImportadorDadosFicheiroGLX();

test('testarFicheiroDesconhecido', async () => {
    await expect(importador.importarDadosFicheiro('ficheiroQueNaoVaiSerEncontrado.wow', false)).rejects.toThrow(FicheiroNaoEncontradoError);
});

test('testarFicheiroValido', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeValido1.glx', false);
    expect(elementos.length === 42).toBe(true);
});

test('testarFicheiroInvalido 1 (Sem XML)', async () => {
    await expect(importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemXML.glx', false)).rejects.toThrow(ParsingFicheiroError);
});

test('testarFicheiroInvalido 2 (Sem Percursos)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemPercursos.glx', false);
    expect(elementos.length === 24).toBe(true);
});

test('testarFicheiroInvalido 3 (Sem Linhas)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemLinhas.glx', false);
    expect(elementos.length === 37).toBe(true);
});

test('testarFicheiroInvalido 4 (Sem Nos)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemNos.glx', false);
    expect(elementos.length === 7).toBe(true);
});

test('testarFicheiroInvalido 5 (Sem Tipos Viaturas)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemTiposViaturas.glx', false);
    expect(elementos.length === 40).toBe(true);
});

test('testarFicheiroInvalido 6 (Sem Tipos Tripulantes)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemTiposTripulantes.glx', false);
    expect(elementos.length === 42).toBe(true);
});

test('testarFicheiroInvalido 7 (Sem Conteúdo)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoSemConteudo.glx', false);
    expect(elementos.length === 0).toBe(true);
});

test('testarFicheiroInvalido 8 (Estrutura Errada 1)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoEstruturaErrada1.glx', false);
    expect(elementos.length === 0).toBe(true);
});

test('testarFicheiroInvalido 9 (Estrutura Errada 2)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoEstruturaErrada2.glx', false);
    expect(elementos.length === 0).toBe(true);
});

test('testarFicheiroInvalido 10 (Estrutura Errada 3)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoEstruturaErrada3.glx', false);
    expect(elementos.length === 0).toBe(true);
});

test('testarFicheiroInvalido 11 (Estrutura Errada 4)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoEstruturaErrada4.glx', false);
    expect(elementos.length === 0).toBe(true);
});

test('testarFicheiroInvalido 12 (Estrutura Errada 5)', async () => {
    const elementos = await importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoEstruturaErrada5.glx', false);
    expect(elementos.length === 0).toBe(true);
});

test('testarFicheiroInvalido 13 (XML Com Erros)', async () => {
    await expect(importador.importarDadosFicheiro('tests/ficheiros/importacao/testeInvalidoXMLComErros.glx', false)).rejects.toThrow(ParsingFicheiroError);
});