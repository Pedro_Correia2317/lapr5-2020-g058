import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { TipoViaturaDTO } from 'src/app/models/tipoViatura/TipoViaturaDTO';
import { AcessoTiposViaturasService } from 'src/app/services/rede/AcessoTiposViaturasService';

@Component({
  selector: 'visualizador-tipos-viaturas',
  templateUrl: './visualizador-tipos-viaturas.component.html',
  styleUrls: ['./visualizador-tipos-viaturas.component.css']
})
export class VisualizadorTiposViaturasComponent implements OnInit {

  @Output() tipoViaturaClicada: EventEmitter<TipoViaturaDTO> = new EventEmitter();

  tiposViaturas: TipoViaturaDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  pesquisa: string;

  constructor(private todosTiposV: AcessoTiposViaturasService) {
    this.tiposViaturas = [];
  }

  ngOnInit() {
  }

  onClickedVehicleType(tipoVeiculoSelecionado: TipoViaturaDTO){
    this.tipoViaturaClicada.emit(tipoVeiculoSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarTiposViaturas(pagina, this.tamanho, this.sort);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarTiposViaturas(1, this.tamanho, this.sort, pesquisa);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarTiposViaturas(1, tamanho, this.sort, this.pesquisa);
  }

  private atualizarTiposViaturas(pagina: number, tamanho: number, sort?: string, pesquisa?: string){
    this.todosTiposV.procurarTiposViaturas(pagina, tamanho, sort, pesquisa).subscribe((resultados: DetalhesPesquisaDTO<TipoViaturaDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.tiposViaturas = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
      }
    });
  }

}
