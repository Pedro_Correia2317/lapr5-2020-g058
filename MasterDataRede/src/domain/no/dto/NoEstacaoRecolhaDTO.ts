

import { NoPontoRendicaoDTO } from "./NoPontoRendicaoDTO";

export class NoEstacaoRecolhaDTO extends NoPontoRendicaoDTO {

    public capacidade: number;

}