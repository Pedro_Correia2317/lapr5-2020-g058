
import { CriarNoPontoRendicaoUseCaseDirector } from '../../../src/application/no/director/CriarNoPontoRendicaoUseCaseDirector';
import { NoPontoRendicaoBuilderImpl } from '../../../src/application/no/services/NoPontoRendicaoBuilderImpl';
import { CriarNoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoUseCaseImpl';
import { CriarNoPontoRendicaoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoPontoRendicaoUseCaseImpl';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { NoRepositoryMockFalse, NoRepositoryMockTrue } from '../../mocks/no/NoRepositoryMock';
import { PedidoCriarNoPontoRendicaoDTO, PedidoTempoDeslocacaoDTO } from '../../../src/application/no/dto/PedidoCriarNoPontoRendicaoDTO';

const loggerMock = new ServicoLoggerMock();
const nUseCase = new CriarNoPontoRendicaoUseCaseImpl(new CriarNoUseCaseImpl());
const nRepoTrue = new NoRepositoryMockTrue();
const nRepoFalse = new NoRepositoryMockFalse();

test('testarPedidoValidoComValoresUnicos', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComAbreviaturaRepetida', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoFalse);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComAbreviaturaInvalida', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'Sou Invalido...';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemAbreviatura', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    // pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComNomeInvalido', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = '?';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemNome', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    // pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemLatitude', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    // pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemLongitude', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    // pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemTipoNo', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    // pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTipoNoInvalido1', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTipoNoInvalido2', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'TIPO_NO_INVALIDO';
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTemposDeslocacao', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1234;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTemposDeslocacaoNosInvalidos', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1234;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoFalse);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = -1245;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos2', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1245;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    //tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos3', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1245;
    tempo1.abreviaturaNo = 'ESTLO';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    //tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComTemposDeslocacaoTemposInvalidos4', async () => {
    const pedido: PedidoCriarNoPontoRendicaoDTO = new PedidoCriarNoPontoRendicaoDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tempo1: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo1.tempo = 1245;
    tempo1.abreviaturaNo = 'SouInvalido...';
    const tempo2: PedidoTempoDeslocacaoDTO = new PedidoTempoDeslocacaoDTO();
    tempo2.tempo = 1263;
    tempo2.abreviaturaNo = 'VCCAR';
    pedido.tempos = [tempo1, tempo2];
    const tvBuilder = new NoPontoRendicaoBuilderImpl();
    const director = new CriarNoPontoRendicaoUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});