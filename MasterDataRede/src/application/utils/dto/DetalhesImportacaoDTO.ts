import { ObjetoImportado } from "../../importacao/objetosImportacao/ObjetoImportado";

export class DetalhesImportacaoDTO {

    constructor(){
        this.sucesso = false;
        this.dadosImportados = null;
    }

    public sucesso: boolean;

    public descricaoErro: string;

    public dadosImportados: ObjetoImportado[];
}