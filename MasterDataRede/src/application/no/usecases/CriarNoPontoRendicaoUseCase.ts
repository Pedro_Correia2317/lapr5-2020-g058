
import {NoRepository} from '../../../repository/interfaces/NoRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {NoPontoRendicaoBuilder} from '../services/NoPontoRendicaoBuilder';
import {CriarNoUseCase} from './CriarNoUseCase';

export interface CriarNoPontoRendicaoUseCase extends CriarNoUseCase {

    adicionarTempoDeslocacaoTripulacao(noBuilder: NoPontoRendicaoBuilder,
                    logger: ServicoLogger, todosNos: NoRepository,
                    tempo: number, abreviatura: string): Promise<DetalhesOperacaoDTO>;

}