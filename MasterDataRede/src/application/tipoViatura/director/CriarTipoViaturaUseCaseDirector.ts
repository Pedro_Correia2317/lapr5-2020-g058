

import { TipoViaturaBuilder } from "../services/TipoViaturaBuilder";
import { CriarTipoViaturaUseCase } from "../usecases/CriarTipoViaturaUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoViaturaRepository } from "../../../repository/interfaces/TipoViaturaRepository";
import { PedidoCriarTipoViaturaDTO } from "../dto/PedidoCriarTipoViaturaDTO";

export class CriarTipoViaturaUseCaseDirector {

    private criarUseCase: CriarTipoViaturaUseCase;

    private todosTiposV: TipoViaturaRepository;

    private logger: ServicoLogger;

    constructor(c: CriarTipoViaturaUseCase, tv: TipoViaturaRepository, l: ServicoLogger){
        this.criarUseCase = c;
        this.todosTiposV = tv;
        this.logger = l;
    }

    async processarPedido(req: PedidoCriarTipoViaturaDTO, tBuilder: TipoViaturaBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarUseCase.aplicarCodigo(tBuilder, this.logger, this.todosTiposV, req.codigo);
        if (!resultado.sucesso) return resultado;
        resultado = await this.criarUseCase.aplicarDescricao(tBuilder, this.logger, this.todosTiposV, req.descricao);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarAutonomia(tBuilder, this.logger, req.autonomia);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarCusto(tBuilder, this.logger, req.custo);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarConsumo(tBuilder, this.logger, req.consumo);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarVelocidadeMedia(tBuilder, this.logger, req.velocidadeMedia);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarEmissoes(tBuilder, this.logger, req.emissoes);
        if (!resultado.sucesso) return resultado;
        resultado = this.criarUseCase.aplicarTipoCombustivel(tBuilder, this.logger, req.tipoCombustivel);
        if (!resultado.sucesso) return resultado;
        return await this.criarUseCase.registarTipoViatura(tBuilder, this.logger, this.todosTiposV);
    }
    
}