
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { Routes, RouterModule } from '@angular/router';
import { VisualizadorPercursosModule } from '../../visualizadores/visualizador-percursos/visualizador-percursos.module';
import { VisualizadorTiposTripulantesModule } from '../../visualizadores/visualizador-tipos-tripulantes/visualizador-tipos-tripulantes.module';
import { VisualizadorTiposViaturasModule } from '../../visualizadores/visualizador-tipos-viaturas/visualizador-tipos-viaturas.module';

import { AdicaoPercursoComponent } from './adicao-percurso/adicao-percurso.component';
import { AdicaoTipoTripulanteComponent } from './adicao-tipo-tripulante/adicao-tipo-tripulante.component';
import { AdicaoTipoViaturaComponent } from './adicao-tipo-viatura/adicao-tipo-viatura.component';
import { CriacaoLinhasComponent } from './criacao-linhas.component';

const routes: Routes = [
    {path: '', component: CriacaoLinhasComponent}
];

@NgModule({
  declarations: [AdicaoPercursoComponent, AdicaoTipoTripulanteComponent, AdicaoTipoViaturaComponent, CriacaoLinhasComponent],
  imports: [VisualizadorPercursosModule, 
            VisualizadorTiposViaturasModule, 
            VisualizadorTiposTripulantesModule,
            MatDialogModule, 
            CommonModule, 
            FormsModule, 
            ReactiveFormsModule, 
            RouterModule.forChild(routes)],
  exports: [RouterModule],
  entryComponents: [AdicaoPercursoComponent, AdicaoTipoTripulanteComponent, AdicaoTipoViaturaComponent]
})
export class CriacaoLinhasModule { }