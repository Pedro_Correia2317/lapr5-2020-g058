
export class Config {

    private static readonly dotenv = require('dotenv');
  
    public static SERVER_PORT: number;
  
    public static DATABASE_URL: string;
  
    public static LOG_LEVEL: string;
  
    static readConfigurationValues(path: string): boolean {
      const envFound = Config.dotenv.config({path: path});
      if (envFound.error) {
        return false;
      }
      this.SERVER_PORT = parseInt(process.env.PORT, 10);
      this.DATABASE_URL = process.env.DATABASE_URL;
      this.LOG_LEVEL = process.env.LOG_LEVEL;
      return true;
    }
  
  }