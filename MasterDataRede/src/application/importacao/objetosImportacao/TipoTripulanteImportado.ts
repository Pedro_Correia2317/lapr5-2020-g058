import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../director/ImportarDadosFicheiroUseCaseDirector";
import { ObjetoImportado } from "./ObjetoImportado";


export class TipoTripulanteImportado implements ObjetoImportado {

    public codigo: string;

    public descricao: string;

    visitar(director: ImportarDadosFicheiroUseCaseDirector): Promise<DetalhesOperacaoDTO> {
        return director.processarTipoTripulante(this);
    }
    
}