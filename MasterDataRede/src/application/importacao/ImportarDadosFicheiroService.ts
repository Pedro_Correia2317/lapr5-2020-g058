
import { DetalhesImportacaoDTO } from "../utils/dto/DetalhesImportacaoDTO";
import { ServicoLogger } from "../utils/services/ServicoLogger";
import { ImportadorDadosFicheiroContext } from "./services/ImportadorDadosFicheiroContext";

export interface ImportarDadosFicheiroService {

    importarDadosFicheiro(impContext: ImportadorDadosFicheiroContext, logger: ServicoLogger, dir: string, apagarFicheiro: boolean): Promise<DetalhesImportacaoDTO>;

}