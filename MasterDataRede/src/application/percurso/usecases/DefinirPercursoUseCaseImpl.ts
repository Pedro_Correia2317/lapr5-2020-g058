import {Percurso} from '../../../domain/percurso/model/Percurso';
import {NoRepository} from '../../../repository/interfaces/NoRepository';
import {PercursoRepository} from '../../../repository/interfaces/PercursoRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {PercursoBuilder} from '../services/PercursoBuilder';
import {SegmentoRedeBuilder} from '../services/SegmentoRedeBuilder';
import {DefinirPercursoUseCase} from './DefinirPercursoUseCase';

export class DefinirPercursoUseCaseImpl implements DefinirPercursoUseCase {

    async aplicarCodigo(
pBuilder: PercursoBuilder, logger: ServicoLogger,
                    todosPercursos: PercursoRepository,
                    codigo: string
): Promise<DetalhesOperacaoDTO> {
    const resultado = new DetalhesOperacaoDTO();
        try {
            if (await todosPercursos.isCodigoUnico(codigo)){
                pBuilder.aplicarCodigo(codigo);
                resultado.sucesso = true;
            } else {
                resultado.mensagem = 'newPath.repeatedCode';
                logger.registarFalha(new Error('Repeated path code inserted'));
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultado.mensagem = 'newPath.nullCode';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.invalidCode';
            } else {
                throw err;
            }
        }

return resultado;
    }

    async aplicarNoInicial(
pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                    logger: ServicoLogger, todosNosRede: NoRepository,
                    abreviaturaNo: string
): Promise<DetalhesOperacaoDTO> {
        const resultado = new DetalhesOperacaoDTO();
        try {
            if (await todosNosRede.existeNo(abreviaturaNo)){
                pBuilder.aplicarNovaAbreviaturaNoInicial(abreviaturaNo);
                sBuilder.aplicarNoInicio(abreviaturaNo);
                resultado.sucesso = true;
            } else {
                resultado.mensagem = 'newPath.initNodeNonRecognized';
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultado.mensagem = 'newPath.initNodeNull';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.initNodeInvalid';
            } else {
                throw err;
            }
        }

return resultado;
    }

    async aplicarNoFinal(
pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                    logger: ServicoLogger, todosNosRede: NoRepository,
                    abreviaturaNo: string
): Promise<DetalhesOperacaoDTO> {
        const resultado = new DetalhesOperacaoDTO();
        try {
            if (await todosNosRede.existeNo(abreviaturaNo)){
                pBuilder.aplicarNovaAbreviaturaNoFinal(abreviaturaNo);
                sBuilder.aplicarNoFim(abreviaturaNo);
                resultado.sucesso = true;
            } else {
                resultado.mensagem = 'newPath.endNodeNonRecognized';
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultado.mensagem = 'newPath.endNodeNull';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.endNodeInvalid';
            } else {
                throw err;
            }
        }

return resultado;
    }

    adicionarDuracao(
pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                    logger: ServicoLogger, duracao: number
): DetalhesOperacaoDTO {
    const resultado = new DetalhesOperacaoDTO();
        try {
            sBuilder.aplicarDuracao(duracao);
            pBuilder.aplicarNovaDuracao(duracao);
            resultado.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultado.mensagem = 'newPath.NaNSegDuration';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.InvalidSegDuration';
            } else {
                throw err;
            }
        }

return resultado;
    }

    adicionarDistancia(
pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                logger: ServicoLogger, distancia: number
): DetalhesOperacaoDTO {
    const resultado = new DetalhesOperacaoDTO();
        try {
            sBuilder.aplicarDistancia(distancia);
            pBuilder.aplicarNovaDistancia(distancia);
            resultado.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultado.mensagem = 'newPath.NaNSegDistance';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.InvalidSegDistances';
            } else {
                throw err;
            }
        }

return resultado;
    }

    adicionarSequencia(
sBuilder: SegmentoRedeBuilder, logger: ServicoLogger,
                                    sequencia: number
): DetalhesOperacaoDTO {
    const resultado = new DetalhesOperacaoDTO();
        try {
            sBuilder.aplicarSequencia(sequencia);
            resultado.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultado.mensagem = 'newPath.NaNSegSequence';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.InvalidSegSequence';
            } else {
                throw err;
            }
        }

return resultado;
    }

    adicionarSequenciaAutomatica(
sBuilder: SegmentoRedeBuilder,
                                    logger: ServicoLogger
): DetalhesOperacaoDTO {
    const resultado = new DetalhesOperacaoDTO();
        try {
            sBuilder.aplicarSequenciaAutomatico();
            resultado.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultado.mensagem = 'newPath.NaNSegSequence';
            } else if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.InvalidSegSequence';
            } else {
                throw err;
            }
        }

return resultado;
    }

    adicionarSegmentoCriadoAPercurso(
pBuilder: PercursoBuilder,
                                sBuilder: SegmentoRedeBuilder,
                                logger: ServicoLogger
): DetalhesOperacaoDTO {
    const resultado = new DetalhesOperacaoDTO();
        try {
            const segmentoRede = sBuilder.obterResultado();
            pBuilder.aplicarSegmento(segmentoRede);
            resultado.sucesso = true;
        } catch (err) {
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultado.mensagem = 'newPath.segCreationFailure';
            } else {
                throw err;
            }
        }

return resultado;
    }

    async registarPercurso(
pBuilder: PercursoBuilder, logger: ServicoLogger,
        todosPercursos: PercursoRepository
): Promise<DetalhesOperacaoDTO> {
    const resultado = new DetalhesOperacaoDTO();
            try {
                const percurso: Percurso = pBuilder.obterResultado();
                if (await todosPercursos.save(percurso)){
                    resultado.sucesso = true;
                    resultado.mensagem = 'newPath.creationSuccess';
                } else {
                    resultado.mensagem = 'newPath.errorRegisteringPath';
                }
            } catch (err) {
                logger.registarFalha(err);
                if (err instanceof SyntaxError){
                    resultado.mensagem = 'newPath.pathCreationFailure';
                } else {
                    throw err;
                }
            }

return resultado;
    }

}