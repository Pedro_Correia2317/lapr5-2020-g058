
export class SegmentoRedeDTO {

    public abreviaturaNoInicio: string;

    public abreviaturaNoFim: string;

    public distancia: number;

    public sequencia: number;

    public duracao: number;
}