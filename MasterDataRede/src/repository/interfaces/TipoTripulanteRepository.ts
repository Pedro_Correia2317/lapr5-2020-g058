
import { TipoTripulanteBuilder } from '../../application/tipoTripulante/services/TipoTripulanteBuilder';
import {CodigoTipoTripulante} from '../../domain/tipoTripulante/model/CodigoTipoTripulante';
import {TipoTripulante} from '../../domain/tipoTripulante/model/TipoTripulante';
import {Repository} from './Repository';

export interface TipoTripulanteRepository extends Repository<TipoTripulante, CodigoTipoTripulante> {

    isCodigoUnico(codigo: string): Promise<boolean>;

    isDescricaoUnico(descricao: string): Promise<boolean>;

    existe(codigo: string): Promise<boolean>;

    obterTiposTripulantes(builder: TipoTripulanteBuilder, sort: string, numPag: number, 
                    tamPag: number, descricao: string): Promise<TipoTripulante[]>;

    countComFiltros(descricao: string): Promise<number>;

}