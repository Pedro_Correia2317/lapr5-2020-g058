
import { DescricaoPercurso } from '../../../../src/domain/percurso/model/DescricaoPercurso';

test("testarDescricaoNulo", () => {
    expect(() => {new DescricaoPercurso(null)}).toThrow(ReferenceError);
    expect(() => {new DescricaoPercurso(undefined)}).toThrow(ReferenceError);
});

test("testarDescricaoVazio", () => {
    expect(() => {new DescricaoPercurso('')}).toThrow(SyntaxError);
});

test("testarDescricaoDemasiadoComprido", () => {
    expect(() => {new DescricaoPercurso('1234567890123456789012345678901234567890a')}).toThrow(SyntaxError); //251 chars
});

test("testarDescricaoDemasiadoComprido2", () => {
    expect(() => {new DescricaoPercurso('AGUIAR > 1234567890123456789012345678901234567890a')}).toThrow(SyntaxError); //251 chars
});

test("testarDescricaoDemasiadoComprido3", () => {
    expect(() => {new DescricaoPercurso('AGUIAR > ESTLO > 1234567890123456789012345678901234567890a')}).toThrow(SyntaxError); //251 chars
});

test("testarDescricaoDemasiadoComprido3", () => {
    expect(() => {new DescricaoPercurso('AGUIAR > ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > ESTPA > '
                            + 'ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > '
                            + 'ESTPA > ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > ESTPA')}).toThrow(SyntaxError); //21 estacoes
});

test("testarDescricaoUsualComCarateresInvalidos", () => {
    expect(() => {new DescricaoPercurso('🌺ESTPA > ESTLO🌺')}).toThrow(SyntaxError);
});

test("testarDescricaoDemasiadoPequeno", () => {
    expect(() => {new DescricaoPercurso('1')}).toThrow(SyntaxError);
});

test("testarDescricaoInvalida", () => {
    expect(() => {new DescricaoPercurso('ESTLO > ESTPA > ')}).toThrow(SyntaxError);
});

test("testarDescricaoPequenoValido", () => {
    expect(new DescricaoPercurso('12')).toBeInstanceOf(DescricaoPercurso);
});

test("testarDescricaoGrandeValido", () => {
    expect(new DescricaoPercurso('AGUIAR > ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > ESTPA > '
                + 'ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > ESTPA > ESTLO > '
                + 'ESTPA > ESTLO > ESTPA > ESTLO > ESTPA > ESTLO')).toBeInstanceOf(DescricaoPercurso); //20 estacoes
});

test("testarDescricaoUsualValido", () => {
    expect(new DescricaoPercurso('AGUIAR > ESTLO > ESTPA')).toBeInstanceOf(DescricaoPercurso);
});

test("testarDescricaoSoNumeros", () => {
    expect(new DescricaoPercurso('15367 > 363 > 211')).toBeInstanceOf(DescricaoPercurso);
});

test("testarDescricaoSoLetras", () => {
    expect(new DescricaoPercurso('cbjjcjh > fdHJJbbcjBJ > bjvUBbuubu > UDwbbeS')).toBeInstanceOf(DescricaoPercurso);
});

test("testarDescricaoSoEspacos", () => {
    expect(() => {new DescricaoPercurso('         ')}).toThrow(SyntaxError);
});

test("testarDescricaoUsualComCarateresAcentos", () => {
    expect(new DescricaoPercurso('Estação > Principal > N12 > Aguiar')).toBeInstanceOf(DescricaoPercurso);
});