import { SegmentoRedeBuilder } from "../../../src/application/percurso/services/SegmentoRedeBuilder";
import { AbreviaturaNo } from "../../../src/domain/no/model/AbreviaturaNo";
import { DistanciaSegmento } from "../../../src/domain/percurso/model/DistanciaSegmento";
import { DuracaoSegmento } from "../../../src/domain/percurso/model/DuracaoSegmento";
import { SegmentoRede } from "../../../src/domain/percurso/model/SegmentoRede";
import { SequenciaSegmento } from "../../../src/domain/percurso/model/SequenciaSegmento";

export class SegmentoRedeBuilderMockSucesso implements SegmentoRedeBuilder {

    reset(): void {}

    aplicarNoInicio(codigoNo: string): void {}

    aplicarNoFim(codigoNo: string): void {}
    aplicarDistancia(distancia: number): void {}
    aplicarDuracao(duracao: number): void {}
    aplicarSequencia(sequencia: number): void {}
    aplicarSequenciaAutomatico(): void {}
    obterResultado(): SegmentoRede {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        return new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao);
    }

}

export class SegmentoRedeBuilderMockErro implements SegmentoRedeBuilder {

    reset(): void {}

    aplicarNoInicio(codigoNo: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarNoFim(codigoNo: string): void {
        throw new SyntaxError('Teste');
    }

    aplicarDistancia(distancia: number): void {
        throw new SyntaxError('Teste');
    }

    aplicarDuracao(duracao: number): void {
        throw new SyntaxError('Teste');
    }

    aplicarSequencia(sequencia: number): void {
        throw new SyntaxError('Teste');
    }

    aplicarSequenciaAutomatico(): void {
        throw new SyntaxError('Teste');
    }
    
    obterResultado(): SegmentoRede {
        throw new SyntaxError('Teste');
    }

}