
import { NoParagemBuilder } from "../services/NoParagemBuilder";
import { CriarNoParagemUseCase } from "../usecases/CriarNoParagemUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { NoRepository } from "../../../repository/interfaces/NoRepository";
import { PedidoCriarNoParagemDTO } from '../dto/PedidoCriarNoParagemDTO';
import { CriarNoUseCaseDirectorCommand } from "./CriarNoUseCaseDirectorCommand";

export class CriarNoParagemUseCaseDirector implements CriarNoUseCaseDirectorCommand {

    private criarNoService: CriarNoParagemUseCase;

    private logger: ServicoLogger;

    private todosNos: NoRepository;

    constructor(c: CriarNoParagemUseCase, l: ServicoLogger, tn: NoRepository){
        this.criarNoService = c;
        this.logger = l;
        this.todosNos = tn;
    }
    
    async executar(req: PedidoCriarNoParagemDTO, noBuilder: NoParagemBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarNoService.aplicarAbreviatura(noBuilder, this.logger, this.todosNos, req.abreviatura);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarNome(noBuilder, this.logger, req.nome);
        if(!resultado.sucesso) return resultado;
        resultado = this.criarNoService.aplicarCoordenadas(noBuilder, this.logger, req.lat, req.lon);
        if(!resultado.sucesso) return resultado;
        return this.criarNoService.registarNo(noBuilder, this.logger, this.todosNos);
    }
}
