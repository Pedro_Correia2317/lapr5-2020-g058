
import 'reflect-metadata';
import {Config} from './config/Config';
import {DependencyInjectorContainer} from './config/DependencyInjectorContainer';
import {PersistenceContext} from './config/PersistenceContext';
import {ServerConnection} from './config/ServerConnection';

async function main() {
  if (!Config.readConfigurationValues('././.env')){
    throw new Error('Error reading the properties!!!!');
  }
  if (!await PersistenceContext.connectToDatabase(Config.DATABASE_URL)){
    throw new Error('Error connecting to database!!!!');
  }
  DependencyInjectorContainer.setAllDependencies(Config.LOG_LEVEL);
  ServerConnection.insertAllRoutes();
  ServerConnection.startServer(Config.SERVER_PORT);
  console.log(`Server has started on port ${Config.SERVER_PORT}!!!!!`);
}

main();