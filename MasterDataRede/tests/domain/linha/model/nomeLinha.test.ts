
import { NomeLinha } from '../../../../src/domain/linha/model/NomeLinha';

test("testarNomeNulo", () => {
    expect(() => {new NomeLinha(null)}).toThrow(ReferenceError);
    expect(() => {new NomeLinha(undefined)}).toThrow(ReferenceError);
});

test("testarNomeVazio", () => {
    expect(() => {new NomeLinha('')}).toThrow(SyntaxError);
});

test("testarNomeDemasiadoComprido", () => {
    expect(() => {new NomeLinha('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
                         + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
                         + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
                         + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetbv')}).toThrow(SyntaxError); //201 chars
});

test("testarNomeUsualComCarateresInvalidos", () => {
    expect(() => {new NomeLinha('🌺Estação Principal N12 de Aguiar🌺')}).toThrow(SyntaxError);
});

test("testarNomeDemasiadoPequeno", () => {
    expect(() => {new NomeLinha('1')}).toThrow(SyntaxError);
});

test("testarNomePequenoValido", () => {
    expect(new NomeLinha('12')).toBeInstanceOf(NomeLinha);
});

test("testarNomeGrandeValido", () => {
    expect(new NomeLinha('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
    + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
    + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
    + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetb')).toBeInstanceOf(NomeLinha); //200 chars
});

test("testarNomeUsualValido", () => {
    expect(new NomeLinha('Estacao Principal N12 de Aguiar')).toBeInstanceOf(NomeLinha);
});

test("testarNomeSoNumeros", () => {
    expect(new NomeLinha('15367363211')).toBeInstanceOf(NomeLinha);
});

test("testarNomeSoLetras", () => {
    expect(new NomeLinha('cbjjcjhfdHJJbbcjBJbjvUBbuubuUDwbbeS')).toBeInstanceOf(NomeLinha);
});

test("testarNomeSoEspacos", () => {
    expect(() => {new NomeLinha('         ')}).toThrow(SyntaxError);
});

test("testarNomeUsualComCarateresAcentos", () => {
    expect(new NomeLinha('Estação Principal N12 de Aguiar')).toBeInstanceOf(NomeLinha);
});