

export class DetalhesPesquisaDTO<T> {

    constructor(){
        this.sucesso = false;
        this.lista = [];
    }

    public sucesso: boolean;

    public count: number;

    public lista: T[];

    public descricao: string;

}