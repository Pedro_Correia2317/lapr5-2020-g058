import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { LinhaDTO } from 'src/app/models/linhas/LinhaDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { AcessoLinhasService } from 'src/app/services/rede/AcessoLinhas.service';

@Component({
  selector: 'visualizador-linhas',
  templateUrl: './visualizador-linhas.component.html',
  styleUrls: ['./visualizador-linhas.component.css']
})
export class VisualizadorLinhasComponent implements OnInit {

  @Output() linhaClicada: EventEmitter<LinhaDTO> = new EventEmitter();

  linhas: LinhaDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  pesquisa: string;

  constructor(private todasLinhas: AcessoLinhasService) {
    this.linhas = [];
  }

  ngOnInit() {
  }

  onClickedLine(linhaSelecionada: LinhaDTO){
    this.linhaClicada.emit(linhaSelecionada);
  }

  mudarPagina(pagina: number){
    this.atualizarLinhas(pagina, this.tamanho, this.sort);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarLinhas(1, this.tamanho, this.sort, pesquisa);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarLinhas(1, tamanho, this.sort, this.pesquisa);
  }

  private atualizarLinhas(pagina: number, tamanho: number, sort?: string, pesquisa?: string){
    this.todasLinhas.procurarLinhas(pagina, tamanho, sort, pesquisa).subscribe((resultados: DetalhesPesquisaDTO<LinhaDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.linhas = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
      }
    });
  }

}
