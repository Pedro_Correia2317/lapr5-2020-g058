

import { CriarTipoViaturaUseCaseImpl } from '../../../src/application/tipoViatura/usecases/CriarTipoViaturaUseCaseImpl';
import { TipoViaturaRepositoryMockFalse, TipoViaturaRepositoryMockTrue } from '../../mocks/tipoViatura/tipoViaturaRepositoryMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { TipoViaturaBuilderMockErro, TipoViaturaBuilderMockSucesso } from '../../mocks/tipoViatura/tipoViaturaBuilderMock';

const ttBuilderErro = new TipoViaturaBuilderMockErro();
const ttBuilderSucesso = new TipoViaturaBuilderMockSucesso();
const logger = new ServicoLoggerMock();
const ttRepoTrue = new TipoViaturaRepositoryMockTrue();
const ttRepoFalse = new TipoViaturaRepositoryMockFalse();
const useCase = new CriarTipoViaturaUseCaseImpl();

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderErro, logger, ttRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoRepetido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderSucesso, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoInvalido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderErro, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarCodigoValido", async () => {
    expect((await useCase.aplicarCodigo(ttBuilderSucesso, logger, ttRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarDescricaoInvalido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderErro, logger, ttRepoTrue, "")).sucesso).toBe(false);
});

test("testarAplicarDescricaoRepetido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderSucesso, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarDescricaoInvalido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderErro, logger, ttRepoFalse, "")).sucesso).toBe(false);
});

test("testarAplicarDescricaoValido", async () => {
    expect((await useCase.aplicarDescricao(ttBuilderSucesso, logger, ttRepoTrue, "")).sucesso).toBe(true);
});

test("testarAplicarAutonomiaInvalido", async () => {
    expect((useCase.aplicarAutonomia(ttBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarAutonomiaValido", async () => {
    expect((useCase.aplicarAutonomia(ttBuilderSucesso, logger, 0)).sucesso).toBe(true);
});

test("testarAplicarCustoInvalido", async () => {
    expect((useCase.aplicarCusto(ttBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarCustoValido", async () => {
    expect((useCase.aplicarCusto(ttBuilderSucesso, logger, 0)).sucesso).toBe(true);
});

test("testarAplicarEmissoesInvalido", async () => {
    expect((useCase.aplicarEmissoes(ttBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarEmissoesValido", async () => {
    expect((useCase.aplicarEmissoes(ttBuilderSucesso, logger, 0)).sucesso).toBe(true);
});

test("testarAplicarConsumoInvalido", async () => {
    expect((useCase.aplicarConsumo(ttBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarConsumoValido", async () => {
    expect((useCase.aplicarConsumo(ttBuilderSucesso, logger, 0)).sucesso).toBe(true);
});

test("testarAplicarVelocidadeMediaInvalido", async () => {
    expect((useCase.aplicarVelocidadeMedia(ttBuilderErro, logger, 0)).sucesso).toBe(false);
});

test("testarAplicarVelocidadeMediaValido", async () => {
    expect((useCase.aplicarVelocidadeMedia(ttBuilderSucesso, logger, 0)).sucesso).toBe(true);
});

test("testarAplicarTipoCombustivelInvalido", async () => {
    expect((useCase.aplicarTipoCombustivel(ttBuilderErro, logger, "")).sucesso).toBe(false);
});

test("testarAplicarTipoCombustivelValido", async () => {
    expect((useCase.aplicarTipoCombustivel(ttBuilderSucesso, logger, "")).sucesso).toBe(true);
});

test("testarRegistoTipoViaturaInvalido", async () => {
    expect((await useCase.registarTipoViatura(ttBuilderErro, logger, ttRepoTrue)).sucesso).toBe(false);
});

test("testarRegistoTipoViaturaErroAcessoBD", async () => {
    expect((await useCase.registarTipoViatura(ttBuilderSucesso, logger, ttRepoFalse)).sucesso).toBe(false);
});

test("testarRegistoTipoViaturaInvalido2", async () => {
    expect((await useCase.registarTipoViatura(ttBuilderErro, logger, ttRepoFalse)).sucesso).toBe(false);
});

test("testarRegistoTipoViaturaValido", async () => {
    expect((await useCase.registarTipoViatura(ttBuilderSucesso, logger, ttRepoTrue)).sucesso).toBe(true);
});