
import { CodigoTipoViatura } from "../../tipoViatura/model/CodigoTipoViatura";
import { NecessidadeTipoViaturaDTO } from "../dto/NecessidadeTipoViaturaDTO";
import { TipoNecessidade } from "./TipoNecessidade";

export class NecessidadesTipoViatura {

    private tiposViaturas: CodigoTipoViatura[];

    private tipoNecessidade: TipoNecessidade;

    constructor(tiposV: CodigoTipoViatura[], tipo: TipoNecessidade){
        if (tiposV == null || tipo == null){
            throw new ReferenceError('The inserted values are null');
        }
        if (tiposV.includes(null)){
            throw new ReferenceError('There are null vehicle types');
        }
        if (tiposV.length === 0 && tipo !== TipoNecessidade.NAO_APLICAVEL){
            throw new SyntaxError('No vehicle types, so necessity type ' + tipo + ' is invalid.');
        }
        if (tiposV.length !== 0 && tipo === TipoNecessidade.NAO_APLICAVEL){
            throw new SyntaxError('Vehicle types not empty, so necessity type ' + tipo + ' is invalid.');
        }
        this.tiposViaturas = tiposV.slice();
        this.tipoNecessidade = tipo;
    }

    getTiposViaturas(): CodigoTipoViatura[] {
        return this.tiposViaturas;
    }

    getTipoNecessidade(): TipoNecessidade {
        return this.tipoNecessidade;
    }
    toNecessidadesTipoViaturaDTO(): NecessidadeTipoViaturaDTO {
        const listaDTO = new NecessidadeTipoViaturaDTO();
        listaDTO.tipoNecessidade = this.tipoNecessidade.toBaseDTO().descricao;
        listaDTO.codigoTipoViatura = [];
        this.tiposViaturas.forEach((cod: CodigoTipoViatura) => {
            listaDTO.codigoTipoViatura.push(cod.getValue());
        });
        return listaDTO;
    }

    toString(): string {
        return `NecessidadesTipoViatura: [${this.tiposViaturas}, ${this.tipoNecessidade}]`;
    }
}