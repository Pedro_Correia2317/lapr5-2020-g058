

import { ImportarDadosFicheiroService } from "../ImportarDadosFicheiroService";
import { ImportadorDadosFicheiroContext } from "../services/ImportadorDadosFicheiroContext";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { CriarNoUseCaseDirector } from "../../no/director/CriarNoUseCaseDirector";
import { CriarPercursoUseCaseDirector } from "../../percurso/director/CriarPercursoUseCaseDirector";
import { CriarLinhaUseCaseDirector } from "../../linha/director/CriarLinhaUseCaseDirector";
import { CriarTipoTripulanteUseCaseDirector } from "../../tipoTripulante/director/CriarTipoTripulanteUseCaseDirector";
import { CriarTipoViaturaUseCaseDirector } from "../../tipoViatura/director/CriarTipoViaturaUseCaseDirector";
import { PercursoImportado } from "../objetosImportacao/PercursoImportado";
import { PedidoCriarPercursoDTO } from "../../percurso/dto/PedidoCriarPercursoDTO";
import { PercursoBuilder } from "../../percurso/services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../../percurso/services/SegmentoRedeBuilder";
import { NoImportado } from "../objetosImportacao/NoImportado";
import { LinhaImportado } from "../objetosImportacao/LinhaImportado";
import { TipoViaturaImportado } from "../objetosImportacao/TipoViaturaImportado";
import { TipoTripulanteImportado } from "../objetosImportacao/TipoTripulanteImportado";
import { PedidoCriarTipoTripulanteDTO } from "../../tipoTripulante/dto/PedidoCriarTipoTripulanteDTO";
import { LinhaBuilder } from "../../linha/services/LinhaBuilder";
import { TipoTripulanteBuilder } from "../../tipoTripulante/services/TipoTripulanteBuilder";
import { TipoViaturaBuilder } from "../../tipoViatura/services/TipoViaturaBuilder";
import { PedidoCriarTipoViaturaDTO } from "../../tipoViatura/dto/PedidoCriarTipoViaturaDTO";
import { PedidoCriarLinhaDTO } from "../../linha/dto/PedidoCriarLinhaDTO";
import { DetalhesImportacaoDTO } from "../../utils/dto/DetalhesImportacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoNo } from "../../../domain/no/model/TipoNo";

export class ImportarDadosFicheiroUseCaseDirector {

    private useCase: ImportarDadosFicheiroService;
    
    private context: ImportadorDadosFicheiroContext;

    private logger: ServicoLogger;

    private criarNo: CriarNoUseCaseDirector;

    private criarPercurso: CriarPercursoUseCaseDirector;

    private criarLinha: CriarLinhaUseCaseDirector;

    private criarTipoViatura: CriarTipoViaturaUseCaseDirector;

    private criarTipoTripulante: CriarTipoTripulanteUseCaseDirector;

    private pBuilder: PercursoBuilder;

    private sBuilder: SegmentoRedeBuilder;

    private lBuilder: LinhaBuilder;

    private tvBuilder: TipoViaturaBuilder;

    private ttBuilder: TipoTripulanteBuilder;

    constructor(useCase: ImportarDadosFicheiroService, context: ImportadorDadosFicheiroContext, 
                cn: CriarNoUseCaseDirector, cp: CriarPercursoUseCaseDirector, cl: CriarLinhaUseCaseDirector, 
                ctv: CriarTipoViaturaUseCaseDirector, ctt: CriarTipoTripulanteUseCaseDirector, l: ServicoLogger){
        this.useCase = useCase;
        this.context = context;
        this.logger = l;
        this.criarNo = cn;
        this.criarPercurso = cp;
        this.criarLinha = cl;
        this.criarTipoViatura = ctv;
        this.criarTipoTripulante = ctt;
    }

    aplicarBuilders(pb: PercursoBuilder, sb: SegmentoRedeBuilder, 
                    lb: LinhaBuilder, tvb: TipoViaturaBuilder, ttb: TipoTripulanteBuilder): void {
        this.pBuilder = pb;
        this.sBuilder = sb;
        this.lBuilder = lb;
        this.tvBuilder = tvb;
        this.ttBuilder = ttb;
    }

    async importarDados(dir: string, apagarFicheiro: boolean): Promise<DetalhesOperacaoDTO[]>{
        const dados: DetalhesImportacaoDTO = await this.useCase.importarDadosFicheiro(this.context, this.logger, dir, apagarFicheiro);
        const resultados: DetalhesOperacaoDTO[] = [];
        if (!dados.sucesso) {
            const dto = new DetalhesOperacaoDTO();
            dto.mensagem = dados.descricaoErro;
            resultados.push(dto);
        } else {
            for(const obj of dados.dadosImportados){
                resultados.push(await obj.visitar(this));
            }
        }
        return resultados;
    }

    processarPercurso(obj: PercursoImportado): Promise<DetalhesOperacaoDTO> {
        const dto: PedidoCriarPercursoDTO = new PedidoCriarPercursoDTO();
        dto.codigo = obj.codigo;
        dto.segmentos = obj.segmentos;
        this.pBuilder.reset();
        this.sBuilder.reset();
        return this.criarPercurso.processarPedido(dto, this.pBuilder, this.sBuilder);
    }

    processarNo(obj: NoImportado): Promise<DetalhesOperacaoDTO> {
        let tipoNo: string;
        if(obj.isEstacaoRecolha){
            tipoNo = TipoNo.NO_ESTACAO_RECOLHA.toBaseDTO().nome;
        } else if (obj.isPontoRendicao) {
            tipoNo = TipoNo.NO_PONTO_RENDICAO.toBaseDTO().nome;
        } else {
            tipoNo = TipoNo.NO_PARAGEM.toBaseDTO().nome;
        }
        const dto = {
            abreviatura: obj.abreviatura,
            nome: obj.nome,
            lat: obj.lat,
            lon: obj.lon,
            tipoNo: tipoNo,
            tempos: obj.temposDeslocacao,
            capacidade: obj.capacidade,
        };
        return this.criarNo.processarPedido(dto);
    }

    processarLinha(obj: LinhaImportado): Promise<DetalhesOperacaoDTO> {
        const dto: PedidoCriarLinhaDTO = new PedidoCriarLinhaDTO();
        dto.codigo = obj.codigo;
        dto.nome = obj.nome;
        dto.percursosIda = obj.percursosIda;
        dto.percursosVolta = obj.percursosVolta;
        this.lBuilder.reset();
        return this.criarLinha.processarPedido(dto, this.lBuilder);
    }

    processarTipoViatura(obj: TipoViaturaImportado): Promise<DetalhesOperacaoDTO> {
        const dto: PedidoCriarTipoViaturaDTO = new PedidoCriarTipoViaturaDTO();
        dto.codigo = obj.codigo;
        dto.descricao = obj.descricao;
        dto.autonomia = obj.autonomia;
        dto.custo = obj.custo;
        dto.consumo = obj.consumo;
        dto.velocidadeMedia = obj.velocidadeMedia;
        dto.emissoes = obj.emissoes;
        dto.tipoCombustivel = obj.tipoCombustivel;
        this.tvBuilder.reset();
        return this.criarTipoViatura.processarPedido(dto, this.tvBuilder);
    }

    processarTipoTripulante(obj: TipoTripulanteImportado): Promise<DetalhesOperacaoDTO> {
        const dto: PedidoCriarTipoTripulanteDTO = new PedidoCriarTipoTripulanteDTO();
        dto.codigo = obj.codigo;
        dto.descricao = obj.descricao;
        this.ttBuilder.reset();
        return this.criarTipoTripulante.processarPedido(dto, this.ttBuilder);
    }


}