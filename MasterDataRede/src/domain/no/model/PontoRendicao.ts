
import {TempoDeslocacaoTripulacaoDTO} from '../dto/TempoDeslocacaoTripulacaoDTO';
import {No} from './No';

export interface PontoRendicao extends No {

    toTempoDeslocacaoTripulacaoDTO(): TempoDeslocacaoTripulacaoDTO[];

}