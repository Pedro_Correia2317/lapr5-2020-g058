

import {NoPontoRendicaoBuilder} from './NoPontoRendicaoBuilder';

export interface NoEstacaoRecolhaBuilder extends NoPontoRendicaoBuilder {

    aplicarCapacidadeVeiculos(capacidade: number): void;

}