import { Linha } from "../../../domain/linha/model/Linha";

export interface LinhaBuilder {

    aplicarCodigo(codigo: string): void;

    aplicarNome(nome: string): void;

    adicionarPercursoIda(codigoPercurso: string): void;

    adicionarPercursoVolta(codigoPercurso: string): void;

    adicionarPercursoReforco(codigoPercurso: string): void;

    adicionarPercursoVazio(codigoPercurso: string): void;

    adicionarTipoViatura(codigoTipoViatura: string): void;

    adicionarTipoTripulante(codigoTipoTripulante: string): void;

    adicionarTipoNecessidadeTipoViatura(tipo: string): void;

    adicionarTipoNecessidadeTipoTripulante(tipo: string): void;

    obterResultado(): Linha;

    reset(): void;
}