
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarPercursoDTO } from 'src/app/models/percursos/PedidoCriarPercursoDTO';
import { SegmentoRedeDTO } from 'src/app/models/percursos/SegmentoRedeDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { PercursoDTO } from 'src/app/models/percursos/PercursoDTO';

@Injectable({
  providedIn: 'root'
})
export class AcessoPercursosService {

  private enderecoHttp: string = `${Configurations.API_SERVER_ADDRESS}/percursos`;

  private endPercursoUnico: string = `${Configurations.API_SERVER_ADDRESS}/percurso/`;

  constructor(private http: HttpClient) { }

  procurarPercursos(pagina: number, tamanho: number, sort?: string, desc?: string): Observable<DetalhesPesquisaDTO<PercursoDTO>> {
    const aux: string = sort? sort : '';
    const descAux: string = desc? desc : '';
    return this.http.get<DetalhesPesquisaDTO<PercursoDTO>>(this.enderecoHttp, {
      observe: 'body',
      responseType: 'json',
      params: {
        page: '' + pagina,
        size: '' + tamanho,
        sort: aux,
        descricao: descAux
      }
    });
  }

  procurarSegmentosPercurso(codigo: string): Observable<DetalhesPesquisaDTO<SegmentoRedeDTO>>{
    return this.http.get<DetalhesPesquisaDTO<SegmentoRedeDTO>>(this.endPercursoUnico + codigo, {
      observe: 'body',
      responseType: 'json'
    });
  }

  enviarPercurso(pedido: PedidoCriarPercursoDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }
}