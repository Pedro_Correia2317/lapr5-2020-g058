
import { NomeNo } from '../../../../src/domain/no/model/NomeNo';

test("testarNomeNulo", () => {
    expect(() => {new NomeNo(null)}).toThrow(ReferenceError);
    expect(() => {new NomeNo(undefined)}).toThrow(ReferenceError);
});

test("testarNomeVazio", () => {
    expect(() => {new NomeNo('')}).toThrow(SyntaxError);
});

test("testarNomeDemasiadoComprido", () => {
    expect(() => {new NomeNo('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
                         + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
                         + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
                         + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetbv')}).toThrow(SyntaxError); //201 chars
});

test("testarNomeUsualComCarateresInvalidos", () => {
    expect(() => {new NomeNo('🌺Estação Principal N12 de Aguiar🌺')}).toThrow(SyntaxError);
});

test("testarNomeDemasiadoPequeno", () => {
    expect(() => {new NomeNo('1')}).toThrow(SyntaxError);
});

test("testarNomePequenoValido", () => {
    expect(new NomeNo('12')).toBeInstanceOf(NomeNo);
});

test("testarNomeGrandeValido", () => {
    expect(new NomeNo('93230c2i99nr92n92cn29r29bv9b9bv92b9n2c92n92cn92n'
    + '92rn92rn9r2cn99rb9rb92b92bv9b2999rccn29rn9cn29n292rn2'
    + '9n29rn9nr9n29r2b29b9nr92n9ch92h9fh92fh92fh9v9v2n92hf92' 
    + 'hf9gfjgbjfgbjfdjbfgjbdfjgbjfbgjdbgjbeuetubetb')).toBeInstanceOf(NomeNo); //200 chars
});

test("testarNomeUsualValido", () => {
    expect(new NomeNo('Estacao Principal N12 de Aguiar')).toBeInstanceOf(NomeNo);
});

test("testarNomeSoNumeros", () => {
    expect(new NomeNo('15367363211')).toBeInstanceOf(NomeNo);
});

test("testarNomeSoLetras", () => {
    expect(new NomeNo('cbjjcjhfdHJJbbcjBJbjvUBbuubuUDwbbeS')).toBeInstanceOf(NomeNo);
});

test("testarNomeSoEspacos", () => {
    expect(() => {new NomeNo('         ')}).toThrow(SyntaxError);
});

test("testarNomeUsualComCarateresAcentos", () => {
    expect(new NomeNo('Estação Principal N12 de Aguiar')).toBeInstanceOf(NomeNo);
});