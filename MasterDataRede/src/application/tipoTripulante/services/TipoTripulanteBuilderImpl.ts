
import { CodigoTipoTripulante } from "../../../domain/tipoTripulante/model/CodigoTipoTripulante";
import { DescricaoTipoTripulante } from "../../../domain/tipoTripulante/model/DescricaoTipoTripulante";
import { TipoTripulante } from "../../../domain/tipoTripulante/model/TipoTripulante";
import { TipoTripulanteBuilder } from "./TipoTripulanteBuilder";

export class TipoTripulanteBuilderImpl implements TipoTripulanteBuilder {

    private codigo: CodigoTipoTripulante;

    private descricao: DescricaoTipoTripulante;

    reset(): void {
        this.codigo = null;
        this.descricao = null;
    }

    aplicarCodigo(codigo: string): void {
        this.codigo = new CodigoTipoTripulante(codigo);
    }

    aplicarDescricao(descricao: string): void {
        this.descricao = new DescricaoTipoTripulante(descricao);
    }

    obterResultado(): TipoTripulante {
        return new TipoTripulante(this.codigo, this.descricao);
    }

}