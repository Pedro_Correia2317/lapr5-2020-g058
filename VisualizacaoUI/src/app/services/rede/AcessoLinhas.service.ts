
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { Configurations } from '../../config/Configurations';
import { PedidoCriarLinhaDTO } from 'src/app/models/linhas/PedidoCriarLinhaDTO';
import { LinhaDTO } from 'src/app/models/linhas/LinhaDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { PercursoDTO } from 'src/app/models/percursos/PercursoDTO';

@Injectable({
  providedIn: 'root'
})
export class AcessoLinhasService {

  private enderecoHttp: string = `${Configurations.API_SERVER_ADDRESS}/linhas`;

  private endLinhaUnica: string = `${Configurations.API_SERVER_ADDRESS}/linha/`;

  constructor(private http: HttpClient) { }

  procurarLinhas(pagina: number, tamanho: number, sort?: string, nome?: string): Observable<DetalhesPesquisaDTO<LinhaDTO>> {
    const aux: string = sort? sort : '';
    const nomeAux: string = nome? nome : '';
    return this.http.get<DetalhesPesquisaDTO<LinhaDTO>>(this.enderecoHttp, { 
        observe: 'body',
        responseType: 'json',
        params: {
          page: '' + pagina,
          size: '' + tamanho,
          sort: aux,
          nome: nomeAux
        }
      });
  }

  procurarPercursosLinha(codigo: string){
    return this.http.get<DetalhesPesquisaDTO<PercursoDTO[]>>(this.endLinhaUnica + codigo, { 
      observe: 'body',
      responseType: 'json'
    });
  }

  enviarLinha(pedido: PedidoCriarLinhaDTO): Observable<DetalhesOperacaoDTO> {
    return this.http.post<DetalhesOperacaoDTO>(this.enderecoHttp, pedido, {
      observe: 'body',
      responseType: 'json'
    });
  }
}