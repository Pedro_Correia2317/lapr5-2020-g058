

import {No} from '../../../domain/no/model/No';

export interface NoBuilder {

    reset(): void;

    aplicarNome(nome: string): void;

    aplicarAbreviatura(abreviatura: string): void;

    aplicarCoordenadas(lat: number, lon: number): void;

    obterResultado(): No;

}