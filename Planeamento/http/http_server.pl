
:- module(http_server, [start_server/1]).

:- use_module(http_client_connections).
:- use_module("../services/find_shortest_path_service").

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_unix_daemon)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_cors)).
:- use_module(library(http/json)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)). 
%Cors
:- set_setting(http:cors, [*]).


start_server(Port) :-
        http_server(http_dispatch, [port(Port)]).

stop_server:-
    retract(port(Port)),
    http_stop_server(Port,_).

:- json_object time_json(node:string, time:float).
:- json_object times_json_array(array:list(time_json)).

:- http_handler('/planeamento', get_shortest_path, []).
get_shortest_path(Request):-
    cors_enable(Request, [methods([get])]),
    http_client_connections:remove_knowledge_base,
    http_client_connections:add_knowledge_base,
    http_parameters(Request, [orig(Orig, []), dest(Dest, []), current_time(T, [])]),
    atom_string(Orig, Origin),
    atom_string(Dest, Destination),
    atom_number(T, Time),
    calculate_shortest_path(Origin, Destination, Time, Path, Times),
    convert_to_reply(Path, Times, JSONArray),
    %Reply = times_json_array(JSONArray),
    prolog_to_json(JSONArray, JSONObject),
    reply_json(JSONObject, [json_object(dict)]).

convert_to_reply([], _, []).

%convert_to_reply([Node|OtherNodes], [Time|OtherTimes], [R|Reply]) :-
convert_to_reply([Node|OtherNodes], [Time|OtherTimes], R) :-
    R = time_json(Node, Time),
    convert_to_reply(OtherNodes, OtherTimes, Reply).
