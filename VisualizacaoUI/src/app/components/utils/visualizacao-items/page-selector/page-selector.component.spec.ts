import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { PageSelectorComponent } from './page-selector.component';

@Component({
  template: `<app-page-selector [dadosCriacaoPaginas]="dadosCriacaoPaginas"></app-page-selector>`,
})
export class TestWrapperComponent {

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
}


describe('PageSelectorComponent', () => {

  let component: TestWrapperComponent;
  let fixture: ComponentFixture<TestWrapperComponent>;
  let element: any;
  let childComponent: PageSelectorComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestWrapperComponent, PageSelectorComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestWrapperComponent);
    component = fixture.componentInstance;
    childComponent = fixture.debugElement.query(By.directive(PageSelectorComponent)).componentInstance;
    element = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create new pages', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 200, tamanhoPagina: 5, pagina: 1};
    fixture.detectChanges();
    expect(element.querySelector('#page').childElementCount).toBe(40);
  });

  it('should create new pages 2', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 54, tamanhoPagina: 34, pagina: 1};
    fixture.detectChanges();
    expect(element.querySelector('#page').childElementCount).toBe(2);
  });

  it('should create new pages 3', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 2, tamanhoPagina: 34, pagina: 1};
    fixture.detectChanges();
    expect(element.querySelector('#page').childElementCount).toBe(1);
  });

  it('should create new pages 4', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 0, pagina: 1};
    fixture.detectChanges();
    expect(element.querySelector('#page').childElementCount).toBe(0);
  });

  it('should change to correct page', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 2};
    fixture.detectChanges();
    expect(childComponent.paginaSelecionada).toBe(2);
  });

  it('should change to correct page 2', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 1};
    fixture.detectChanges();
    expect(childComponent.paginaSelecionada).toBe(1);
  });

  it('should change to correct page 3', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 10};
    fixture.detectChanges();
    expect(childComponent.paginaSelecionada).toBe(1);
  });

  it('should change to correct page 4', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: -10};
    fixture.detectChanges();
    expect(childComponent.paginaSelecionada).toBe(1);
  });

  it('should change to correct page 5', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 3};
    fixture.detectChanges();
    expect(childComponent.paginaSelecionada).toBe(1);
  });

  it('should emit selected page', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 1};
    fixture.detectChanges();
    childComponent.selectedValue.subscribe((selectedPage: number) => expect(selectedPage).toBe(1));
  });

  it('should emit selected page 2', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 2};
    fixture.detectChanges();
    childComponent.selectedValue.subscribe((selectedPage: number) => expect(selectedPage).toBe(2));
  });

  it('should emit selected page 3', () => {
    component.dadosCriacaoPaginas = {numeroTotal: 20, tamanhoPagina: 10, pagina: 20};
    fixture.detectChanges();
    childComponent.selectedValue.subscribe((selectedPage: number) => expect(selectedPage).toBe(1));
  });
});
