
import Mongoose, { Schema } from 'mongoose';
import { NoBuilder } from '../../application/no/services/NoBuilder';
import { NoEstacaoRecolhaBuilder } from '../../application/no/services/NoEstacaoRecolhaBuilder';
import { NoPontoRendicaoBuilder } from '../../application/no/services/NoPontoRendicaoBuilder';
import { AcessoDadosError } from '../../application/utils/errors/AcessoDadosError';
import { NoEstacaoRecolhaDTO } from '../../domain/no/dto/NoEstacaoRecolhaDTO';
import { TempoDeslocacaoTripulacaoDTO } from '../../domain/no/dto/TempoDeslocacaoTripulacaoDTO';
import { AbreviaturaNo } from '../../domain/no/model/AbreviaturaNo';
import { No } from "../../domain/no/model/No";
import { PontoRendicao } from '../../domain/no/model/PontoRendicao';
import { NoRepository } from "../interfaces/NoRepository";
import { INo } from './models/INo';
import { MongooseRepositoryImpl } from './RepositoryImpl';

export class NoRepositoryImpl implements NoRepository {

    private static readonly SCHEMA_NO: Mongoose.Schema = new Schema({
        'abreviatura': {'type': String, 'required': true, 'unique': true},
        'nome': {'type': String, 'required': true},
        'latitude': {'type': Number, 'required': true},
        'longitude': {'type': Number, 'required': true},
        'tipoNo': {'type': String, 'required': true},
        'capacidade': {'type': Number, 'required': false},
        'temposDeslocacaoTripulacao': [
            {
            'tempo': {'type': Number, 'required': true},
            'abreviaturaNo': {'type': String, 'required': true}
            }
        ]
    });

    private static readonly MODELO_NO = Mongoose.model<INo>('No', NoRepositoryImpl.SCHEMA_NO);

    static BASE_REPO: MongooseRepositoryImpl = new MongooseRepositoryImpl();

    private modelarNo(no: No): INo {
        const modelo = new NoRepositoryImpl.MODELO_NO(),
        noDTO = no.toBaseDTO();
        modelo.abreviatura = noDTO.abreviatura;
        modelo.nome = noDTO.nome;
        modelo.latitude = noDTO.latitude;
        modelo.longitude = noDTO.longitude;
        modelo.tipoNo = no.tipoNo().toBaseDTO().nome;
        if(noDTO instanceof NoEstacaoRecolhaDTO){
            modelo.capacidade = noDTO.capacidade;
        }
        if(this.isPontoRendicao(no)){
            no.toTempoDeslocacaoTripulacaoDTO().forEach((tempo: TempoDeslocacaoTripulacaoDTO) => {
                modelo.temposDeslocacaoTripulacao.push({
                    'tempo': tempo.tempo,
                    'abreviaturaNo': tempo.no
                });
            });
        }
        return modelo;
    }

    private isPontoRendicao(no: No): no is PontoRendicao {
        return (<PontoRendicao>no).toTempoDeslocacaoTripulacaoDTO !== undefined;
    }
    
    async isAbreviaturaUnico(abreviatura: string): Promise<boolean> {
        return await NoRepositoryImpl.BASE_REPO.isUnico(NoRepositoryImpl.MODELO_NO, {abreviatura});
    }

    async existeNo(abreviatura: string): Promise<boolean> {
        return await NoRepositoryImpl.MODELO_NO.exists({abreviatura});
    }

    save(objecto: No): Promise<boolean> {
        const modeloNo: INo = this.modelarNo(objecto);
        return NoRepositoryImpl.BASE_REPO.save(modeloNo);
    }

    private mapearDataModel(builders: Map<string, NoBuilder>, dataModel: INo): No {
        const builder: NoBuilder = builders.get(dataModel.tipoNo);
        builder.reset();
        builder.aplicarNome(dataModel.nome);
        builder.aplicarCoordenadas(dataModel.latitude, dataModel.longitude);
        builder.aplicarAbreviatura(dataModel.abreviatura);
        if (this.isPontoRendicaoBuilder(builder)) {
            dataModel.temposDeslocacaoTripulacao.forEach((tempo) => {
                builder.adicionarTempoDeslocacaoTripulacao(tempo.tempo, tempo.abreviaturaNo);
            });
            if (this.isEstacaoRecolhaBuilder(builder)) {
                builder.aplicarCapacidadeVeiculos(dataModel.capacidade);
            }
        }
        return builder.obterResultado();
    }

    private isPontoRendicaoBuilder(no: NoBuilder): no is NoPontoRendicaoBuilder {
        return (<NoPontoRendicaoBuilder>no).adicionarTempoDeslocacaoTripulacao !== undefined;
    }

    private isEstacaoRecolhaBuilder(no: NoBuilder): no is NoEstacaoRecolhaBuilder {
        return (<NoEstacaoRecolhaBuilder>no).aplicarCapacidadeVeiculos !== undefined;
    }

    async obterNos(builders: Map<string, NoBuilder>, sort: string, numPag: number, 
                        tamPag: number, abreviatura: string, nome: string): Promise<No[]> {
        const nos: No[] = [];
        const regexAbr = new RegExp(abreviatura, 'i');
        const regexNome = new RegExp(nome, 'i');
        await NoRepositoryImpl.MODELO_NO.find({abreviatura: regexAbr, nome: regexNome}).sort(sort)
                                        .skip((numPag-1) * tamPag).limit(tamPag).then((results) => {
            results.forEach((no) => {
                nos.push(this.mapearDataModel(builders, no));
            });
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return nos;
    }

    async countComFiltros(abreviatura: string, nome: string): Promise<number> {
        let numero: number;
        const regexAbr = new RegExp(abreviatura, 'i');
        const regexNome = new RegExp(nome, 'i');
        await NoRepositoryImpl.MODELO_NO.countDocuments({abreviatura: regexAbr, nome: regexNome}).exec().then((num) => {
            numero = num;
        }).catch((err) => {
            throw new AcessoDadosError(err.message);
        });
        return numero;
    }

    findById(codigo: AbreviaturaNo): Promise<No> {
        throw new Error("Method not implemented.");
    }

    findPage(numPagina: number, tamanhoPagina: number): Promise<No[]> {
        throw new Error("Method not implemented.");
    }

    count(): Promise<number> {
        return NoRepositoryImpl.MODELO_NO.countDocuments().exec();
    }

}