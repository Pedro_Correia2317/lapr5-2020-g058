

import {No} from '../../../../domain/no/model/No';
import {NoRepository} from '../../../../repository/interfaces/NoRepository';
import {DetalhesOperacaoDTO} from '../../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../../utils/services/ServicoLogger';
import {NoBuilder} from '../../services/NoBuilder';
import {CriarNoUseCase} from '../CriarNoUseCase';

export class CriarNoUseCaseImpl implements CriarNoUseCase {

    aplicarNome(noBuilder: NoBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO {
        const resultDTO: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            noBuilder.aplicarNome(nome);
            resultDTO.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultDTO.mensagem = 'createNode.nullName';
            } else if (err instanceof SyntaxError){
                resultDTO.mensagem = 'createNode.invalidName';
            } else {
                throw err;
            }
        }

return resultDTO;
    }

    async aplicarAbreviatura(
noBuilder: NoBuilder, logger: ServicoLogger,
                    todosNos: NoRepository, abreviatura: string
): Promise<DetalhesOperacaoDTO> {
        const resultDTO: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            if (await todosNos.isAbreviaturaUnico(abreviatura)){
                noBuilder.aplicarAbreviatura(abreviatura);
                resultDTO.sucesso = true;
            } else {
                logger.registarFalha(new Error('Repeated node code inserted'));
                resultDTO.mensagem = 'createNode.repeatedShortName';
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof ReferenceError){
                resultDTO.mensagem = 'createNode.nullShortName';
            } else if (err instanceof SyntaxError){
                resultDTO.mensagem = 'createNode.invalidShortName';
            } else {
                throw err;
            }
        }

return resultDTO;
    }

    aplicarCoordenadas(noBuilder: NoBuilder, logger: ServicoLogger, lat: number, lon: number): DetalhesOperacaoDTO {
        const resultDTO: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            noBuilder.aplicarCoordenadas(lat, lon);
            resultDTO.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultDTO.mensagem = 'createNode.invalidCoordinates';
            } else {
                throw err;
            }
        }

return resultDTO;
    }

    async registarNo(noBuilder: NoBuilder, logger: ServicoLogger, todosNos: NoRepository): Promise<DetalhesOperacaoDTO> {
        const resultDTO: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            const no: No = noBuilder.obterResultado();
            if (await todosNos.save(no)) {
                resultDTO.sucesso = true;
                resultDTO.mensagem = 'createNode.creationSuccess';
            } else {
                resultDTO.mensagem = 'createNode.nodeRegistrationError';
            }
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof SyntaxError){
                resultDTO.mensagem = 'createNode.nodeCreationError';
            } else {
                throw err;
            }
        }

return resultDTO;
    }

}