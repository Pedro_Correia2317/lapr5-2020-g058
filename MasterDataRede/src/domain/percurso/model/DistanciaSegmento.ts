
export class DistanciaSegmento {

    private distancia: number;

    constructor(distancia: number){
        if (!Number.isFinite(distancia)){
            throw new TypeError(`The distance is not a number: ${distancia}`);
        }
        const aux = parseFloat(distancia.toFixed(3));
        if (aux <= 0 || aux > 25000){
            throw new SyntaxError(`The distance ${aux} is <= 0 or > 25000`);
        }
        this.distancia = aux;
    }

    getValue(): number {
        return this.distancia;
    }

    toString(): string {
        return `DistanciaSegmento: [${this.distancia}]`;
    }
}