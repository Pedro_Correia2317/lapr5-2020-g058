
import { LinhaBuilder } from '../../../src/application/linha/services/LinhaBuilder';
import { AcessoDadosError } from '../../../src/application/utils/errors/AcessoDadosError';
import { CodigoLinha } from '../../../src/domain/linha/model/CodigoLinha';
import { Linha } from '../../../src/domain/linha/model/Linha';
import { LinhaRepository } from '../../../src/repository/interfaces/LinhaRepository';

export class LinhaRepositoryMockTrue implements LinhaRepository {

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return true;
    }

    async save(objecto: Linha): Promise<boolean> {
        return true;
    }

    async findById(codigo: CodigoLinha): Promise<Linha> {
        throw new Error('Method not implemented.');
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<Linha[]> {
        throw new Error('Method not implemented.');
    }

    async count(): Promise<number> {
        throw new Error('Method not implemented.');
    }

    async obterLinhas(builder: LinhaBuilder, sort: string, numPag: number, tamPag: number, codigo: string, nome: string): Promise<Linha[]> {
        return [];
    }

    async countComFiltros(codigo: string, nome: string): Promise<number> {
        return 0;
    }

    async findByCodigo(builder: LinhaBuilder, codigo: string): Promise<Linha> {
        return null;
    }

}

export class LinhaRepositoryMockFalse implements LinhaRepository {

    async isCodigoUnico(codigo: string): Promise<boolean> {
        return false;
    }

    async save(objecto: Linha): Promise<boolean> {
        return false;
    }

    async findById(codigo: CodigoLinha): Promise<Linha> {
        throw new Error('Method not implemented.');
    }

    async findPage(numPagina: number, tamanhoPagina: number): Promise<Linha[]> {
        throw new Error('Method not implemented.');
    }

    async count(): Promise<number> {
        throw new Error('Method not implemented.');
    }

    async obterLinhas(builder: LinhaBuilder, sort: string, numPag: number, tamPag: number, codigo: string, nome: string): Promise<Linha[]> {
        throw new AcessoDadosError('Teste');
    }

    async countComFiltros(codigo: string, nome: string): Promise<number> {
        throw new AcessoDadosError('Teste');
    }

    async findByCodigo(builder: LinhaBuilder, codigo: string): Promise<Linha> {
        throw new AcessoDadosError('Teste');
    }

}