
import { Router, Request, Response } from "express";
import { Inject, Service } from "typedi";
import { DetalhesOperacaoDTO } from "../../application/utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../../application/importacao/director/ImportarDadosFicheiroUseCaseDirector";
import { MulterConfig } from "../../config/MulterConfig";

@Service()
export class RotasImportacaoFicheiros {

    @Inject("PrimaryImportarDadosFicheiroUseCaseDirector")
    private servico: ImportarDadosFicheiroUseCaseDirector;
    
    insertRoutes = (router: Router): void => {
        const multer = new MulterConfig();
        router.post('/importacao', multer.obtainConfig().single('ficheiro'), this.inserirRotaImportacaoFicheiro);
    }

    inserirRotaImportacaoFicheiro = async (req: Request, res: Response): Promise<void> => {
        if(!req.file){
            res.status(400).send('Insira um ficheiro');
        } else {
            const dados: DetalhesOperacaoDTO[] = await this.servico.importarDados(req.file.path, true);
            res.send(dados);
        }
    }

}