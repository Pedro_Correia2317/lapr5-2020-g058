import { TipoCombustivelDTO } from "../../../domain/tipoViatura/dto/TipoCombustivelDTO";
import { TipoViaturaDTO } from "../../../domain/tipoViatura/dto/TipoViaturaDTO";
import { TipoViaturaRepository } from "../../../repository/interfaces/TipoViaturaRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoViaturaBuilder } from "../services/TipoViaturaBuilder";

export interface ListarTiposViaturasUseCase {

    obterTiposCombustiveis(): TipoCombustivelDTO[];

    listarTiposViaturas(builder: TipoViaturaBuilder, logger: ServicoLogger, ttRepo: TipoViaturaRepository, 
        sort: string, numPag: number, tamPag: number, nome: string): Promise<DetalhesPesquisaDTO<TipoViaturaDTO>>;
}