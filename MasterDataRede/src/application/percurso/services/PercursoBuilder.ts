import {Percurso} from '../../../domain/percurso/model/Percurso';
import {SegmentoRede} from '../../../domain/percurso/model/SegmentoRede';

export interface PercursoBuilder {

    aplicarCodigo(codigo: string): void;

    aplicarSegmento(segmento: SegmentoRede): void;

    aplicarNovaAbreviaturaNoInicial(abreviatura: string): void;

    aplicarNovaAbreviaturaNoFinal(abreviatura: string): void;

    aplicarNovaDistancia(distancia: number): void;

    aplicarNovaDuracao(duracao: number): void;

    obterResultado(): Percurso;

    reset(): void;
    
}