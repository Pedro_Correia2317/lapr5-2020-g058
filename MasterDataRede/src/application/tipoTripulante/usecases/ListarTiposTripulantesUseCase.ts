
import { TipoTripulanteDTO } from "../../../domain/tipoTripulante/dto/TipoTripulanteDTO";
import { TipoTripulanteRepository } from "../../../repository/interfaces/TipoTripulanteRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoTripulanteBuilder } from "../services/TipoTripulanteBuilder";

export interface ListarTiposTripulantesUseCase {

    listarTiposTripulantes(builder: TipoTripulanteBuilder, logger: ServicoLogger, ttRepo: TipoTripulanteRepository, 
        sort: string, numPag: number, tamPag: number, nome: string): Promise<DetalhesPesquisaDTO<TipoTripulanteDTO>>;
    
}