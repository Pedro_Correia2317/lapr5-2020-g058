

import { Request, Response, Router } from 'express';
import { Inject } from 'typedi';
import { DetalhesOperacaoDTO } from '../../application/utils/dto/DetalhesOperacaoDTO';
import { CriarNoUseCaseDirector } from "../../application/no/director/CriarNoUseCaseDirector";
import { ListarNosUseCase } from '../../application/no/usecases/ListarNosUseCase';
import { NoBuilder } from '../../application/no/services/NoBuilder';
import { NoRepository } from '../../repository/interfaces/NoRepository';
import { ServicoLogger } from '../../application/utils/services/ServicoLogger';

export class RotasVariosNos {

    @Inject("PrimaryEscolhaCriarNoDirector")
    private escolhaCriarNoDirector: CriarNoUseCaseDirector;

    @Inject("PrimaryListarNosUseCase")
    private listarNosUseCase: ListarNosUseCase;

    @Inject("PrimaryBuilders")
    private builders: Map<string, NoBuilder>;

    @Inject("PrimaryNoRepository")
    private noRepository: NoRepository;

    @Inject("PrimaryServicoLogger")
    private servicoLogger: ServicoLogger;
    
    insertRoutes = (router: Router): void => {
        router.route('/tiposNos')
            .get(this.rotaListagemTiposNos);
        router.route('/nos')
            .post(this.inserirRotaCriacaoNo)
            .get(this.rotaListagemNos);
    }
    
    rotaListagemTiposNos = (req: Request, res: Response): void => {
        res.send({'lista': this.listarNosUseCase.obterTiposNos(), 'sucesso': true});
    }

   inserirRotaCriacaoNo = async (req: Request, res: Response): Promise<void> => {
        const resultado: DetalhesOperacaoDTO = await this.escolhaCriarNoDirector.processarPedido(req.body);
        if (typeof resultado === 'undefined'){
            res.send({'mensagem': 'createNode.invalidNodeType', 'sucesso': false});
        } else {
            res.send({'sucesso': resultado.sucesso, 'mensagem': resultado.mensagem});
        }
    }

    rotaListagemNos = async (req: Request, res: Response): Promise<void> => {
        let num = req.query.page? Number.parseInt(req.query.page.toString()) : 1;
        num = num <= 0? 1 : num;
        let tam = req.query.size? Number.parseInt(req.query.size.toString()) : 24;
        tam = tam <= 0? 24 : tam;
        const abr = req.query.abreviatura? req.query.abreviatura.toString() : '';
        const nome = req.query.nome? req.query.nome.toString(): '';
        const sort = req.query.sort? req.query.sort.toString(): '';
        const resultado = await this.listarNosUseCase.obterNos(this.builders, this.servicoLogger, this.noRepository, sort, num, tam, abr, nome);
        res.send({'sucesso': resultado.sucesso, 'lista': resultado.lista, 'numeroTotal': resultado.count, 'mensagem': resultado.descricao});
    }

}