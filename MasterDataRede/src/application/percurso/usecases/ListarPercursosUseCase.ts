
import { PercursoDTO } from "../../../domain/percurso/dto/PercursoDTO";
import { SegmentoRedeDTO } from "../../../domain/percurso/dto/SegmentoRedeDTO";
import { PercursoRepository } from "../../../repository/interfaces/PercursoRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { PercursoBuilder } from "../services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../services/SegmentoRedeBuilder";

export interface ListarPercursosUseCase { 

    obterSegmentosPercurso(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, logger: ServicoLogger, pRepo: PercursoRepository,
                    codigo: string): Promise<DetalhesPesquisaDTO<SegmentoRedeDTO>>;

    listarPercursos(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder, logger: ServicoLogger, pRepo: PercursoRepository,
                    sort: string, numPag: number, tamPag: number, descricao: string): Promise<DetalhesPesquisaDTO<PercursoDTO>>;

}