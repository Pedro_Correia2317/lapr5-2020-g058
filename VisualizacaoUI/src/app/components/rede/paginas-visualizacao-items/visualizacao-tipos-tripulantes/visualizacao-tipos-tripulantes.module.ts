
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisualizadorTiposTripulantesModule } from '../../visualizadores/visualizador-tipos-tripulantes/visualizador-tipos-tripulantes.module';

import { VisualizacaoTiposTripulantesComponent } from './visualizacao-tipos-tripulantes.component';

const routes: Routes = [
    {path: '', component: VisualizacaoTiposTripulantesComponent}
];

@NgModule({
  declarations: [VisualizacaoTiposTripulantesComponent],
  imports: [VisualizadorTiposTripulantesModule, CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisualizacaoTiposTripulantesModule { }