
import { AbreviaturaNo } from "../../no/model/AbreviaturaNo";
import { SegmentoRedeDTO } from "../dto/SegmentoRedeDTO";
import { DistanciaSegmento } from "./DistanciaSegmento";
import { DuracaoSegmento } from "./DuracaoSegmento";
import { SequenciaSegmento } from "./SequenciaSegmento";

export class SegmentoRede {

    private abrNoInicio: AbreviaturaNo;

    private abrNoFim: AbreviaturaNo;

    private distancia: DistanciaSegmento;

    private duracao: DuracaoSegmento;

    private sequencia: SequenciaSegmento;

    constructor(abrI: AbreviaturaNo, abrF: AbreviaturaNo, seq: SequenciaSegmento, 
                            dis: DistanciaSegmento, dur: DuracaoSegmento){
        if(abrI == null || abrF == null || seq == null || dis == null || dur == null){
            throw new SyntaxError('There are Null Values');
        }
        this.abrNoInicio = abrI;
        this.abrNoFim = abrF;
        this.sequencia = seq;
        this.distancia = dis;
        this.duracao = dur;
    }

    toBaseDTO(): SegmentoRedeDTO {
        const dto = new SegmentoRedeDTO();
        dto.abreviaturaNoInicio = this.abrNoInicio.getValue();
        dto.abreviaturaNoFim = this.abrNoFim.getValue();
        dto.distancia = this.distancia.getValue();
        dto.duracao = this.duracao.getValue();
        dto.sequencia = this.sequencia.getValue();

        return dto;
    }
    
}