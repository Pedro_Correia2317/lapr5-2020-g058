import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ImportarDadosFicheiroUseCaseDirector } from "../director/ImportarDadosFicheiroUseCaseDirector";
import { ObjetoImportado } from "./ObjetoImportado";

export class NoImportado implements ObjetoImportado {

    public abreviatura: string;

    public nome: string;

    public lat: number;

    public lon: number;
    
    public isEstacaoRecolha: boolean;

    public isPontoRendicao: boolean;

    public capacidade: number;

    public temposDeslocacao: {tempo: number, abreviaturaNo: string}[];

    visitar(director: ImportarDadosFicheiroUseCaseDirector): Promise<DetalhesOperacaoDTO> {
        return director.processarNo(this);
    }
}