
import { AbreviaturaNo } from "./AbreviaturaNo";

export class TempoDeslocacaoTripulacao {

    private tempo: number;

    private no: AbreviaturaNo;

    constructor(tempo: number, no: AbreviaturaNo){
        if(!Number.isInteger(tempo) || no == null){
            throw new ReferenceError('Null values inserted');
        }
        if(tempo <= 0 || tempo > 10800){ // 3 horas
            throw new SyntaxError(tempo + ' is <= 0 or > 10800');
        }
        this.tempo = tempo;
        this.no = no;
    }

    getTempo(): number {
        return this.tempo;
    }

    getNo(): AbreviaturaNo {
        return this.no;
    }

    toString(): string {
        return `TempoDeslocacaoTripulacao: [${this.tempo}, ${this.no}]`;
    }
}