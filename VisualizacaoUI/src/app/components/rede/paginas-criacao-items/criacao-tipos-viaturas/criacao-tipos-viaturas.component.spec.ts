import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriacaoTiposViaturasComponent } from './criacao-tipos-viaturas.component';

describe('CriacaoTiposViaturasComponent', () => {
  let component: CriacaoTiposViaturasComponent;
  let fixture: ComponentFixture<CriacaoTiposViaturasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriacaoTiposViaturasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriacaoTiposViaturasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
