
import Mongoose from 'mongoose';

export interface ITipoViatura extends Mongoose.Document {

    codigo: string;

    descricao: string;

    autonomia: number;

    velocidadeMedia: number;

    custo: number;

    consumo: number;

    emissoes: number;

    tipoCombustivel: string;

}