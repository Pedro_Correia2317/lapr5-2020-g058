
import { CriarNoParagemUseCaseDirector } from '../../../src/application/no/director/CriarNoParagemUseCaseDirector';
import { NoParagemBuilderImpl } from '../../../src/application/no/services/NoParagemBuilderImpl';
import { CriarNoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoUseCaseImpl';
import { CriarNoParagemUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoParagemUseCaseImpl';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { NoRepositoryMockFalse, NoRepositoryMockTrue } from '../../mocks/no/NoRepositoryMock';
import { PedidoCriarNoParagemDTO } from '../../../src/application/no/dto/PedidoCriarNoParagemDTO';

const loggerMock = new ServicoLoggerMock();
const nUseCase = new CriarNoParagemUseCaseImpl(new CriarNoUseCaseImpl());
const nRepoTrue = new NoRepositoryMockTrue();
const nRepoFalse = new NoRepositoryMockFalse();

test('testarPedidoValidoComValoresUnicos', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComAbreviaturaRepetida', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoFalse);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComAbreviaturaInvalida', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'Sou Invalido...';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemAbreviatura', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    // pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoComNomeInvalido', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = '?';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemNome', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    // pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemLatitude', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    // pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemLongitude', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    // pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(false);
});

test('testarPedidoValidoSemTipoNo', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    // pedido.tipoNo = 'NO_PARAGEM';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTipoNoInvalido1', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'NO_ESTACAO_RECOLHA';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});

test('testarPedidoValidoComTipoNoInvalido2', async () => {
    const pedido: PedidoCriarNoParagemDTO = new PedidoCriarNoParagemDTO();
    pedido.abreviatura = 'ESTLO';
    pedido.nome = 'Estação (Lordelo)';
    pedido.lat = 41.2090666564063;
    pedido.lon = -8.35109395257277;
    pedido.tipoNo = 'TIPO_NO_INVALIDO';
    const tvBuilder = new NoParagemBuilderImpl();
    const director = new CriarNoParagemUseCaseDirector(nUseCase, loggerMock, nRepoTrue);
    expect((await director.executar(pedido, tvBuilder)).sucesso).toBe(true);
});