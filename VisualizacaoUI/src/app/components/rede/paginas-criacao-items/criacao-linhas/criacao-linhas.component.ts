import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { PedidoCriarLinhaDTO } from 'src/app/models/linhas/PedidoCriarLinhaDTO';
import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { AcessoLinhasService } from 'src/app/services/rede/AcessoLinhas.service';
import { AdicaoPercursoComponent } from './adicao-percurso/adicao-percurso.component';
import { AdicaoTipoTripulanteComponent } from './adicao-tipo-tripulante/adicao-tipo-tripulante.component';
import { AdicaoTipoViaturaComponent } from './adicao-tipo-viatura/adicao-tipo-viatura.component';

@Component({
  selector: 'app-criacao-linhas',
  templateUrl: './criacao-linhas.component.html',
  styleUrls: ['./criacao-linhas.component.css']
})
export class CriacaoLinhasComponent implements OnInit {

  formCriacaoLinha: any;

  percursosIda: string[];

  percursosVolta: string[];

  percursosReforco: string[];

  percursosVazio: string[];

  tiposViaturas: string[];

  tiposTripulantes: string[];

  constructor(private todasLinhas: AcessoLinhasService,
              private builder: FormBuilder,
              private dialog: MatDialog) {
    this.formCriacaoLinha = this.builder.group({
      codigo: '',
      nome: '',
      necTipoViatura: '',
      necTipoTripulante: ''
    });
    this.percursosIda = [];
    this.percursosVolta = [];
    this.percursosReforco = [];
    this.percursosVazio = [];
    this.tiposViaturas = [];
    this.tiposTripulantes = [];
  }

  ngOnInit() {
  }

  onSubmit(dadosFormulario: any) {
    const dto = new PedidoCriarLinhaDTO();
    dto.codigo = dadosFormulario.codigo;
    dto.nome = dadosFormulario.nome;
    dto.percursosIda = this.percursosIda;
    dto.percursosVolta = this.percursosVolta;
    dto.percursosReforco = this.percursosReforco;
    dto.percursosVazio = this.percursosVazio;
    dto.tiposViaturas = this.tiposViaturas;
    dto.tiposTripulantes = this.tiposTripulantes;
    dto.tipoNecessidadeTipoViatura = dadosFormulario.necTipoViatura;
    dto.tipoNecessidadeTipoTripulante = dadosFormulario.necTipoTripulante;
    this.todasLinhas.enviarLinha(dto).subscribe((resultados: DetalhesOperacaoDTO) => {
      alert(resultados.sucesso + ' na operação: ' + resultados.mensagem);
    });
  }

  openDialogAdicaoPercursoIda(){
    const dialogRef = this.dialog.open(AdicaoPercursoComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.percursosIda.push(result);
      }
    });
  }

  openDialogAdicaoPercursoVolta(){
    const dialogRef = this.dialog.open(AdicaoPercursoComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.percursosVolta.push(result);
      }
    });
  }

  openDialogAdicaoPercursoReforco(){
    const dialogRef = this.dialog.open(AdicaoPercursoComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.percursosReforco.push(result);
      }
    });
  }

  openDialogAdicaoPercursoVazio(){
    const dialogRef = this.dialog.open(AdicaoPercursoComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.percursosVazio.push(result);
      }
    });
  }

  openDialogAdicaoTipoViatura(){
    const dialogRef = this.dialog.open(AdicaoTipoViaturaComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.tiposViaturas.push(result);
      }
    });
  }

  openDialogAdicaoTipoTripulante(){
    const dialogRef = this.dialog.open(AdicaoTipoTripulanteComponent);
    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.tiposTripulantes.push(result);
      }
    });
  }

  eliminarPercursoIda(codPercurso: string){
    const index = this.percursosIda.indexOf(codPercurso, 0);
    if(index > -1){
      this.percursosIda.splice(index, 1);
    }
  }

  eliminarPercursoVolta(codPercurso: string){
    const index = this.percursosVolta.indexOf(codPercurso, 0);
    if(index > -1){
      this.percursosVolta.splice(index, 1);
    }
  }

  eliminarPercursoReforco(codPercurso: string){
    const index = this.percursosReforco.indexOf(codPercurso, 0);
    if(index > -1){
      this.percursosReforco.splice(index, 1);
    }
  }

  eliminarPercursoVazio(codPercurso: string){
    const index = this.percursosVazio.indexOf(codPercurso, 0);
    if(index > -1){
      this.percursosVazio.splice(index, 1);
    }
  }

  eliminarTipoTripulante(codTipoTripulante: string){
    const index = this.tiposTripulantes.indexOf(codTipoTripulante, 0);
    if(index > -1){
      this.tiposTripulantes.splice(index, 1);
    }
  }

  eliminarTipoViatura(codTipoViatura: string){
    const index = this.tiposViaturas.indexOf(codTipoViatura, 0);
    if(index > -1){
      this.tiposViaturas.splice(index, 1);
    }
  }

}
