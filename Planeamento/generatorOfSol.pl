
:-consult("bc_autocarros2.pl").
:- consult("utility.pl").

%----------------------------------------------------
% without find all and with time as the key factor
%----------------------------------------------------
:- dynamic fastestSolution/2.
:- dynamic nSol/1.

caminho_time(Src,Dest,StartingTime, Path, TimeCosts):- 
    caminho_time(Src,Dest, StartingTime, [],Path, 0, TimeCosts).

caminho_time(Dest,Dest,_, Visited,Path, TripDuration, TripDuration):- 
    reverse(Visited,Path),!.

caminho_time(Src,Dest,StartingTime, Visited,Path, TripDuration,TimeCosts):-
    liga(Src,Next,Line),
    \+member((_,_,Line),Visited),
    \+member((Next,_,_),Visited),
    \+member((_,Next,_),Visited),
    CurrentTime is StartingTime + TripDuration,
    getTimeCostsForWaitingAndTravelling(CurrentTime,Line,Src,Next,Costs),
    TripDurationNew is TripDuration + Costs,
    caminho_time(Next,Dest,StartingTime,[(Src,Next,Line)|Visited],Path, TripDurationNew, TimeCosts).

plan_mud_mot1_time(Src,Dest,StartingTime,BestPath,Time):-
    get_time(Ti),
    asserta(nSol(0)),
    (fastestRoute(Src,Dest,StartingTime);true),
    retract(fastestSolution(BestPath,Time)),
    get_time(Tf),TSol is Tf-Ti,
    retract(nSol(NS)),
    write('Numero de Solucoes:'),write(NS),nl,
    write('Tempo de geracao da solucao:'),write(TSol),nl.

fastestRoute(Src,Dest,StartingTime):-
    asserta(fastestSolution(_,999999)),
    caminho_time(Src,Dest,StartingTime, Path, TimeCosts),
    retract(nSol(NS)), NS1 is NS+1, asserta(nSol(NS1)),
    updateBest(Path,TimeCosts),
    fail.

updateBest(Path,TimeCosts):-
    fastestSolution(_,N),
    N >= TimeCosts, retract(fastestSolution(_,_)),
    asserta(fastestSolution(Path,TimeCosts)).


