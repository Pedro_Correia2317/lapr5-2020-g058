
import { AbreviaturaNo } from '../../../../src/domain/no/model/AbreviaturaNo';
import { CodigoPercurso } from '../../../../src/domain/percurso/model/CodigoPercurso';
import { DescricaoPercurso } from '../../../../src/domain/percurso/model/DescricaoPercurso';
import { DistanciaSegmento } from '../../../../src/domain/percurso/model/DistanciaSegmento';
import { DistanciaTotalPercurso } from '../../../../src/domain/percurso/model/DistanciaTotalPercurso';
import { DuracaoSegmento } from '../../../../src/domain/percurso/model/DuracaoSegmento';
import { DuracaoTotalPercurso } from '../../../../src/domain/percurso/model/DuracaoTotalPercurso';
import { Percurso } from '../../../../src/domain/percurso/model/Percurso';
import { SegmentoRede } from '../../../../src/domain/percurso/model/SegmentoRede';
import { SequenciaSegmento } from '../../../../src/domain/percurso/model/SequenciaSegmento';


test("testarCodigoNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(null, segmentos, descricao, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarSegmentosNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, null, descricao, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarDescricaoNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, null, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarDistanciaNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, descricao, null, duracaoT)}).toThrow(SyntaxError);
});

test("testarDuracaoNulo", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, descricao, distanciaT, null)}).toThrow(SyntaxError);
});

test("testarCodigoIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(undefined, segmentos, descricao, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarSegmentosIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, undefined, descricao, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarDescricaoIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, undefined, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarDistanciaIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, descricao, undefined, duracaoT)}).toThrow(SyntaxError);
});

test("testarDuracaoIndefinido", () => {
    expect(() => {
        const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
        const codigoNoFim = new AbreviaturaNo('12345678901234567890');
        const sequencia = new SequenciaSegmento(1);
        const distancia = new DistanciaSegmento(12.4567);
        const duracao = new DuracaoSegmento(3457);
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, descricao, distanciaT, undefined)}).toThrow(SyntaxError);
});

test("testarPercursoSemSegmentos", () => {
    expect(() => {
        const codigoPercurso = new CodigoPercurso('12345678901234567890');
        const segmentos = [];
        const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
        const distanciaT = new DistanciaTotalPercurso(12.4567);
        const duracaoT = new DuracaoTotalPercurso(3457);
        new Percurso(codigoPercurso, segmentos, descricao, distanciaT, duracaoT)}).toThrow(SyntaxError);
});

test("testarValoresValidos", () => {
    const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
    const codigoNoFim = new AbreviaturaNo('12345678901234567890');
    const sequencia = new SequenciaSegmento(1);
    const distancia = new DistanciaSegmento(12.4567);
    const duracao = new DuracaoSegmento(3457);
    const codigoPercurso = new CodigoPercurso('12345678901234567890');
    const segmentos = [];
    segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
    segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
    const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
    const distanciaT = new DistanciaTotalPercurso(12.4567);
    const duracaoT = new DuracaoTotalPercurso(3457);
    expect(new Percurso(codigoPercurso, segmentos, descricao, distanciaT, duracaoT)).toBeInstanceOf(Percurso);
});

test("testarPercursoApenasUmSegmento", () => {
    const codigoNoInicio = new AbreviaturaNo('12345678901234567890');
    const codigoNoFim = new AbreviaturaNo('12345678901234567890');
    const sequencia = new SequenciaSegmento(1);
    const distancia = new DistanciaSegmento(12.4567);
    const duracao = new DuracaoSegmento(3457);
    const codigoPercurso = new CodigoPercurso('12345678901234567890');
    const segmentos = [];
    segmentos.push(new SegmentoRede(codigoNoInicio, codigoNoFim, sequencia, distancia, duracao));
    const descricao = new DescricaoPercurso('AGUIAR > ESTLO > ESTPA');
    const distanciaT = new DistanciaTotalPercurso(12.4567);
    const duracaoT = new DuracaoTotalPercurso(3457);
    expect(new Percurso(codigoPercurso, segmentos, descricao, distanciaT, duracaoT)).toBeInstanceOf(Percurso);
});