import { PedidoCriarNoPontoRendicaoDTO } from "./PedidoCriarNoPontoRendicaoDTO";


export class PedidoCriarNoEstacaoRecolhaDTO extends PedidoCriarNoPontoRendicaoDTO {

    public capacidade: number;

}