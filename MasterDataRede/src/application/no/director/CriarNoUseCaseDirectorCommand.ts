
import { NoBuilder } from '../services/NoBuilder';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import { PedidoCriarNoDTO } from '../dto/PedidoCriarNoDTO';

export interface CriarNoUseCaseDirectorCommand {

    executar(request: PedidoCriarNoDTO, noBuilder: NoBuilder): Promise<DetalhesOperacaoDTO>;

}