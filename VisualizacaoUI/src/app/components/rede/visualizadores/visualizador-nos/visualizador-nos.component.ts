
import { Output } from '@angular/core';
import { Component, EventEmitter, OnInit } from '@angular/core';
import { NoDTO } from 'src/app/models/no/NoDTO';
import { DetalhesPesquisaDTO } from 'src/app/models/pedidos/DetalhesPesquisaDTO';
import { AcessoNosService } from 'src/app/services/rede/AcessoNos.service';

@Component({
  selector: 'visualizador-nos',
  templateUrl: './visualizador-nos.component.html',
  styleUrls: ['./visualizador-nos.component.css']
})
export class VisualizadorNosComponent implements OnInit {

  @Output() noClicado: EventEmitter<NoDTO> = new EventEmitter();

  nos: NoDTO[];

  dadosCriacaoPaginas: {numeroTotal: number, tamanhoPagina: number, pagina: number};
  
  tamanho: number;
  
  sort: string;

  pesquisa: string;

  constructor(private todosNos: AcessoNosService) {
    this.nos = [];
  }

  ngOnInit() {
  }

  onClickedNode(noSelecionado: NoDTO){
    this.noClicado.emit(noSelecionado);
  }

  mudarPagina(pagina: number){
    this.atualizarNos(pagina, this.tamanho, this.sort);
  }

  mudarPesquisa(pesquisa: string){
    this.pesquisa = pesquisa;
    this.atualizarNos(1, this.tamanho, this.sort, pesquisa);
  }

  mudarTamanho(tamanho: number){
    this.tamanho = tamanho;
    this.atualizarNos(1, tamanho, this.sort, this.pesquisa);
  }

  private atualizarNos(pagina: number, tamanho: number, sort?: string, pesquisa?: string){
    this.todosNos.procurarNos(pagina, tamanho, sort, pesquisa).subscribe((resultados: DetalhesPesquisaDTO<NoDTO>) => {
      if(!resultados.sucesso){
        alert(resultados.descricao);
      } else {
        this.nos = resultados.lista;
        this.dadosCriacaoPaginas = { numeroTotal: resultados.numeroTotal, tamanhoPagina: tamanho, pagina: pagina };
      }
    });
  }

}