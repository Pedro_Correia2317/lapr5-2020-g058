
:- module(find_shortest_path_service, [calculate_shortest_path/5]).

:- use_module("../http/http_client_connections").

calculate_shortest_path(Orig,Dest,CurrentTime,Path,Times) :- 
	calculate_shortest_path_2(Dest,[(_,[CurrentTime],[Orig])],Path,Times).

calculate_shortest_path_2(Dest,[(_,OutTimes,[Dest|T])|_],Path,Times) :- 
    reverse([Dest|T],Path), 
    reverse(OutTimes, Times).

calculate_shortest_path_2(Dest, [ (_,[CurrentTime|OtherTimes],SearchedNodes) | OtherPaths ], Path, Times) :- 
    SearchedNodes=[CurrentNode|_],
	findall( (CEX, [CaX|[CurrentTime|OtherTimes]], [X|SearchedNodes]) ,(
		Dest \== CurrentNode,
		findNextNode(CurrentNode, X, CurrentTime, NextOutTime),
		NextOutTime > -1,
		\+ member(X,SearchedNodes),
						
		CaX is NextOutTime + 120, 
		estimative(X, Dest, EstX),
		CEX is CaX + EstX
		),NewNodes),
	append(OtherPaths,NewNodes,AllNodes),
	%write('NewNodes='),write(NewNodes),nl,
	sort(AllNodes,AllSortedNodes),
	%write('AllSortedNodes='),write(AllSortedNodes),nl,
	calculate_shortest_path_2(Dest,AllSortedNodes,Path,Times).

findNextNode(Node1, Node2, CurrentTime, NextOutTime) :- 
				http_client_connections:connection(Node1, Node2, L),
				http_client_connections:all_timetables_path(L, Timetables),
				http_client_connections:line(L, _, Path, _, _),
				nth1(Position, Path, Node1),
				findNextSuitableTime(Position, Timetables, CurrentTime, NextOutTime).



findNextSuitableTime(_, [], _, -1).

findNextSuitableTime(Pos, [TimetableSet|_], CurrentTime, NextOutTime) :-
				nth1(Pos, TimetableSet, OutTime),
				CurrentTime =< OutTime, !,
				NextOutTime is OutTime.

findNextSuitableTime(Position, [_|OtherTimeTables], CurrentTime, NextOutTime) :-
				findNextSuitableTime(Position, OtherTimeTables, CurrentTime, NextOutTime).

estimative(Node1, Node2, Estimative) :- 
			http_client_connections:node(_,Node1,_,_,Lat1,Lon1),
			http_client_connections:node(_,Node2,_,_,Lat2,Lon2), !,
			http_client_connections:top_average_speed(Speed),
    			P is 0.017453292519943295,
   			A is (0.5-cos((Lat2-Lat1)*P)/2 + cos(Lat1*P)*cos(Lat2*P)*(1-cos((Lon2-Lon1)*P))/2),
    			Estimative is (12742 * asin(sqrt(A)) * 1000)/Speed.