import { TipoViaturaDTO } from "../dto/TipoViaturaDTO";
import { AutonomiaTipoViatura } from "./AutonomiaTipoViatura";
import { CodigoTipoViatura } from "./CodigoTipoViatura";
import { ConsumoTipoViatura } from "./ConsumoTipoViatura";
import { CustoTipoViatura } from "./CustoTipoViatura";
import { DescricaoTipoViatura } from "./DescricaoTipoViatura";
import { EmissoesTipoViatura } from "./EmissoesTipoViatura";
import { TipoCombustivel } from "./TipoCombustivel";
import { VelocidadeMediaTipoViatura } from "./VelocidadeMediaTipoViatura";

export class TipoViatura {

    private codigo: CodigoTipoViatura;

    private descricao: DescricaoTipoViatura;

    private autonomia: AutonomiaTipoViatura;

    private consumo: ConsumoTipoViatura;

    private custo: CustoTipoViatura;

    private emissoes: EmissoesTipoViatura;

    private velocidadeMedia: VelocidadeMediaTipoViatura;

    private tipoCombustivel: TipoCombustivel;

    constructor(cod: CodigoTipoViatura, desc: DescricaoTipoViatura, 
                auto: AutonomiaTipoViatura, con: ConsumoTipoViatura, 
                custo: CustoTipoViatura, emiss: EmissoesTipoViatura, 
                velMedia: VelocidadeMediaTipoViatura, comb: TipoCombustivel){
        if(cod == null || desc == null || auto == null 
            || con == null || custo == null || emiss == null 
            || velMedia == null || comb == null){
            throw new SyntaxError('Null values were inserted');
        }
        this.codigo = cod;
        this.descricao = desc;
        this.autonomia = auto;
        this.consumo = con;
        this.custo = custo;
        this.emissoes = emiss;
        this.velocidadeMedia = velMedia;
        this.tipoCombustivel = comb;
    }

    toBaseDTO(): TipoViaturaDTO {
        const dto = new TipoViaturaDTO();
        dto.codigo = this.codigo.getValue();
        dto.descricao = this.descricao.getValue();
        dto.autonomia = this.autonomia.getValue();
        dto.consumo = this.consumo.getValue();
        dto.custo = this.custo.getValue();
        dto.emissoes = this.emissoes.getValue();
        dto.velocidadeMedia = this.velocidadeMedia.getValue();
        dto.tipoCombustivel = this.tipoCombustivel.toBaseDTO();

        return dto;
    }
}