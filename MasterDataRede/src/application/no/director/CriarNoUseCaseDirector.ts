
import Container from "typedi";
import { NoBuilder } from "../services/NoBuilder";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { PedidoCriarNoDTO } from "../dto/PedidoCriarNoDTO";
import { CriarNoUseCaseDirectorCommand } from "./CriarNoUseCaseDirectorCommand";

export class CriarNoUseCaseDirector {

    private mapaUis: Map<string, CriarNoUseCaseDirectorCommand>;

    private builderUIs: Map<string, string>

    constructor(mapaUis: Map<string, CriarNoUseCaseDirectorCommand>, builderUIs: Map<string, string>){
        this.mapaUis = mapaUis;
        this.builderUIs = builderUIs;
    }

    processarPedido(req: PedidoCriarNoDTO): Promise<DetalhesOperacaoDTO> {
        const rota: CriarNoUseCaseDirectorCommand = this.mapaUis.get(req.tipoNo);
        if(rota === undefined) return undefined;
        const builder: NoBuilder = Container.get(this.builderUIs.get(req.tipoNo));
        return rota.executar(req, builder);
    }
}