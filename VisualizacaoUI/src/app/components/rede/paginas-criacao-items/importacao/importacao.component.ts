
import { Component, OnInit } from '@angular/core';
import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { AcessoImportacaoService } from 'src/app/services/rede/AcessoImportacao.service';

@Component({
  selector: 'app-importacao',
  templateUrl: './importacao.component.html',
  styleUrls: ['./importacao.component.css']
})
export class ImportacaoComponent implements OnInit {

  importacoes: DetalhesOperacaoDTO[];

  ficheiro: File;

  constructor(private importacao: AcessoImportacaoService) {
        this.importacoes = [];
    }

  ngOnInit(): void {
  }

  onFileChange(event: any){
    this.ficheiro = event.target.files[0];
  }

  onSubmit() {
    if(this.ficheiro != null){
      this.importacao.enviarFicheiro(this.ficheiro).subscribe((resultados: DetalhesOperacaoDTO[]) => {
        this.importacoes = resultados;
      });
    }
  }

}
