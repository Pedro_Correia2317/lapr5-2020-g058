
import { NoRepository } from "../../../../repository/interfaces/NoRepository";
import { DetalhesOperacaoDTO } from "../../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../../utils/services/ServicoLogger";
import { NoEstacaoRecolhaBuilder } from "../../services/NoEstacaoRecolhaBuilder";
import { CriarNoEstacaoRecolhaUseCase } from "../CriarNoEstacaoRecolhaUseCase";
import { CriarNoPontoRendicaoUseCase } from "../CriarNoPontoRendicaoUseCase";

export class CriarNoEstacaoRecolhaUseCaseImpl implements CriarNoEstacaoRecolhaUseCase {

    private criarNo: CriarNoPontoRendicaoUseCase;

    constructor(criarNo: CriarNoPontoRendicaoUseCase){
        this.criarNo = criarNo;
    }

    async adicionarTempoDeslocacaoTripulacao(noBuilder: NoEstacaoRecolhaBuilder, logger: ServicoLogger, todosNos: NoRepository, tempo: number, abreviatura: string): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.adicionarTempoDeslocacaoTripulacao(noBuilder, logger, todosNos, tempo, abreviatura);
    }

    aplicarCapacidade(noBuilder: NoEstacaoRecolhaBuilder, logger: ServicoLogger, capacidade: number): DetalhesOperacaoDTO {
        const resultDTO: DetalhesOperacaoDTO = new DetalhesOperacaoDTO();
        try {
            noBuilder.aplicarCapacidadeVeiculos(capacidade);
            resultDTO.sucesso = true;
        } catch (err){
            logger.registarFalha(err);
            if (err instanceof TypeError){
                resultDTO.mensagem = 'createNode.NaNCapacity';
            } else if (err instanceof SyntaxError){
                resultDTO.mensagem = 'createNode.invalidCapacity';
            } else {
                throw err;
            }
        }
        return resultDTO;
    }

    aplicarNome(noBuilder: NoEstacaoRecolhaBuilder, logger: ServicoLogger, nome: string): DetalhesOperacaoDTO {
        return this.criarNo.aplicarNome(noBuilder, logger, nome);
    }

    async aplicarAbreviatura(noBuilder: NoEstacaoRecolhaBuilder, logger: ServicoLogger, todosNos: NoRepository, abreviatura: string): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.aplicarAbreviatura(noBuilder, logger, todosNos, abreviatura);
    }

    aplicarCoordenadas(noBuilder: NoEstacaoRecolhaBuilder, logger: ServicoLogger, lat: number, lon: number): DetalhesOperacaoDTO {
        return this.criarNo.aplicarCoordenadas(noBuilder, logger, lat, lon);
    }

    registarNo(noBuilder: NoEstacaoRecolhaBuilder, logger: ServicoLogger, todosNos: NoRepository): Promise<DetalhesOperacaoDTO> {
        return this.criarNo.registarNo(noBuilder, logger, todosNos);
    }

}