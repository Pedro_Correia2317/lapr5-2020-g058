

export class VelocidadeMediaTipoViatura {

    private velocidadeMedia: number;

    constructor(velocidadeMedia: number){
        if (!Number.isInteger(velocidadeMedia)){
            throw new TypeError(`The average speed is not a integer: ${velocidadeMedia}`);
        }
        if (velocidadeMedia <= 0 || velocidadeMedia > 300){
            throw new SyntaxError(`The average speed ${velocidadeMedia} is <= 0 or > 300`);
        }
        this.velocidadeMedia = velocidadeMedia;
    }

    getValue(): number {
        return this.velocidadeMedia;
    }

    toString(): string {
        return `VelocidadeMediaTipoViatura: [${this.velocidadeMedia}]`;
    }
}