
:- module(http_client_connections, [add_knowledge_base/0, add_lines/0, add_timetables/0]).
:- use_module(http_connection_service).

% Knowledge Base Facts
:- dynamic node/6.
:- dynamic line/5.
%:- dynamic timetable/2.
:- dynamic connection/3.
:- dynamic all_timetables_path/2.
:- dynamic top_average_speed/1.

add_knowledge_base :-
	add_nodes,
	add_lines, 
	generate_connections, 
	generate_timetables,
	generate_top_average_speed, !.

remove_knowledge_base :-
	retractall(node(_,_,_,_,_,_)),
	retractall(line(_,_,_,_,_)).

add_nodes :-
	search_nodes(Data),
	parse_nodes(Data.get(lista)).

add_lines :-
	search_lines(Data),
	parse_lines(Data).

add_timetables :-
	search_timetables(Data),
	parseTimetables(Data, LineNumber, Timetables),
	assertz(timetable(LineNumber, Timetables)).

parse_nodes([]).
parse_nodes([H|Data]) :-
	Name = H.get(nome),
	ShortName = H.get(abreviatura),
	TN = H.get(tipoNo),
	(TN = "NO_ESTACAO_RECOLHA" -> IsCollectionStation = "t"; IsCollectionStation = "f"),
	(TN = "NO_PONTO_RENDICAO" -> IsRenditionPoint = "t"; IsRenditionPoint = "f"),
	Lat = H.get(latitude),
	Lon = H.get(longitude),
	assertz(node(Name, ShortName, IsCollectionStation, IsRenditionPoint, Lat, Lon)),
	parse_nodes(Data).

parse_lines([]).
parse_lines([AllLinePaths|Data]) :-
	normalize_list(AllLinePaths.get(lista), H),
	parse_paths(H),
	parse_lines(Data).

parse_paths([]).
parse_paths([H|Data]) :-
	Name = H.get(descricao),
	PathCode = H.get(codigo),
	split_string(Name, ">", " ", Path),
	DurationMinutes = H.get(duracaoTotal),
	DistanceMeters = H.get(distanciaTotal),
	assertz(line(Name, PathCode, Path, DurationMinutes, DistanceMeters)),
	parse_paths(Data).
	
normalize_list([H|T], R) :- !, normalize_list(H, L1), normalize_list(T, L2), append(L1, L2, R).
normalize_list([], []) :- !.
normalize_list(L, [L]).



generate_connections :- 
	retractall(connection(_,_,_)),
	findall(_,
		((node(_,Node1,"t","f",_,_);node(_,Node1,"f","t",_,_)),
		(node(_,Node2,"t","f",_,_);node(_,Node2,"f","t",_,_)),
		Node1\==Node2,
		line(Description,_,Path,_,_),
		nodes_order(Node1,Node2,Path),
		assertz(connection(Node1,Node2,Description))
	),_).

nodes_order(Node1,Node2,[Node1|L]) :- member(Node2,L),!.
nodes_order(Node1,Node2,[_|L]):- nodes_order(Node1,Node2,L).



generate_timetables :- 
		retractall(all_timetables_path(_,_)),
		findall(_,(
			line(PathDescription, _, _, _, _),
			setof(List, timetable(PathDescription, List), AllTimetables), 
			assertz(all_timetables_path(PathDescription,AllTimetables))
		),_).



generate_top_average_speed :-
			retractall(top_average_speed(_)),
			findall((Duration, Distance), (line(_,_,_,Duration,Distance)), List),
			generate_top_average_speed_2(List, TopSpeed),
			TopSpeed > -1,
			assertz(top_average_speed(TopSpeed)).

generate_top_average_speed_2([], -1) :- !.
generate_top_average_speed_2([(Duration, Distance)|T], TopSpeed) :- 
			generate_top_average_speed_2(T, TopSpeed1),
			CurrentSpeed is Distance/(Duration*60),
			(
				CurrentSpeed > TopSpeed1,
				TopSpeed is CurrentSpeed
			;
				TopSpeed is TopSpeed1
			).


