

import { TipoCombustivelDTO } from "../dto/TipoCombustivelDTO";

export class TipoCombustivel {

    static DIESEL = new TipoCombustivel('DIESEL', 'l/100Km');

    static GASOLINA = new TipoCombustivel('GASOLINA', 'l/100Km');

    static GPL = new TipoCombustivel('GPL', 'l/100Km');
    
    static HIDROGENIO = new TipoCombustivel('HIDROGENIO', 'l/100Km');
    
    static ELECTRICO = new TipoCombustivel('ELECTRICO', 'kW/100Km');

    private descricao: string;

    private unidade: string;

    private constructor(descricao: string, unidade: string) {
      this.descricao = descricao;
      this.unidade = unidade;
    }

    toString(): string {
      return `TipoNo.${this.descricao}`;
    }

    toBaseDTO(): TipoCombustivelDTO {
        const tipoNoDTO = new TipoCombustivelDTO();
        tipoNoDTO.descricao = this.descricao;
        tipoNoDTO.unidade = this.unidade;
        return tipoNoDTO;
    }

    static valor(tipoComb: string): TipoCombustivel {
        let valor: TipoCombustivel;
        switch (tipoComb) {
            case 'DIESEL':
                valor = TipoCombustivel.DIESEL;
                break;
            case 'GASOLINA':
                valor = TipoCombustivel.GASOLINA;
                break;
            case 'GPL':
                valor = TipoCombustivel.GPL;
                break;
            case 'HIDROGENIO':
                valor = TipoCombustivel.HIDROGENIO;
                break;
            case 'ELECTRICO':
                valor = TipoCombustivel.ELECTRICO;
                break;
            default:
                throw new SyntaxError('Unknown fuel type');
        }
        return valor;
    }

    static values(): TipoCombustivel[] {
      const array: TipoCombustivel[] = [];
      array.push(TipoCombustivel.DIESEL);
      array.push(TipoCombustivel.GASOLINA);
      array.push(TipoCombustivel.GPL);
      array.push(TipoCombustivel.HIDROGENIO);
      array.push(TipoCombustivel.ELECTRICO);

      return array;
    }

    static valuesInDTO(): TipoCombustivelDTO[] {
      const array: TipoCombustivelDTO[] = [];
      array.push(TipoCombustivel.DIESEL.toBaseDTO());
      array.push(TipoCombustivel.GASOLINA.toBaseDTO());
      array.push(TipoCombustivel.GPL.toBaseDTO());
      array.push(TipoCombustivel.HIDROGENIO.toBaseDTO());
      array.push(TipoCombustivel.ELECTRICO.toBaseDTO());

      return array;
    }

}