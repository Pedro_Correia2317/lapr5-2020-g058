
import { Router, Request, Response } from "express";
import Container, { Inject } from "typedi";
import { LinhaBuilder } from "../../application/linha/services/LinhaBuilder";
import { ListarPercursosLinhaUseCase } from "../../application/linha/usecases/ListarPercursosLinhaUseCase";
import { PercursoBuilder } from "../../application/percurso/services/PercursoBuilder";
import { SegmentoRedeBuilder } from "../../application/percurso/services/SegmentoRedeBuilder";
import { ServicoLogger } from "../../application/utils/services/ServicoLogger";
import { LinhaRepository } from "../../repository/interfaces/LinhaRepository";
import { PercursoRepository } from "../../repository/interfaces/PercursoRepository";

export class RotasLinhaUnica {

    @Inject("PrimaryListarPercursosLinhaUseCase")
    private listarUseCase: ListarPercursosLinhaUseCase;

    @Inject("PrimaryServicoLogger")
    private logger: ServicoLogger;

    @Inject("PrimaryLinhaRepository")
    private linhaRepo: LinhaRepository;

    @Inject("PrimaryPercursoRepository")
    private pRepo: PercursoRepository;
    
    insertRoutes = (router: Router): void => {
        router.route('/linha/:codigo')
            .get(this.rotaListagemLinhas);
    }

    rotaListagemLinhas = async (req: Request, res: Response): Promise<void> => {
        const codigo = req.params.codigo? req.params.codigo.toString(): '';
        const lBuilder: LinhaBuilder = Container.get('PrimaryLinhaBuilder');
        const pBuilder: PercursoBuilder = Container.get('PrimaryPercursoBuilder');
        const sBuilder: SegmentoRedeBuilder = Container.get('PrimarySegmentoRedeBuilder');
        const resultado = await this.listarUseCase.obterPercursosLinha(lBuilder, pBuilder, sBuilder, this.linhaRepo, this.pRepo, this.logger, codigo);
        res.send({'sucesso': resultado.sucesso, 'lista': resultado.lista, 'numeroTotal': resultado.count, 'mensagem': resultado.descricao});
    }
}