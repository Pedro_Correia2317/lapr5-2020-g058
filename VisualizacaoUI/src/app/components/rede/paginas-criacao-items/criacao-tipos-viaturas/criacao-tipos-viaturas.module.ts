
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { CriacaoTiposViaturasComponent } from './criacao-tipos-viaturas.component';

const routes: Routes = [
    {path: '', component: CriacaoTiposViaturasComponent}
];

@NgModule({
  declarations: [CriacaoTiposViaturasComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CriacaoTiposViaturasModule { }