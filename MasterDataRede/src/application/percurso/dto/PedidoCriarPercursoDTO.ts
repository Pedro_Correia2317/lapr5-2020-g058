
import { PedidoCriarSegmentoDTO } from "./PedidoCriarSegmentoDTO";

export class PedidoCriarPercursoDTO {

    public codigo: string;

    public segmentos: PedidoCriarSegmentoDTO[];

}