
import { CapacidadeVeiculos } from '../../../../src/domain/no/model/CapacidadeVeiculos';

test('testarCapacidadeNulo', () => {
    expect(() => {new CapacidadeVeiculos(null)}).toThrow(TypeError);
    expect(() => {new CapacidadeVeiculos(undefined)}).toThrow(TypeError);
});

test('testarCapacidadeValoresPositivos', () => {
    expect(new CapacidadeVeiculos(1)).toBeInstanceOf(CapacidadeVeiculos);
    expect(new CapacidadeVeiculos(13.0)).toBeInstanceOf(CapacidadeVeiculos);
    expect(new CapacidadeVeiculos(200)).toBeInstanceOf(CapacidadeVeiculos);
    expect(new CapacidadeVeiculos(0)).toBeInstanceOf(CapacidadeVeiculos);
});

test('testarCapacidadeValoresNegativos', () => {
    expect(() => {new CapacidadeVeiculos(-11)}).toThrow(SyntaxError);
    expect(() => {new CapacidadeVeiculos(-21)}).toThrow(SyntaxError);
    expect(() => {new CapacidadeVeiculos(-1)}).toThrow(SyntaxError);
});

test('testarCapacidadeValoresCasasDecimais', () => {
    expect(() => {new CapacidadeVeiculos(1.3346)}).toThrow(TypeError);
    expect(() => {new CapacidadeVeiculos(2.0000000001)}).toThrow(TypeError);
    expect(() => {new CapacidadeVeiculos(-1.236)}).toThrow(TypeError);
});

test('testarCapacidadeValoresExtremos', () => {
    expect(() => {new CapacidadeVeiculos(24674322334653)}).toThrow(SyntaxError);
    expect(() => {new CapacidadeVeiculos(201)}).toThrow(SyntaxError);
});