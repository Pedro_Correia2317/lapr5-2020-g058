
import { PedidoCriarNoDTO } from "./PedidoCriarNoDTO"

export class PedidoCriarNoPontoRendicaoDTO extends PedidoCriarNoDTO {

    tempos: PedidoTempoDeslocacaoDTO[];

}

export class PedidoTempoDeslocacaoDTO {

    public tempo: number;
    
    public abreviaturaNo: string;
}