
import { Router, Request, Response } from 'express';
import { Container, Inject, Service } from 'typedi';
import { PercursoBuilder } from '../../application/percurso/services/PercursoBuilder';
import { SegmentoRedeBuilder } from '../../application/percurso/services/SegmentoRedeBuilder';
import { CriarPercursoUseCaseDirector } from '../../application/percurso/director/CriarPercursoUseCaseDirector';
import { ListarPercursosUseCase } from '../../application/percurso/usecases/ListarPercursosUseCase';
import { ServicoLogger } from '../../application/utils/services/ServicoLogger';
import { PercursoRepository } from '../../repository/interfaces/PercursoRepository';

@Service()
export class RotasVariosPercursos {

    @Inject("PrimaryCriarPercursoUseCaseDirector")
    private criarDirector: CriarPercursoUseCaseDirector;

    @Inject("PrimaryListarPercursosUseCase")
    private listarUseCase: ListarPercursosUseCase;

    @Inject("PrimaryServicoLogger")
    private logger: ServicoLogger;

    @Inject("PrimaryPercursoRepository")
    private pRepo: PercursoRepository;
    
    insertRoutes = (router: Router): void => {
        router.route('/percursos')
            .post(this.inserirRotaCriacaoPercurso)
            .get(this.rotaListagemPercursos);
    }

    inserirRotaCriacaoPercurso = async (req: Request, res: Response): Promise<void> => {
        const pBuilder: PercursoBuilder = Container.get("PrimaryPercursoBuilder");
        const sBuilder: SegmentoRedeBuilder = Container.get("PrimarySegmentoRedeBuilder")
        const resultado = await this.criarDirector.processarPedido(req.body, pBuilder, sBuilder);
        res.send({'sucesso': resultado.sucesso, 'mensagem': resultado.mensagem});
    }

    rotaListagemPercursos = async (req: Request, res: Response): Promise<void> => {
        const pBuilder: PercursoBuilder = Container.get("PrimaryPercursoBuilder");
        const sBuilder: SegmentoRedeBuilder = Container.get("PrimarySegmentoRedeBuilder");
        let num = req.query.page? Number.parseInt(req.query.page.toString()) : 1;
        num = num <= 0? 1 : num;
        let tam = req.query.size? Number.parseInt(req.query.size.toString()) : 24;
        tam = tam <= 0? 24 : tam;
        const desc = req.query.descricao? req.query.descricao.toString() : '';
        const sort = req.query.sort? req.query.sort.toString(): '';
        const resultado = await this.listarUseCase.listarPercursos(pBuilder, sBuilder, this.logger, this.pRepo, sort, num, tam, desc);
        res.send({'sucesso': resultado.sucesso, 'lista': resultado.lista, 'numeroTotal': resultado.count, 'mensagem': resultado.descricao});
    }

}