
import { NoParagemBuilderMockSucesso, NoParagemBuilderMockErro } from '../../mocks/no/NoParagemBuilderMock';
import { NoRepositoryMockTrue, NoRepositoryMockFalse } from '../../mocks/no/NoRepositoryMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { CriarNoUseCaseImpl } from '../../../src/application/no/usecases/criarNoImpl/CriarNoUseCaseImpl';

const logger = new ServicoLoggerMock();
const noBuilderSucesso = new NoParagemBuilderMockSucesso();
const noBuilderErro = new NoParagemBuilderMockErro();
const noRepoTrue = new NoRepositoryMockTrue();
const noRepoFalse = new NoRepositoryMockFalse();
const useCase = new CriarNoUseCaseImpl();

test('testarAplicarNomeInvalido', () => {
    expect(( useCase.aplicarNome(noBuilderErro, logger, "")).sucesso).toBe(false);
});

test('testarAplicarNomeValido', () => {
    expect(( useCase.aplicarNome(noBuilderSucesso, logger, "")).sucesso).toBe(true);
});

test('testarAplicarAbreviaturaInvalido', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderErro, logger, noRepoTrue, "")).sucesso).toBe(false);
});

test('testarAplicarAbreviaturaInvalido2', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderSucesso, logger, noRepoFalse, "")).sucesso).toBe(false);
});

test('testarAplicarAbreviaturaInvalido3', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderErro, logger, noRepoFalse, "")).sucesso).toBe(false);
});

test('testarAplicarAbreviaturaValido', async () => {
    expect((await useCase.aplicarAbreviatura(noBuilderSucesso, logger, noRepoTrue, "")).sucesso).toBe(true);
});

test('testarAplicarCoordenadasInvalido', () => {
    expect(( useCase.aplicarCoordenadas(noBuilderErro, logger, 0, 0)).sucesso).toBe(false);
});

test('testarAplicarCoordenadasValido', () => {
    expect(( useCase.aplicarCoordenadas(noBuilderSucesso, logger, 0, 0)).sucesso).toBe(true);
});

test('testarRegistarNoInvalido', async () => {
    expect(( await useCase.registarNo(noBuilderErro, logger, noRepoTrue)).sucesso).toBe(false);
});

test('testarRegistarNoErroRegisto', async () => {
    expect(( await useCase.registarNo(noBuilderSucesso, logger, noRepoFalse)).sucesso).toBe(false);
});

test('testarRegistarNoInvalido2', async () => {
    expect(( await useCase.registarNo(noBuilderErro, logger, noRepoFalse)).sucesso).toBe(false);
});

test('testarRegistarNoSucesso', async () => {
    expect(( await useCase.registarNo(noBuilderSucesso, logger, noRepoTrue)).sucesso).toBe(true);
});