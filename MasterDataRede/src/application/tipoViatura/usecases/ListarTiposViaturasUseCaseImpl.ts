import { TipoCombustivelDTO } from "../../../domain/tipoViatura/dto/TipoCombustivelDTO";
import { TipoViaturaDTO } from "../../../domain/tipoViatura/dto/TipoViaturaDTO";
import { TipoCombustivel } from "../../../domain/tipoViatura/model/TipoCombustivel";
import { TipoViaturaRepository } from "../../../repository/interfaces/TipoViaturaRepository";
import { DetalhesPesquisaDTO } from "../../utils/dto/DetalhesPesquisaDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoViaturaBuilder } from "../services/TipoViaturaBuilder";
import { ListarTiposViaturasUseCase } from "./ListarTiposViaturasUseCase";


export class ListarTiposViaturasUseCaseImpl implements ListarTiposViaturasUseCase {

    obterTiposCombustiveis(): TipoCombustivelDTO[] {
        return TipoCombustivel.valuesInDTO();
    }

    async listarTiposViaturas(builder: TipoViaturaBuilder, logger: ServicoLogger, 
                        ttRepo: TipoViaturaRepository, sort: string, numPag: number, 
                        tamPag: number, descricao: string): Promise<DetalhesPesquisaDTO<TipoViaturaDTO>> {
        const resultados = new DetalhesPesquisaDTO<TipoViaturaDTO>();
        await ttRepo.obterTiposViaturas(builder, sort, numPag, tamPag, descricao).then((tiposT) => {
            tiposT.forEach((tipoT) => {
                resultados.lista.push(tipoT.toBaseDTO());
            });
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findVehicleTypes.errorFindingVehicleTypes';
        });
        await ttRepo.countComFiltros(descricao).then((numero) => {
            resultados.count = numero;
        }).catch((err) => {
            logger.registarFalha(err);
            resultados.descricao = 'findVehicleTypes.errorCountingVehicleTypes';
        });
        if (!resultados.descricao){
            resultados.sucesso = true;
            resultados.descricao = 'findVehicleTypes.searchSuccess';
        }
        return resultados;
    }

}