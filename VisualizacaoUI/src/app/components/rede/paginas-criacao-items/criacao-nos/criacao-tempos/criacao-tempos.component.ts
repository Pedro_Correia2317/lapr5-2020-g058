
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { NoDTO } from 'src/app/models/no/NoDTO';
import { PedidoTempoDeslocacaoDTO } from 'src/app/models/no/PedidoNoDTO';

@Component({
  selector: 'criacao-tempos',
  templateUrl: './criacao-tempos.component.html',
  styleUrls: ['./criacao-tempos.component.css']
})
export class CriacaoTemposComponent implements OnInit {

    tempoSegundos: number;

    abreviaturaNo: string;

    constructor(public dialogRef: MatDialogRef<CriacaoTemposComponent>) {
    }
  
    ngOnInit() {
    }

    onCancelarClick(){
        this.dialogRef.close();
    }

    adicionarTempo(tempo: any){
        var aux = tempo.target.value.split(':');
        var segundos = (+aux[0]) * 60 * 60 + (+aux[1]) * 60 + (+aux[2]);
        this.tempoSegundos = segundos;
    }

    adicionarAbreviaturaNo(abreviaturaNo: string){
        this.abreviaturaNo = abreviaturaNo;
    }

    onAdicionarClick(){
        let dto = undefined;
        if(this.tempoSegundos != null && this.abreviaturaNo != null){
            dto = new PedidoTempoDeslocacaoDTO();
            dto.tempo = this.tempoSegundos;
            dto.abreviaturaNo = this.abreviaturaNo;
        }
        this.dialogRef.close(dto);
    }

    onClickedNode(no: NoDTO){
        this.adicionarAbreviaturaNo(no.abreviatura);
    }

}