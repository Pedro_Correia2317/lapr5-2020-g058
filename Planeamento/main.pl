
:- [timetables].
:- use_module(http/http_client_connections).
:- use_module(http/http_server).
:- initialization main.

main :-
    add_knowledge_base,
    start_server(5200).
