
import { CodigoLinha } from '../../../../src/domain/linha/model/CodigoLinha';
import { Linha } from '../../../../src/domain/linha/model/Linha';
import { NecessidadesTipoTripulante } from '../../../../src/domain/linha/model/NecessidadesTipoTripulante';
import { NecessidadesTipoViatura } from '../../../../src/domain/linha/model/NecessidadesTipoViatura';
import { NomeLinha } from '../../../../src/domain/linha/model/NomeLinha';
import { PercursosLinha } from '../../../../src/domain/linha/model/PercursosLinha';
import { TipoNecessidade } from '../../../../src/domain/linha/model/TipoNecessidade';
import { CodigoPercurso } from '../../../../src/domain/percurso/model/CodigoPercurso';

test("testarCodigoNulo", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(null, nome, percs, necTV, necTT)}).toThrow(ReferenceError);
});

test("testarCodigoIndefinido", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(undefined, nome, percs, necTV, necTT)}).toThrow(ReferenceError);
});

test("testarNomeNulo", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, null, percs, necTV, necTT)}).toThrow(ReferenceError);
});

test("testarNomeIndefinido", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, undefined, percs, necTV, necTT)}).toThrow(ReferenceError);
});

test("testarPercursosNulo", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, nome, null, necTV, necTT)}).toThrow(ReferenceError);
});

test("testarPercursosIndefinido", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, nome, undefined, necTV, necTT)}).toThrow(ReferenceError);
});

test("testarNecessidadesTipoViaturaNulo", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, nome, percs, null, necTT)}).toThrow(ReferenceError);
});

test("testarNecessidadesTipoViaturaIndefinido", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, nome, percs, undefined, necTT)}).toThrow(ReferenceError);
});

test("testarNecessidadesTipoTripulanteNulo", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, nome, percs, necTV, null)}).toThrow(ReferenceError);
});

test("testarNecessidadesTipoTripulanteIndefinido", () => {
    expect(() => {
        const cod = new CodigoLinha('12345678901234567890');
        const nome = new NomeLinha('nfnenfinifwinf');
        const codPerc = new CodigoPercurso('12345678901234567890');
        const percs = new PercursosLinha([codPerc], [codPerc], [], []);
        const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
        const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
        new Linha(cod, nome, percs, necTV, undefined)}).toThrow(ReferenceError);
});

test("testarLinhaValida", () => {
    const cod = new CodigoLinha('12345678901234567890');
    const nome = new NomeLinha('nfnenfinifwinf');
    const codPerc = new CodigoPercurso('12345678901234567890');
    const percs = new PercursosLinha([codPerc], [codPerc], [], []);
    const necTV = new NecessidadesTipoViatura([], TipoNecessidade.NAO_APLICAVEL);
    const necTT = new NecessidadesTipoTripulante([], TipoNecessidade.NAO_APLICAVEL);
    expect(new Linha(cod, nome, percs, necTV, necTT)).toBeInstanceOf(Linha);
});