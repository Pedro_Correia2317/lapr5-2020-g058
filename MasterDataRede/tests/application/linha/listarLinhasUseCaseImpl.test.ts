
import { LinhaRepositoryMockTrue, LinhaRepositoryMockFalse } from '../../mocks/linha/linhaRepositoryMock';
import { LinhaBuilderMockSucesso } from '../../mocks/linha/linhaBuilderMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { ListarLinhasUseCaseImpl } from '../../../src/application/linha/usecases/ListarLinhasUseCaseImpl';
import { LinhaBuilder } from '../../../src/application/linha/services/LinhaBuilder';
import { Linha } from '../../../src/domain/linha/model/Linha';

const logger = new ServicoLoggerMock();
const linhaRepoTrue = new LinhaRepositoryMockTrue();
const linhaRepoFalse = new LinhaRepositoryMockFalse();
const useCase = new ListarLinhasUseCaseImpl();
const builder = new LinhaBuilderMockSucesso();

test('testarPesquisaComErroProcurarLinhas', async () => {
    expect((await useCase.obterLinhas(builder, logger, linhaRepoFalse, '', 0, 0, '', '')).sucesso).toBe(false);
});

test('testarPesquisaComErroContarLinha', async () => {
    const aux = linhaRepoFalse.obterLinhas;
    linhaRepoFalse.obterLinhas = async (builder: LinhaBuilder, sort: string, 
                            numPag: number, tamPag: number, abr: string, linhame: string)
                            : Promise<Linha[]> => { return [];};
    expect((await useCase.obterLinhas(builder, logger, linhaRepoFalse, '', 0, 0, '', '')).sucesso).toBe(false);
    linhaRepoFalse.obterLinhas = aux;
});

test('testarPesquisaComSucesso', async () => {
    expect((await useCase.obterLinhas(builder, logger, linhaRepoTrue, '', 0, 0, '', '')).sucesso).toBe(true);
});