
import multer from "multer";

export class MulterConfig {

    private storage: multer.StorageEngine;

    constructor(){
        this.storage = multer.diskStorage({
        destination: function(req, file, cb) {
          cb(null, "uploads/");
        },
        filename: function(req, file, cb) {
          cb(null, file.originalname);
        }
      });
    }

    obtainConfig(): multer.Multer {
        return multer({ storage: this.storage });
    }

}