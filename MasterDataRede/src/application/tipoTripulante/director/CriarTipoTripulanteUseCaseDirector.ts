
import { TipoTripulanteBuilder } from "../services/TipoTripulanteBuilder";
import { CriarTipoTripulanteUseCase } from "../usecases/CriarTipoTripulanteUseCase";
import { DetalhesOperacaoDTO } from "../../utils/dto/DetalhesOperacaoDTO";
import { ServicoLogger } from "../../utils/services/ServicoLogger";
import { TipoTripulanteRepository } from "../../../repository/interfaces/TipoTripulanteRepository";
import { PedidoCriarTipoTripulanteDTO } from "../dto/PedidoCriarTipoTripulanteDTO";

export class CriarTipoTripulanteUseCaseDirector {

    private criarUseCase: CriarTipoTripulanteUseCase;

    private todosTiposT: TipoTripulanteRepository;

    private logger: ServicoLogger;

    constructor(c: CriarTipoTripulanteUseCase, tv: TipoTripulanteRepository, l: ServicoLogger){
        this.criarUseCase = c;
        this.todosTiposT = tv;
        this.logger = l;
    }

    async processarPedido(req: PedidoCriarTipoTripulanteDTO, tBuilder: TipoTripulanteBuilder): Promise<DetalhesOperacaoDTO> {
        let resultado = await this.criarUseCase.aplicarCodigo(tBuilder, this.logger, this.todosTiposT, req.codigo);
        if (!resultado.sucesso) return resultado;
        resultado = await this.criarUseCase.aplicarDescricao(tBuilder, this.logger, this.todosTiposT, req.descricao);
        if (!resultado.sucesso) return resultado;
        return await this.criarUseCase.registarTipoTripulante(tBuilder, this.logger, this.todosTiposT);
    }

}