

import { NoRepositoryMockTrue, NoRepositoryMockFalse } from '../../mocks/no/NoRepositoryMock';
import { ServicoLoggerMock } from '../../mocks/ServicoLoggerMock';
import { ListarNosUseCaseImpl } from '../../../src/application/no/usecases/ListarNosUseCaseImpl';
import { NoBuilder } from '../../../src/application/no/services/NoBuilder';
import { No } from '../../../src/domain/no/model/No';

const logger = new ServicoLoggerMock();
const noRepoTrue = new NoRepositoryMockTrue();
const noRepoFalse = new NoRepositoryMockFalse();
const useCase = new ListarNosUseCaseImpl();
const builders = new Map<string, NoBuilder>();

test('testarPesquisaComErroProcurarNos', async () => {
    expect((await useCase.obterNos(builders, logger, noRepoFalse, '', 0, 0, '', '')).sucesso).toBe(false);
});

test('testarPesquisaComErroContarNos', async () => {
    const aux = noRepoFalse.obterNos;
    noRepoFalse.obterNos = async (builder: Map<string, NoBuilder>, sort: string, 
                            numPag: number, tamPag: number, abr: string, nome: string)
                            : Promise<No[]> => { return [];};
    expect((await useCase.obterNos(builders, logger, noRepoFalse, '', 0, 0, '', '')).sucesso).toBe(false);
    noRepoFalse.obterNos = aux;
});

test('testarPesquisaComSucesso', async () => {
    expect((await useCase.obterNos(builders, logger, noRepoTrue, '', 0, 0, '', '')).sucesso).toBe(true);
});

test('testarPesquisaTiposNo', () => {
    expect(useCase.obterTiposNos().length).toEqual(3);
});