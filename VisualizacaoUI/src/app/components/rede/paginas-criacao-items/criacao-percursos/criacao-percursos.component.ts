import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DetalhesOperacaoDTO } from 'src/app/models/pedidos/DetalhesOperacaoDTO';
import { PedidoCriarPercursoDTO } from 'src/app/models/percursos/PedidoCriarPercursoDTO';
import { PedidoCriarSegmentoDTO } from 'src/app/models/percursos/PedidoCriarSegmentoDTO';
import { AcessoPercursosService } from 'src/app/services/rede/AcessoPercursos.service';
import { CriacaoTemposComponent } from '../criacao-nos/criacao-tempos/criacao-tempos.component';
import { AdicaoSegmentoComponent } from './adicao-segmento/adicao-segmento.component';

@Component({
  selector: 'criacao-percursos',
  templateUrl: './criacao-percursos.component.html',
  styleUrls: ['./criacao-percursos.component.css']
})
export class CriacaoPercursosComponent implements OnInit {

  formCriacaoPercurso: FormGroup;

  segmentos: PedidoCriarSegmentoDTO[];

  abreviaturaNoInicialAtual: string;

  constructor(private todosPercursos: AcessoPercursosService,
              private builder: FormBuilder,
              private dialog: MatDialog) {
    this.formCriacaoPercurso = this.builder.group({
      codigo: ''
    });
    this.segmentos = [];
    this.abreviaturaNoInicialAtual = '';
  }

  ngOnInit() {
  }

  onSubmit(dadosFormulario: any) {
    const dto = new PedidoCriarPercursoDTO();
    dto.codigo = dadosFormulario.codigo;
    dto.segmentos = this.segmentos;
    this.todosPercursos.enviarPercurso(dto).subscribe((resultados: DetalhesOperacaoDTO) => {
      alert(resultados.sucesso + ' na operação: ' + resultados.mensagem);
    });
  }

  eliminarSegmento(event: PedidoCriarSegmentoDTO){
    const index = this.segmentos.indexOf(event, 0);
    if(index > -1){
      this.segmentos.splice(index, 1);
      if(this.segmentos.length > 0){
        this.abreviaturaNoInicialAtual = this.segmentos[this.segmentos.length-1].abreviaturaNoFinal;
      } else {
        this.abreviaturaNoInicialAtual = '';
      }
    }
  }

  openDialog(){
    const dialogRef = this.dialog.open(AdicaoSegmentoComponent, {
      data: {
        abreviaturaNoInicial: this.abreviaturaNoInicialAtual
      }
    });

    dialogRef.afterClosed().subscribe((result: PedidoCriarSegmentoDTO) => {
      if(result != null){
        this.segmentos.push(result);
        this.abreviaturaNoInicialAtual = result.abreviaturaNoFinal;
      }
    });
  }

}