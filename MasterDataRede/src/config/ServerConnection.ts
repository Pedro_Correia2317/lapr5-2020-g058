
import Container from "typedi";
import * as bodyParser from "body-parser";
import cors from 'cors';
import { RotasVariosNos } from "../server/no/RotasVariosNos";
import { RotasVariosPercursos } from "../server/percurso/RotasVariosPercursos";
import { RotasVariosTiposTripulante } from "../server/tipoTripulante/RotasVariosTiposTripulantes";
import { RotasVariosTiposViatura } from "../server/tipoViatura/RotasVariosTiposViaturas";
import { RotasVariasLinhas } from "../server/linha/RotasVariasLinhas";
import { RotasImportacaoFicheiros } from "../server/importacao/RotasImportacaoFicheiros";
import { RotasLinhaUnica } from "../server/linha/RotasLinhaUnica";
import { RotaPercursoUnico } from "../server/percurso/RotaPercursoUnico";

export class ServerConnection {

    private static readonly express = require('express');

    private static readonly app = ServerConnection.express();

    private static readonly router = ServerConnection.express.Router();

    static insertAllRoutes(): void{
        Container.get(RotasVariosNos).insertRoutes(ServerConnection.router);
        Container.get(RotasVariosPercursos).insertRoutes(ServerConnection.router);
        Container.get(RotasVariosTiposViatura).insertRoutes(ServerConnection.router);
        Container.get(RotasVariosTiposTripulante).insertRoutes(ServerConnection.router);
        Container.get(RotasVariasLinhas).insertRoutes(ServerConnection.router);
        Container.get(RotasImportacaoFicheiros).insertRoutes(ServerConnection.router);
        Container.get(RotasLinhaUnica).insertRoutes(ServerConnection.router);
        Container.get(RotaPercursoUnico).insertRoutes(ServerConnection.router);
    }

    public static startServer(port: number): void {
        ServerConnection.app.use(bodyParser.urlencoded({'extended': true}));
        ServerConnection.app.use(bodyParser.json());
        ServerConnection.app.use(cors());
        ServerConnection.app.use('/', ServerConnection.router);
        ServerConnection.app.listen(port, (err: unknown) => {
            if(err){
                process.exit(1);
            }
        });
    }

}