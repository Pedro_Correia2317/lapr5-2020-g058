import {NoRepository} from '../../../repository/interfaces/NoRepository';
import {PercursoRepository} from '../../../repository/interfaces/PercursoRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {PercursoBuilder} from '../services/PercursoBuilder';
import {SegmentoRedeBuilder} from '../services/SegmentoRedeBuilder';

export interface DefinirPercursoUseCase {

    aplicarCodigo(pBuilder: PercursoBuilder, logger: ServicoLogger,
                    todosPercursos: PercursoRepository,
                    codigo: string): Promise<DetalhesOperacaoDTO>;

    aplicarNoInicial(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                    logger: ServicoLogger, todosNosRede: NoRepository,
                    codigoNo: string): Promise<DetalhesOperacaoDTO>;

    aplicarNoFinal(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                    logger: ServicoLogger, todosNosRede: NoRepository,
                    codigoNo: string): Promise<DetalhesOperacaoDTO>;

    adicionarDuracao(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                logger: ServicoLogger, duracao: number): DetalhesOperacaoDTO;

    adicionarDistancia(pBuilder: PercursoBuilder, sBuilder: SegmentoRedeBuilder,
                logger: ServicoLogger, distancia: number): DetalhesOperacaoDTO;

    adicionarSequencia(sBuilder: SegmentoRedeBuilder, logger: ServicoLogger,
                                    sequencia: number): DetalhesOperacaoDTO;

    adicionarSequenciaAutomatica(sBuilder: SegmentoRedeBuilder,
                                    logger: ServicoLogger): DetalhesOperacaoDTO;

    adicionarSegmentoCriadoAPercurso(pBuilder: PercursoBuilder,
        sBuilder: SegmentoRedeBuilder, logger: ServicoLogger): DetalhesOperacaoDTO;

    registarPercurso(pBuilder: PercursoBuilder, logger: ServicoLogger,
            todosPercursos: PercursoRepository): Promise<DetalhesOperacaoDTO>;
}