
import {TipoViaturaRepository} from '../../../repository/interfaces/TipoViaturaRepository';
import {DetalhesOperacaoDTO} from '../../utils/dto/DetalhesOperacaoDTO';
import {ServicoLogger} from '../../utils/services/ServicoLogger';
import {TipoViaturaBuilder} from '../services/TipoViaturaBuilder';

export interface CriarTipoViaturaUseCase {

    aplicarCodigo(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, todosTiposV: TipoViaturaRepository, codigo: string): Promise<DetalhesOperacaoDTO>;

    aplicarDescricao(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, todosTiposV: TipoViaturaRepository, descricao: string): Promise<DetalhesOperacaoDTO>;

    aplicarAutonomia(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, autonomia: number): DetalhesOperacaoDTO;

    aplicarCusto(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, custo: number): DetalhesOperacaoDTO;

    aplicarVelocidadeMedia(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, velocidade: number): DetalhesOperacaoDTO;

    aplicarConsumo(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, consumo: number): DetalhesOperacaoDTO;

    aplicarEmissoes(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, emissoes: number): DetalhesOperacaoDTO;

    aplicarTipoCombustivel(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, tipoCombustivel: string): DetalhesOperacaoDTO;

    registarTipoViatura(tBuilder: TipoViaturaBuilder, logger: ServicoLogger, todosTiposV: TipoViaturaRepository): Promise<DetalhesOperacaoDTO>;

}