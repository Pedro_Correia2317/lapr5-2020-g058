
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VisualizacaoItemsModule } from '../../../utils/visualizacao-items/visualizacao-items.module';

import { VisualizadorTiposViaturasComponent } from './visualizador-tipos-viaturas.component';

@NgModule({
  declarations: [VisualizadorTiposViaturasComponent],
  imports: [CommonModule, VisualizacaoItemsModule],
  exports: [VisualizadorTiposViaturasComponent, RouterModule]
})
export class VisualizadorTiposViaturasModule { }