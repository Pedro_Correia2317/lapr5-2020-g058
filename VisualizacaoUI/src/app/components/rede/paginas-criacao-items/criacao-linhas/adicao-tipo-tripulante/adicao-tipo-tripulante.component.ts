
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { TipoTripulanteDTO } from 'src/app/models/tiposTripulantes/TipoTripulanteDTO';

@Component({
  selector: 'adicao-tipo-tripulante',
  templateUrl: './adicao-tipo-tripulante.component.html',
  styleUrls: ['./adicao-tipo-tripulante.component.css']
})
export class AdicaoTipoTripulanteComponent implements OnInit {

    codTipoTripulante: string;

    constructor(public dialogRef: MatDialogRef<AdicaoTipoTripulanteComponent>) {
    }
  
    ngOnInit() {
    }

    onCancelarClick(){
        this.dialogRef.close();
    }

    adicionarCodigoTipoTripulante(codTipoTripulante: string){
        this.codTipoTripulante = codTipoTripulante;
    }

    onAdicionarClick(){
        let codTipoTripulante = undefined;
        if(this.codTipoTripulante != null){
            codTipoTripulante = this.codTipoTripulante;
        }
        this.dialogRef.close(codTipoTripulante);
    }

    onClickedDriverType(tipoTripulante: TipoTripulanteDTO){
        this.adicionarCodigoTipoTripulante(tipoTripulante.codigo);
    }

}