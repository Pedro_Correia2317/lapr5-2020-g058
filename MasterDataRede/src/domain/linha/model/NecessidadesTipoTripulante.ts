
import { CodigoTipoTripulante } from "../../tipoTripulante/model/CodigoTipoTripulante";
import { NecessidadeTipoTripulanteDTO } from "../dto/NecessidadeTipoTripulanteDTO";
import { TipoNecessidade } from "./TipoNecessidade";

export class NecessidadesTipoTripulante {

    private tiposTripulantes: CodigoTipoTripulante[];

    private tipoNecessidade: TipoNecessidade;

    constructor(tiposV: CodigoTipoTripulante[], tipo: TipoNecessidade){
        if (tiposV == null || tipo == null){
            throw new ReferenceError('The inserted values are null');
        }
        if (tiposV.includes(null)){
            throw new ReferenceError('There are null driver types');
        }
        if (tiposV.length === 0 && tipo !== TipoNecessidade.NAO_APLICAVEL){
            throw new SyntaxError('No vehicle types, so necessity type ' + tipo + ' is invalid.');
        }
        if (tiposV.length !== 0 && tipo === TipoNecessidade.NAO_APLICAVEL){
            throw new SyntaxError('Vehicle types not empty, so necessity type ' + tipo + ' is invalid.');
        }
        this.tiposTripulantes = tiposV.slice();
        this.tipoNecessidade = tipo;
    }

    getTiposTripulantes(): CodigoTipoTripulante[] {
        return this.tiposTripulantes;
    }

    getTipoNecessidade(): TipoNecessidade {
        return this.tipoNecessidade;
    }

    toNecessidadesTipoTripulanteDTO(): NecessidadeTipoTripulanteDTO {
        const listaDTO = new NecessidadeTipoTripulanteDTO();
        listaDTO.tipoNecessidade = this.tipoNecessidade.toBaseDTO().descricao;
        listaDTO.codigoTipoTripulante = [];
        this.tiposTripulantes.forEach((cod: CodigoTipoTripulante) => {
            listaDTO.codigoTipoTripulante.push(cod.getValue());
        });
        return listaDTO;
    }

    toString(): string {
        return `NecessidadesTipoTripulante: [${this.tiposTripulantes}, ${this.tipoNecessidade}]`;
    }
}